$(document).on("click", ".add_product", function( e ){
    e.preventDefault();

    var id = $(this).attr("href");
    var qty = $("#input_qty").val();
    var color = ( $("#select-color").val() ) ? $("#select-color").val() : null;
    var texture = ( $("#select-texture").val() ) ? $("#select-texture").val() : null;

    addProduct( id, qty, color, texture );
});

function addProduct( id, quantity, color, texture ){
    $.ajax({
        url      : base_url + "index.php/Store/addProduct",
        data     : { 
            id_producto: id,
            quantity : quantity,
            color : color,
            texture : texture
        },
        type     : "post",
        dataType : "json",
        beforeSend:function(){

        },
        success:function( data ){

            $("#tot_quantity").html( data.body.cart.total_qty )

            $.toast({
                heading: 'AÑADIDO AL CARRITO',
                text: 'Ahora puede visualizar el producto en su carrito de compras',
                position: 'top-right',
                loaderBg:'#9EC600',
                bgColor: '#bf9000',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });

        
        },
        error: function( data ){
        }
    });
}