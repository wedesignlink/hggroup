$(document).on('click', '#option-color', function (event) {
    $("#select-color").val($(this).attr("value")); 
    $("[id='option-color']").css("border-color","gray");
    $(this).css("border-color","blue");
});

$(document).on('click', '#option-texture', function (event) {
    $("#select-texture").val($(this).attr("value"));
    $("[id='option-texture']").css("border-color","gray");
    $(this).css("border-color","blue");
});

//Hiding buttons slider
$('#colors-carousel').on('slid', '', checkitemColors);  // on caroussel move
$('#colors-carousel').on('slid.bs.carousel', '', checkitemColors); // on carousel move
$(document).ready(function(){               // on document ready
    checkitemColors();
});
function checkitemColors()                        // check function
{
    var $this = $('#colors-carousel');
    if($('#colors-carousel .item:first').hasClass('active')) {
        $('#prevColors').hide();
        $('#nextColors').show();
    } else if($('#colors-carousel .item:last').hasClass('active')) {
        $('#prevColors').show();
        $('#nextColors').hide();
    } else {
        $('#prevColors').show();
        $('#nextColors').show();
    } 
}
$('#textures-carousel').on('slid', '', checkitemTextures);  // on caroussel move
$('#textures-carousel').on('slid.bs.carousel', '', checkitemTextures); // on carousel move
$(document).ready(function(){               // on document ready
    checkitemTextures();
});
function checkitemTextures()                        // check function
{
    var $this = $('#textures-carousel');
    if($('#textures-carousel .item:first').hasClass('active')) {
        $('#prevTextures').hide();
        $('#nextTextures').show();
    } else if($('#textures-carousel .item:last').hasClass('active')) {
        $('#prevTextures').show();
        $('#nextTextures').hide();
    } else {
        $('#prevTextures').show();
        $('#nextTextures').show();
    } 
}


$(document).ready(function() {
    // Select de ordenamiento de productos
    $('.order_catalog_select').change(function() {
        
        let order   = $(this).val();
        var category = ( $('#categoryInput').val() ) ? $('#categoryInput').val() : null ;
        var search = ( $("#input_search").val() ) ? $("#input_search").val() : null;

        $.ajax({
            async: true,
            url:base_url +'index.php/Store/searchOrFilter',
            data: { category:category ,order:order, search:search, page:0},
            type:'POST',
            beforeSend: function(){
                $('#prod_page_container').empty();
                $('#loader').fadeIn('slow');
            }
        }).success(function(resp) {
            $('#loader').fadeOut('slow', function(){
                if(resp){
                    $('#prod_page_container').html(resp);
                }
            });

        }).fail(function(fail) {
           console.log(fail);
        });
    });

});
