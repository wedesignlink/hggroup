$(document).on('keydown', '#input_search', function (e) {    
    // enter pressed
    if (e.keyCode === 13) {
        var search = $(this).val();

        // restart category value
        $('#categoryInput').val("0");

        $.ajax({
            url: base_url + 'index.php/Store/searchOrFilter',
            data: { category:null, order:null, search:search, page:-1 },
            method: 'POST',
            beforeSend: function () {
                $('#prod_page_container').empty();
                $('#loader').fadeIn('slow');
            },
            success: function (res) {
                $('#loader').fadeOut('slow', function () {
                    if (res) {
                        $('#prod_page_container').html(res);
                    }
                });
            },
            complete: function () {
                $('#loader').fadeOut();
            }
        });
    }
});

// finalized
$(document).on('click', '.category', function (e) {
    e.preventDefault();

    var id_category = ( $(this).attr('id') ) ? $(this).attr('id') : null;
    var order = $('.order_catalog_select').val();

    // change category selected
    $('#categoryInput').val(id_category);
    // clear input search value
    $('#input_search').val("");

    $.ajax({
        url: base_url + 'index.php/Store/searchOrFilter',
        data: { category:id_category, order:order, search:null, page:0 },
        method: 'POST',
        beforeSend: function () {
            $('#prod_page_container').empty();
            $('#loader').fadeIn('slow');
        },
        success: function (res) {
            $('#loader').fadeOut('slow', function () {
                if (res) {
                    $('#prod_page_container').html(res);
                }
            });
        },
        complete: function () {
            $('#loader').fadeOut();
        }
    });
});

$(document).on('click', '.page-number', function (event) {
    event.preventDefault();

    var id_page = $(this).attr('value');
    var order = $('.order_catalog_select').val();
    var category = $('#categoryInput').attr('value');
    var search = ( $("#input_search").val() ) ? $("#input_search").val() : null;
  
    $.ajax({
        url: base_url + 'index.php/Store/searchOrFilter',
            data: { category:category, order:order, search:search, page: id_page },
        method: 'POST',
        beforeSend: function () {
            $('#prod_page_container').empty();
            $('#loader').fadeIn('slow');
        },
        success: function (res) {
            $('#loader').fadeOut('slow', function () {
                if (res) {
                    $('#prod_page_container').html(res);
                }
            });
        },
        complete: function () {
            $('#loader').fadeOut();
        }
    });
});

$(document).on('click', '.page-button', function (event) {
    event.preventDefault();
    var value_button = $(this).attr('value');
    var actual_page = $("#actual_page").val();
    var order = $('.order_catalog_select').val();
    var category = $('#categoryInput').attr('value');
    var search = ( $("#input_search").val() ) ? $("#input_search").val() : null;

    if(value_button == "next")
        actual_page++;
    else 
        actual_page--;

  
    $.ajax({
        url: base_url + 'index.php/Store/searchOrFilter',
            data: { category:category, order:order, search:search, page: actual_page },
        method: 'POST',
        beforeSend: function () {
            $('#prod_page_container').empty();
            $('#loader').fadeIn('slow');
        },
        success: function (res) {
            $('#loader').fadeOut('slow', function () {
                if (res) {
                    $('#prod_page_container').html(res);
                }
            });
        },
        complete: function () {
            $('#loader').fadeOut();
        }
    });
});
