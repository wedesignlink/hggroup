/*
 * Task          Date            Author                                         Remarks
 * EMX-10       15 Dec 2020     Diego Huerta <diego.huerta@devux.mx>        Start js for function of register
 */

var base_url = "http://app.entrevistandomexico.com/";

function saveUser() {
    try {
        var FUNC = "/saveUser";
        let data = getDataSerializable('#registerForm') || '';
        
        validateFormRequired('#registerForm');

        validateEmail(data.email);

        validatePhoneNumber(data.telefono);

        validateMatchPassword(data.passwd, data.passwdRepeat);

        validatePassword(data.passwd);

        createRegister(data);
    } catch (error) {
        alertify.alert('Entrevistando MX', error.message, function(){
            alertify.message('Error al registrar panelista');
        });
    }
}


function getDataSerializable(form) {
    try {
        let data = $(form).serializeObject();
        return data;
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

function validateEmail(mail) {
    let mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (mail.match(mailformat))
        return true;

    throw {
        message: "Ingresaste un correo electronico invalido"
    }
}

function validateFormRequired(formId) {
    try {
        let form = $(formId);
        form.find(':input').each(function () {
            let elemento = this;
            if (elemento.attributes.required && elemento.value == '') {
                throw {
                    message: 'Favor de llenar todos los campos requeridos'
                }
            }
        });
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

function validatePhoneNumber(inputtxt) {
    try {
        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if ((inputtxt.match(phoneno))) {
            return true;
        }

        throw {
            message: 'Favor de Ingresar un numero de telefono valido'
        }
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

function validateMatchPassword(pass, confirmPass) {
    try {
        if (pass === confirmPass)
            return true;

        throw {
            message: 'Las contraseñas deben coincidir'
        }
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

function createRegister(data) {
    try {
        delete data.passwdRepeat;
        $.ajax({
            type: 'POST',
            data: data,
            url: base_url + 'index.php/Panelistas/guardarPanelista',
            success: function (response) {
                if (response) {
                    response = JSON.parse(response);
                    if (response.isSuccess) {
                        alertify.alert('Entrevistando MX', response.message, function(){
                            alertify.success('Bienvenido a Entrevistando MX');
                        });
                        clearFields('#registerForm');
                    } else {
                        alertify.alert('Entrevistando MX', response.message, function(){
                            alertify.error('Error al guardar panelista');
                        });
                    }
                } else {
                    throw {
                        message: response.message
                    }
                }
            }
        });
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function clearFields(formId) {
    try {
        let form = $(formId)
        form.find(':input').each(function () {
            let elemento = this;
            $(elemento).val('');
        });
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

function validatePassword(passwd) {
    try {
        let regExPassword = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;

        if (passwd.match(regExPassword))
            return true;

        throw {
            message: 'La contraseña debe contener de 8 - 16 caracteres, al menos una mayuscula y una minuscula'
        }


    } catch (error) {
        throw {
            message: error.message
        }
    }
}