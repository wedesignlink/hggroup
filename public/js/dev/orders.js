
$(document).ready(function(){
 var paqueteSel = $('#paquete').val();
 	if(paqueteSel==4){
 		$('#admins').removeAttr('disabled');
 		$('#users').removeAttr('disabled');
 		var admins = $('#admins').val();
 		var users = $('#users').val();
 		if(users==0 || admins ==0){
			$('#continuar').addClass('disabled');
		}else{
			$('#continuar').removeClass('disabled');
		}
 	}else{
 		$('#admins').attr('disabled','disabled');
 		$('#users').attr('disabled','disabled');
 	}
});


$(document).on('click','.modulo',function(e){
	e.preventDefault();
	var modulo = $(this).attr('id');
	var modulo = modulo.split('_');
	var data = 'idModulo='+modulo[1];

	$.ajax({
		url: base_url+'index.php/Ordenes/updateResumen',
		data: data,
		method: 'POST',
		dataType: 'JSON',
		success: function(res){
			console.log(res);

			if(res==1){
				swal({   
		            title: "Correcto",
		            type: "success",    
		            text: "El módulo seleccionado ha sido agregado",
		            showConfirmButton: false,
		        });
		        location.reload();
			}else{
				swal({   
		            title: "Error",
		            type: "warning",    
		            text: "El módulo seleccionado ya ha sido agregado con anterioridad",
		        });
			}
		}
	});

});

$(document).on('click','.borrarModulo',function(e){
	e.preventDefault();
	var modulo = $(this).attr('id');
	var modulo = modulo.split('_');

	var data = 'idModulo='+ modulo[1];

	// alert(data);

	$.ajax({
		url: base_url+'index.php/Ordenes/borrarModulo',
		data: data,
		method: 'POST',
		// dataType: 'JSON',
		success: function(res){
			console.log(res);

			if(res==1){
				swal({   
		            title: "Correcto",
		            type: "success",    
		            text: "El módulo seleccionado ha sido eliminado",
		            showConfirmButton: false,
		        });
		         location.reload();
			}else{
				swal({   
		            title: "Error",
		            type: "warning",    
		            text: "El módulo seleccionado no ha podido eliminarse",
		        });
			}
		}
	});

});


$(document).on('change','#paquete',function(){

	var paquete = $(this).val();

	if(paquete==4){
		$('#admins').removeAttr('disabled');
		$('#users').removeAttr('disabled');
		
	}else if(paquete==6){
        
		$('#plazo').attr('disabled','disabled');
		$('#admins').attr('disabled','disabled');
		$('#users').attr('disabled','disabled');

		// $('#paquete')
		//     .val('mensual')
		//     .trigger('change');
	} else{
		$('#admins').attr('disabled','disabled');
		$('#users').attr('disabled','disabled');
		$('#plazo').removeAttr('disabled');
	}
	
	// var modulo = modulo.split('_');

	var data = 'paquete='+paquete;

	$.ajax({
		url: base_url+'index.php/Ordenes/updatePaquete',
		data: data,
		method: 'POST',
		dataType: "json",
		beforeSend: function(){
			// $('.preloader').show();
		},
		success: function(result){
			// $('.preloader').fadeToggle();
			
			var obj = result;
			 console.log(obj);

			var precioPaquete = parseInt(obj.precioProducto); 
			var montoTotal = parseInt(obj.montoTotal);
			var montoIVA = obj.montoImpuestos.iva;
		 	var montoImpuestos = obj.montoImpuestos.montoImpuestos;
		 	var montoPeriodo = parseInt(obj.montoPeriodo);

			if(isNaN(precioPaquete)){
				precioPaquete = 0; 
			}


			if(precioPaquete>0){
				var mensual = parseInt(precioPaquete*1);
				var trimestral = parseInt(precioPaquete*3);
				var semestral = parseInt(precioPaquete*6);
				var anual = parseInt(precioPaquete*11);
			}else{
				var mensual =0;
				var trimestral =0;
				var semestral =0;
				var anual =0;
			}

			if(obj.idPaquete=="6"){
				// console.log('entro');
				$('option[value="mensual"]').prop('selected', true);
				$('#plazo').attr('disabled','disabled');
			}

			if(lang=="spanish"){
				$('#admins').val(obj.admins);
				$('#users').val(obj.users);

				var admins = $('#admins').val();
				var users = $('#users').val();

				if(users!=0 && admins !=0){
					$('#continuar').removeClass('disabled');
				}else{
					$('#continuar').addClass('disabled');
				}
				$('#nombrePaquete').html(obj.nombrePaquete);
				$('#precioPaquete').html(montoPeriodo);
				// $('#montoTotal').html(montoTotal);
				$('#mensual').val('mensual');
				$('#mensual').text('Mensual $'+mensual);
				$('#trimestral').val('trimestral');
				$('#trimestral').text('Trimestral $'+trimestral);
				$('#semestral').val('semestral');
				$('#semestral').text('Semestral $'+semestral);
				$('#anual').val('anual');
				$('#anual').text('Anual $'+anual);
				$('.nombrePeriodo').html(obj.periodo);
				$('#subtotal').html(montoTotal);
				$('#iva').html(montoIVA);
				$('#montoImpuestos').html(montoImpuestos);
			}else if(lang=="english"){
				console.log("entro "+lang);
				$('#admins').val(obj.admins);
				$('#users').val(obj.users);

				var admins = $('#admins').val();
				var users = $('#users').val();

				if(users!=0 && admins !=0){
					$('#continuar').removeClass('disabled');
				}else{
					$('#continuar').addClass('disabled');
				}

				$('#nombrePaquete').html(obj.nombrePaquete);
				$('#precioPaquete').html(montoPeriodo);
				// $('#montoTotal').html(montoTotal);
				$('#mensual').val('monthly');
				$('#mensual').text('Monthly $'+mensual);
				$('#trimestral').val('quarterly');
				$('#trimestral').text('Quarterly $'+trimestral);
				$('#semestral').val('semiannual');
				$('#semestral').text('Semiannual $'+semestral);
				$('#anual').val('annual');
				$('#anual').text('Annual $'+anual);
				$('.nombrePeriodo').html(obj.periodo);
				$('#subtotal').html(montoTotal);
				$('#iva').html(montoIVA);
				$('#montoImpuestos').html(montoImpuestos);

			}

		}
	});

});

$(document).on('change','#plazo',function(){

	var plazoMonto = $(this).val();
	var plazoNombre = $(this).children(":selected").attr("id");

	 var data = 'plazoMonto='+plazoMonto+'&plazoNombre='+plazoNombre;

	 // console.log(data);

	$.ajax({
		url: base_url+'index.php/Ordenes/updatePlazo',
		data: data,
		method: 'POST',
		dataType: "json",
		beforeSend: function(){
			// $('.preloader').show();
		},
		success: function(result){
			// $('.preloader').fadeToggle();
			
			var obj = result;
			// console.log(obj.periodo);

		 	var montoTotal = parseInt(obj.montoTotal);
		 	var precioPaquete = parseInt(obj.montoPeriodo);
		 	var multiplicador = parseInt(obj.multiplicador);
		 	var montoIVA = obj.montoImpuestos.iva;
		 	var montoImpuestos = obj.montoImpuestos.montoImpuestos;

		 	// console.log(obj.montoImpuestos.iva);
			
			$('#precioPaquete').html(precioPaquete);
		 	$('#subtotal').html(montoTotal);
		 	$('#iva').html(montoIVA);
		 	$('#montoImpuestos').html(montoImpuestos);

		 	$('.nombrePeriodo').html(obj.periodo);


			$('.precioModulo').each(function(){
				var precioModulo = parseInt($(this).next('.precioBase').val());
				$(this).html(precioModulo*multiplicador);

			});

		}
	});
});

$(document).on('change','#admins, #users',function(){

	var admins = $('#admins').val();
	var users = $('#users').val();

	var data = 'admins='+admins+'&users='+users;


	if(users==0 || admins ==0){
		$('#continuar').addClass('disabled');
	}else{
		$('#continuar').removeClass('disabled');
	}

	// var plazoMonto = $(this).val();
	// var plazoNombre = $(this).children(":selected").attr("id");

	// var data = 'plazoMonto='+plazoMonto+'&plazoNombre='+plazoNombre;

	// console.log(data);

	$.ajax({
		url: base_url+'index.php/Ordenes/updateUsers',
		data: data,
		method: 'POST',
		dataType: "json",
		beforeSend: function(){
			// $('.preloader').show();
		},
		success: function(result){
			// $('.preloader').fadeToggle();
			
			var obj = result;
			// console.log(obj);

		 	var montoTotal = parseInt(obj.montoTotal);
		 	var precioPaquete = parseInt(obj.montoPeriodo);
		 	var multiplicador = parseInt(obj.multiplicador);
		 	var montoIVA = obj.montoImpuestos.iva;
		 	var montoImpuestos = obj.montoImpuestos.montoImpuestos;

		 	// console.log(obj.montoImpuestos.iva);
			
			$('#precioPaquete').html(precioPaquete);
		 	$('#subtotal').html(montoTotal);
		 	$('#iva').html(montoIVA);
		 	$('#montoImpuestos').html(montoImpuestos);

		 	if(precioPaquete>0){
				var mensual = parseInt(precioPaquete*1);
				var trimestral = parseInt(precioPaquete*3);
				var semestral = parseInt(precioPaquete*6);
				var anual = parseInt(precioPaquete*11);
			}else{
				var mensual =0;
				var trimestral =0;
				var semestral =0;
				var anual =0;
			}

			$('#mensual').val(mensual);
			$('#mensual').text('Mensual $'+mensual);
			$('#trimestral').val(trimestral);
			$('#trimestral').text('Trimestral $'+trimestral);
			$('#semestral').val(semestral);
			$('#semestral').text('Semestral $'+semestral);
			$('#anual').val(anual);
			$('#anual').text('Anual $'+anual);

		 


			// $('.precioModulo').each(function(){
			// 	var precioModulo = parseInt($(this).next('.precioBase').val());
			// 	$(this).html(precioModulo*multiplicador);

			// });

		}
	});

});

$(document).on('click','#btnRegistrar',function(){

		var campos = $('#registro').serializeArray();
		console.log("entro a btn");	

		var requeridos=0;
		var llenos=0;
		$(':required').each(function(){
			valorCampo = $(this).val();
			// console.log(valorCampo);
			if(valorCampo != "" && valorCampo != 0 ){
				llenos++;
				// console.log(valorCampo);
				$(this).closest('.form-group').removeClass('has-danger');
			}else{
				$(this).closest('.form-group').addClass('has-danger');
			}
			requeridos++;
		});

		var validar = validaCampos(campos);

		// console.log('cuantos campos = '+requeridos);
		// console.log('cuantos requeridos llenos = '+llenos);

		var terminos = $('#checkbox1').is(':checked');
		
		if(llenos<requeridos){
			console.log("entro a lleno");

			if(lang =='spanish' || lang ==''){
				swal('Error','Debe llenar todos los campos requeridos','warning');
			}
			if(lang =='english'){
				swal('Error','Please fill required fields','warning');
			}
			
			$(this).prop('disabled', true);
			$(this).prop('disabled', false);
		}else if(!terminos){
			if(lang=='spanish' || lang ==''){
				swal('Error','Debe aceptar el aviso de privacidad y los terminos','warning');
			}
			if(lang=='english'){
				swal('Error','Please accept terms an conditions','warning');
			}
			$(this).prop('disabled', false);
		}else if(validar != true){
			if(lang=='spanish' || lang ==''){
				swal('Error','Algunos campos tienen datos inválidos','warning');
			}
			if(lang=='english'){
				swal('Error','Invalid data fields','warning');
			}
			$(this).prop('disabled', false);
		}else{
			$(this).prop('disabled', true);
			guardarRegistro()
			console.log("guardo");
			
		}
});

function guardarRegistro(){
	var data = $('#registro').serialize();
	// console.log(data);

	$.ajax({
		url: base_url+'index.php/Ordenes/registraUsuario',
		data: data,
		method: 'POST',
		// dataType: "json",
		beforeSend: function(){
			 $('.preloader').show();
		},
		success: function(result){
			 $('.preloader').fadeToggle();
			// var obj = result;
			console.log(result);

			if(result==1){
				if(lang=='spanish'|| lang ==''){
					swal('Ok','Se ha registrado correctamente','success');
				}
				if(lang=='english'){
					swal('Ok','Register has been completed succesfully','success');
				}
				$('#registro').fadeToggle();
				$('#registroCorrecto').fadeToggle();
				$('#continuar').removeClass('disabled');

			}else if(result==0){
				if(lang=='spanish'|| lang ==''){
					swal('Error','No se ha podido registrar con los datos proporcionados, por favor intentelo de nuevo','warning');
				}
				if(lang=='english'){
					swal('Error','an error has occurred, please try again','warning');
				}
				$('#btnRegistrar').prop('disabled', false);
			}else if(result==3){
				$('#btnRegistrar').prop('disabled', false);
				if(lang=='spanish'|| lang ==''){
					swal('Error','Su email o RFC ya se encuentran registrados, por favor intentelo de nuevo','warning');
				}
				if(lang=='english'){
					swal('Error','Your email or rfc already found in database, please try again','warning');
				}
			}

		}
	});
	
}

$(document).on('blur','input,select',function(){
	var campos = $('#registro').serializeArray();
	valorCampo = $(this).val();
			 // console.log(valorCampo);
			if(valorCampo != "" && valorCampo != 0 ){
				$(this).closest('.form-group').removeClass('has-danger');
			}else{
				$(this).closest('.form-group').addClass('has-danger');
			}
	validaCampos(campos);
});

function validateEmail(mail) 
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return (true)
  }
    // alert("You have entered an invalid email address!")
    return (false)
}

function validaCampos(campos){
	
	var resultado = [];

	for (var i = 0; i < campos.length; i++) {
		
		if(campos[i]['name'] == 'password'){
			var id = '#'+campos[i]['name'];
			if(campos[i]['value'].length < 8){
				resultado.push(false);
				$(id).closest('.form-group').addClass('has-danger');
			}else{
				$(id).closest('.form-group').removeClass('has-danger');
			}
		}
		if(campos[i]['name'] == 'confirmar'){
			var id = '#'+campos[i]['name'];
			if(campos[i]['value'] != $('#password').val()){
				resultado.push(false);
				$(id).closest('.form-group').addClass('has-danger');
			}else{
				$(id).closest('.form-group').removeClass('has-danger');
			}
		}
		if(campos[i]['name'] == 'email'){
			var id = '#'+campos[i]['name'];
			var validar = validateEmail(campos[i]['value']);
			// console.log("email valida="+validar);
			if( validar == false  ){
				resultado.push(false);
				$(id).closest('.form-group').addClass('has-danger');
			}else{
				$(id).closest('.form-group').removeClass('has-danger');
			}
		}
	}
	
	// console.log(resultado);

	if(resultado.length > 0){
		var res = false;
	}else{
		var res = true;
	}

	// console.log("resultado de validar="+res);
	 return res;
}

function cambiarOrden(){

	if(lang=='spanish'|| lang ==''){
		var titulo = "Confirme la acción";
		var text = "Será generada una nueva orden, la nueva orden será activa una vez realizado su pago y concluido el periodo de la orden anterior";
		var confirmar = "Confirmar";
		var cancelar = "Cancelar";
	}

	if(lang=='english'){
		var titulo = "Confirm an action";
		var text = "Do you want generate new order?, new orders replace previous";
		var confirmar = "Confirm";
		var cancelar = "Cancel";
	}

	
	swal({
		title: titulo, 
		text: text, 
		type: "info",
		showCancelButton: true,
		CancelButtonText: cancelar,
		closeOnConfirm: false,
		confirmButtonText: confirmar,
	}, function() {
                    window.location.href=base_url+'index.php/Ordenes/GenerarSessionCompra'; // DeleteElement(table,campo_id,id,msjn,msjs,controlador);
                 });
}



