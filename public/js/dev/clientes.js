$(document).on('click','#btnCliente',function(){

    var campos = $('#registro').serializeArray();
    console.log("entro a btn");	

    var requeridos=0;
    var llenos=0;
    $(':required').each(function(){
        valorCampo = $(this).val();
        // console.log(valorCampo);
        if(valorCampo != "" && valorCampo != 0 ){
            llenos++;
            // console.log(valorCampo);
            $(this).closest('.form-group').removeClass('has-danger');
        }else{
            $(this).closest('.form-group').addClass('has-danger');
        }
        requeridos++;
    });

    var validar = validaCampos(campos);

    // console.log('cuantos campos = '+requeridos);
    // console.log('cuantos requeridos llenos = '+llenos);

    var terminos = $('#checkbox1').is(':checked');
    
    if(llenos<requeridos){
        console.log("entro a lleno");

        if(lang =='spanish' || lang ==''){
            swal('Error','Debe llenar todos los campos requeridos','warning');
        }
        if(lang =='english'){
            swal('Error','Please fill required fields','warning');
        }
        
        $(this).prop('disabled', true);
        $(this).prop('disabled', false);
    }else if(validar != true){
        if(lang=='spanish' || lang ==''){
            swal('Error','Algunos campos tienen datos inválidos','warning');
        }
        if(lang=='english'){
            swal('Error','Invalid data fields','warning');
        }
        $(this).prop('disabled', false);
    }else{
        $(this).prop('disabled', true);
        guardarProyecto()
        console.log("guardo");
        
    }
});

function guardarProyecto(){
	var data = $('#registro').serialize();
	// console.log(data);

	$.ajax({
		url: base_url+'index.php/Clientes/registraCliente',
		data: data,
		method: 'POST',
		// dataType: "json",
		beforeSend: function(){
			 $('.preloader').show();
		},
		success: function(result){
			 $('.preloader').fadeToggle();
			// var obj = result;
			console.log(result);

			if(result==1){
				if(lang=='spanish'|| lang ==''){
					swal('Ok','Se ha registrado correctamente','success');
				}
				if(lang=='english'){
					swal('Ok','Register has been completed succesfully','success');
				}
				// $('#registro').fadeToggle();
				// // $('#registroCorrecto').fadeToggle();
                // $('#continuar').removeClass('disabled');
                window.location.href=base_url+'index.php/Proyectos';

			}else if(result==0){
				if(lang=='spanish'|| lang ==''){
					swal('Error','No se ha podido registrar con los datos proporcionados, por favor intentelo de nuevo','warning');
				}
				if(lang=='english'){
					swal('Error','an error has occurred, please try again','warning');
				}
				$('#btnRegistrar').prop('disabled', false);
			}else if(result==3){
				$('#btnRegistrar').prop('disabled', false);
				if(lang=='spanish'|| lang ==''){
					swal('Error','Su email o RFC ya se encuentran registrados, por favor intentelo de nuevo','warning');
				}
				if(lang=='english'){
					swal('Error','Your email or rfc already found in database, please try again','warning');
				}
			}

		}
	});
	
}