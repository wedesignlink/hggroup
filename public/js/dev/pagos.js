// var billingPlanAttribs = {
//   name: 'Food of the World Club Membership: Standard',
//   description: 'Monthly plan for getting the t-shirt of the month.',
//   type: 'fixed',
//   payment_definitions: [{
//     name: 'Standard Plan',
//     type: 'REGULAR',
//     frequency_interval: '1',
//     frequency: 'MONTH',
//     cycles: '11',
//     amount: {
//       currency: 'MXN',
//       value: '19.99'
//     }
//   }],
//   merchant_preferences: {
//     setup_fee: {
//       currency: 'MXN',
//       value: '1'
//     },
//     cancel_url: 'http://localhost/cancel',
//     return_url: 'http://localhost/processagreement',
//     max_fail_attempts: '0',
//     auto_bill_amount: 'YES',
//     initial_fail_amount_action: 'CONTINUE'
//   }
// };

// paypal.billingPlan.create(billingPlanAttribs, function (error, billingPlan){
//   var billingPlanUpdateAttributes;

//   if (error){
//     console.error(JSON.stringify(error));
//     throw error;
//   } else {
//     // Create billing plan patch object
//     billingPlanUpdateAttributes = [{
//       op: 'replace',
//       path: '/',
//       value: {
//         state: 'ACTIVE'
//       }
//     }];

//     // Activate the plan by changing status to active
//     paypal.billingPlan.update(billingPlan.id, billingPlanUpdateAttributes, function(error, response){
//       if (error){
//         console.error(JSON.stringify(error));
//         throw error;
//       } else {
//         console.log('Billing plan created under ID: ' + billingPlan.id);
//       }
//     });
//   }
// });



// var billingPlan = "1234567890";
// var billingAgreementAttributes;
// var isoDate = new Date();

// isoDate.setSeconds(isoDate.getSeconds() + 4);
// isoDate.toISOString().slice(0, 19) + 'Z';

// billingAgreementAttributes = {
//   name: 'Standard Membership',
//   description: 'Food of the World Club Standard Membership',
//   start_date: isoDate,
//   plan: {
//     id: billingPlan
//   },
//   payer: {
//     payment_method: 'paypal'
//   },
//   shipping_address: {
//     line1: 'W 34th St',
//     city: 'New York',
//     state: 'NY',
//     postal_code: '10001',
//     country_code: 'US'
//   }
// };


// var links = {};

// // Use activated billing plan to create agreement
// paypal.billingAgreement.create(billingAgreementAttributes, function (error, billingAgreement){
//   if (error){
//     console.error(JSON.stringify(error));
//     throw error;
//   } else {
//     // Capture HATEOAS links
//     billingAgreement.links.forEach(function(linkObj){
//       links[linkObj.rel] = {
//         href: linkObj.href,
//         method: linkObj.method
//       };
//     })

//     // If redirect url present, redirect user
//     if (links.hasOwnProperty('approval_url')){
//       //REDIRECT USER TO links['approval_url'].href
//     } else {
//       console.error('no redirect URI present');
//     }
//   }
// });

// var token = req.query.token;

// paypal.billingAgreement.execute(token, {}, function (error, billingAgreement){
//   if (error){
//     console.error(JSON.stringify(error));
//     throw error;
//   } else {
//     console.log(JSON.stringify(billingAgreement));
//     console.log('Billing Agreement Created Successfully');
//   }
// });

// $(document).ready(function(){
//             pay_ok(recibo);
//         })
$(document).on('change','#fpago',function(){
  var fpago = $(this).val();
  // alert(fpago);
  if(fpago == 1){
     $('#paypal-button-container').show();
  }else{
    $('#paypal-button-container').hide();
  }
  if(fpago == 3){
    $('#codigo').show();
  }else{
    $('#codigo').hide();
  }
});
        
function pay_ok(id){
    var url = base_url+'index.php/Ordenes/Ok';
    // console.log(id);
    // console.log(url);    
  // alert('pago correcto! :)');
  // var data = 'idRecibo='+id;
  $.post(url,{ idRecibo: id },function(res){

     console.log(res);
     swal({
            title: "Correcto!",
            text: res,
            type: "success"
        }, function() {
            location.reload();
        });

  });
}