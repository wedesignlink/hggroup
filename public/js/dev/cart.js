/*
 * Task          Date            Author                                         Remarks
 * EMX-15       11 Jan 2020     Diego Huerta <diego.huerta@devux.mx>        Start js for function of cart
 */

var base_url = "http://app.entrevistandomexico.com/";

function addCart(element){
    try{
        let productId = $(element).attr('data-id');
        let levelProduct = $(element).attr('data-level');
        let level = $('#level').val();
        let puntos = $(element).attr('data-puntos');
        let quantity = $('.qty_' + productId).val() || 1;
        let url = $(element).attr('data-url');
        let nameItem = $(element).attr('data-name-item');
        
        if(levelProduct <= level){
            $.ajax({
                type: 'POST',
                data: {
                    item: productId,
                    quantity: quantity,
                    puntos: puntos,
                    name: nameItem,
                    url: url
                },
                url: base_url + 'index.php/Panelistas/addItemCart',
                success: function(response){
                    response = JSON.parse(response);
                    if(response.isSuccess){
                        alertify.alert('Entrevistando MX', 'Articulo Agregado Correctamente!!');
                        $('#cartList').val(response.cart);
                    }else{
                        alertify.alert('Entrevistando MX', response.message);
                    }
                }
            });
        }else{
            alertify.alert('Entrevistando MX', 'Lo siento pero este producto es de un nivel mayor al suyo');
        }
    }catch(error){
        alertify.alert('Entrevistando MX', error.message);
    }
}

$(document).on('click', '.deleteItem', function(){
    let key  = $(this).attr('data-key');
    
    if(key){
        $.ajax({
            type: 'POST',
            data: {
                key: key
            },
            url: base_url + 'index.php/Panelistas/removeItem',
            success: function(response){
                response = JSON.parse(response);
                var total = 0;
                $("#cartDetailBody").empty();
                response.forEach((item) => {
                    var html = '';
                    html += '<tr>';
                    html += '    <td class="w-25">';
                    html += '        <img src="' + item.url +'" class="img-fluid img-thumbnail" alt="Sheep">';
                    html += '    </td>';
                    html += '   <td>' + item.name + '</td>';
                    html += '   <td>' + item.puntos + '</td>';
                    html += '   <td class="qty">' + item.quantity + '</td>';
                    html += '   <td>' + (item.puntos * item.quantity) + '</td>';
                    html += '   <td>';
                    html += '       <a href="#" class="btn btn-danger btn-sm deleteItem" data-key="' + item.key + '">';
                    html += '           <i class="fa fa-times"></i>';
                    html += '       </a>';
                    html += '   </td>';
                    html += '</tr>';
        
                    total += (item.puntos * item.quantity);
        
                    $("#cartDetailBody").prepend(html);
                });
        
                $('#totalModal').text(total);
        
                if(response.length > 0){
                    $('#buy').show();
                }
            }
        })
    }
});