/*
 * Task          Date            Author                                         Remarks
 * EMX-11       19 Dec 2020     Diego Huerta <diego.huerta@devux.mx>        Start js for function of register information
 */
var base_url = "http://app.entrevistandomexico.com/";

function callModalView(obj) {
    try {
        var form = $(obj).attr('data-view');

        $.get(base_url + 'index.php/Panelistas/loadViewForm?view=' + form, function (data) {
            $("#modalBody").html(data);
        });
        $('#modalRegister').modal('show');
    } catch (error) {
        alertify.alert('Entrevistando MX', error.message, function () {
            alertify.message('Error al querer mostrar la vista');
        });
    }
}

function getAmai() {
    try {
        let data = getDataSerializable('#formAmai', 'array') || '';

        validateFormRequired('#formAmai');

        let idUser = data[0].value;
        data.shift();
        let amai = sumAmai(data);

        getAmaiServer(amai, idUser);

    } catch (error) {
        alertify.alert('Entrevistando MX', error.message, function () {
            alertify.message('Error al obtener el calculo amai');
        });
    }
}

function getDataSerializable(form, type) {
    try {
        let data = type == "array" ? $(form).serializeArray() : $(form).serializeObject();
        return data;
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

function validateFormRequired(formId) {
    try {
        let form = $(formId);
        form.find(':input').each(function () {
            let elemento = this;
            if (elemento.attributes.required && elemento.value == '') {
                throw {
                    message: 'Favor de llenar todos los campos requeridos'
                }
            }
        });
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

function getAmaiServer(amai, idUser) {
    try {
        $.ajax({
            type: 'GET',
            url: base_url + 'index.php/Panelistas/getAmai?score=' + parseInt(amai) + '&idUser=' + idUser,
            success: function (response) {
                if (response) {
                    response = JSON.parse(response);
                    if (response.isSuccess) {
                        $('#modalRegister').modal('hide');
                        alertify.alert('Entrevistando MX', "El amai obtenido fue el siguiente " + response.amai, function () {
                            alertify.success('El AMAI Obtenido fue: ' + response.amai);
                            clearFields('#formAmai');
                            updateSession('user', [{
                                key: 'amai', value: response.amai 
                            },{
                                key: 'amai_check', value: 1
                            }]);
                        });
                        //location.reload();
                    } else {
                        alertify.alert('Entrevistando MX', response.message, function () {
                            alertify.error('Error al obtener el AMAI');
                        });
                        clearFields('#formAmai');
                        location.reload();
                    }
                } else {
                    throw {
                        message: response.message
                    }
                }
            }
        });
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

function sumAmai(data) {
    try {
        let amai = 0;
        for (const [key, value] of Object.entries(data)) {
            amai += parseInt(value.value);
        }
        return amai;
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

function clearFields(formId) {
    try {
        let form = $(formId)
        form.find(':input').each(function () {
            let elemento = this;
            $(elemento).val('');
        });
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function saveConsumo() {
    try {
        debugger;
        let data = getDataSerializable('#formConsumo', 'object') || '';
        console.log(data);
        validateFormRequired('#formConsumo');

        saveConsumoServer(data);
    } catch (error) {
        alertify.alert('Entrevistando MX', error.message, function () {
            alertify.message('Error al guardar consumo de usuario');
        });
    }
}

function saveConsumoServer(data) {
    try {
        $.ajax({
            type: 'POST',
            data: data,
            url: base_url + 'index.php/Panelistas/saveConsumo',
            success: function (response) {
                response = JSON.parse(response);
                if (response) {
                    if (response.isSuccess) {
                        $('#modalRegister').modal('hide');
                        alertify.alert('Entrevistando MX', response.message, function () {
                            alertify.success('Consumo guardado correctamente en Entrevistando MX');
                            clearFields('#formConsumo');
                            updateSession('user', [{
                                key: 'consumoCheck', value: 1  
                            }]);
                        });
                    } else {
                        alertify.alert('Entrevistando MX', response.message, function () {
                            alertify.error('Error al guardar consumo');
                        });
                    }
                } else {
                    throw {
                        message: response.message
                    }
                }
            }
        });
    } catch (error) {
        throw {
            message: error.message
        }
    }
}

$(document).on('change', '#cboxAvisoPrivacidad', function () {
    var valueChbx = $(this).is(":checked");
    var idUser = $('#idUser').val();
    if (valueChbx) {
        alertify.confirm("Entrevistando MX", "¿Seguro de Aceptar Terminos y condiciones?.",
            function () {
                $('#modalRegister').modal('hide');
                $.ajax({
                    url: base_url + 'index.php/Panelistas/saveAviso/' + idUser,
                    type: 'POST',
                    data: {
                        avisoPrivacidad: 1,
                        avisoPrivacidadCheck: 1
                    },
                    success: function (response) {
                        alertify.success('Acepto Terminos y condiciones');

                        updateSession('user', [{
                            key: 'avisoPrivacidad', value: 1  
                        },{
                            key: 'avisoPrivacidadCheck', value: 1
                        }]);
                    }
                });
            },
            function () {
                $('#modalRegister').modal('hide');
                alertify.error('Cancelo Terminos y Condiciones');
            });
    }
});

function updateSession(keyArray, updateData){
    try {
        $.ajax({
            type: 'POST',
            url: base_url + 'index.php/Panelistas/reloadSession',
            data: {
                sessionArray: keyArray,
                sessionKeys: updateData
            }, 
            success: function(response){
                location.reload();
            }
        });

        return true;
    } catch (error) {
        throw{
            message: error.message
        }
    }
}