
new Chartist.Line('.ct-bar-chart1', {
  labels: tm,
  series: [
    vm,
    // [2, 1, 3.5, 7, 3],
    // [1, 3, 4, 5, 6]
  ]
}, {
  fullWidth: true,
  
  plugins: [
    Chartist.plugins.tooltip()
  ],
  showArea: true,
  chartPadding: {
    right: 40
  }
});


var data = {
  labels: trp,
  series: [
    vrp,
    // vrn
  ]
};

var options = {
  seriesBarDistance: 0,
  high: 10,
  low: -10,
  plugins: [
    Chartist.plugins.tooltip()
  ],
  showPoint: true,
  axisX: {
    labelInterpolationFnc: function(value, index) {
      return index % 2 === 0 ? value : null;
    }
  }
};

var responsiveOptions = [
  ['screen and (max-width: 640px)', {
    seriesBarDistance: 0,
    axisX: {
      labelInterpolationFnc: function (value) {
        return value[0];
      }
    }
  }]
];

new Chartist.Line('.ct-bar-chart3', data, options, responsiveOptions);






$(document).on('change', '#filtro_cs #id_centro,#filtro_cs #id_depto', function () {
  var id_centro = $('#id_centro').val();
  var id_depto = $('#id_depto').val();
  var data = 'id_centro=' + id_centro+'&id_depto='+id_depto;

  $.ajax({
    url: base_url + 'index.php/Dashboard/cargarGraphComportamiento',
    data: data,
    method: 'POST',
    beforeSend: function () {
      $('.preloader').show();
    },
    // dataType: 'JSON',
    success: function (res) {

      $('.preloader').hide();
      // console.log(res);
      if (res != 0) {
        $('#comportamiento').html(res);
      } 

    }
  });
});

$(document).on('change', '#filtro_ta #id_centrota,#filtro_ta #id_deptota', function () {
  var id_centro = $('#id_centrota').val();
  var id_depto = $('#id_deptota').val();
  var data = 'id_centro=' + id_centro+'&id_depto='+id_depto;

  $.ajax({
    url: base_url + 'index.php/Dashboard/cargarGraphTendencia',
    data: data,
    method: 'POST',
    beforeSend: function () {
      $('.preloader').show();
    },
    // dataType: 'JSON',
    success: function (res) {

      $('.preloader').hide();
      // console.log(res);
      if (res != 0) {
        $('#tendencia_ambiente').html(res);
      } 

    }
  });
});



$(document).on('click','.exportChartCS',function(){
  var centro = $('#id_centro').val();
  var depto = $('#id_depto').val();
  window.location.href=base_url+'index.php/Dashboard/ExportarComportamientoSeguro/'+centro+"/"+depto;
  
});

$(document).on('click','.exportChartTA',function(){
  var centro = $('#id_centrota').val();
  var depto = $('#id_deptota').val();
  window.location.href=base_url+'index.php/Dashboard/ExportarTA/'+centro+"/"+depto;
});

$(document).on('click','.exportChartPD',function(){
  var centro = $('#id_centropd').val();
  var depto = $('#id_deptopd').val();
  var mes = $('#mespd').val();
  window.location.href=base_url+'index.php/Dashboard/ExportarPD/'+centro+"/"+depto+"/"+mes;
});

$(document).on('click','.exportChartPU',function(){
  var centro = $('#id_centropu').val();
  var depto = $('#id_deptopu').val();
  var mes = $('#mespu').val();
  window.location.href=base_url+'index.php/Dashboard/ExportarPU/'+centro+"/"+depto+"/"+mes;
});

$(document).on('click','.exportChartTD',function(){
  var centro = $('#id_centrotd').val();
  var depto = $('#id_deptotd').val();
  var mes = $('#mestd').val();
  window.location.href=base_url+'index.php/Dashboard/ExportarTD/'+centro+"/"+depto+"/"+mes;
});
$(document).on('click','.exportChartTO',function(){
  var centro = $('#id_centroto').val();
  var depto = $('#id_deptoto').val();
  var mes = $('#mesto').val();
  window.location.href=base_url+'index.php/Dashboard/ExportarTO/'+centro+"/"+depto+"/"+mes;
});

$(document).on('change', '#filtro_pd #mespd,#filtro_pd #id_centropd,#filtro_pd #id_deptopd', function () {
  var id_centro = $('#id_centropd').val();
  var id_depto = $('#id_deptopd').val();
  var mes = $('#mespd').val();
  var data = 'id_centro=' + id_centro+'&id_depto='+id_depto+'&mes='+mes;

  $.ajax({
    url: base_url + 'index.php/Dashboard/cargarGraphParticipacion',
    data: data,
    method: 'POST',
    beforeSend: function () {
      $('.preloader').show();
    },
    // dataType: 'JSON',
    success: function (res) {

      $('.preloader').hide();
      // console.log(res);
      if (res != 0) {
        $('#participacion_departamento').html(res);
      } 

    }
  });
});

$(document).on('change', '#filtro_pu #mespu,#filtro_pu #id_centropu,#filtro_pu #id_deptopu', function () {
  var id_centro = $('#id_centropu').val();
  var id_depto = $('#id_deptopu').val();
  var mes = $('#mespu').val();
  var data = 'id_centro=' + id_centro+'&id_depto='+id_depto+'&mes='+mes;

  $.ajax({
    url: base_url + 'index.php/Dashboard/cargarGraphParticipacionU',
    data: data,
    method: 'POST',
    beforeSend: function () {
      $('.preloader').show();
    },
    // dataType: 'JSON',
    success: function (res) {

      $('.preloader').hide();
      // console.log(res);
      if (res != 0) {
        $('#participacion_usuario').html(res);
      } 

    }
  });
});

$(document).on('change', '#mestd,#id_centrotd,#id_deptotd', function () {
  var id_centro = $('#id_centrotd').val();
  var id_depto = $('#id_deptotd').val();
  var mes = $('#mestd').val();
  var data = 'id_centro=' + id_centro+'&id_depto='+id_depto+'&mes='+mes;

  $.ajax({
    url: base_url + 'index.php/Dashboard/cargarGraphTD',
    data: data,
    method: 'POST',
    beforeSend: function () {
      $('.preloader').show();
    },
    // dataType: 'JSON',
    success: function (res) {

      $('.preloader').hide();
      // console.log(res);
      if (res != 0) {
        $('#top_desempeno').html(res);
      } 
    }
  });
});

$(document).on('change', '#mesto,#id_centroto,#id_deptoto', function () {
  var id_centro = $('#id_centroto').val();
  var id_depto = $('#id_deptoto').val();
  var mes = $('#mesto').val();
  var data = 'id_centro=' + id_centro+'&id_depto='+id_depto+'&mes='+mes;

  $.ajax({
    url: base_url + 'index.php/Dashboard/cargarGraphTO',
    data: data,
    method: 'POST',
    beforeSend: function () {
      $('.preloader').show();
    },
    // dataType: 'JSON',
    success: function (res) {

      $('.preloader').hide();
      // console.log(res);
      if (res != 0) {
        $('#top_oportunidad').html(res);
      } 
    }
  });
});