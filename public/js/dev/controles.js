


$(document).on('click', '.accion', function (e) {
    e.preventDefault();
    var accion = $(this).attr('id');
    var tbl = $(this).attr('rel');
    if (accion != 'agregar' && accion != 'borrarAdmin') {
        var id = $(this).closest('tr').find('.dato').first().text();
        var campo_id = $(this).closest('tr').find('td').first().attr('class').split(' ')[0];
        var datos = $(this).closest('tr').find('td .dato');
    }

    if (accion == 'asignar') {
        alert(id);
    }
    if (accion == 'editar') {

        // datos.each(function(){
        // var dato = $(this).text();
        // var nombre = $(this).closest('td').attr('class').split(' ')[0];
        //     var selector = '#'+nombre;
        //     $(selector).val(dato);
        //     // console.log(selector+'='+dato);
        // });
        // $("#modalEdit").modal();

        var campo = campo_id;
        var tabla = tabla_editar;
        if (lang == "spanish" || lang == "") {
            var msjn = "No cuenta con más administradores disponibles, aumente su paquete o elimine un administrador. ";
            var msjs = "El usuario seleccionado ahora es un administrador";
        }
        if (lang == "spanish") {
            var msjn = "Your account no longer have admins, upgrade your package or delete another admin.";
            var msjs = "The admin was successfully created";
        }

        var controlador = 'getDataForm';

        getDataForm(tabla, campo, campos_ver, id, msjn, msjs, controlador);

    }
    if (accion == 'admin') {
        var name = $(this).closest('tr').find('.rol').text();
        if (name != 'Administrador') {
            var update = "id_role=2";
            var campo = "id_user";

            if (lang == "spanish" || lang == "") {
                var msjn = "No cuenta con más administradores disponibles, aumente su paquete o elimine un administrador. ";
                var msjs = "El usuario seleccionado ahora es un administrador";
                var msje = "El usuario ya es Administrador";
            }

            if (lang == "english") {
                var msjn = "Your account no longer have admins, upgrade your package or delete another admin. ";
                var msjs = "The admin was successfully created";
                var msje = "The user is already an Administrator";
            }

            var controlador = 'UPD_User_Admin';
            UpdateElement(table, campo, id, update, msjn, msjs, controlador);
        } else {
            swal({
                title: "Error!",
                text: msje,
                type: "warning"
            });
        }
    }
    if (accion == 'desactivar') {

        var marcado = $(this).closest('tr').hasClass('table-danger');


        if (marcado) {
            var status = 8;
            $('#desactivar').text('Activar');
        } else {
            var status = 1;
            $('#desactivar').text('Desactivar');
        }

        // alert(marcado);

        if (status == 1) {
            var update = "id_status=8";
        } else if (status == 8) {
            var update = "id_status=1";
        }

        var campo = $('#campo').val();
        // var campo="id_user";
        if (lang == "spanish") {
            var msjn = "No se ha podido cambiar el estatus del usuario";
            var msjs = "Se ha cambiado el estatus del usuario";
        }
        if (lang == "english") {
            var msjn = "An error has occurred on the user status, please try again";
            var msjs = "User status has been succesfully changed";
        }
        var controlador = 'UPD_User';
        UpdateElement(table, campo, id, update, msjn, msjs, controlador);
    }
    if (accion == 'borrarAdmin') {
        var borrar = $(this).attr('rel');
        var update = "id_role=3";
        var campo = "id_user";
        if (lang == "spanish") {
            var msjn = "No cuenta con más administradores disponibles, aumente su paquete o elimine un administrador. ";
            var msjs = "El usuario seleccionado ha dejado de ser administrador";
            var confirme = "Confirme la acción";
            var seguro = "Esta seguro que desea quitar el administrador";
            var cancelar = "Cancelar";
            var eliminar = "Eliminar";
        }
        if (lang == "english") {
            var msjn = "Your account no longer have admins, upgrade your package or delete another admin.";
            var msjs = "User status has been changed, the user is no longer an admistrator";
            var seguro = "Are you sure you wish to remove the administrator";
            var cancelar = "Cancel";
            var eliminar = "Delete";
        }
        var controlador = 'UPD_User_Admin';

        swal({
            title: confirme,
            text: seguro,
            type: "warning",
            showCancelButton: true,
            CancelButtonText: cancelar,
            closeOnConfirm: false,
            confirmButtonText: eliminar,
        }, function () {
            UpdateElement(table, campo, borrar, update, msjn, msjs, controlador);
        });
    }
    if (accion == 'eliminar') {
        var campo = "id_user";
        if (lang == "spanish" || lang == "") {
            var titleConfirm = "Confirme la acción";
            var textConfirm = "¿De verdad quieres eliminarlo?";
            var cancelar = "Cancelar";
            var eliminar = "Eliminar";
            var msjn = "No se borro lo que deseas, por favor intenta otra vez";
            var msjs = "Listo ya quedo hecho el cambio!";
        } else {
            var titleConfirm = "Please confirm this action";
            var textConfirm = "Are you sure you want to delete?";
            var cancelar = "Cancel";
            var eliminar = "Delete";
            var msjn = "An error occurred while processing your request.";
            var msjs = "Changes Saved Successfully";
        }
        // var msjn = "No se borro lo que deseas, por favor intenta otra vez";
        // var msjs = "Listo ya quedo hecho el cambio!";
        var controlador = 'DEL_Registro';

        swal({
            title: titleConfirm,
            text: textConfirm,
            type: "warning",
            showCancelButton: true,
            CancelButtonText: cancelar,
            closeOnConfirm: false,
            confirmButtonText: eliminar,
        }, function () {
            DeleteElement(tbl, campo_id, id, msjn, msjs, controlador);
        });

    }
    if (accion == 'agregar') {
        $("#modalAdd").modal();
    }
    if (accion == 'detalle') {

        location.href = base_url + 'index.php/Usuarios/Detalle/' + id;
    }
    if (accion == 'bitacora') {
        location.href = base_url + 'index.php/Modulos/Detalle_bitacora/' + id;
    }
});

$(document).on('click', '.delete', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    if (lang == "spanish" || lang == "") {
        var titleConfirm = "Confirme la acción";
        var textConfirm = "¿De verdad quieres eliminarlo?";
        var cancelar = "Cancelar";
        var eliminar = "Eliminar";
        var msjn = "No se borro lo que deseas, por favor intenta otra vez";
        var msjs = "Listo ya quedo hecho el cambio!";
    } else {
        var titleConfirm = "Please confirm this action";
        var textConfirm = "Are you sure you want to delete?";
        var cancelar = "Cancel";
        var eliminar = "Delete";
        var msjn = "An error occurred while processing your request.";
        var msjs = "Changes Saved Successfully";
    }

    var controlador = 'DEL_Registro';

    swal({
        title: titleConfirm,
        text: textConfirm,
        type: "warning",
        showCancelButton: true,
        CancelButtonText: cancelar,
        closeOnConfirm: eliminar,
        confirmButtonText: "Eliminar",
    }, function () {
        DeleteElement(table, campo_id, id, msjn, msjs, controlador);
    });
});

$(document).on('click', '.deleteTree', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    if (lang == "spanish" || lang == "") {
        var msjn = "No se borro lo que deseas, por favor intenta otra vez";
        var msjs = "Listo ya quedo hecho el cambio!";
    } else {
        var msjn = "An error has occurred";
        var msjs = "The Element has been deleted";
    }

    var controlador = 'DEL_Registro_Tree';

    var hijos = "";
    $(this).siblings('ol').find('li').each(function () {
        var idhijo = $(this).attr('data-id');
        hijos = hijos + idhijo + ",";
        // console.log(idhijo);
    });
    hijos = hijos.slice(0, -1);
    // console.log(hijos);

    if (lang == "spanish" || lang == "") {
        var confirme = "Confirme la acción";
        var confirme_des = "¿De verdad quieres eliminar el elemento y sus hijos?";
        var cancelar = "Cancelar";
        var eliminar = "Eliminar";
    } else {
        var confirme = "Please confirm this action";
        var confirme_des = "Are you sure to delete this element";
        var cancelar = "Cancel";
        var eliminar = "Change Status"
    }


    swal({
        title: confirme,
        text: confirme_des,
        type: "warning",
        showCancelButton: true,
        CancelButtonText: cancelar,
        closeOnConfirm: false,
        confirmButtonText: eliminar,
    }, function () {
        DeleteElementTree(table, campo_id, id, msjn, msjs, controlador, hijos);
    });
});

$(document).on('click', '.control', function (e) {
    e.preventDefault();
    var accion = $(this).attr('id');
    var tbl = $(this).attr('rel');
});

function Editar() {

    var vacios = revisarVacios();

    if (vacios) {
        var datos = $('#form_editar').serializeArray();

        // Ajuste para checboxes vacios----------------------
        var checks = {};
        var item = [];

        $("#form_editar input:checkbox").each(function () {
            checks["name"] = this.name;
            checks["value"] = +this.checked;

            const index = datos.findIndex(x => x.name === this.name);

            if (index > 0) {
                datos.splice(index, 1);
            }
            datos.push(checks);

        });
        //----------------------------------------------------

        var tabla = datos[0]['value'];
        var campo = datos[1]['value'];
        var id = datos[2]['value'];
        datos.splice(0, 2);

        var data = ArrayToSerial(datos);
        var update = data;
        if (lang == "spanish" || lang == "") {
            var msjn = "No pudimos realizar los cambios";
            var msjs = "Listo!, se ha realizado el ajuste";
        } else {
            var msjn = "An error occurred while processing your request.";
            var msjs = "Changes Saved Successfully";
        }

        var controlador = 'UPD_User';

        UpdateElement(tabla, campo, id, update, msjn, msjs, controlador);
    }

}
function EditarPanelista() {

    var vacios = revisarVacios();

    if (vacios) {
        var datos = $('#form_editar_panelista').serializeArray();

        // Ajuste para checboxes vacios----------------------
        var checks = {};
        var item = [];

        $("#form_editar_panelista input:checkbox").each(function () {
            checks["name"] = this.name;
            checks["value"] = +this.checked;

            const index = datos.findIndex(x => x.name === this.name);

            if (index > 0) {
                datos.splice(index, 1);
            }
            datos.push(checks);

        });
        //----------------------------------------------------

        var tabla = datos[0]['value'];
        var campo = datos[1]['value'];
        var id = datos[2]['value'];
        datos.splice(0, 2);

        var data = ArrayToSerial(datos);
        var update = data;
        if (lang == "spanish" || lang == "") {
            var msjn = "No pudimos realizar los cambios";
            var msjs = "Listo!, se ha realizado el ajuste";
        } else {
            var msjn = "An error occurred while processing your request.";
            var msjs = "Changes Saved Successfully";
        }

        var controlador = 'UPD_Panelista';

        UpdateElement(tabla, campo, id, update, msjn, msjs, controlador);
    }

}

function EditarPass() {

    var vacios = revisarVacios();
    var pass= $('#form_editar_pass #password').val();
    var confirma= $('#form_editar_pass #confirma').val();


    if (vacios && confirma==pass) {
        var datos = $('#form_editar_pass').serializeArray();

        // Ajuste para checboxes vacios----------------------
        // var checks = {};
        // var item = [];

        // $("#form_editar_pass input:checkbox").each(function () {
        //     checks["name"] = this.name;
        //     checks["value"] = +this.checked;

        //     const index = datos.findIndex(x => x.name === this.name);

        //     if (index > 0) {
        //         datos.splice(index, 1);
        //     }
        //     datos.push(checks);

        // });
        //----------------------------------------------------

        // var tabla = datos[0]['value'];
        // var campo = datos[1]['value'];
        var id = datos[2]['value'];
        // datos.splice(0, 2);

        
    }else{
        swal({
            title: "Error!",
            text: "Las contraseñas no coinciden",
            type: "warning"
        }); 
    }

}

function Agregar(funcion = '') {

    var vacios = revisarVacios();

    if (vacios) {
        var datos = $('#form_add').serializeArray();
        // datos.splice(0, 2);
        var data = ArrayToSerial(datos);
        var update = data;
        if (lang == "spanish" || lang == "") {
            var msjn = "Ups! no hemos podido agregarlo";
            var msjs = "Ya quedo agregado!";
        } else {
            var msjn = "An error occurred while processing your request.";
            var msjs = "Changes Saved Successfully";
        }
        var controlador = 'INS_registro'

        // console.log(tabla);

        AddElement(tabla, update, msjn, msjs, controlador, funcion);
    }

}

function AgregarDetalle(tabla, id) {

    // var vacios = revisarVacios();

    // if(vacios){
    // var datos = $('#form_add').serializeArray();
    // datos.splice(0, 2);
    tabla = 'det_colaborador';
    var data = 'id_user=' + id;
    var update = data;
    if (lang == "spanish" || lang == "") {
        var msjn = "Ups! no hemos podido agregarlo";
        var msjs = "Ya quedo agregado!";
    } else {
        var msjn = "An error occurred while processing your request.";
        var msjs = "Changes Saved Successfully";
    }
    var controlador = 'INS_registro';
    var funcion = "";

    // console.log(tabla);

    AddElement(tabla, update, msjn, msjs, controlador, funcion);
    sendNotificacion(id);
    // }

}

function sendNotificacion(id) {
    // var data = 'id='+id;
    var ruta = base_url + 'index.php/Controles/notificarUsuario/' + id;
    $.post(ruta, function (res) {
        console.log(res);
    });
}

function AgregarTree(funcion = '') {

    var vacios = revisarVacios();
    var depto = $('#combo_id_depto').val();


    if (vacios && depto != 0) {
        var datos = $('#form_add_tree').serializeArray();
        var data = ArrayToSerial(datos);

        var update = data;

        if (lang == "spanish" || lang == "") {
            var msjn = "Ups! no hemos podido agregarlo";
            var msjs = "Ya quedo agregado!";
        } else {
            var msjn = "An error has occurred";
            var msjs = "Changes Saved Successfully";
        }

        // console.log(lang);
        var controlador = 'INS_registro'
        AddElement(tableTree, update, msjn, msjs, controlador, funcion);

    } else {
        if (lang == "spanish" || lang == "") {
            msjn = 'Debe seleccionar un departamento';
        } else {
            msjn = 'Please select department';
        }
        swal({
            title: "Ups!",
            text: msjn,
            type: "warning"
        });
    }

}

function removeFromArray(original, remove) {
    return original.filter(value => !remove.includes(value));
}

function AgregarEncuesta(funcion = '') {

    var vacios = revisarVacios();
    var seccion = $('#form_add_encuesta #id_seccion').val();

    if (vacios && seccion != 0) {
        $("#data_options").prop("disabled", false);
        var datos = $('#form_add_encuesta').serializeArray();
        // console.log(datos);
        var opciones = '';
        var respuestas = '';
        var lista = [];
        $.each(datos, function (i) {
            if (datos[i]['name'] == 'data_options') {
                opciones = opciones + '|' + datos[i]['value'];
                lista.push(i);
            }
            if (datos[i]['name'] == 'respuesta') {
                respuestas = respuestas + '|' + datos[i]['value'];
                lista.push(i);
                // datos.splice(i, 1);
            }
        });

        for (var i = lista.length - 1; i >= 0; i--)
            datos.splice(lista[i], 1);

        var options = opciones.substr(1, opciones.length);
        var res = respuestas.substr(1);
        res = res.substr(1);

        var valueToPush = {};
        valueToPush["name"] = 'data_options';
        valueToPush["value"] = options;
        datos.push(valueToPush);

        var valueToPush2 = {};
        valueToPush2["name"] = 'respuesta';
        valueToPush2["value"] = res;
        datos.push(valueToPush2);

        var data = ArrayToSerial(datos);

        var update = data;
        if (lang == "spanish" || lang == "") {
            var msjn = "Ups! no hemos podido agregarlo";
            var msjs = "Ya quedo agregado!";
        } else {
            var msjn = "An error has occurred, please retry";
            var msjs = "Changes Saved Successfully";
        }

        var controlador = 'INS_registro'

        AddElement(tableTree, update, msjn, msjs, controlador, funcion);

    } else {
        if (lang == "spanish" || lang == "") {
            msjn = 'Debe seleccionar una seccion y llenar todos los campos';
        } else {
            msjn = 'Please select one section';
        }

        swal({
            title: "Ups!",
            text: msjn,
            type: "warning"
        });
    }

}

function ArrayToSerial(data) {
    var lista = "";

    data.forEach(function (i) {
        lista = lista + "&" + i.name + "=" + i.value;
    });

    lista = lista.substr(1);
    return lista;
}


function revisarVacios() {
    var requeridos = 0;
    var llenos = 0;
    var tipo = "";
    var res = "";
    // console.log('entro a la funcion');

    $('.modal [required]:visible').each(function () {
        valorCampo = $(this).val();
        tipo = $(this).attr("type");
        // console.log('campo');
        // console.log($(this).attr('id'));
        // console.log($(this).val());


        if (valorCampo != "" && valorCampo != 0) {
            llenos++;
            // console.log(valorCampo);
            $(this).closest('.form-group').removeClass('has-danger');
        } else {
            $(this).closest('.form-group').addClass('has-danger');
        }
        if (tipo == 'email') {
            res = revisarEmail(valorCampo);
            if (res == false) {
                $(this).closest('.form-group').addClass('has-danger');
            } else {
                $(this).closest('.form-group').removeClass('has-danger');
            }
        }
        requeridos++;
    });

    if (lang == "spanish" || lang == "") {
        var msjn = "Debe llenar correctamente todos los campos requeridos";
    } else {
        var msjn = "Please fill correctly all required fields";
    }

    // console.log(llenos);
    // console.log(requeridos);

    if (llenos < requeridos) {
        swal('Error', msjn, 'warning');
        return false;
    } else {
        // console.log('entro a true');
        return true;

    }
}

function revisarVacios_form() {
    var requeridos = 0;
    var llenos = 0;
    var checked = "";
    var pregunta = "";
    var res=true;

    // reviso div de requeridos
    if ($('div').hasClass("required")) {
        if (lang == "spanish" || lang == "") {
            msjc = "Debe seleccionar al menos una opción";
        } else {
            msjc = "You must check at least one checkbox.";
        }
        // recorro la div de requeridos
        $('.required').each(function () {
            pregunta = $(this).attr('rel');
            checked = $(this).find('input[type=checkbox]:checked').length;

            console.log(pregunta);
            console.log(checked);
            if (!checked) {
                $(this).addClass('has-danger');
                swal('Error', msjc, 'warning');
                res = false;
            }
        })
    
    }


    $('[required]:visible').each(function () {
        valorCampo = $(this).val();
        tipo = $(this).attr('type');
        // console.log(valorCampo);
        if (valorCampo != "" && valorCampo != 0) {
            if (tipo == 'radio') {
                if ($(this).closest('.demo-radio-button').find('input').is(':checked')) {
                    llenos++;
                    $(this).closest('.form-group').removeClass('has-danger');
                } else {
                    $(this).closest('.form-group').addClass('has-danger');
                }
            } else {
                llenos++;
                // console.log(valorCampo);
                $(this).closest('.form-group').removeClass('has-danger');
            }

        } else {
            $(this).closest('.form-group').addClass('has-danger');
        }
        requeridos++;
    });

    if (lang == "spanish" || lang == "") {
        var msjn = "Debe llenar todos los campos requeridos";
    } else {
        var msjn = "Please fill all required fields";
    }

    if (llenos < requeridos || res==false) {
        swal('Error', msjn, 'warning');
        return false;
    } else {
        return true;
    }
}

function revisarVacios_form_obs() {
    var requeridos = 0;
    var llenos = 0;
    var checked = "";
    var pregunta = "";
    var res=true;

    $('[required]:visible').each(function () {
        valorCampo = $(this).val();
        tipo = $(this).attr('type');
        name = $(this).attr('name');
        // console.log(valorCampo);
        if (valorCampo != "" && valorCampo != 0) {
            if (tipo == 'radio') {
                if ($(this).closest('.radio-group').find('input').is(':checked')) {
                    llenos++;
                    // $(this).closest('.form-group').removeClass('has-danger');
                    $(this).closest('.radio-group').find('label').css('color', '#67757c');
                } else {
                    // $(this).closest('.form-group').addClass('has-danger');
                    $(this).closest('.radio-group').find('label').css('color', 'red');
                }
            } else {
                llenos++;
                console.log(valorCampo);
                $(this).closest('.form-group').removeClass('has-danger');
            }

        } else {
            $(this).closest('.form-group').addClass('has-danger');
        }
        requeridos++;
    });

    if ($('div').hasClass("required")) {
        if (lang == "spanish" || lang == "") {
            msjc = "Debe seleccionar al menos una opción";
        } else {
            msjc = "You must check at least one checkbox.";
        }
        // recorro la div de requeridos
        $('.required').each(function () {
            pregunta = $(this).attr('rel');
            checked = $(this).find('input[type=checkbox]:checked').length;

            console.log(pregunta);
            console.log(checked);
            if (!checked) {
                $(this).addClass('has-danger');
                swal('Error', msjc, 'warning');
                res = false;
            }
        })

    }

    if (lang == "spanish" || lang == "") {
        var msjn = "Debe llenar todos los campos requeridos";
    } else {
        var msjn = "Please fill all required fields";
    }

    // console.log('llenos ='+llenos);
    // console.log('requeridos ='+requeridos);
    // console.log('res ='+res);
    if (llenos < requeridos || res == false) {
        swal('Error', msjn, 'warning');
        return false;
    } else {
        return true;
    }
}

function DeleteElement(table, campo, id, msjn, msjs, controlador) {

    var data = "table=" + table + "&campo=" + campo + "&id=" + id;

    $.ajax({
        url: base_url + 'index.php/Controles/' + controlador,
        data: data,
        method: 'POST',
        beforeSend: function () {
            $('.preloader').show();
        },
        success: function (res) {
            $('.preloader').hide();
            console.log(res);
            if (res == 0) {
                swal({
                    title: "Ups!",
                    text: msjn,
                    type: "warning"
                });
            } else {
                swal({
                    title: "Ok",
                    text: msjs,
                    type: "success"
                },
                    function () {
                        window.location.reload();
                    }
                );
            }

        }
    });
}

function getDataForm(table, campo, campos_ver, id, msjn, msjs, controlador) {

    var data = "table=" + table + "&campo=" + campo + "&campos_ver=" + campos_ver + "&id=" + id;
    var valSel = "";
    $.ajax({
        url: base_url + 'index.php/Controles/' + controlador,
        data: data,
        method: 'POST',
        dataType: 'json',
        cache: false,
        beforeSend: function () {
            $('.preloader').show();
        },
        success: function (res) {
            $('.preloader').hide();
            // console.log(res);
            if (res == 0) {
                swal({
                    title: "Ups!",
                    text: msjn,
                    type: "warning"
                });
            } else {
                // $(".editar_form").html(res);
                // console.log(res);
                $.each(res, function (i, item) {
                    // $(document).ajaxComplete(function(){ 
                    $('#' + i).val(res[i]).trigger('change');
                    // });
                    if (i == 'id_company') {
                        valSel = res[i];
                    }
                    if (i == 'id_puesto') {
                        valSel = res[i];
                    }
                    if (i == 'id_depto') {
                        valSel = res[i];
                    }
                    
                });

                $("#modalEdit").modal();
            }

        }
    });

    $(document).ajaxComplete(function () {
        $('#id_puesto').val(valSel);
    });
}

function DeleteElementTree(table, campo, id, msjn, msjs, controlador, hijos) {

    var data = "table=" + table + "&campo=" + campo + "&id=" + id + '&hijos=' + hijos;

    $.ajax({
        url: base_url + 'index.php/Controles/' + controlador,
        data: data,
        method: 'POST',
        beforeSend: function () {
            $('.preloader').show();
        },
        success: function (res) {
            $('.preloader').hide();
            console.log(res);
            if (res == 0) {
                swal({
                    title: "Ups!",
                    text: msjn,
                    type: "warning"
                });
            } else {
                swal({
                    title: "Ok",
                    text: msjs,
                    type: "success"
                },
                    function () {
                        window.location.reload();
                    }
                );
            }

        }
    });
}

function UpdateElement(table, campo, id, update, msjn, msjs, controlador, funcion = '') {

    var data = "table=" + table + "&campo=" + campo + "&id=" + id + "&" + update;

    $.ajax({
        url: base_url + 'index.php/Controles/' + controlador,
        data: data,
        method: 'POST',
        beforeSend: function () {
            $('.preloader').show();
        },
        success: function (res) {
            $('.preloader').hide();
            // console.log(res);
            if (res == 0) {
                swal({
                    title: "Ups!",
                    text: msjn,
                    type: "warning"
                });
            } else {

                swal({
                    title: "Ok",
                    text: msjs,
                    type: "success"
                },
                    function () {
                        if (funcion != '') {
                            runFunction(funcion, [table, res]);
                        } else {
                            window.location.reload();
                        }

                    }
                );
            }

        }
    });
}

function AddElement(table, update, msjn, msjs, controlador, funcion) {

    var data = "table=" + table + "&" + update;

    $.ajax({
        url: base_url + 'index.php/Controles/' + controlador,
        data: data,
        method: 'POST',
        beforeSend: function () {
            $('.preloader').show();
        },
        success: function (res) {
            // console.log(res);
            if (res == 0) {
                swal({
                    title: "Ups!",
                    text: msjn,
                    type: "warning"
                });
            } else {
                if (funcion != '') {
                    // alert(res);
                    runFunction(funcion, [table, res]);
                } else {
                    swal({
                        title: "Ok",
                        text: msjs,
                        type: "success"
                    },
                        function () {
                            window.location.reload();
                        }
                    );
                }

            }

        }
    });
}

$(document).on('click', '.desactivar', function () {
    var res = $(this).attr('href');
    var id = res.replace("#", "");
    var tbl = $(this).attr('rel');
    var status = $(this).attr('alt');

    if (status == 1) {
        var update = "id_estatus=8";
    } else if (status == 8 || status == 4) {
        var update = "id_estatus=1";
    }

    var campo = "id_encuesta";
    if (lang == "spanish" || lang == "") {
        var msjn = "No ha podido activarse/desactivarse la evaluación";
        var msjs = "La evaluación se ha activado/desactivado";
        var msjtitle = "Confirme la acción";
        var msjtext = "Esta seguro que desea activar/desactivar la evaluación,\n una vez activa ya no podrá ser editada.";
        var cancelar = "Cancelar";
        var cambiar = "Cambiar estatus";
    } else {
        var msjn = "An error occurred while processing your request";
        var msjs = "The form changed successfully";
        var msjtitle = "Please confirm this action";
        var msjtext = "Are you sure to status change form?";
        var cancelar = "Cancel";
        var cambiar = "Change Status";
    }
    var controlador = 'UPD_User';

    swal({
        title: msjtitle,
        text: msjtext,
        type: "warning",
        showCancelButton: true,
        CancelButtonText: cancelar,
        closeOnConfirm: false,
        confirmButtonText: cambiar,
    }, function () {
        UpdateElement(tbl, campo, id, update, msjn, msjs, controlador);
    });

});

$(document).on('click', '.desactivarS', function () {
    var res = $(this).attr('href');
    var id = res.replace("#", "");
    var tbl = $(this).attr('rel');
    var status = $(this).attr('alt');

    if (status == 1) {
        var update = "id_estatus=8";
    } else if (status == 8 || status == 4) {
        var update = "id_estatus=1";
    }

    var campo = "id_encuesta";

    if (lang == "spanish" || lang == "") {
        var msjn = "No ha podido activarse/desactivarse la evaluación";
        var msjs = "La evaluación se ha activado/desactivado";
        var msjtitle = "Confirme la acción";
        var msjtext = "Esta seguro que desea activar/desactivar la evaluación,\n una vez activa ya no podrá ser editada.";
        var cancelar = "Cancelar";
        var cambiar = "Cambiar estatus";
    } else {
        var msjn = "An error occurred while processing your request";
        var msjs = "The form changed successfully";
        var msjtitle = "Please confirm this action";
        var msjtext = "Are you sure to status change form?";
        var cancelar = "Cancel";
        var cambiar = "Change Status";
    }


    var controlador = 'UPD_Status_Obs';

    swal({
        title: msjtitle,
        text: msjtext,
        type: "warning",
        showCancelButton: true,
        CancelButtonText: cancelar,
        closeOnConfirm: false,
        confirmButtonText: cambiar,
    }, function () {
        UpdateElement(tbl, campo, id, update, msjn, msjs, controlador);
    });

});

$(document).on('click', '.editar', function () {
    var res = $(this).attr('href');
    var id = res.replace("#", "");
    var title = $(this).closest('.card').find('.card-header').find('.text-white').text();
    var descripcion = $(this).closest('.card').find('.card-body').find('.card-text').text();
    var link = $(this).closest('.card').find('#link').val();
    var obligatorio = $(this).closest('.card').find('#obligatorio_val').val();
    var observacion = $(this).closest('.card').find('#observacion_val').val();
    var calificacion = $(this).closest('.card').find('#calificacion_val').val();
    var status = $(this).closest('.card').find('#status').val();

    $('#modalEdit #titulo').val(title);
    $('#modalEdit #descripcion').val(descripcion);
    $('#modalEdit #link_elearning').val(link);
    $('#modalEdit #id_encuesta').val(id);
    $('#modalEdit #calificacion').val(calificacion);
    $('#modalEdit #id_estatus').val(status);
    if (obligatorio == 1) {
        $('#modalEdit #obligatorio').prop('checked', true);
    } else {
        $('#modalEdit #obligatorio').prop('checked', false);
    }
    if (observacion == 1) {
        $('#modalEdit #observacion').prop('checked', true);
    } else {
        $('#modalEdit #observacion').prop('checked', false);
    }

    $("#modalEdit").modal();
});



$(document).on('blur', '#link_elearning,#quick_video', function () {
    var link = $(this).val();
    var clean = changeYoutubeLink(link);
    $(this).val(clean);
});

function changeYoutubeLink(link) {
    var res = link.replace("watch?v=", "embed/");
    return res;
}

function recibo(id) {
    window.location.href = base_url + "index.php/Pagos/Recibo/" + id;
}

function runFunction(name, arguments) {
    var fn = window[name];
    if (typeof fn !== 'function') {
        return;
    }
    fn.apply(window, arguments);
}

$(document).on('change', '#id_tipo', function () {
    var id_tipo_pregunta = $(this).val();

    if (id_tipo_pregunta == 1) {
        $('.dataoptions').hide();
        $('.respuestas').show();
        $('.respuesta_check').hide();
        $('#estadistico').closest('div').hide();
    } else if (id_tipo_pregunta == 2) {
        $('.dataoptions').show();
        $('.respuestas').hide();
        $('.respuesta_check').show();
        $('#estadistico').closest('div').hide();
    } else if (id_tipo_pregunta == 3) {
        $('.dataoptions').show();
        $('.respuestas').hide();
        $('#estadistico').closest('div').show();
        $('.respuesta_check').show();
    } else if (id_tipo_pregunta == 4) {
        $('.dataoptions').show();
        $('.respuestas').hide();
        $('#estadistico').closest('div').hide();
        $('.respuesta_check').show();
    } else if (id_tipo_pregunta == 6 || id_tipo_pregunta == 7 || id_tipo_pregunta == 5) {
        $('.dataoptions').hide();
        $('#estadistico').closest('div').hide();
        // $('#estadistico').closest('div').show();
    } else {
        $('.dataoptions').show();
        $('.respuestas').show();
        $('.respuesta_check').hide();
        $('#estadistico').closest('div').hide();
    }

})

$("#estadistico").click(function () {
    if ($(this).is(':checked')) {
        $('#data_options').val(null).trigger('change');
        $("#data_options").html("");

        if (lang == "spanish" || lang == "") {
            var data = {
                id: 'Seguro',
                text: 'Seguro'
            };
            var data2 = {
                id: 'Con Oportunidad',
                text: 'Con Oportunidad'
            };
        } else if (lang == "english") {
            var data = {
                id: 'Seguro',
                text: 'Safe'
            };
            var data2 = {
                id: 'Con Oportunidad',
                text: 'Opportunity'
            };
        }


        var newOption = new Option(data.text, data.id, false, false);
        $('#data_options').append(newOption).trigger('change');



        var newOption2 = new Option(data2.text, data2.id, false, false);
        $('#data_options').append(newOption2).trigger('change');
        $('#data_options').val(['Seguro', 'Con Oportunidad']);
        $('#data_options').trigger('change');
        $("#data_options").prop("disabled", true);

    } else {
        $('#data_options').val(null).trigger('change');
        $("#data_options").html("");
        $("#data_options").prop("disabled", false);
    }
});

$(document).on('click', '.enviar_observacion', function () {

    var comentario = $('#comentario').val();
    var id_respuesta = $('#id_respuesta').val();
    var id_user = $('#id_user').val();

    // datos.splice(0, 2);
    var tbl = "tbl_bitacora";
    var data = 'id_respuesta_master=' + id_respuesta + '&id_user=' + id_user + '&comentario=' + comentario;
    var update = data;

    if (lang == "spanish" || lang == "") {
        var msjn = "Ups! no se ha agregado el comentario";
        var msjs = "Ya quedo agregado el comentario!";
    } else {
        var msjn = "Ups! An error has occurred";
        var msjs = "Comment has been succesfully saved";
    }

    var controlador = 'INS_registro';
    var funcion = "";

    // console.log(tabla);

    AddElement(tbl, update, msjn, msjs, controlador, funcion);

})

$(document).on('change', '#asignar', function () {

    var id_user = $(this).val();
    var id_respuesta = $('#id_respuesta').val();

    var tbl = "tbl_respuestas_encuesta";
    var campo = "id_respuesta_master";
    var id = id_respuesta;
    var update = "id_user_assigned=" + id_user;
    if (lang == "spanish" || lang == "") {
        var msjn = "No ha sido asignado al usuario";
        var msjs = "Se ha asignado al usuario";
    } else {
        var msjn = "An error has occurred";
        var msjs = "User has been assigned succesfully";
    }


    var controlador = 'UPD_Registro';

    UpdateElement(tbl, campo, id, update, msjn, msjs, controlador);
})

$(document).on('change', '#estatus', function () {

    var id_status = $(this).val();
    var id_respuesta = $('#id_respuesta').val();

    var tbl = "tbl_respuestas_encuesta";
    var campo = "id_respuesta_master";
    var id = id_respuesta;
    var update = "id_estatus=" + id_status;

    if (lang == "spanish" || lang == "") {
        var msjn = "No se ha cambiado el estatus";
        var msjs = "Se ha cambiado el estatus";
    } else {
        var msjn = "An error has occurred, please retry";
        var msjs = "Status has been succesfully changed";
    }

    var controlador = 'UPD_Registro';

    UpdateElement(tbl, campo, id, update, msjn, msjs, controlador);
})
// function desactivar(table,id,){
//     var borrar = $(this).attr('rel');
//             var update="id_role=3";
//             var campo="id_user";
//             var msjn = "No cuenta con más administradores disponibles, aumente su paquete o elimine un administrador. ";
//             var msjs = "El usuario seleccionado ha dejado de ser administrador";
//             var controlador = 'UPD_User_Admin';

//                 swal({
//                   title: "Confirme la acción", 
//                   text: "Esta seguro que desea quitar el administrador", 
//                   type: "warning",
//                   showCancelButton: true,
//                   CancelButtonText: "Cancelar",
//                   closeOnConfirm: false,
//                   confirmButtonText: "Eliminar",
//                 }, function() {
//                      UpdateElement(table,campo,borrar,update,msjn,msjs,controlador);
//                 }); 
// }

function CambiarPass() {
    var id_user = $('#form_cambiar_pass #id_user').val();
    var password = $('#form_cambiar_pass #password').val();
    var confirm = $('#form_cambiar_pass #confirm').val();
    var msjne = "Las contraseñas no coinciden";

    if (revisarVacios()) {

        if (password == confirm) {

            if (password.length >= 8) {

                var tbl = "tbl_user";
                var campo = "id_user";
                var id = id_user;
                var update = "id_user=" + id_user + "&password=" + password;
                if (lang == "spanish" || lang == "") {
                    var msjn = "No se ha actualizao el password";
                    var msjs = "La contraseña se ha actualizado";
                    var chars = "Debe tener almenos 8 caracteres";
                } else {
                    var msjn = "An error has occurred, please retry";
                    var msjs = "Password has been succesfully changed";
                    var chars = "Password must be 8 chars length";
                }
                var controlador = 'UPD_User_Pass';

                UpdateElement(tbl, campo, id, update, msjn, msjs, controlador);
            } else {
                swal({
                    title: "Error!",
                    text: chars,
                    type: "warning"
                });
            }
        } else {
            swal({
                title: "Error!",
                text: msjne,
                type: "warning"
            });
        }
    }

}

function CambiarConfig() {
    var campo = $('#campo_config').val();
    var valor = $('#valor_config').val();



    if (lang == "spanish" || lang == "") {
        var msjnP = "Debe el campo debe tener un valor";
        var msjs = "Se ha guardado la configuración";
    } else {
        var msjnP = "Input required";
        var msjs = "settings it was saved successfully";
    }


    var valida = 0;
    if (campo == 'Participacion' && valor != "") {
        valida = 1;
    } else if (campo == 'Participacion' && valor == "") {
        swal({
            title: "Error!",
            text: msjnP,
            type: "warning"
        });
        $('#valor_config').val(10);
    }

    if (campo == 'validate_elearning') {
        if ($('#valor_config').is(":checked")) {
            valor = $('#valor_config').val();
        } else {
            valor = 0;
        }
        valida = 1;
    }

    var data = 'campo=' + campo + '&valor=' + valor;

    // console.log(data);


    if (valida == 1) {
        $.ajax({
            url: base_url + 'index.php/Configurar/CambiarConfig',
            data: data,
            method: 'POST',
            // dataType: 'JSON',
            success: function (res) {
                // console.log(res);
                $.toast({
                    heading: 'Ok',
                    text: msjs,
                    position: 'top-right',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });
            }
        });
    }
}

$(document).on('click', '#guardar_observacion', function () {
    var respuestas = $('#observacion').serializeArray();

    var valor = "";
    var id = "";
    var numero = 0;
    var j = 0;
    var borrar = [];

    $.each(respuestas, function (i) {

        if (id == respuestas[i].name) {
            respuestas[numero].value = valor + '|' + respuestas[i].value;
            valor = respuestas[numero].value;
            borrar[j] = i;

            j++;
        } else {
            id = respuestas[i].name;
            valor = respuestas[i].value;
            numero = i;
        }

    });

    // console.log(borrar);

    for (var i = borrar.length - 1; i >= 0; i--)
        respuestas.splice(borrar[i], 1);

    // console.log(respuestas);

    var datos = $.param(respuestas);

    if (lang == "spanish" || lang == "") {
        msjs = "Se han guardado todas las respuestas";
        msjn = "No se han guardado todas las respuestas, por favor intente de nuevo";
        msju = "No se han guardado las respuestas, para realizar observaciones debe tener un sito, departamento y rol de liderazgo";
    } else {
        msjs = "Form saved succesfully";
        msjn = "An error has occurred, please retry";
        msju = "An error has occurred, user doesn't have have department, role or site";
    }

    if (revisarVacios_form_obs()) {
        // console.log('entro');
        $.ajax({
            url: base_url + 'index.php/Observacion/Guardar',
            data: datos,
            method: 'POST',
            beforeSend: function () {
                $('.preloader').show();
            },
            // dataType: 'JSON',
            success: function (res) {
                console.log(res);
                $('.preloader').hide();
                if (res == 1) {
                    swal({
                        title: "Ok",
                        text: msjs,
                        type: "success"
                    },
                        function () {
                            window.location.reload();
                        }
                    );
                } else if (res == 0) {
                    swal({
                        title: "Error",
                        text: msjn,
                        type: "warning"
                    },
                        function () {
                            window.location.reload();
                        }
                    );
                } else if (res == 3) {
                    swal({
                        title: "Error",
                        text: msju,
                        type: "warning"
                    },
                        function () {
                            window.location.reload();
                        }
                    );
                }

            }
        });
    }
});

$(document).on('click', '#guardar_elearning', function () {
    var respuestas = $('#elearning').serializeArray();


    var valor = "";
    var id = "";
    var numero = 0;
    var j = 0;
    var borrar = [];

    $.each(respuestas, function (i) {

        if (id == respuestas[i].name) {
            respuestas[numero].value = valor + '|' + respuestas[i].value;
            valor = respuestas[numero].value;
            borrar[j] = i;

            j++;
        } else {
            id = respuestas[i].name;
            valor = respuestas[i].value;
            numero = i;
        }

    });

    for (var i = borrar.length - 1; i >= 0; i--)
        respuestas.splice(borrar[i], 1);


    var datos = $.param(respuestas);

    // console.log(datos);
    if (lang == "spanish" || lang == "") {
        msjn = "No se han guardado todas las respuestas, por favor intente de nuevo";
    } else {
        msjn = "An error has occurred, please retry";
    }

    if (revisarVacios_form()) {

        $.ajax({
            url: base_url + 'index.php/Curso/Guardar',
            data: datos,
            method: 'POST',
            beforeSend: function () {
                $('.preloader').show();
            },
            success: function (res) {

                $('.preloader').hide();
                console.log(res);
                if (res != 0) {
                    swal({
                        title: "Result",
                        text: res,
                        type: "info"
                    },
                        function () {
                            window.location.href = base_url + 'index.php/Curso';
                        }
                    );
                } else {
                    swal({
                        title: "Error",
                        text: msjn,
                        type: "warning"
                    },
                        function () {
                            window.location.reload();
                            // window.location.href=base_url+'index.php/Curso';
                        }
                    );
                }

            }
        });
    }
});

$(document).on('dblclick', ':radio', function () {
    if (this.checked) {
        $(this).prop('checked', false);
    }
});

$(document).on('click', '#cancelar_cuenta', function () {
    cancelar_cuenta();
});


$(document).on('change', '#quickstart_check', function () {
    var quick = $('#quickstart_check').is(':checked');
    var valor = quick ? 1 : 0;

    if (lang == "spanish" || lang == "") {
        var msjs = "Se ha guardado la configuración";
    } else {
        var msjs = "Settings saved succesfully";
    }
    // alert(valor);
    $.ajax({
        url: base_url + 'index.php/Controles/Quickstart',
        data: 'quickstart=' + valor,
        method: 'POST',
        success: function (res) {

            console.log(res);
            if (res != 0) {
                $.toast({
                    heading: 'Ok',
                    text: msjs,
                    position: 'top-right',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });
            }

        }
    });
});

$(function () {
    var url = $("#quickstart_video").attr('src');
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#quickstart").on('hide.bs.modal', function () {
        $("#quickstart_video").attr('src', '');
    });

    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#quickstart").on('show.bs.modal', function () {
        $("#quickstart_video").attr('src', url);
    });
});

$(document).on('click', '#save_quick', function () {
    var link = $('#quick_video').val();

    if (lang == "spanish" || lang == "") {
        var msjs = "Se ha guardado la configuración";
    } else {
        var msjs = "Settings saved succesfully";
    }

    if (link != '') {
        $.ajax({
            url: base_url + 'index.php/Controles/QuickstartSave',
            data: 'quickstart=' + link,
            method: 'POST',
            success: function (res) {

                console.log(res);
                if (res != 0) {
                    swal({
                        title: "Ok",
                        text: msjs,
                        type: "success"
                    },
                        function () {
                            window.location.reload();
                        }
                    );
                }
            }

        });
    }
});


function cancelar_cuenta() {
    if (lang == "spanish" || lang == "") {
        var titulo = "Confirme la acción";
        var texto = "Esta seguro que desea cancelar su cuenta, \n esta acción no puede revertirse";
        var cancelar = "Cancelar";
        var eliminar = "Eliminar";
    } else {
        var titulo = "Please confirm this action";
        var texto = "Are sure to remove your account?, \n this action cannot be undone.";
        var cancelar = "Cancel";
        var eliminar = "Delete Account";
    }


    swal({
        title: titulo,
        text: texto,
        type: "error",
        showCancelButton: true,
        CancelButtonText: cancelar,
        closeOnConfirm: false,
        confirmButtonText: eliminar,
    }, function () {
        //    UpdateElement(table,campo,borrar,update,msjn,msjs,controlador);
        window.location.href = base_url + 'index.php/Login/CancelarCuenta';
    });
}

function recargarSesion() {
    window.location.href = base_url + 'index.php/Controles/RecargaSesion';
}

function revisarEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true)
    }
    return (false)
}