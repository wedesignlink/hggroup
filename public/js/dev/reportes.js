$(document).on('click','#crear_tabla',function(){

    var datos_reporte = $('#datos_reporte').find(':visible').serialize();
    var id_company = $('#id_company').val();
    datos_reporte = datos_reporte+'&id_company='+id_company;

    // console.log(datos_reporte);

    
    if(revisarVacios_form()){
        //console.log(datos_reporte);

        $.ajax({
                  url: base_url+'index.php/Modulos/Reportes/0',
                  data: datos_reporte,
                  method: 'POST',
                  beforeSend: function(){
                        $('.preloader').show();
                    },
                  // dataType: 'JSON',
                  success: function(res){
                    
                    $('.preloader').hide();
                    // console.log(res);
                    $('#contenedor').html(res);
                    
                    
                }
            });
    }
});

$(document).on('click','#crear_grafica',function(){

    var datos_reporte = $('#datos_reporte').find(':visible').serialize();
    var id_company = $('#id_company').val();
    datos_reporte = datos_reporte+'&id_company='+id_company;


    
    if(revisarVacios_form()){
        // console.log(datos_reporte);

        $.ajax({
                  url: base_url+'index.php/Modulos/Reportes/1',
                  data: datos_reporte,
                  method: 'POST',
                  beforeSend: function(){
                        $('.preloader').show();
                    },
                  // dataType: 'JSON',
                  success: function(res){
                    
                    $('.preloader').hide();
                    // console.log(res);
                    $('#contenedor').html(res);
                    
                    
                }
            });
    }
});




$(document).on('change','#tipo',function(){
  var valor = $(this).val();
  console.log(valor);
  if(valor==1){
    $('#ponderadom').hide();
    $('#ponderadoa').hide();
    $('#forms').hide();
    $('#puestos2').hide();
    $('#rango_mes').hide();
    $('#periodo_tipo').hide();
    $('#forms_all').hide();
    $('#rango,#puestos,#crear_grafica').show();
  }else if(valor==4){
      $('#rango_mes').hide();
      $('#periodo_tipo').hide();
      $('#forms_all').hide();
      $('#rango,#puestos,#puestos2,#crear_grafica,#ponderadoa,#forms').hide();
      $('#ponderadom').show();
  }else if(valor==5){
      $('#rango_mes').hide();
      $('#periodo_tipo').hide();
      $('#forms_all').hide();
      $('#rango,#puestos,#puestos2,#crear_grafica,#ponderadom,#forms').hide();
      $('#ponderadoa').show();
  }else if(valor==3){
    $('#ponderadom').hide();
    $('#ponderadoa').hide();
    $('#puestos').hide();
    $('#rango_mes').hide();
    $('#periodo_tipo').hide();
    $('#forms_all').hide();
    $('#rango,#puestos2,#crear_grafica,#forms').show();
  }else if(valor==2){
    $('#ponderadom').hide();
    $('#ponderadoa').hide();
    $('#puestos').hide();
    $('#crear_grafica').hide();
    $('#rango_mes').hide();
    $('#periodo_tipo').hide();
    $('#forms_all').hide();
    $('#rango,#puestos2,#forms').show();
  }else if(valor==6){
    $('#ponderadom').hide();
    $('#ponderadoa').hide();
    $('#puestos2').hide();
    $('#crear_grafica').hide();
    $('#rango').hide();
    $('#forms').hide();
    $('#rango_mes,#puestos,#periodo_tipo,#forms_all').show();
  }
});





