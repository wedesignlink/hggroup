$(document).ready(function()
{
    $('#filtro_cs #id_depto:visible').prop('disabled',true);
    $('#filtro_ta #id_deptota:visible').prop('disabled',true);
    $('#filtro_pu #id_deptopu:visible').prop('disabled',true);
    $('#filtro_td #id_deptotd:visible').prop('disabled',true);
    $('#filtro_to #id_deptoto:visible').prop('disabled',true);
});

$(document).on('change','#id_centro',function(){
    var id = $(this).val();
    var Newid = 'id_depto';
    var Nombre = 0;

    // id_puesto
    var data = 'id_combo='+id+'&idNew='+Newid+'&Name='+Nombre;
    
    $.ajax({
        url: base_url+'index.php/Controles/cargarComboDeptoExterno',
        data: data,
        method: 'POST',
        beforeSend: function(){
            $('#filtro_cs #id_depto:visible').html('<option>Cargando...</option>');
            $('#filtro_cs #id_depto:visible').prop('disabled',true);
        },
        success: function(res){
            //console.log(res);
            $('#filtro_cs #id_depto:visible').html('');
            $('#filtro_cs #id_depto:visible').prop('disabled',false);
            $('#filtro_cs #id_depto:visible').html(res);               
        }
    });
});

$(document).on('change','#id_centrota',function(){
    var id = $(this).val();
    var Newid = 'id_deptota';
    var Nombre = 0;

    // id_puesto
    var data = 'id_combo='+id+'&idNew='+Newid+'&Name='+Nombre;

    
    $.ajax({
        url: base_url+'index.php/Controles/cargarComboDeptoExterno',
        data: data,
        method: 'POST',
        beforeSend: function(){
            $('#filtro_ta #id_deptota:visible').html('<option>Cargando...</option>');
            $('#filtro_ta #id_deptota:visible').prop('disabled',true);
        },
        success: function(res){
            $('#filtro_ta #id_deptota:visible').html('');
            $('#filtro_ta #id_deptota:visible').prop('disabled',false);
            $('#filtro_ta #id_deptota:visible').html(res);               
        }
    });
});

$(document).on('change','#id_centropu',function(){
    var id = $(this).val();
    var Newid = 'id_deptopu';
    var Nombre = 0;

    // id_puesto
    var data = 'id_combo='+id+'&idNew='+Newid+'&Name='+Nombre;

    
    $.ajax({
        url: base_url+'index.php/Controles/cargarComboDeptoExterno',
        data: data,
        method: 'POST',
        beforeSend: function(){
            $('#filtro_pu #id_deptopu:visible').html('<option>Cargando...</option>');
            $('#filtro_pu #id_deptopu:visible').prop('disabled',true);
        },
        success: function(res){
            $('#filtro_pu #id_deptopu:visible').html('');
            $('#filtro_pu #id_deptopu:visible').prop('disabled',false);
            $('#filtro_pu #id_deptopu:visible').html(res);               
        }
    });
});

$(document).on('change','#id_centrotd',function(){
    var id = $(this).val();
    var Newid = 'id_deptotd';
    var Nombre = 0;

    // id_puesto
    var data = 'id_combo='+id+'&idNew='+Newid+'&Name='+Nombre;

    
    $.ajax({
        url: base_url+'index.php/Controles/cargarComboDeptoExterno',
        data: data,
        method: 'POST',
        beforeSend: function(){
            $('#filtro_td #id_deptotd:visible').html('<option>Cargando...</option>');
            $('#filtro_td #id_deptotd:visible').prop('disabled',true);
        },
        success: function(res){
            $('#filtro_td #id_deptotd:visible').html('');
            $('#filtro_td #id_deptotd:visible').prop('disabled',false);
            $('#filtro_td #id_deptotd:visible').html(res);               
        }
    });
});

$(document).on('change','#id_centroto',function(){
    var id = $(this).val();
    var Newid = 'id_deptoto';
    var Nombre = 0;

    // id_puesto
    var data = 'id_combo='+id+'&idNew='+Newid+'&Name='+Nombre;
    
    $.ajax({
        url: base_url+'index.php/Controles/cargarComboDeptoExterno',
        data: data,
        method: 'POST',
        beforeSend: function(){
            $('#filtro_to #id_deptoto:visible').html('<option>Cargando...</option>');
            $('#filtro_to #id_deptoto:visible').prop('disabled',true);
        },
        success: function(res){
            $('#filtro_to #id_deptoto:visible').html('');
            $('#filtro_to #id_deptoto:visible').prop('disabled',false);
            $('#filtro_to #id_deptoto:visible').html(res);               
        }
    });
});