$(document).on('click','#start', function () {
    let step = "One";
    $.ajax({
        url:base_url +'index.php/Grill/changeStep',
        data: {step : step},
        type:'POST',
        
        beforeSend: function () {
            $('#projectView').empty();
            $('.overlaySpin').fadeIn('slow');
        },
        success: function (res) {
            $('.overlaySpin').fadeOut('slow', function () {
                if (res) {
                    $('#stepButtons').removeClass('hidden');
                    $('#projectView').html(res);
                    $('#actualStep').val(step);
                }                 
            });
        },
        complete: function () {
            $('.overlaySpin').fadeOut();
        }
    });

});

$(document).on('click','.directionButton', function () {
    let direction = $(this).attr('value');
    let actualStep = $("#actualStep").val();
    let step="None";
    if(direction == "next")
    {
        switch(actualStep)
        {
            case "One":
                step="Two";
                break;
            case "Two":
                step="Three";
                break;
            case "Three":
                step="Four";
                break;
            case "Four":
                step="Five";
                break;
            case "Five":
                step="Quote";
                $('.nextButton').addClass('hidden');
                break;
        }
    }
    else 
    {
        switch(actualStep)
        {
            case "One":
                step="Start";
                $('#stepButtons').addClass('hidden');
                break;
            case "Two":
                step="One";
                break;
            case "Three":
                step="Two";
                break;
            case "Four":
                step="Three";
                break;
            case "Five":
                step="Four";
                break;
            case "Quote":
                step="Five";
                $('.nextButton').removeClass('hidden');
                break;
        }
    }
    $.ajax({
        url:base_url +'index.php/Grill/changeStep',
        data: {step : step},
        type:'POST',
        beforeSend: function () {
            $('#projectView').empty();
            $('.overlaySpin').fadeIn('slow');
        },
        success: function (res) {

            $('.overlaySpin').fadeOut('slow', function () {
                if (res) {
                    $('#projectView').html(res);
                    if(step!="Start")
                    {
                        $('#'+actualStep).removeClass('activeButton').addClass('stepButton');
                        $('#'+step).addClass("activeButton").removeClass('stepButton');
                    }
                    $('#actualStep').val(step);
                }       
            });

        },
        complete: function () {
            $('.overlaySpin').fadeOut();
        }
    });

});

$(document).on('click','.stepButton', function () {
    let step = $(this).attr('id');
    let actualStep = $("#actualStep").val();
    $.ajax({
        url:base_url +'index.php/Grill/changeStep',
        data: {step : step},
        type:'POST',
        beforeSend: function () {
            $('#projectView').empty();
            
            $('.overlaySpin').fadeIn('slow');
        },
        success: function (res) {

            $('.overlaySpin').fadeOut('slow', function () {
                if (res) {
    
                    $('#projectView').html(res);
                    $('#'+actualStep).removeClass('activeButton').addClass('stepButton');
    
                    $('#'+step).addClass("activeButton").removeClass('stepButton');
                    
                    $('#actualStep').val(step);
                }        
            });

        },
        complete: function () {
            $('.overlaySpin').fadeOut();
        }
    });

});