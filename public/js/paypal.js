function createOrderObject (data) {
    let order = {
        "products":[],
        "total_items": 0
    };

    // not void cart
    if ( data.total_qty ) {
        const productsArray = Object.values(data.products);

        productsArray.forEach(p => {
            order.products.push({
                "sku"       :p.product.sku,
                "name"      :p.product.nombre,
                "unit_amount" :{
                    "currency_code": "USD",
                    "value": p.product.precio
                },
                "quantity"  :p.quantity
            });
            order.total_items += p.quantity;
        });
        return order;
    }
    return false;
}