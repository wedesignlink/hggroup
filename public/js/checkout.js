$(document).on('click', '#direction-button', function (e){
    e.preventDefault();

    let status_id = $('#status_direction').val();
    let data = $('#form_direction').serializeArray();    
    let dataObj = {};
    
    for (let i = 0; i < data.length; i++) 
        dataObj[data[i].name] = data[i].value;

        let errors = verifyDirectionData( dataObj, data );
        
        if( !errors.length ) {
            $.ajax({
                url: base_url + 'index.php/Checkout/direction_insert',
                data: {
                    status_id       :status_id,      
                    id_cliente      :dataObj["id_cliente"],
                    titulo          :dataObj["titulo"],
                    calle           :dataObj["calle"],
                    numero          :dataObj["numero"],
                    int             :dataObj["int"],
                    ciudad          :dataObj["ciudad"],
                    estado          :dataObj["estado"],
                    cp              :dataObj["cp"],

                },
                method: 'POST',
                beforeSend: function() {
                    $('#loader').fadeIn('slow');
                },
                  success: function(res){
                       $('#loader').fadeOut('slow', function (){
                            $('#forms_container').html(res);
                            $('#paypal-payment').css("display", "");
                            if(res){
                                swal({
                                    title: ( status_id == 1 ) ? "Your address has been updated successfully!" : "Your Addres registered successfully",
                                    timer: 2000
                                });
                                
                            } 
                       });                   
                },
                complete: function() {
                    $('#loader').fadeOut();
                }
            });           
        }
        else {
            swal({
                title: errors[0],
                timer: 2000
            });
        }    
});

function verifyDirectionData(data, keys) {
    let mistakes = [];

    // change color of border
    for (let i = 0; i < keys.length; i++) {
        if ( !keys[i].value && keys[i].name != "int" )
            $(`[name="${keys[i].name}"]`).css("border","solid rgba(139,0,0,0.7) 1px");
        else 
            $(`[name="${keys[i].name}"]`).css("border","1px solid #ccc");
    } 

    if(!data.titulo || !data.calle || !data.numero || !data.ciudad || !data.estado || !data.cp ){
        mistakes.push( "Please complete all fields" );  
    }
    return mistakes;
}

$(document).on('click', '.select_form', function (e) {
    e.preventDefault();
    
    let form = $(this).attr('id');
    $('#login_register_option').fadeOut('fast', function () {

        if (form == 'login') $('#login_form').fadeIn('slow');
        else $('#register_form').fadeIn('slow');
    });
});

$(document).on('click', '.cancel-button', function(e) {
    e.preventDefault();

    $('#register_form').hide();
    $('#login_form').hide();
    $('#login_register_option').fadeIn('slow');
});

$(document).on('click', '#login-button', function (e) {
    e.preventDefault();

    let email = $('#email-login').val();
    let password = $('#password-login').val();
    
    if ( !IsEmail(email) ) {
        swal({
            title:'Enter a valid email :(',
            timer: 2000
        });
    }
    else if (password.length < 7) {
        swal({
            title:'The password is to short :(',
            timer: 2000
        });
    }
    else {
        $.ajax({
            url: base_url + 'index.php/Checkout/login',
            data: { email:email, password:password },
            method: 'POST',
            beforeSend: function () {
                $('#loader').fadeIn('slow');
            },
            success: function (res) {
                $('#loader').fadeIn('slow', function () {
                    if (res) {
                        swal({
                            title: "Welcome Back :)", 
                            timer: 2000,
                        });
                        
                        $('#forms_container').html(res);
                        if ($('#status_direction').val()) $('#paypal-payment').css('display','');
                        $('#logged_link').attr('href',base_url+'index.php/Dashboard/logout');
                        $('#logged_link').text("LOGOUT");
                    }
                    else {
                        swal({
                            title: "The email or password are not correct :(", 
                            timer: 2000,
                        });
                    }
                });
            },
            complete: function () {
                $('#loader').fadeOut();
            }
        });
    }

});

$(document).on('click', '#register-button', function (e) {
    e.preventDefault();

    let data = $('#form-to-regiter').serializeArray();
    
    let dataObj = {};

    // convert array to object
    for (let i = 0; i < data.length; i++) 
        dataObj[data[i].name] = data[i].value;

    let errors = verifyRegisterData( dataObj, data ); 

    // valid form
    if ( !errors.length ){
        $.ajax({
            url: base_url + 'index.php/Checkout/registerThenLogin',
            data: { 
                nombre   :dataObj.name,
                apellido :dataObj.last_name,
                email    :dataObj.email,
                password :dataObj.password, 
                telefono :dataObj.phone 
            },
            method: 'POST',
            beforeSend: function () {
                $('#loader').fadeIn('slow');
            },
            success: function (res) {
                $('#loader').fadeOut('slow', function () {
                    if (res) {
                        $('#forms_container').html(res);
                        swal({
                            title: "Thanks for registering!",
                            timer: 2000
                        });
                        $('#logged_link').attr('href',base_url+'index.php/Dashboard/logout');
                        $('#logged_link').text("LOGOUT");                  
                    } else {
                        swal({
                            title: "The email is already registered :(",
                            timer: 2000
                        });
                    }
                });
            },
            complete: function () {
                $('#loader').fadeOut();
            }
        });
    }
    else {
        swal({
            title: errors[0],
            timer: 2000
        });
    }

});

function verifyRegisterData( data, keys ) {
    let mistakes = [];

    // change color of border
    for (let i = 0; i < keys.length; i++) {
        if ( !keys[i].value )
            $(`[name="${keys[i].name}"]`).css("border","solid red 10px");
        else 
            $(`[name="${keys[i].name}"]`).css("border","10px solid red");
    } 
    
    // name
    if ( !data.name || !data.last_name || !data.email
         || !data.email || !data.phone || !data.password
         || !data.c_password) 
    {
        mistakes.push( "Please complete all fields" );
        return mistakes;
    }
    
    if ( !IsEmail( data.email ) ) {
        mistakes.push( "Not valid email" );
        $(`[name="email"]`).css("border","solid rgba(139,0,0,0.7) 1px");
    }

    if ( String(data.phone).length != 10 ) {
        mistakes.push( "Not valid phone number" );
        $(`[name="phone"]`).css("border","solid rgba(139,0,0,0.7) 1px");
    }

    if ( data.password.length < 7) {
        mistakes.push( "The password is to short" );
        $(`[name="password"]`).css("border","solid rgba(139,0,0,0.7) 1px");
        $(`[name="c_password"]`).css("border","solid rgba(139,0,0,0.7) 1px");
    }

    else if ( data.password !== data.c_password ) {
        mistakes.push( "Passwords do not match" );
        $(`[name="password"]`).css("border","solid rgba(139,0,0,0.7) 1px");
        $(`[name="c_password"]`).css("border","solid rgba(139,0,0,0.7) 1px");
    }

    return mistakes;
}

function IsEmail( email ) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) return false;
    else return true;
}

$(document).on('click', '#contact-button', function(e){
    e.preventDefault();

    let data = $('#form_contact').serializeArray();
    let dataObj = [];

    for(let i = 0; i < data.length; i++)
    dataObj[data[i].name] = data[i].value;

    let errors = verifyContactData( dataObj, data);

    if(!errors.length){
        $.ajax({
            url: base_url + 'index.php/Checkout/ContactForm',
            data: {
                nombre   :dataObj.name,
                email    :dataObj.email,
                comment  :dataObj.comment
            },
            method: 'POST',
            complete: function() {
                swal({
                    title: " your data has been sent successfully", 
                    timer: 2000

            });
            
                    $("#name, #email, #comment").val('');
            }
        });
        }
    else {
        swal({
            title: errors[0],
            timer: 2000
        });
    }
    });

function verifyContactData(data, keys) {
    let mistakes = [];

    // change color of border
    for (let i = 0; i < keys.length; i++) {
        if ( !keys[i].value )
            $(`[name="${keys[i].name}"]`).css("border","solid rgba(139,0,0,0.7) 1px");
        else 
            $(`[name="${keys[i].name}"]`).css("border","1px solid #ccc");
    } 

    if(!data.name || !data.email || !data.comment){
        mistakes.push( "Please complete all fields" );  
        return mistakes;
    }

    if ( !IsEmail( data.email ) ) {
        mistakes.push( "Not valid email" );
        $(`[name="email"]`).css("border","solid rgba(139,0,0,0.7) 1px");
    }
    return mistakes;
}