<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class LangSwitch extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Store_model");
	}
function switchLanguage($language = "") {
        $language = ($language != "") ? strtolower($language) : "english";
        $this->session->set_userdata('site_lang', $language);
        $currentURL = $_SERVER['HTTP_REFERER'];
        redirect($currentURL);
    }
}