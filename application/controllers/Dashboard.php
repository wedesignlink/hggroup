<?php

class Dashboard extends CI_Controller{
  
  public  function index(){
    $data['lang']=$this->session->userdata('site_lang');
    $this->load->view('login',$data);
  }

  public function login(){
    //All login process
    
    $config = array(
      array(
        'field' => 'email',
        'label' => 'Correo Electronico',
        'rules' => 'required|valid_email|trim|xss_clean'
      ),
      array(
        'field' => 'pass',
        'label' => 'Contrasena',
        'rules' => 'required|trim|xss_clean'
      )
    );
    

    $this->form_validation->set_rules($config);

    //If Form Rules($config) aren't respected, reload login view with a mesagge
      
    if($this->form_validation->run($config)==FALSE){
      $this->session->set_flashdata('error', validation_errors());
      redirect(base_url()."index.php/Dashboard");
    }

    $username = $this->input->post('email');
    $passw=$this->input->post('pass');
        

    //save model function answer in a variable 
    $usuario= $this->Store_model->CNS_Access(array(
      "email" => $username,
      "pass" => $passw
    ));


    //If Model found a result set a session
    if(!empty($usuario)){
      $this->session->set_userdata($usuario);
      $this->session->set_userdata(array(
        'logged_in' => 1
      ));

      //If a session was set , redirect to catalog
        redirect(base_url()."index.php/Store/catalog");
    }
      //If Model Function didn't found a match on db, set logged_in to 0
    else{
      $this->session->set_userdata(array(
        'logged_in'=>0
      ));

      $this->session->set_flashdata('error', 'El usuario y/o contraseña no son correctos');
      redirect(base_url()."index.php/Dashboard");
    }
  }
        



  //logout
  public function logout(){
    session_destroy();
    redirect(base_url()."index.php/Store");
  } 



  public function Recovery(){
    
    //If Form Rules($config) aren't respected, reload login view with a mesagge
    $config2= array(
      'field' => 'emailr',
      'label' => 'Correo Electronico',
      'rules' => 'required|valid_email|trim|xss_clean'
    );

    if($this->form_validation->run($config2)==TRUE){
      $this->session->set_flashdata('Noregistrado', validation_errors());
      redirect(base_url()."index.php/Dashboard");
    }
    else{
      $this->load->helper('string');
      $token=random_string('alnum', 70);
      $emaile=$this->input->post('emailr');
      $data=array('email' => $emaile,'token'=>$token);
      $usuario=$this->Store_model->PassRecovery(array('email' => $emaile));
      $mensaje=$this->load->view('emails/recuperacion',$data,TRUE);
      $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
      $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      $cabeceras .= '"To:$usuario[nombre]"' . "\r\n";
      $cabeceras .= 'Resetea tu contraseña' . "\r\n";
    }

    if(!empty($usuario)){ 
      $this->Store_model->Insert_token($data);
      sendEmail('Reseteo De Contraseña',$mensaje,$emaile,$cabeceras);
      $this->session->set_flashdata('Reseteo', 'El Correo ha sido enviado, revisa tu bandeja para continuar con el proceso');
      redirect(base_url()."index.php/Dashboard");
    }

    else{
      $this->session->set_flashdata('Noregistrado', 'Usuario no registrado');
      redirect(base_url()."index.php/Dashboard"); 
    }
  }


  public function Reseting(){
    $token=$this->input->get('token');
    $access=$this->Store_model->get_token($token);
    if(!empty($access)){

      $passre=$this->input->post('nuevapass');
      $passres=$this->input->post('nuevapassc');
      

      if($passre==$passres){
         $encrypt=md5($passre);
         $reset=$this->Store_model->PassReset($encrypt,$access);
         $this->session->set_flashdata('Reseteo', 'tu contraseña ha sido actualizada');
         $this->Store_model->Deletokens($access[0]['email']);
         redirect(base_url()."index.php/Dashboard/");
      }
      else{
        $this->session->set_flashdata('noiguales', 'Las contraseñas no coinciden');
        redirect(base_url()."index.php/Dashboard/Reset?token=".$token);
      }
    }
    else{
      redirect(base_url()."index.php/Dashboard/Reset");
    }
  }


  public function Reset(){
    $data = array(
      'token' => $this->input->get('token'),
      'access'=> $this->Store_model->get_token($this->input->get('token'))
    );    
    

    $this->load->view('store/catalog/header');
		$this->load->view('passreset',$data); 
		$this->load->view('store/catalog/footer');
    
  }
}			