<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grill extends CI_Controller {

	public function index()
	{
		$this->load->view('grill/utiles/header');
		$this->load->view('grill/grill');
		$this->load->view('grill/utiles/footer');
	}

	public function changeStep()
    {

        $step = $this->input->post('step');

		$data = [
			"step" => $step,
		];

        $this->load->view('grill/steps/step'.$step,$data);

    }
}


