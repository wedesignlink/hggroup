<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Store extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Store_model");

		$site_lang = $this->session->userdata('site_lang');
        if ($site_lang) {
            $this->lang->load('home',$this->session->userdata('site_lang'));
        } else {
            $this->lang->load('home','spanish');
        }

		$cart_session=$this->session->userdata("cart");
		if (!$cart_session) {
			$cart_session=1;
        }
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['lang']=$this->session->userdata('site_lang');
		$data['products']= $this->Store_model->CNS_ProductsByManyFilteres(null, null, null, 0);
		$this->load->view('store/home/header',$data);
		$this->load->view('store/home/store',$data);
		$this->load->view('store/home/footer',$data);
	}

	public function catalog( $category = null, $offset=0)
	{
		$datalang['lang']=$this->session->userdata('site_lang');
		// session_destroy();
		$products = $this->Store_model->CNS_ProductsByCategory();
		$page = count($products)/9;
		$page =	ceil($page);

		$data = array(
			"categories" => $this->Store_model->CNS_Categories(),
			"products"   => $this->Store_model->CNS_ProductsByManyFilteres(null, null, null, 0),
			"pages" => $page,
		);

		$this->load->view('store/catalog/header',$datalang);
		$this->load->view('store/catalog/catalog', $data,$datalang);
		$this->load->view('store/catalog/footer',$datalang);
	}

	public function addProduct() {
		try {
			$id_producto = $this->input->post( "id_producto" );
			$quantity    = $this->input->post( "quantity" );
			$color 		 = $this->input->post( "color" );
			$texture 	 = $this->input->post( "texture" );

			$producto    = $this->Store_model->CNS_Producto( $id_producto );
			
			$cart_session = $this->session->userdata("cart");

			#	If cart is empty total_qty is 0
			$cart_session["total_qty"]    = isset($cart_session["total_qty"]) ? $cart_session["total_qty"] : 0 ;
			$cart_session["total_price"]  = isset($cart_session["total_price"]) ? $cart_session["total_price"] : 0 ;

			#	Add quantity to total products
			$cart_session["total_qty"] += $quantity;
			$cart_session["total_price"] += ($quantity * $producto["precio"]);

			#	If product already exists, add quantity
			if ( isset( $cart_session["products"]["PROD_" . $id_producto] ))
				$quantity  += $cart_session["products"][ "PROD_" . $id_producto]["quantity"];
			
			#	Arm item record
			$cart_session["products"][ "PROD_" . $producto["id_producto"] ] = array(
				"product"     => $producto,
				"quantity"    => (int) $quantity,
				"color"	      => $color,
				"texture"	  => $texture,
			);

			#	Set cart to userdata
			$this->session->set_userdata( "cart" , $cart_session);

			echo json_encode(array(
				"head" => "success",
				"body" => array(
					"cart" => $cart_session
			)));			
			
		} catch (\Throwable $th) {
			echo json_encode(array(
				"head" => "error",
				"body" => "Error al agregar carrito" 
			));
		}
	}

	public function cart( )
	{
		$data["cart_session"] = $this->session->userdata("cart");

		if($this->session->userdata("cart")){
		$this->load->view('store/catalog/header');
		$this->load->view('store/catalog/cart', $data);
		$this->load->view('store/catalog/footer');
		}
		else
		{
			redirect(base_url()."index.php/Store/catalog");
		}
	}

	public function detail( $id )
	{
		$product = $this->Store_model->CNS_ProductoFull( $id );

		// format description column
		$product["descripcion"] = str_replace( "\\n", '<br/>', $product["descripcion"] );

		if(($product["id_categoria"]=="1")||($product["id_categoria"]=="9"))
		{
			$data = array(
				"product"   => $this->Store_model->CNS_ProductoFull( $id ),
				"images"    => $this->Store_model->CNS_ImagesProduct( $id ),
				"colors" 	=> $this->Store_model->CNS_ColorsProduct($id),
				"textures"	=> $this->Store_model->CNS_TexturesProduct($id),
			);
		}
		else
		{
			$data = array(
				"product"   => $this->Store_model->CNS_ProductoFull( $id ),
				"images"    => $this->Store_model->CNS_ImagesProduct( $id ),
				"colors" 	=> $this->Store_model->CNS_ColorsProduct("25"),
				"textures"	=> $this->Store_model->CNS_TexturesProduct("25"),
			);
		}
		
		$this->load->view('store/catalog/header');
		$this->load->view('store/catalog/detail', $data);
		$this->load->view('store/catalog/footer');
	}

	public function searchOrFilter()
    {
        $category = $this->_category = $this->input->post('category');
        $search = $this->_search = $this->input->post('search');
        $order = $this->_order = $this->input->post('order');
        $page = $this->input->post('page');
		$category_products = [];
		
		// searching
		if ( $search ) {
			$category_products = $this->Store_model->CNS_ProductsByManyFilteres( $category, $order, $search, -1 );
		}
		// filter
		else {
			$category_products = $this->Store_model->CNS_ProductsByCategory( null, $category );
		}

		if ($page == -1) $page = 0;

        // pages by filters
        $pages = ceil(count($category_products)/9);

        $data = array(
            "products"    => $this->Store_model->CNS_ProductsByManyFilteres( $category, $order, $search, $page),
            "actual_page" => $page,
            "pages"       => $pages,
        );

        $this->load->view( 'store/catalog/products', $data );
    }

	public function register()
	{
		$data['lang']=$this->session->userdata('site_lang');
        $this->load->view('templates/header',$data);
        $this->load->view('register',$data);
        $this->load->view('templates/footer',$data);
    }

    public function doRegister()
	{
        $this->form_validation->set_rules('email', 'Correo Electrónico', 'required|valid_email|max_length[200]|trim|xss_clean');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[200]|trim|xss_clean');
        $this->form_validation->set_rules('apellido', 'Apellido', 'required|max_length[200]|trim|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[12]|trim|xss_clean');
        $this->form_validation->set_rules('password_cnf', 'Password Confirmation', 'required|matches[password]');
        $this->form_validation->set_rules('telefono', 'Teléfono', 'required|max_length[10]|trim|xss_clean');

        if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msg', validation_errors());
			redirect(base_url()."index.php/Store/register");
        }else{
            $nombre 	= $this->input->post("nombre");
			$apellido 	= $this->input->post("apellido");
			$email 		= $this->input->post("email");
			$telefono 	= $this->input->post("telefono");
			$password	= md5($this->input->post("password"));
			$verificationText = md5((string)time().$email);
			

			$data = array('nombre' => $nombre, 'apellido' => $apellido, 'email' => $email, 'telefono' => $telefono, 'id_status' => 0, 'password'=> $password,'email_verification_code'=>$verificationText);
			$mensaje = $this->load->view('correos/confirmacion', $data, true);
			$this->load->model('Register_user');
			if($this->Register_user->insert_user($data)){

				$data = array(
					"destinatario" => "$email",
					"asunto"       => "Correo de confimación de Email",
					"mensaje"      => "$mensaje",
				);
				if(sendEmail("Prueba", $data["mensaje"], $data["destinatario"])){
					echo "Email enviado";
					$this->session->set_flashdata('success', 'El usuario ha sido registrado exitosamente. Revisa tu email o spam para confirmar tu cuenta.');
					redirect(base_url()."index.php/Store/register");
				}else{
					$this->session->set_flashdata('msg', 'Error al enviar el email.');
					redirect(base_url()."index.php/Store/register");
				}
				
			}else{
				$this->session->set_flashdata('msg', 'El email ya está en uso. Intentar nuevamente.');
				redirect(base_url()."index.php/Store/register");
			}
    	}

	}

	public function verify( $verificationText = NULL )
	{  
		$this->load->model('Verification_email');
		$noRecords = $this->Verification_email->verifyEmailAddress($verificationText);  
		if ($noRecords > 0)
		{
		 $this->session->set_flashdata('success', 'El email ha sido verificado exitosamente.');
		 redirect(base_url()."index.php/Store/register");
		}
		else
		{
			$this->session->set_flashdata('msg', 'El email no se pudo verificar o ya se verificó previamente.');
			redirect(base_url()."index.php/Store/register");
		}
		// $data['errormsg'] = $error; 
		//  $this->load->view('index.php', $data);   
	}

    public function deleteCart() {
        $id_producto = $this->input->post( "id_product" );

        // Brings session variables
        $cart = $this->session->userdata( "cart" );
        // print_r( $this->session->userdata( "cart" ) );

        unset( $cart["products"]["PROD_" . $id_producto] );
        $total_quantity = 0;
        $total_price = 0;
        foreach ( $cart["products"] as $key ) {
            $total_quantity += $key["quantity"];
            $total_price += $key["quantity"] * $key["product"]["precio"];
        }

        // Updates the session variables
        $cart["total_qty"] = $total_quantity;
        $cart["total_price"] = $total_price;

        $this->session->set_userdata( "cart", $cart );

        $data["cart_session"] = $this->session->userdata( "cart" );
        $this->load->view( 'store/catalog/updateCart', $data );
    }

    public function updateCart() {
        $id_producto = $this->input->post( "id_product" );
        $product_quantity = $this->input->post( "quantity" );

        // Brings session variables
        $cart = $this->session->userdata( "cart" );

        $cart["products"]["PROD_" . $id_producto]["quantity"] = $product_quantity;
        $total_quantity = 0;
        $total_price = 0;
        foreach ( $cart["products"] as $key ) {
            $total_quantity += $key["quantity"];
            $total_price += $key["quantity"] * $key["product"]["precio"];
        }

        // Updates the session variables
        $cart["total_qty"] = $total_quantity;
        $cart["total_price"] = $total_price;

        $this->session->set_userdata( "cart", $cart );

        $data["cart_session"] = $this->session->userdata( "cart" );
        $this->load->view( 'store/catalog/updateCart', $data );

    }

    public function clearCart() {

        $cart = $this->session->userdata( "cart" );
        unset( $cart["products"] );
        $cart["products"] = [];
        $cart["total_qty"] = 0;
        $cart["total_price"] = 0;
        $this->session->set_userdata( "cart", $cart );

        $data["cart_session"] = $this->session->userdata( "cart" );
        $this->load->view( 'store/catalog/updateCart', $data );
    }
}
