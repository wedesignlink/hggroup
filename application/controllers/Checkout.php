<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Checkout extends CI_Controller {
	
	private $US_states = Array(
		"Alabama","Alaska","American Samoa","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","District Of Columbia","Federated States Of Micronesia","Florida","Georgia","Guam","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Marshall Islands","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Northern Mariana Islands","Ohio","Oklahoma","Oregon","Palau","Pennsylvania","Puerto Rico","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virgin Islands","Virginia","Washington","West Virginia","Wisconsin","Wyoming","abbreviation"
	);

	 

    public function __construct()
	{
		parent::__construct();
		$this->load->model("Checkout_model");
		$site_lang = $this->session->userdata('site_lang');
        if ($site_lang!="") {
            $this->lang->load('store_checkout',$this->session->userdata('site_lang'));
			$this->lang->load('home',$this->session->userdata('site_lang'));
        } else {
            $this->lang->load('store_checkout','spanish');
			$this->lang->load('home','spanish');
        }
    }

    public function index()
    {
        $cart_session = $this->session->userdata("cart");
        $taxes=(8.375/100)*$cart_session["total_price"];
		
		
		$data = array(
            "cart_session" => $this->session->userdata("cart"),
            "taxes" => $taxes,
        );
		
		
		if($this->session->userdata('logged_in') == 1 ){
            $user = $this->session->userdata("client_session")["id_cliente"];
            $direction = $this->Checkout_model->CNS_DirectionClientData($user);  
            $data["direccion"] = $direction;
        } else {
            $data["direccion"] = null;

        }

		$data["us_states"] = $this->US_states;
		$data['lang']=$this->session->userdata('site_lang');
        
		$cart_session = $this->session->userdata("cart");
		if ( count( $cart_session["products"] ) > 0 ) {
			$this->load->view('store/home/header',$data);
			$this->load->view('store/checkout/checkout', $data);
			$this->load->view('store/home/footer',$data);
		}
		else
		{
			redirect(base_url()."index.php/Store/catalog");
		}
        
    }

    public function direction_insert(){

			$status = $this->input->post('status_id');

            $data = Array(
				'id_cliente' => $this->input->post("id_cliente"),
                'titulo' => $this->input->post("titulo"),
                'calle' => $this->input->post('calle'),
                'numero' => $this->input->post('numero'),
                'int' => $this->input->post('int'),
                'ciudad' => $this->input->post('ciudad'),
                'estado' => $this->input->post('estado'),
                'cp' => $this->input->post('cp')
            );
			if( $status == 0) {
				$this->Checkout_model->Direccion_insert($data, 0);
			} else {
				$this->Checkout_model->Direccion_insert($data, 1);
			}	

			$user = $this->session->userdata("client_session")["id_cliente"];
			$direction = $this->Checkout_model->CNS_DirectionClientData($user);  
			$data["direccion"] = $direction;
			$data["us_states"] = $this->US_states;

        	$this->load->view('store/checkout/forms',$data);
    }

    public function registerThenLogin()
	{
		$data = Array(
			'nombre' => $this->input->post("nombre"),
			'apellido' => $this->input->post("apellido"),
			'email' => $this->input->post("email"),
			'telefono' => $this->input->post("telefono"),
			'id_status' => 0,
			'password'=> md5($this->input->post("password")),
			'email_verification_code' =>  md5((string)time().$this->input->post("email"))
		);

		$this->load->model('Register_user');
		
		// Complete register, login
		if( $this->Register_user->insert_user( $data ) ) {
			$this->load->model("Store_model");

			$user = $this->Store_model->CNS_Access(Array(
				"email"	=>	$this->input->post('email'),
				"pass"	=>	$this->input->post('password'),
			));
			

			$this->session->set_userdata( Array( 'logged_in' => 1, "client_session" => $user[0] ) );
			
            $direction = $this->Checkout_model->CNS_DirectionClientData($user[0]["id_cliente"]);  
            $data["direccion"] = $direction;
			$data["us_states"] = $this->US_states;

			$this->load->view('store/checkout/forms',$data);
		}
		// not exit user	
		else {
			$this->session->set_userdata( Array( 'logged_in' => 0, "client_session" => null ) );
			return false;
		}
		

		// if correct insert of client
		if( $this->session->userdata('logged_in') ){
			$HTML = $this->load->view('emails/confirmacion',Array(
				"verify_code"=>$user[0]["email_verification_code"],
				"name" => $user[0]["nombre"]." ".$user[0]["apellido"]
			));

			$data = array(
				"destinatario" => $this->input->post("email"),
				"asunto"       => "Correo de confimación de Email",
				"mensaje"      => $HTML,
			);

			sendEmail( "Registro de nueva cuenta", $data["mensaje"], $data["destinatario"], true );
		}
	}

	// set client credentials
	public function login ()
	{
		$this->load->model("Store_model");

		$user = $this->Store_model->CNS_Access(Array(
			"email"	=>	$this->input->post('email'),
			"pass"	=>	$this->input->post('password'),
		));

		// if exist user
        if ( !empty( $user ) ) {
			$this->session->set_userdata( Array( 'logged_in' => 1, "client_session" => $user[0] ) );

            $direction = $this->Checkout_model->CNS_DirectionClientData($user[0]["id_cliente"]);  
            $data["direccion"] = $direction;
			$data["us_states"] = $this->US_states;
			$this->load->view('store/checkout/forms', $data);
		}

		// not exit user	
		else {
			$this->session->set_userdata( Array( 'logged_in' => 0, "client_session" => null ) );
			return false;
		}
		
	}

	public function ContactForm()
	{

		$data = Array(
			'nombre' => $this->input->post('nombre'),
			'email'  => $this->input->post('email'),
			'comentario' => $this->input->post('comment')
		);
		var_dump($data);
		$data["destinatario"] = "contact@hgroupcustombbqspatio.com";
		$mensaje = $this->load->view('emails/ContactForm',$data, true);
		echo $mensaje;
		sendEmail("Contacto", $mensaje, $data["destinatario"]);
	}
	public function view_success_and_error(){
		
		$fech = date_create();
		$folio = $fech->format('ymdHi');
		$fecha = $fech->format('Y-m-d H:i:s');
		$cart = json_encode( $this->session->userdata( "cart" ));
		$montoImpuestos = number_format( $this->session->userdata('cart')["total_price"]*(8.375/100),2);
		
		
		// $data post ajax
		$data = Array(
			'id_cliente' => $this->session->userdata('client_session')['id_cliente'],
			'folio' => $folio,
			'cart' =>  $cart,
			'montoTotal' => $this->session->userdata('cart')["total_price"],
			'montoImpuestos' => $montoImpuestos,
			'referencia' => 1,
			'id_status' => 1,
			'id_fpago' => 1,
			'fecha_pago' => $fecha,
			'id_status_pago' => 1,
			
		);

			$this->Checkout_model->receipt_registered($data);
			
			// $data post ajax
		$data2 = Array(
			
			'nombre' => $this->session->userdata('client_session')['nombre'],
			'folio' => $folio,
			'cart' =>  $cart,
			'montoTotal' => $this->session->userdata('cart')["total_price"],
			'montoImpuestos' => $montoImpuestos,
			'referencia' => 1,
			'id_status' => 1,
			'id_fpago' => 1,
			'fecha_pago' => $fecha,
			'id_status_pago' => 1,
			'total' => $this->session->userdata('cart')["total_price"] + $montoImpuestos,
			"cart_session" => $this->session->userdata("cart"),
		);
			$mensaje = $this->load->view('emails/recibo_pago',$data2,true);

			$cart = $this->session->userdata( "cart" );
			unset( $cart["products"] );
			$cart["products"] = [];
			$cart["total_qty"] = 0;
			$cart["total_price"] = 0;
			$this->session->set_userdata( "cart", $cart );

			$data["cart_session"] = $this->session->userdata( "cart" );	

			// if correct insert of client
			if( $this->session->userdata('logged_in') ){
			$data = array(
				"destinatario" => $this->session->userdata('client_session')['email'],
				"asunto"       => "Detalles de Compra",
				
			);

			sendEmail("Ticket", $mensaje, $data["destinatario"]);
		}

	}
}
?>
