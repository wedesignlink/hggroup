<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model("Design_model");
    }

    // standardized response
    private function makeAnswer( $url, $method="GET", $id=null, $data=null )
    {
        return json_encode( Array(
            "url" => "/API/".$url."/".$id,
            "method" => $method,
            "message" => ( $data ) ? "Ok" : "Not Found",
            "data" => ( $data )  ? $data : null
        ) );
    }

    // get all shapes on db
    public function getManyShapes()
    {
        $res = $this->Design_model->getShapes();
        echo $this->makeAnswer("shapes","GET", null, $res);
    }
    // get one shape // require ID
    public function getOneShape()
    {
        $id_shape = $this->input->get('id');
        $res = $this->Design_model->getOneShape( $id_shape );

        echo $this->makeAnswer("shapes","GET",$id_shape, $res);
    }

    // get all islands from a shape
    public function getIslandsFromShape()
    {
        $id_shape = $this->input->get('id');
        $res = $this->Design_model->getIslandsFromShape( $id_shape );

        echo $this->makeAnswer("islands/shape","GET" ,$id_shape, $res);
    }
    public function getOneIsland()
    {
        $id_island = $this->input->get('id');
        $res = $this->Design_model->getOneIsland( $id_island );

        echo $this->makeAnswer("islands","GET",$id_island, $res);
    } 

    // get all sizes
    public function getManySizes()
    {
        $res = $this->Design_model->getSizes();
        echo $this->makeAnswer("sizes","GET",null, $res);
    }
    // get one size // require ID
    public function getOneSize()
    {
        $id_size = $this->input->get('id');
        $res = $this->Disign_model->getOneSize( $id_size );

        echo $this->makeAnswer("sizes","GET",$id_size, $res);
    }

    // get one accesorie by id
    public function getOneAccessory()
    {
        $id_accesories = $this->input->get('id');
        $res = $this->Design_model->getOneAccessory($id_accesories);

        echo $this->makeAnswer("accesories","GET",$id_accesories, $res);
    }
   
    //get all Accesories
    public function getManyAccessories()
    {
        $res = $this->Design_model->getAccessories();
        echo $this->makeAnswer("accesories","GET", null, $res);
    }

    // get all distributions // require ID_island and ID_size
    public function getDistributions()
    {
        $id_island = $this->input->get('island');
        $id_size = $this->input->get('size');

        $res = $this->Design_model->getDistributions( $id_island, $id_size );
        echo $this->makeAnswer( "distributions/island=".$id_island.",size=".$id_size, "GET", null, $res );
    }

    public function getOneFullDistribution()
    {
        $id_dist = $this->input->get('id');

        $dist = $this->Design_model->getOneDistribution( $id_dist )[0];
        $accesories = $this->Design_model->getAccesoriesFromDist( $id_dist );
        $size = $this->Design_model->getOneSize( $dist["id_tamaño"] );
        $island = $this->Design_model->getOneIsland( $dist["id_isla"] );

        $res = Array(
            "distribution" => $dist,
            "size" => $size[0],
            "island" => $island[0],
            "accesories" => $accesories
        );

        echo $this->makeAnswer( "distributions/full", "GET", $id_dist, $res );
    }

    // get one texture by id
    public function getOneTexture()
    {
        $id_accesories = $this->input->get('id');
        $res = $this->Design_model->getOneTexture($id_accesories);

        echo $this->makeAnswer("textures","GET",$id_accesories, $res);
    }
   
    //get all Texture
    public function getManyTextures()
    {
        $res = $this->Design_model->getTextures();
        echo $this->makeAnswer("textures","GET", null, $res);
    }

    // get one bar by id
    public function getOneBar()
    {
        $id_bar = $this->input->get('id');
        $res = $this->Design_model->getOneBar($id_bar);

        echo $this->makeAnswer("bars","GET",$id_bar, $res);
    }
   
    //get all bars
    public function getManyBars()
    {
        $res = $this->Design_model->getBars();
        echo $this->makeAnswer("bars","GET", null, $res);
    }

    // get one plan by id
    public function getOnePlan()
    {
        $id_plan = $this->input->get('id');
        $res = $this->Design_model->getOnePlan($id_plan);

        echo $this->makeAnswer("plans","GET",$id_plan, $res);
    }
   
    //get all plans
    public function getManyPlans()
    {
        $res = $this->Design_model->getPlans();
        echo $this->makeAnswer("plans","GET", null, $res);
    }

    // get one fuel by id
    public function getOneFuel()
    {
        $id_fuel = $this->input->get('id');
        $res = $this->Design_model->getOneFuel($id_fuel);

        echo $this->makeAnswer("fuels","GET",$id_fuel, $res);
    }
   
    //get all plans
    public function getManyFuels()
    {
        $res = $this->Design_model->getFuels();
        echo $this->makeAnswer("fuels","GET", null, $res);
    }

    // get all grills
    public function getManyGrills()
    {
        $res = $this->Design_model->getGrills();
        echo $this->makeAnswer("grills","GET", null, $res);
    }
    
    // get props from accessory
    public function getPropsFromAccesory()
    {
        $id_accessory = $this->input->get('id');
        $res = $this->Design_model->getPropsFromAccessory( $id_accessory );
        echo $this->makeAnswer("props","GET", null, $res);
    }

    // get all 3d models to render preview
    /* require data model
        model: {
            id_bar: number,
            id_finish: number,
            id_shape: number,
            islands: [
                {id_dist:number, id_products:[number,...]},...
            ]
        }
    */
    public function getRespectiveModels()
    {
        $model = $this->input->get('model');

        $shape = $this->Design_model->getOneShape($model["id_shape"])[0];
        $bar = $this->Design_model->getOneBar($model["id_bar"])[0];
        $finish = $this->Design_model->getOneTexture($model["id_finish"])[0];

        $islands = Array();
        foreach ($model["islands"] as $island) {
            $res = $this->Design_model->getProductsModel( $island["id_products"] );
            array_push($islands, $res);
        }

        $res = Array(
            "shape" => $shape["url_modelo"],
            "bar" => $bar["url"],
            "finish" => $finish["url"],
            "islands" => $islands
        );

        echo $this->makeAnswer("3Dmodel","GET",null, $res );
    }

    // post design model
    /* require data model
        model :{
            id_cliente: number,
            name: string,
            id_shape: number,
            id_finish: number,
            id_bar: number,
            id_plan: number,
            id_fuel: number,
            commentary: string,
            islands: [
                {id_dist:number, id_products:[number,...]}
            ]
        }
    */
    public function postDesign()
    {
        $model = $this->input->post('model');

        $res = $this->Design_model->postDesign( $model );
        echo $this->makeAnswer("design","POST",null, $res);
    }
}
?>