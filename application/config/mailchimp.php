<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//----------------------------------------------------------------------------
// Mailchimp API v3 REST Client
// ---------------------------------------------------------------------------
// Settings file
//
// @author    Stefan Ashwell
// @version   1.0
// @updated   14/03/2016
//----------------------------------------------------------------------------

/**
 * API Key
 *
 * Your API Key from your account
 */

$config['api_key']      = '428acd71a3367ed3c5bbf13e1bd24742-us7';

/**
 * API Endpoint
 *
 * Typically this can remain as the default https://<dc>.api.mailchimp.com/3.0/
 */

$config['api_endpoint'] = 'https://us7.api.mailchimp.com/3.0/';
