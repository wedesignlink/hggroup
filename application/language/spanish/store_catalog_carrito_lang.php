<?php
$lang["carrito"] = "MI CARRO";
$lang["description"] = "Descripción";
$lang["color"] = "Color";
$lang["texture"] = "Textura";
$lang["quantity"] = "Cantidad";
$lang["unitario"] = "Precio Unitario";
$lang["total"] = "Total";
$lang["clear"] = "Vaciar Carrito";
$lang["proceed"] = "Realizar pago";