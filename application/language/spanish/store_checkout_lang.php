<?php
$lang["subtotal"] = "Subtotal";
$lang["taxes"] = "Impuestos (estimación)";
$lang["total"] = "Total";
$lang["paypal"] = "PAGAR CON PAYPAL";
$lang["hello"] = "¡Hola! Para comprar,";
$lang["ingresa"] = "ingresa a tu cuenta";
$lang["i_have"] = "Tengo cuenta";
$lang["no_have"] = "No tengo cuenta";
$lang["inf_contact"] = "Información de contacto";
$lang["cancel"] = "Cancelar";
$lang["continua"] = "Continuar";
$lang["back"] = "Bienvenido de nuevo";
$lang["inf_account"] = "Información de cuenta";
$lang["carro"] = "Carro";
$lang["payment"] = "Pago";
$lang["subtotal"] = "Subtotal";
$lang["direcc"] = "Dirección";
$lang["agregar"] = "Agregar";
$lang["editar"] = "Editar";
$lang["continue_shop"] = "Continuar compra";