<?php
$lang["color_d"] = "Colores:";
$lang["textura_d"] = "Texturas:";
$lang["dimension"] = "Dimensiones:";
$lang["peso"] = "Peso:";
$lang["altura"] = "Altura:";
$lang["longitud"] = "Longitud";
$lang["ancho"] = "Ancho:";
$lang["area_cook"] = "Área de cocina:";
$lang["caracteristica"] = "Caracteristicas:";
$lang["sku"] = "Código:";
$lang["categoria"] = "Categoría:";
$lang["agregar_cart"] = "Agregar al carro";
$lang["internalDiam"] = "Diametro Interno";
$lang["externalDiam"] = "Diametro Externo"; 