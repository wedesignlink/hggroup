<?php
$lang["descripcion"] = "Revisa y renueva tus servicios, consulta tus pagos.";
$lang["estado"] = "Estado del servicio";
$lang["modifique"] = "Modifique el periodo de pago, cantidad de usuarios y/o módulos adicionales.";
$lang["cambiar"] = "Aumentar o Cambiar características";
$lang["pagar_recibo"] = "Pagar recibo pendiente";
$lang["mis_recibos"] = "Mis Recibos";
$lang["recibo"] = "Recibo";
$lang["forma_pago"] = "SELECCIONE UNA FORMA DE PAGO";
$lang["seleccione"] = "Seleccione una forma de pago";
$lang["pagar_con"] = "Pagar con";
$lang["PayPal"] = "PayPal";
$lang["Cdigo"] = "Código";
$lang["pagar_a"] = "Pagar a";
$lang["facturado"] = "Facturado a";
$lang["productos"] = "Productos/Servicios";
$lang["descripcion"] = "Descripción";
$lang["importe"] = "Importe";
$lang["subtotal"] = "Subtotal";
$lang["iva"] = "I.V.A.";
$lang["total"] = "Total";
$lang["imprimir"] = "Imprimir";
$lang["escriba"] = "Escriba su código";
$lang["pagar"] = "Pagar";
$lang["prueba"] = "Comenzar Prueba de 30 dias";
$lang["metodo"] = "Método de Pago";
$lang["fecha_pago"] = "Fecha de Pago";
$lang["id_trans"] = "ID Transacción";
$lang["sera_activado"] = "Su nuevo servicio será activado el";
$lang["revise_recibos"] = "Revise sus recibos el periodo de pago, cantidad de usuarios y/o módulos adicionales.";







