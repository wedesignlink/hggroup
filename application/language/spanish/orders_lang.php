<?php
$lang["selecciona"] = "Selecciona un paquete";
$lang["sel_descripcion"] = "Selecciona el grupo de módulos adecuados para tus necesidades y posteriormente podrás aumentar sus caractéristicas.";
$lang["iniciar"] = "Iniciar prueba de 30 dias";
$lang["mes"] = "Mes";
$lang["SistemaparaSeguridadBasadaenComportamiento"] = "Sistema para seguridad basada en comportamiento";
$lang["configurar"] = "Configurar";
$lang["configurar_des"] = "Selecciona y define la cantidad de usuarios y administradores que requieres para tu sistema contamos con paquetes de acuerdo a tus necesidades.";
$lang["paquete"] = "Paquete";
$lang["asunto_bienvenido"]="Bienvenid@ a Entrevistando mx";
$lang["asunto_recibo"]="Recibo de tu plan Entrevistando mx";
$lang["asunto_activado"]="Tu plan Entrevistando mx ha sido Activado";
$lang["asunto_activar"]="Tienes un Recibo proximo a activarse de tu plan Entrevistando mx";
$lang["asunto_pendiente"]="Tienes un Recibo pendiente de pago de tu plan Entrevistando mx";
$lang["asunto_vencido"]="Tu plan Entrevistando mx ha vencido";
$lang['gracias']="¡Muchas Gracias!";
$lang['hola']="Hola";
$lang['confirmacion_recibo']="Te escribimos para confirmarte que hemos recibido el pago de tu recibo.";
$lang['confirmacion_felicidades']="¡Felicidades! Ahora ya puedes hacer uso de tu plataforma.";
$lang['confirmacion_email']="Por favor confirme su correo electrónico haciendo click en la siguiente dirección:";
$lang['por_registrarse']="por registrarse en  Entrevistando MX";
$lang['click_enlace']="Por favor haga click en el enlace para activar su cuenta";
$lang['click_enlace_pass']="Por favor haga click en el enlace para establecer su contraseña";
$lang['click_aqui']="Click aquí para activar su cuenta.";
$lang['click_aqui_email']="Click aquí para confirmar tu email.";
$lang['click_aqui_pass']="Click aquí para establecer su contraseña.";
$lang['info_recibo']="Información sobre tu recibo:";
$lang['folio']="Folio";
$lang['fecha_inicio']="Fecha de inicio";
$lang['fecha_vencimiento']="Fecha de vencimiento";
$lang['servicios']="Servicios";
$lang['subtotal']="Sub Total";
$lang['iva']="IVA";
$lang['total']="Total";
$lang['ingresa_email']="¡Ingresa ahora! con tu email y password.";
$lang['pagar']="Realizar mi pago";
$lang['recibo']="Tu recibo";
$lang['recibo_generado']="Hemos generado tu recibo correspondiente a este periodo. Para ver tus recibos ingresa a Pagos o sigue el vínculo. <br>Para continuar disfrutando de los beneficios de tu plataforma realiza el pago en los 5 días posteriores.";
$lang['recibo_activado']="Ha sido activado correctamente, no te preocupes este proceso se realiza automáticamente, este es sólo un correo informativo.";
$lang['recibo_activar']="Ya ha sido pagado y en 5 dias será activado, no te preocupes este proceso se realiza automáticamente, este es sólo un correo informativo.";
$lang['recibo_pendiente']="está pendiente de pago y está próximo a vencer (5 dias), realiza tu pago ahora en la pesataña de pagos de tu sistema y continúa disfrutando de los beneficios de Entrevistando mx.";
$lang['regreso_recibo']="¡Te queremos de regreso en Entrevistando mx!";
$lang['recibo_vencido']="Desafortunadamente ha vencido, te invitamos a continuar disfrutando de los beneficios de tu sistema,  realiza tu pago ahora en la pesataña de pagos de tu sistema y continúa disfrutando de Entrevistando mx.";
// --------------- configurar -------------------
$lang['usuarios']="Usuarios";
$lang['bsico']="Básico";
$lang['medio']="Medio";
$lang['avanzado']="Avanzado";
$lang['full']="Full";
$lang['personalizado']="Personalizado";
$lang['prueba30das']="Prueba 30 días";
$lang['licencias_admin']="Número de licencias de administrador requeridas";
$lang['licencias_user']="Número de Usuarios requeridos";
$lang['periodo_pago']="Periodo de facturación";
$lang['mensual']="Mensual";
$lang['trimestral']="Trimestral";
$lang['semestral']="Semestral";
$lang['anual']="Anual";
$lang['paga_once']="Paga sólo 11 Meses";
$lang['precios_iva']="Precios mas I.V.A.";
$lang['precio_mensual']="Precio Mensual";
$lang['periodo']="Periodo";
$lang['continuar']="Continuar";
$lang['pagar_orden']="Pagar mi orden";
// -------------- confirmar y registro --------
$lang['confirma_orden']="Confirma tu orden";
$lang['confirma_descript']="LLena el formulario con tu información para confirmar y poder realizar tu compra";
$lang['personal_info']="Información personal";
$lang['contrasena']="Contraseña";
$lang['contrasena_confirm']="Confirmar Contraseña";
$lang['company_info']="Información de la empresa";
$lang['colonia']="Colonia";
$lang['municipio']="Ciudad/Municipio";
$lang['estado']="Estado";
$lang['cp']="Código Postal";
$lang['acepto']="Acepto el";
$lang['aviso_privacidad']="Aviso de privacidad";
$lang['politicas_venta']="Políticas de venta";
$lang['registro']="Registrarme";
$lang['cancelar']="Cancelar orden";
$lang['guardar']="Guardar mi orden";
$lang['bienvenido']="Bienvenido";
$lang['orden_lista_titulo']="Su orden esta lista";
$lang['mensaje_bienvenido']="Has sido registrado correctamente para activar tu cuenta completa tu orden y confirma tu correo electrónico";
$lang['orden_lista']="Su orden esta lista para pagar, puede realizar el pago en este momento o guardarla para realizar el pago despues.";
$lang['orden_lista_cancelada']="Le recordamos que la fecha de pago no debe ser mayor a 5 días posterior al vencimiento de su orden actual, de lo contrario esta será cancelada.";
// ------------------recibo ------------------
$lang["recibo"] = "Recibo";
$lang["forma_pago"] = "SELECCIONE UNA FORMA DE PAGO";
$lang["seleccione"] = "Seleccione una forma de pago";
$lang["pagar_con"] = "Pagar con";
$lang["PayPal"] = "PayPal";
$lang["Cdigo"] = "Código";
$lang["pagar_a"] = "Pagar a";
$lang["facturado"] = "Facturado a";
$lang["productos"] = "Productos/Servicios";
$lang["descripcion"] = "Descripción";
$lang["importe"] = "Importe";
$lang["subtotal"] = "Subtotal";
$lang["iva"] = "I.V.A.";
$lang["total"] = "Total";
$lang["imprimir"] = "Imprimir";
$lang["escriba"] = "Escriba su código";
$lang["pagar"] = "Pagar";
$lang["prueba"] = "Comenzar Prueba de 30 dias";
$lang["metodo"] = "Método de Pago";
$lang["fecha_pago"] = "Fecha de Pago";
$lang["id_trans"] = "ID Transacción";
$lang["entrar_speed"] = "Entrar a Entrevistando mx";
$lang["dias"] = "días";
$lang["gracias_compra"] = "Gracias por su compra";
$lang["confirmacion_email"] = "Se ha confirmado su correo electrónico, muchas gracias :)";
$lang["confirmacion_email_error"] = "error en la confirmación por favor intentelo de nuevo";
$lang["confirmacion_email_error_compra"] = "Para confirmar su cuenta debe concluir el proceso de compra.";









