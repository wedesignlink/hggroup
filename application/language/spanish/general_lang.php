<?php

$lang["entrar"] = "Entrar";
$lang["enviar"] = "Enviar";
$lang["ver_perfil"] = "Ver perfil";
$lang["mi_perfil"] = "Mi perfil";
$lang["mis_recibos"] = "Mis Recibos";
$lang["configurar"] = "Configurar";
$lang["salir"] = "Salir";
$lang["secciones"] = "Secciones";
$lang["rol_liderazgo"] = "Rol de liderazgo";
$lang["mes"] = "Mes";
// -----------------MENU-------------
$lang["usuariosadmin"] = "Usuarios";
$lang["transacciones"] = "Transacciones";
$lang["reportes"] = "Reportes";
$lang["recibos"] = "Recibos";
$lang["empresas"] = "Empresas";
$lang["generales"] = "GENERALES";
$lang["modulos"] = "MÓDULOS";
$lang["usuarios"] = "Usuarios";
$lang["proyectos"] = "Proyectos";
$lang["clientes"] = "Clientes";
$lang["cliente"] = "Cliente";
$lang["incentivos"] = "Incentivos";
$lang["pagos"] = "Pagos";
$lang["pedidos"] = "Pedidos";
$lang["segmentacion"] = "Segmentacion";
$lang["cargapuntos"] = "Cargar puntos";
$lang["invitaciones"] = "Invitaciones";
$lang["perfil"] = "Perfil";
$lang["evaluaciones"] = "Formularios";
$lang["observaciones"] = "Formularios";
$lang["observacion"] = "Observación";
$lang["curso"] = "Curso";
$lang["departamentos"] = "Departamentos";
$lang["e-learning"] = "E-learning";
$lang["bitacora"] = "Registro acciones";
$lang["centros"] = "Centros";
$lang["codigos"] = "Códigos";
$lang["dashboard"] = "Dashboard";
$lang["settings"] = "Settings";
$lang["formularios"] = "Formularios";
// ------------ Top bar -------------
$lang["dudas"] = "Dudas o preguntas";
$lang["ayuda"] = "Ayuda";
$lang["help_center"] = "Ingresa a Entrevistando MX Help Center";
// -----------------Funciones-------------
$lang["agregar"] = "Agregar";
$lang["editar"] = "Editar";
$lang["activar"] = "Activar";
$lang["desactivar"] = "Desactivar";
$lang["admins"] = "Administradores";
$lang["usuarios"] = "Usuarios";
$lang["admin"] = "Admin";
$lang["activo"] = "Activo";
$lang["inactivo"] = "Inactivo";
$lang["activa"] = "Activa";
$lang["inactiva"] = "Inactiva";
$lang["responder"] = "Responder";
$lang["guardar"] = "Guardar";
$lang["borrar"] = "Borrar";
$lang["eliminar"] = "Eliminar";
$lang["regresar"] = "Regresar";
$lang["actualizar"] = "Actualizar";
$lang["seleccione"] = "Seleccione";
// -----------------Roles-------------
$lang["administrador"] = "Administrador";
$lang["observador"] = "Observador";
$lang["superadmin"] = "SuperAdmin";
$lang["manager"] = "Manager";
// -----------------Conceptos-------------
$lang["centro_observado"] = "Centro observado";
$lang["departamento_observado"] = "Departamento observado";
$lang["fecha_observacion"] = "Fecha de observación";
// -----------------status-------------
$lang["Activo"]="Activo";
$lang["SinConfirmar"]="Sin Confirmar";
$lang["Suspendido"]="Suspendido";
$lang["Pendiente"]="Pendiente";
$lang["Pagado"]="Pagado";
$lang["Vencido"]="Vencido";
$lang["Cancelado"]="Cancelado";
$lang["Inactivo"]="Inactivo";
$lang["Terminado"]="Terminado";
$lang["EnEspera"]="En Espera";
$lang["Aceptado"]="Aceptado";
$lang["Rechazado"]="Rechazado";
$lang["Contestado"]="Contestado";
// -----------------descripciones-------------
$lang["tree_des"] = "Crea, organiza y ordena tu arbol de datos";
// -----------------acciones-------------
$lang["cerrado"] = "Cerrado";
$lang["seleccione"] = "Seleccione";
$lang["respuestas"] = "Respuestas";
$lang["observaciones_bitacora"] = "Observaciones";
$lang["asignar"] = "Asignar administrador";
$lang["escriba_observacion"] = "Definir acción";
// -----------------login-------------
$lang["usuario_no_existe"] ="El usuario no existe en la base de datos";
$lang["contrasena_incorrecta_or"] = "Contraseña incorrecta o email sin confirmar";
$lang["contrasena_incorrecta"] = "Contraseña incorrecta!";
$lang["invitaciones"] = "Enviar Invitaciones";
$lang["pedidos"] = "Pedidos";
$lang["cargapuntos"] = "Carga de Puntos";


