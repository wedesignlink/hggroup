<?php
$lang["titulo_bienvenida"] = "LISTOS PARA LA CARNE ASADA";
$lang["subtitulo_bienvenida"] = "Espacios prÁcticos y funcionales para disfrutar";
$lang["design_bbq"] = "DISEÑAR MI BARBECUE";
$lang["design_style"] = "Diseña a tu estilo tu Barbecue ahora!";
$lang["descriptionBBQ"] = "Somos un grupo especialista en el diseño de espacios prácticos, decorativos y funcionales para que pases los mejores momentos, da vida a tu patio con nuestras soluciones, diseña ahora tu espacio ideal";
$lang["parrillerosprofesionales"] = "Espacios para parrilleros profesionales";
$lang["start"] = "Inicia ya!";
$lang["design_grill"] = "DISEÑAR MI GRILL";
$lang["design_grill_text"] = "Diseña ahora tu parrilla ";
$lang["contact"] = "CONTÁCTANOS";
$lang["contact_text"] = "Tienes una duda? podemos ayudarte, comunícate con nosotros y te ayudaremos a diseñar tu patio con un estilo único.";
$lang["sales"] = "VENTAS";
$lang["call"] = "LLÁMANOS HOY!";
$lang["btn_msj"] = "ENVIAR MENSAJE";
$lang["txt_email"] = "* Tu email";
$lang["txt_name"] = "* Tu nombre";
$lang["txt_msj"] = "* Tu mensaje";