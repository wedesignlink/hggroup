<?php
$lang["titulo_bienvenida"] = "READY FOR BBQ";
$lang["subtitulo_bienvenida"] = "Useful and funcional areas to enjoy";
$lang["design_bbq"] = "DESIGN MY BBQ";
$lang["design_style"] = "Design on your style a BBQ now!";
$lang["descriptionBBQ"] = "We are an specialist group on design practical, decorative and functional areas so you can spend the best moments. Give live to your Patio with our solutions. Design now your ideal area.";
$lang["parrillerosprofesionales"] = "Spaces for professional barbecues";
$lang["start"] = "Start now!";
$lang["design_grill"] = "DESIGN MY GRILL";
$lang["design_grill_text"] = "Design now your grill";
$lang["contact"] = "CONTACT US";
$lang["contact_text"] = "Do you have any doubt? We can help you. Contact us and we will help you to design your patio with a unique style:";
$lang["sales"] = "SALES";
$lang["call"] = "CALL US TODAY!";
$lang["btn_msj"] = "SEND A MESSAGE";
$lang["txt_email"] = "* Your Email";
$lang["txt_name"] = "* Your name";
$lang["txt_msj"] = "* Your message";