<?php
$lang["alimneto_mascota"] = "Alimento para mascota";
$lang["vinos_licores"] = "Vinos y licores";
$lang["cervezas"] = "Cervezas";
$lang["dulces_botanas"] = "Dulces y botanas";
$lang["cigarros"] = "Cigarros";
$lang["cigarros_electronicos"] = "Cigarros electrónicos";
$lang["bancos_instituciones_financieras"] = "Bancos e instituciones financieras";
$lang["restaurantes_hoteles"] = "Restaurantes / hoteles";
$lang["tiendas_departamentales_autoservicio"] = "Tiendas departamentales / autoservicio";
$lang["panales_formulas"] = "Pañales / fórmulas";
$lang["refrescos"] = "Refrescos";
$lang["leche"] = "Leche";
$lang["jugos"] = "Jugos";
$lang["cereales"] = "Cereales";
$lang["bebidas_energeticas"] = "Bebidas energéticas";
$lang["vitaminas_suplementos"] = "Vitaminas / Suplementos";