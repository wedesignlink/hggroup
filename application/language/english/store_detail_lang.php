<?php
$lang["color_d"] = "Colors:";
$lang["textura_d"] = "Textures:";
$lang["dimension"] = "Dimensions:";
$lang["peso"] = "Weight:";
$lang["altura"] = "Height:";
$lang["longitud"] = "Length";
$lang["ancho"] = "Depth:";
$lang["area_cook"] = "Cooking Area:";
$lang["caracteristica"] = "Features:";
$lang["sku"] = "SKU:";
$lang["categoria"] = "Category:";
$lang["agregar_cart"] = "Add to cart";
$lang["internalDiam"] = "Internal Diameter";
$lang["externalDiam"] = "External Diameter";