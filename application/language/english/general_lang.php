<?php
// ------------generales -------------
$lang["entrar"] = "Login";
$lang["enviar"] = "Send";
$lang["ver_perfil"] = "Profile";
$lang["mi_perfil"] = "My Profile";
$lang["mis_recibos"] = "Invoices";
$lang["configurar"] = "Settings";
$lang["salir"] = "Logout";
$lang["secciones"] = "Sections";
$lang["rol_liderazgo"] = "Leadership role";
$lang["mes"] = "Month";
// -----------------MENU-------------
$lang["usuariosadmin"] = "Users";
$lang["transacciones"] = "Transactions";
$lang["reportes"] = "Reports";
$lang["recibos"] = "Receipts";
$lang["empresas"] = "Companies";
$lang["generales"] = "GENERAL";
$lang["modulos"] = "MODULES";
$lang["usuarios"] = "Users";
$lang["proyectos"] = "Projects";
$lang["clientes"] = "Customers";
$lang["cliente"] = "Customer";
$lang["incentivos"] = "Gifts";
$lang["pagos"] = "Invoices";
$lang["pedidos"] = "Pedidos";
$lang["segmentacion"] = "Segmentacion";
$lang["cargapuntos"] = "Cargar puntos";
$lang["invitaciones"] = "Invitaciones";
$lang["perfil"] = "profile";
$lang["evaluaciones"] = "Forms";
$lang["departamentos"] = "Departments";
$lang["observaciones"] = "Form configuration";
$lang["observacion"] = "Observation";
$lang["curso"] = "Training";
$lang["e-learning"] = "E-learning";
$lang["bitacora"] = "Actions";
$lang["centros"] = "Site";
$lang["codes"] = "Codes";
$lang["dashboard"] = "dashboard";
$lang["settings"] = "Settings";
$lang["formularios"] = "Forms";
// ------------ Top bar -------------
$lang["dudas"] = "Questions";
$lang["ayuda"] = "Help";
$lang["help_center"] = "Go to Entrevistando MX Help Center";
// -----------------Funciones-------------
$lang["agregar"] = "Add";
$lang["editar"] = "Edit";
$lang["activar"] = "Activate";
$lang["desactivar"] = "Deactivate";
$lang["admins"] = "Admins";
$lang["usuarios"] = "Users";
$lang["admin"] = "Admin";
$lang["activo"] = "Active";
$lang["inactivo"] = "Inactive";
$lang["activa"] = "Active";
$lang["inactiva"] = "Inactive";
$lang["responder"] = "Answer";
$lang["guardar"] = "Submit";
$lang["borrar"] = "Delete";
$lang["eliminar"] = "Delete";
$lang["regresar"] = "Back";
$lang["actualizar"] = "Update";
$lang["seleccione"] = "Select";
// -----------------roles-------------
$lang["administrador"] = "Administrator";
$lang["observador"] = "Observer";
$lang["superadmin"] = "SuperAdmin";
$lang["manager"] = "Manager";
// -----------------Conceptos-------------
$lang["centro_observado"] = "Observed Site";
$lang["departamento_observado"] = "Observed department";
$lang["fecha_observacion"] = "Date";
// -----------------status-------------
$lang["Activo"]="Active";
$lang["SinConfirmar"]="Unconfirmed";
$lang["Suspendido"]="Suspend";
$lang["Pendiente"]="Pending";
$lang["Pagado"]="Paid";
$lang["Vencido"]="Due";
$lang["Cancelado"]="Canceled";
$lang["Inactivo"]="Inactive";
$lang["Terminado"]="Terminated";
$lang["EnEspera"]="Waiting";
$lang["Aceptado"]="Acepted";
$lang["Rechazado"]="Rejected";
$lang["Contestado"]="Answered";
// -----------------descripciones-------------
$lang["tree_des"] = "Create Data tree";
// -----------------acciones-------------
$lang["cerrado"] = "Closed";
$lang["seleccione"] = "Select";
$lang["respuestas"] = "Answers";
$lang["observaciones_bitacora"] = "Observations";
$lang["asignar"] = "Admin asign";
$lang["escriba_observacion"] = "Add an action";
// -----------------login-------------
$lang["usuario_no_existe"] = "This user does not exist in our database";
$lang["contrasena_incorrecta_or"] = "Wrong password or account without confirmation";
$lang["contrasena_incorrecta"] = "Wrong password!";
$lang["invitaciones"] = "Send Invitation";
$lang["pedidos"] = "Orders";
$lang["cargapuntos"] = "Upload Data Points";



