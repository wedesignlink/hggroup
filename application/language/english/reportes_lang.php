<?php

$lang['cuantos'] = 'Qty';
$lang['nombre'] = 'name';
$lang['depto_observado'] = 'Department observed';
$lang['titulo'] = 'Form';
$lang['respuesta'] = 'Aswers';
$lang['label'] = 'Questions';
$lang['id_pregunta'] = 'id_question';
$lang['id_respuesta_master'] = 'id_master';
$lang['seguras'] = 'Safe';
$lang['Seguro'] = 'Safe';
$lang['con_oportunidad'] = 'Opportunnity';
$lang['porcentaje'] = 'Percent';
$lang['semana1'] = 'Week 1';
$lang['semana2'] = 'Week 2';
$lang['semana3'] = 'Week 3';
$lang['semana4'] = 'Week 4';
$lang['nombre_departamento'] = 'Department';
$lang['id_user'] = 'id_user';
$lang['id_depto'] = 'id_dep';
$lang['Enero'] = 'Jan';
$lang['Febrero'] = 'Feb';
$lang['Marzo'] = 'Mar';
$lang['Abril'] = 'Apr';
$lang['Mayo'] = 'May';
$lang['Junio'] = 'Jun';
$lang['Julio'] = 'Jul';
$lang['Agosto'] = 'Aug';
$lang['Septiembre'] = 'Sep';
$lang['Octubre'] = 'Oct';
$lang['Noviembre'] = 'Nov';
$lang['Diciembre'] = 'Dic';
$lang['usuario'] = 'User';
$lang['id_usuario'] = 'id_user';
