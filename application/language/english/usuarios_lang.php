<?php
$lang["gestiona"] = "User Management Administration";
$lang["gestiona_des"] = "Creating, assigning users, user management.";
$lang["url_registro"] = "Sharing register Url";
$lang["observadores"] = "Observers";
$lang["departamento"] = "Deparment";
$lang["rol_usuario"] = "User role";
$lang["rol_liderazgo"] = "Leadership role";
$lang["centro"] = "Site";
$lang["nombres"] = "Name";
$lang["apellidos"] = "Last Name";
$lang["rol"] = "Role";
$lang["seleccione"] = "Select an option";