<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
                <!-- Tell the browser to be responsive to screen width -->
                <meta content="width=device-width, initial-scale=1" name="viewport">
                    <meta content="" name="description">
                        <meta content="" name="author">
                            <!-- Favicon icon -->
                            <section class="no-padding cover-background tz-builder-bg-image" data-img-size="(W)1920px X (H)750px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?=base_url()?>public/images/theme/uploads/bbq.jpg');" id="ui-id-11">
                            <link href="<?=base_url()?>public/assets/images/favicon_logo-negro.png" rel="icon" sizes="16x16" type="image/png">
                                <title>
                                HGROUP & BBQ
                                </title>
                                <!-- Bootstrap Core CSS -->
                                <link href="<?=base_url()?>public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
                                    <!-- Custom CSS -->
                                    <link href="<?=base_url()?>public/css/style.css" rel="stylesheet">
                                        <!-- You can change the theme colors from here -->
                                        <link href="<?=base_url()?>public/css/colors/blue.css" id="theme" rel="stylesheet">
                                            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                                            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                                            <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
                                        </link>
                                    </link>
                                </link>
                            </link>
                        </meta>
                    </meta>
                </meta>
            </meta>
        </meta>
         <link href="<?=base_url()?>public/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet">
         <script>
            var lang = "<?=$this->session->userdata( 'site_lang' );?>";
         </script>
         <?php $this->lang->load( 'login', $this->session->userdata( 'site_lang' ) );?>
    </head>
    <body>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewbox="25 25 50 50">
                <circle class="path" cx="50" cy="50" fill="none" r="20" stroke-miterlimit="10" stroke-width="2">
                </circle>
            </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <section class="login-register login-sidebar" id="wrapper" style="background-image:url(<?=base_url()?>public/images/theme/uploads/bbq.jpg);">
            <div class="login-box card" style="background-color: rgba(0,0,0,.3);">
                <div class="card-body" style="padding-top: 23%;">
                    <?php if ( $this->session->flashdata( 'Noregistrado' ) ) {?>
                        <div class="text-center alert alert-danger">
                            <?=$this->session->flashdata( 'Noregistrado' );?>
                        </div>
                    <?php }?>

                    <?php if ( $this->session->flashdata( 'Reseteo' ) ) {?>
                        <div class="text-center alert alert-success">
                            <?=$this->session->flashdata( 'Reseteo' );?>
                        </div>
                    <?php }?>
                    <form action="<?=base_url()?>index.php/Dashboard/login" class="form-horizontal form-material" id="loginform" method="post">
                        <a class="text-center db" href="javascript:void(0)">
                            <img alt="Home" style="height: 120px; width: 120px !important;" src="<?=base_url()?>public/images/theme/uploads/logo_blanco.png"/>
                            <br/>
                            <!-- <img alt="Home" style="transform: rotate(269deg); width: 60px; height: 90px; margin:0; position: relative; bottom: 30px;" src="<?=base_url()?>public/assets/images/logo-text.png"/> -->
                        </a>
                        <div class="form-group m-t-40">
                            <div class="col-xs-12">
                                <input name="email" style="color: white;" class="form-control" placeholder="<?=( $this->lang->line( 'placeholder_email' ) );?>" type="email" required>
                                </input>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" style="color: white;" name="pass" placeholder="<?=( $this->lang->line( 'placeholder_pwd' ) );?>" required="" type="password">
                                </input>
                                <?php if ( $this->session->flashdata( 'error' ) ) {?>
                                    <div style="color: #C20404;">
                                        <?=$this->session->flashdata( 'error' )?>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">
                                    <?=$this->lang->line( 'entrar' );?>
                                </button>
                                <a style="font-size: 14px;" href="<?=base_url() . "index.php/Store/Register"?>"><?=$this->lang->line( 'no_tiene_cuenta' );?></a>

                                <button id="btnolvide" type="button" class="btn btn-link"><?=$this->lang->line( 'olvido' );?></button>
                            </div>
                        </div>
                        <div class="row" style="display: none;">
                            <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                                <div class="social">
                                    <a class="btn btn-facebook" data-toggle="tooltip" href="#" onclick="loginFB();" title="Login with Facebook">
                                        <i aria-hidden="true" class="fa fa-facebook">
                                        </i>
                                    </a>
                                    <a class="btn btn-googleplus" data-toggle="tooltip" href="javascript:void(0)" title="Login with Google">
                                        <i aria-hidden="true" class="fa fa-google-plus">
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                              <!-- <p>
                                    <? /*$this->lang->line('no_tiene_cuenta'); */?>
                                    <a class="text-light m-l-5" href="<?/*= base_url() */?>index.php/Login/register">
                                    <a class="text-light m-l-5" href="<?/*= base_url() */?>index.php/Ordenes/selectProducts">
                                        <b>
                                           <?/*= $this->lang->line('Registrate_ahora'); */?>
                                        </b>
                                    </a>
                                </p> -->
                            </div>
                        </div>
                    </form>
                    <br><br>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <a href="<?=base_url()?>index.php/LangSwitch/switchLanguage/english" class="btn btn-default">English</a> / <a href="<?=base_url()?>index.php/LangSwitch/switchLanguage/spanish" class="btn btn-default">Español</a>
                        </div>
                    </div>

                    <form action="<?=base_url()?>index.php/Dashboard/Recovery" class="form-horizontal" id="recoverform" method="POST">
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <h3>
                                    <?=$this->lang->line( 'recuperar' );?>
                                </h3>
                                <p class="text-muted">
                                    <?=$this->lang->line( 'recuperar_descrip' );?>
                                </p>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" name="emailr" placeholder="Email" required type="email">
                                </input>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">
                                    <?=$this->lang->line( 'enviar' );?>
                                </button>
                            </div>
                        </div>
                        <a class="text-light pull-right" href="javascript:void(0)" id="close_form">
                                    close
                                </a>
                    </form>
                    </input>
                </div>
            </div>
        </section>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <script src="<?=base_url()?>public/assets/plugins/jquery/jquery.min.js">
        </script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="<?=base_url()?>public/assets/plugins/bootstrap/js/popper.min.js">
        </script>
        <script src="<?=base_url()?>public/assets/plugins/bootstrap/js/bootstrap.min.js">
        </script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="<?=base_url()?>public/js/jquery.slimscroll.js">
        </script>
        <!--Wave Effects -->
        <script src="<?=base_url()?>public/js/waves.js">
        </script>
        <!--Menu sidebar -->
        <script src="<?=base_url()?>public/js/sidebarmenu.js">
        </script>
        <!--stickey kit -->
        <script src="<?=base_url()?>public/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js">
        </script>
        <script src="<?=base_url()?>public/assets/plugins/sparkline/jquery.sparkline.min.js">
        </script>
        <!--Custom JavaScript -->
        <script src="<?=base_url()?>public/js/custom.js">
        </script>

        <!-- ============================================================== -->
        <!-- Style switcher -->
        <!-- ============================================================== -->
        <script src="<?=base_url()?>public/assets/plugins/styleswitcher/jQuery.style.switcher.js">
        </script>
        <script type="text/javascript" src="<?=base_url()?>public/js/configurate/fb_init.js"></script>
        <script type="text/javascript" src="<?=base_url()?>public/js/fbconnect.js"></script>
        <script src="<?=base_url()?>public/assets/plugins/sweetalert/sweetalert.min.js"></script>
        <?php if ( $this->session->flashdata( 'mensaje' ) != "" ) {?>
         <script>
            swal({
                title: "Ok!",
                text: "<?=$this->session->flashdata( 'mensaje' )?>",
                type: "success"
            });
        </script>
        <?php }?>
        <?php if ( $this->session->flashdata( 'mensajeError' ) != "" ) {?>
         <script>
            swal({
                title: "Error!",
                text: "<?=$this->session->flashdata( 'mensajeError' )?>",
                type: "warning"
            });
        </script>
        <?php //$this->session->sess_destroy();
}?>

        <script>
            $(document).on("click", "#btnolvide", function(){
                $("#recoverform").toggle();
                $("#loginform").toggle();
            });
        </script>
    </body>
</html>
