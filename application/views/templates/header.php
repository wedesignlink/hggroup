<!-- 
<?php 
    $config = $_SESSION['user']['config'];
    if($config!=""){
        $color = $_SESSION['user']['config']->color_primario;
    }else{
        $color = 'default';
    }

 ?> -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>public/assets/images/favicon.png">
    <title>Entrevistando MX your business performance</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url() ?>public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- chartist CSS -->
    <link href="<?= base_url() ?>public/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">

    <link href="<?= base_url() ?>public/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />

    <link href="<?= base_url() ?>public/assets/plugins/css-chart/css-chart.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="<?= base_url() ?>public/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url() ?>public/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <!-- <link href="<?= base_url() ?>public/css/colors/blue.css" id="theme" rel="stylesheet"> -->

    <link href="<?= base_url() ?>public/css/colors/<?= $color ?>.css" id="theme" rel="stylesheet">
    <script src="<?= base_url() ?>public/assets/plugins/jquery/jquery.min.js"></script>
    <!-- This is data table -->
    <script src="<?= base_url() ?>public/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <link href="<?= base_url() ?>public/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <script src="<?= base_url() ?>public/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <link href="<?= base_url() ?>public/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?= base_url() ?>public/assets/plugins/select2/dist/js/select2.full.min.js"></script>
     <!-- chartist chart -->
    <script src="<?= base_url() ?>public/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>

    <!--  ---- toast------ -->
    <link href="<?= base_url() ?>public/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <script src="<?= base_url() ?>public/assets/plugins/toast-master/js/jquery.toast.js"></script>
    <!-- -------  -->

     <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= base_url() ?>public/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    


    <script src="<?= base_url() ?>public/assets/plugins/jquery.easy-pie-chart/dist/jquery.easypiechart.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/jquery.easy-pie-chart/easy-pie-chart.init.js"></script>
    
    <!-- <script src="<?= base_url() ?>public/assets/plugins/chartist-js/dist/chartist-init.js"></script> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
    var base_url = "<?= base_url() ?>";
    var idEdit = "";
    var lang = "<?= $this->session->userdata('site_lang'); ?>";
    // alert(lang);
</script>
</head>
<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader" style="background: #00000075;">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">