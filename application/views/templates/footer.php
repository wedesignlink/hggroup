</div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
   
    
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= base_url() ?>public/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?= base_url() ?>public/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= base_url() ?>public/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?= base_url() ?>public/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/select2/dist/js/select2.full.min.js" ></script>
    <script src="<?= base_url() ?>public/assets/plugins/echarts/echarts-all.js" ></script>
    
    <!--Custom JavaScript -->
    <script src="<?= base_url() ?>public/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    <!--c3 JavaScript -->
    <script src="<?= base_url() ?>public/assets/plugins/d3/d3.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/c3-master/c3.min.js"></script>
    <!-- Chart JS -->
    <!-- <script src="<?= base_url() ?>public/js/dashboard1.js"></script> -->

    <!-- ----Nuestra libreria---- -->
    <script src="<?= base_url() ?>public/js/dev/controles.js"></script>
    <script src="<?= base_url() ?>public/js/dev/orders.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?= base_url() ?>public/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    
</body>

</html>