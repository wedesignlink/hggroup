<?php //print_r($menu) 
$modulos = $user['modulos'];
// print_r($modulos);
?>
<!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile" style="background: url(<?= base_url() ?>public/assets/images/background/coral_bk.png) no-repeat; background-size: cover;">
                    <!-- User profile image -->
                    
                    <?php if($user['photo']==""){ ?>
                    <div class="profile-img"> 
                        <img src="<?= base_url() ?>public/assets/images/users/default-user.jpg" alt="user" />
                    </div>
                    <?php }else{ ?>
                    <div class="profile-img"> 
                        <img src="<?= base_url() ?>public/uploads/img/<?= $user['photo'].'?x='.md5(time()); ?>" alt="user" />
                    </div>
                    <?php } ?>
                    <!-- User profile text-->
                    <div class="profile-text" style="padding:0;"> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><?= $user['nombres']; ?> <br> <small><?= $this->lang->line(strtolower($user['rol_nombre'])) ?></small></a>
                        <div class="dropdown-menu animated flipInY"> <a href="<?= base_url() ?>index.php/Modulos/?nombre=Perfil&id=2&tipo=3" class="dropdown-item"><i class="ti-user"></i> <?= $this->lang->line('ver_perfil'); ?></a> 
                        <?php if($user['rol']==1){ ?>
                            <a href="<?= base_url() ?>index.php/Pagos" class="dropdown-item"><i class="ti-wallet"></i> <?= $this->lang->line('mis_recibos'); ?></a>
                            <div class="dropdown-divider"></div>
                            <?php if(!isset($_SESSION['user']['vencido'])){ ?> 
                            <a href="<?= base_url() ?>index.php/Configurar" class="dropdown-item"><i class="ti-settings"></i> <?= $this->lang->line('configurar'); ?></a>
                            <?php } ?>
                        <?php } ?>
                            <div class="dropdown-divider"></div> <a href="<?= base_url() ?>index.php/Dashboard/Salir" class="dropdown-item"><i class="fa fa-power-off"></i> <?= $this->lang->line('salir'); ?></a> </div>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap"><?= $this->lang->line('generales'); ?></li>
                        <li> <a class="waves-effect waves-dark" href="<?= base_url() ?>index.php/Dashboard/Control" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                            <!-- <ul aria-expanded="false" class="collapse">
                                <li><a href="index.html">Dashboard 1</a></li>
                                <li><a href="index2.html">Dashboard 2</a></li>
                                <li><a href="index3.html">Dashboard 3</a></li>
                                <li><a href="index4.html">Dashboard 4</a></li>
                                <li><a href="index5.html">Dashboard 5</a></li>
                                <li><a href="index6.html">Dashboard 6</a></li>
                            </ul> -->
                        </li>
                        <?php if($menu !=""){ ?>
                        <?php foreach ($menu as $key) { 
                                    if($key['nivel'] == 0 && $key['mostrar']!=0 ){

                                        if(!isset($_SESSION['user']['vencido'])){
                                            $ruta = base_url()."index.php/".ucfirst(strtolower(str_replace(" ", "_", str_replace("-","",$key['nombre_seccion']))));
                                            // echo strtolower(clean($key['nombre_seccion']));
                                        }else{

                                            if($key['nombre_seccion']=='PAGOS'){
                                                $ruta = base_url()."index.php/".ucfirst(strtolower(str_replace(" ", "_", $key['nombre_seccion'])));
                                            }else{
                                                $ruta = "#";
                                            }
                                            
                                        }
                                ?>
                        <li> 
                            
                            <a class="waves-effect waves-dark" href="<?= $ruta; ?>" aria-expanded="false" >
                                <i class="mdi <?= $key['icon'] ?>" ></i>
                                <span class="hide-menu"><?= $this->lang->line(strtolower(clean($key['nombre_seccion']))) ?></span>
                            </a>
                            
                            <!-- <ul aria-expanded="false" class="collapse">
                                <li><a href="../minisidebar/index.html">Minisidebar</a></li>
                                <li><a href="../horizontal/index2.html">Horizontal</a></li>
                                <li><a href="../dark/index3.html">Dark Version</a></li>
                                <li><a href="../material-rtl/index4.html">RTL Version</a></li>
                                <li><a href="javascript:angular">Anuglar-CLI Starter kit</a></li>
                            </ul> -->
                        </li>
                        <?php }} ?>
                        <?php } ?>
                        
                        <?php if($user['rol']==1){ ?>
                        <li class="nav-devider"></li>

                        <li class="nav-small-cap"><?= $this->lang->line('modulos'); ?></li>
                        <?php if($modulos !=""){ ?>
                        <?php foreach ($modulos as $key) { ?>
                             <?php if($key['admin']==1) { ?>
                            <?php if(!isset($_SESSION['user']['vencido'])){ 
                                        $rutam =  base_url()."index.php/Modulos/?nombre=".$key['nombre']."&id=".$key['idModulo']."&tipo=".$key['tipo_vista'];
                                    }else{
                                        $this->session->set_flashdata('mensajeError','Ups! para continuar disfrutando de tus sistema, realiza aquí el pago de tu recibo');
                                        $rutam = base_url()."index.php/Pagos/";
                                    }
                                ?>
                        <li> 
                            
                            <a class="waves-effect waves-dark" href="<?= $rutam ?>" aria-expanded="false">
                                <i class="mdi <?= $key['icono'] ?>"></i>
                                <span class="hide-menu" style="font-size: .9em;" ><?=  ucfirst($this->lang->line(strtolower(clean($key['nombre'])))) ?></span>
                            </a>
                            
                            <!-- <ul aria-expanded="false" class="collapse">
                                <li><a href="../minisidebar/index.html">Minisidebar</a></li>
                                <li><a href="../horizontal/index2.html">Horizontal</a></li>
                                <li><a href="../dark/index3.html">Dark Version</a></li>
                                <li><a href="../material-rtl/index4.html">RTL Version</a></li>
                                <li><a href="javascript:angular">Anuglar-CLI Starter kit</a></li>
                            </ul> -->
                        </li>
                        
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                         
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <?php if($user['rol']==1){ ?>
                <!-- item--><a href="<?= base_url() ?>index.php/Configurar" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                <?php } ?>
                <!-- item-->
                <!-- <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a> -->
                <!-- item--><a href="<?= base_url() ?>index.php/Dashboard/Salir" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
            <!-- End Bottom points-->
        </aside>