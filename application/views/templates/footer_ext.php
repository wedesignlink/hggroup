<footer class="footer">
                © 2019 devux.mx
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= base_url() ?>public/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= base_url() ?>public/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?= base_url() ?>public/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= base_url() ?>public/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?= base_url() ?>public/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= base_url() ?>public/js/custom.min.js"></script>
    <script src="<?= base_url() ?>public/js/dev/orders.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <!-- <script src="<?= base_url() ?>public/assets/plugins/chartist-js/dist/chartist.min.js"></script> -->
    <!-- <script src="<?= base_url() ?>public/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script> -->
    <!--c3 JavaScript -->
    <script src="<?= base_url() ?>public/assets/plugins/d3/d3.min.js"></script>
    <script src="<?= base_url() ?>public/assets/plugins/c3-master/c3.min.js"></script>
    <!-- Chart JS -->
    <!-- <script src="<?= base_url() ?>public/js/dashboard1.js"></script> -->
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?= base_url() ?>public/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- <script type="text/javascript" src="<?= base_url() ?>public/js/functions_register.js"></script> -->
</body>

</html>