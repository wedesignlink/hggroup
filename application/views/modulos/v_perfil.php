

<div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> 
                                    <?php if($user['photo']==""){ ?>
                                    <img src="<?= base_url() ?>public/assets/images/users/default-user.jpg" class="img-circle" width="150" data-target="#modalUpload" data-toggle="modal" />
                                    <?php }else{ ?>
                                        <img src="<?= base_url() ?>public/uploads/img/<?= $user['photo'].'?x='.md5(time()); ?>" class="img-circle" width="150" data-target="#modalUpload" data-toggle="modal" />
                                    <?php } ?>
                                    <br><br>
                                    <button class="btn btn-sm btn-primary" data-target="#modalUpload" data-toggle="modal"><?= $this->lang->line('cambiar_imagen') ?></button>
                                    <h4 class="card-title m-t-10"><?= $user['nombres']; ?> <?= $user['apellidos']; ?></h4>
                                    <h6 class="card-subtitle"><?= $user['rol_nombre'] ?><br>  <?= $user['empresa']->name_company; ?></h6>
                                    <br>
                                    <button rel="" class="btn btn-sm btn-secondary pass_change" data-toggle="modal" data-target="#modalPass"><?= $this->lang->line('cambiar_pass') ?></button><br><br>
                                    <?php if($user['rol']==1){ ?> 
                                    <button id="cancelar_cuenta" class="btn btn-sm btn-secondary pass_change" ><?= $this->lang->line('cerrar_cuenta') ?></button>
                                    <?php } ?> 
                                    <!-- <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>
                                    </div> -->
                                </center>
                            </div>
                            

                            <div class="card-body"> 
                                <small class="text-muted">Email</small>
                                <h6><?= $user['email']; ?></h6>
                            <?php if($user['rol']==1){ ?> 
                                <small class="text-muted p-t-30 db"><?= $this->lang->line('tel') ?></small>
                                <h6>+52 <?= $user['empresa']->phone_number ?></h6> 
                                <small class="text-muted p-t-30 db"><?= $this->lang->line('direccion') ?></small>
                                <h6><?= $user['empresa']->direction_company ?> <?= $user['empresa']->num_ext ?> <?= $user['empresa']->num_int ?>, <?= $user['empresa']->colonia ?>, <?= $user['empresa']->municipio ?>, <?= $user['empresa']->estado ?>
                                </h6>
                            <?php } ?> 
                               
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> 
                                    <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal</a> 
                                </li>
                                <?php if($user['rol']==1){ ?> 
                                <li class="nav-item"> 
                                    <a class="nav-link" data-toggle="tab" href="#empresa" role="tab"><?= $this->lang->line('empresa') ?></a> 
                                </li>
                                <?php } ?> 
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                
                                <div class="tab-pane active" id="personal" role="tabpanel">
                                
                                    <?php
                                    $campos_ver='1,2,3,13,14,15'; //Los numeros de campos de la tabla que deseo ver en el form
                                    $defaults = array(
                                        'id_company'=> $user['empresa']->id_company
                                    ); // Array con los campos por default del form

                                    $cond2 ='id_company='.$user['empresa']->id_company.' and id_status=1';
                                    $combo2 = "select,".$this->lang->line('departamento').",tbl_departamentos,".$cond2.",id_depto,departamento,0,0,concat";             
                                    $combo3 = "select2,".$this->lang->line('rol').",tbl_puestos,".$cond2.",id_puesto,nombre,0";
                                    $combo4 = "select,".$this->lang->line('centro').",tbl_centros,".$cond2.",id_centro,centro,0,0,0,'',1";
                                    
                                    $tipos = array(
                                        'photo'=> 'file,File',
                                        'email'=> 'email,Email',
                                        'passwd'=> 'password,Contraseña',
                                        'id_centro'=> $combo4,
                                        'id_depto'=> $combo2,
                                        'id_puesto'=> $combo3
                                    );
                                    $condicion ='id_user='.$user['id_user'];

                                    crear_form_editar('Perfil','tbl_user','id_user',$condicion,$campos_ver,$defaults,$tipos); 
                                    ?>
                                </div>
                                <div class="tab-pane" id="empresa" role="tabpanel">
                                    <?php
                                    $campos_ver='1,2,3,4,5,6,10,11,12,13'; //Los numeros de campos de la tabla que deseo ver en el form
                                    $defaults = array(
                                        'id_company'=> $user['empresa']->id_company
                                    ); // Array con los campos por default del form

                                    // $tipos = array(
                                    //     'photo'=> 'file,File',
                                    //     'email'=> 'email,Email',
                                    //     'passwd'=> 'password,Contraseña'
                                    // );
                                    $condicion ='id_company='.$user['empresa']->id_company;

                                    crear_form_editar('Company','tbl_company','id_company',$condicion,$campos_ver,$defaults,''); 
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                

                <div id="modalPass" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Password</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form id="form_cambiar_pass">
                                                <input type="hidden" class="form-control" id="id_user"name="id_user" value="<?= $user['id_user'] ?>" >

                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Password:</label>
                                                    <input type="password"  class="form-control" name="password" id="password"  required>
                                                    <label for="recipient-name" class="control-label">Confirm:</label>
                                                    <input type="password"  class="form-control" name="confirm" id="confirm"  required>
                                                    <span class="help-block text-muted"><small><?= $this->lang->line("pass_des"); ?></small></span>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="button" id="btn_form" onclick="CambiarPass()" class="btn btn-danger waves-effect waves-light">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                <?php crear_modal_upload('tbl_user','id_user',$user['id_user'],'photo','perfil') ?>
                <!-- <script src="<?= base_url() ?>public/js/dev/controles.js"></script> -->
                <?php $this->load->view('controles/v_mensaje_flash'); ?>

        <script>
            $(document).ready(function()
            {
                $('#id_depto').prop('disabled',true);
            });

            $(document).on('change','#id_centro',function(){
                var id = $(this).val();
                var Newid  = 'id_depto';
                var Nombre = 0;

                // id_puesto
                var data = 'id_combo='+id+'&idNew='+Newid+'&Name='+Nombre;
                
                $.ajax({
                    url: base_url+'index.php/Controles/cargarComboDeptoExterno',
                    data: data,
                    method: 'POST',
                    beforeSend: function(){
                        $('#id_depto:visible').html('<option>Cargando...</option>');
                        $('#id_depto:visible').prop('disabled',true);
                    },
                    success: function(res){
                        //console.log(res);
                        $('#id_depto:visible').html('');
                        $('#id_depto:visible').prop('disabled',false);
                        $('#id_depto:visible').html(res);               
                    }
                });
            });
        </script>