<div class="row">
	<div class="col-lg-12">
    <?php 
        $fields = ["id_orden_compra", "nombre", "telefono", "articulo", "cantidad", "domicilio", "estatus"];
        $confidView = array("titulo" => "Pedidos",
                            "fields" => $fields,
                            "table" => "vw_pedidos",
                            "actions" => array(
                                (object)["labelBtn" => "Cambiar Estatus",
                                        "isModal" => TRUE,
                                        "action" => "modal_estatus"]
                                /*(object)["labelBtn" => "Editar",
                                        "isModal" => TRUE,
                                        "action" => "modal_editar"]*/,
                            ),
                            "keyUpdate" => "estatus_orden",
                            "keyWhere" => "id_orden_compra",
                            "tableUpdate" => "tbl_orden_compra",
                            "options" => "select id_status as value, nombre as text from cat_status where id_status in(" . IDSTATUSSO . ")");
        view_assigned($confidView); 
        //crear_tabla('Pedidos','vw_pedidos','id_orden_compra',$condicion);
    ?>
    </div>
</div>
<?php $this->load->view('controles/v_mensaje_flash'); ?>