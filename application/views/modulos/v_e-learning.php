<?php 
// print_r($_SESSION['user']['config']);
    $validate=$_SESSION['user']['config']->validate_elearning;
?>
<div class="col-4">
    <div class="form-group" >
        <!-- <label for="nparticipacion">E-learning Requerido:</label> -->
        <!-- <input type="checkbox" onblur="CambiarConfig()" name="nparticipacion" id="nparticipacion" class="form-control" placeholder="Participación por departamento" value="<?= $participacion ?>" > -->
        <input type="hidden" id="campo_config" value="validate_elearning">
        <input type="checkbox" onchange="CambiarConfig('1','1')"  name="valor_config" id="valor_config" value="1" <?php if($validate==1){ echo "checked";} ?> >                        
        <label for="valor_config"><?= $this->lang->line('requerido_elearning'); ?></label> <br>
        
                        
        <span class="help-block text-muted"><small>*<?= $this->lang->line('requerido_elearning_des'); ?></small></span>
    </div>
</div>
<div class="row">
    <div class="col-12 m-t-30">
        <!-- <h4 class="m-b-0">Card styles</h4>
        <p class="text-muted m-t-0 font-12">Cards include various options for customizing their backgrounds, borders, and color.</p> -->
        <button id="agregar" rel="tbl_encuesta_master" type="button" class="accion btn waves-effect waves-light btn-rounded btn-success"><?= $this->lang->line('agregar'); ?> <?= $this->lang->line('curso'); ?></button>
        <br><br>
    </div>
    <!-- <div class="col-md-6">
        <div class="card card-outline-inverse">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Card Title</h4></div>
                <div class="card-body">
                    <h3 class="card-title">Special title treatment</h3>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Editar Evaluación</a>
            </div>
        </div>
    </div> -->
    <?php 
    $user = $_SESSION['user'];
    $campos_ver = '1,2,4,8,9,10';
    $tabla = 'vw_encuestas_master';
    $tabla_edit = 'tbl_encuesta_master';
    $campo_id = 'id_encuesta';
    $condicion = 'id_company='.$user['company'].' and id_cat_encuesta=2';
    $defaults = array(
        'id_company'=>$user['company'],
        'id_user'=>$user['id_user'],
        // 'id_estatus'=>4,
        'id_cat_encuesta'=>2,
    );
    $tipos = array(
        'descripcion'=>'textarea',
        'calificacion'=>'number,10,0',//tipo,maximo,minimo
        'link_elearning'=>'link',
        'id_estatus'=>'hidden,Hidden',
        'obligatorio'=>'checkbox,'.$this->lang->line('obligatorio').',1'
    );
    // $this->output->enable_profiler(TRUE);
    crear_cards_control($tabla,$tabla_edit,$campo_id,$condicion,$campos_ver,$defaults,$tipos); 

    ?>
        
</div>

<?php 

// print_r($user);
$titulo = 'Agregar E-learning';
$campos_ver = '1,2,8,9,10';
$defaults = array(
        'id_company'=>$user['company'],
        'id_user'=>$user['id_user'],
        'id_estatus'=>4,
        'id_cat_encuesta'=>2,
    );

$tipos = $tipos = array(
        'link_elearning'=>'link',
        'calificacion'=>'number,10,0',//tipo,maximo,minimo
        'descripcion'=>'textarea',
        // 'id_estatus'=>'hidden,Hidden',
        'obligatorio'=>'checkbox,'.$this->lang->line('obligatorio').',1'
    );

crear_modal_agregar($titulo,$tabla_edit,$campos_ver,$defaults,$tipos);
  
?>