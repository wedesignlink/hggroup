<div class="row">
	<div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h3>Invitaciones a Futuros Panelistas:</h3>
                <br />
                <button id="addTable" class="btn btn-info" onclick="openModal(this);" style="float: right; display:none;" data-action="modal_table" >Agregar</button>
                <br />
                <br />
                <input type="file" name="file_read" id="file_read" data-max-file-size="1M" accept=".xls, .xlsx"  onchange="sendFile(this)">
                <br />

                <div class="row">
                    <div class="col-md-12 col-sm-12 p-20">
                        <div class="table-responsive">
                            <table id="tabla_invitaciones" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead id="tblHead"></thead>
                                <tbody id="tblBody"></tbody>
                            </table>

                            <br />

                            <button class="btn btn-primary" style="display:none;" id="sendData" onclick="sendInvitation();">Enviar Invitacion</button>
                            <input type="hidden" id="data" value="" />
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('controles/v_mensaje_flash'); ?>

<div class="modal fade" id="modal_table" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" id="modalBody">
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" onclick="saveTable(this);" data-action="modal_table">Guardar</button>
        </div>
    </div>
  </div>
</div>

<script>
    var name_file = "No se ha seleccionado un archivo";
    var archivoExcel = "";
    var data = [];
    var accion = -1;
    var fields = [];

    const mappingMailChimp = {
        "A": "FNAME",
        "B": "LNAME",
        "C": "email_address"
    };

    function sendFile(input) {
        //console.log(input.id);
        let peticion = new XMLHttpRequest();
        let formData = new FormData();
        debugger;
        let archivo = document.getElementById(input.id);
        archivoExcel = archivo.files[0];
        formData.append("file_read", archivo.files[0]);
        name_file = archivo.files[0].name;

        input.value = "";

        peticion.onreadystatechange = processResponse;
        peticion.open("POST", "../Controles/readFile", true);
        peticion.send(formData);

        document.getElementById(input.id).value = "";
    }

    function processResponse() {
        try {
            if (this.readyState == 4 && this.status == 200) {
                //Repuesta correcta
                //console.log(this.responseText)
                let response = JSON.parse(this.responseText);
                if (response) {
                    let header = response.header;
                    let values = response.values;
                    createHeaders(header);
                    createBody(values);

                    $('#sendData').show();
                    $('#addTable').show();
                } else {
                    alert("No es posible procesar el archivo");
                }
            }
        } catch (exception) {
            console.log(exception);
            alert("No es posible procesar el archivo");
        }
    }

    function createHeaders(headers){
        try {
            $('#tblHead').empty();
            for(key in headers){
                let data = headers[key];
                var html = "<tr>";
                for(keyVal in data){
                    html += "   <td>" + data[keyVal] + "</td>";
                    fields.push(data[keyVal]);
                }
                html += "</tr>";

                $('#tblHead').html(html);
            }
        } catch (error) {
            throw {
                message: error.message
            }
        }
    }

    function createBody(items){
        try {
            $('#tblBody').empty();
            var dataSave = [];
            for(x in items){
                let data = items[x];
                dataSave.push(data);
                var html = "<tr>";
                for(i in data){
                    html += "    <td class='datacell'>" +  data[i] + "</td>";
                }
                html += "</tr>";

                $('#tblBody').append(html);
            }

            $('#data').val(JSON.stringify(dataSave));
        } catch (error) {
            throw {
                message: error.message
            }
        }
    }

    function openModal(element){
        try{
            let action = $(element).attr('data-action');

            if(fields.length > 0){
                $('#modalBody').empty();
                fields.forEach(row => {
                    let html = "<input type='text' id='" + row + "' placeholder='" + row + "' class='form-control'> <br /><br />";

                    $('#modalBody').append(html);
                });
            }

            $('#' + action).modal('show');
        }catch(error){
            alertify.alert('Entrevistando MX', 'Error al abrir el modal');
        }
    }

    function saveTable(element){
        try{
            let action = $(element).attr('data-action');
            var dataPreview = JSON.parse($('#data').val());
            if(fields.length > 0){
                var html = "<tr>";
                var letter = "A";
                var obj = {};
                fields.forEach(row => {
                    html += "<td>" + $('#' + row).val() + "</td>";
                    obj[letter] = $('#' + row).val();
                    letter = nextChar(letter);
                });
                
                html += "</tr>";
                dataPreview.push(obj);
            }

            $('#tblBody').append(html);
            $('#' + action).modal('hide');
            $('#data').val(JSON.stringify(dataPreview));
        }catch(error){
            alertify.alert('Entrevistando MX', 'Error al guardar en la tabla');
        }
    }

    function sendInvitation(){
        try{
            let info = JSON.parse($('#data').val());
            var dataSend = [];
            /*info.forEach(line => {
                let obj = Object.values(line);
                dataSend.push(obj);
                return true;
            });*/

            info.forEach(line =>{
                var obj = {};
                for(key in line){
                    if(mappingMailChimp[key]){
                        obj[mappingMailChimp[key]] = line[key];
                        delete line[key];
                    }
                }

                dataSend.push(obj);
            });

            if(dataSend.length > 0){
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url() ?>index.php/Panelistas/createListInvitation',
                    data: {
                        subject: 'Te invitamos a nuestra plataforma Entrevistando México',
                        members: dataSend
                    },
                    success: function(response){
                        response = JSON.parse(response);

                        if(response.isSuccess){
                            swal({
                                title: "Felicidades",
                                text: 'Campaña Creada Correctamente!!',
                                type: "success",
                                timer: 3000,
                            });

                            location.reload();
                        }else{
                            swal({
                                title: "Error!",
                                text: response.message,
                                type: "warning",
                                timer: 3000,
                            });
                        }
                    }
                });
            }else{
                swal({
                    title: "Error!",
                    text: 'Sin datos para enviar a mail chimp',
                    type: "warning",
                    timer: 3000,
                });
            }
        }catch(error){
            swal({
                title: "Error!",
                text: 'Error al enviar invitaciones ' + error.message,
                type: "warning",
                timer: 3000,
            });
        }
    }

    function nextChar(c) {
        return String.fromCharCode(c.charCodeAt(0) + 1);
    }

</script>
