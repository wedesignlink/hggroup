<?php  

$fecha = new DateTime(); 

    if(isset($_GET['id_depto'])){
        $id_depto = $_GET['id_depto'];
    }else{
        $id_depto = "0";
    }
    // ------ CONFIGURACION DEL CRUD -------
    $condicion ='id_company='.$user['empresa']->id_company;
    $id = 'id_puesto';

    // ________FORMULARIO__________
                $campos_ver='1'; //Los numeros de campos de la tabla que deseo ver en el form
                $campos_ver_tabla='0,6,7,11,12,13'; //Los numeros de campos de la tabla que deseo ver en el form
                $defaults = array(
                    'id_company'=> $user['empresa']->id_company
                ); // Array con los campos por default del form

                // ------ Acciones del CRUD -------
                $acciones = "bitacora";
                // $acciones2 = "agregar,desactivar,editar,eliminar";
                // ------ Condicion para mostrar datos de la tabla -------
                $condicion ='id_company='.$user['empresa']->id_company;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-success">
            <div class="card-header">
                <h4 class="m-b-0 text-white"><?= $this->lang->line('reporte_des') ?></h4>
            </div>
            <div class="card-body">
                <form action="#" id="datos_reporte" class="form-horizontal">
                    <input type="hidden" name="id_company" id="id_company" value="<?= $user['empresa']->id_company; ?>">

                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('reporte_tipo') ?></label>
                                    <select class="form-control" name="tipo" id="tipo">
                                        <option value="1"><?= $this->lang->line("calculo_totales") ?>   </option>
                                        <option value="2"><?= $this->lang->line("datos_capturados") ?></option>
                                        <option value="3"><?= $this->lang->line("comportamiento_seguro_critico") ?></option>
                                        <option value="4"><?= $this->lang->line("ponderado_mensual") ?></option>
                                        <option value="5"><?= $this->lang->line("ponderado_anual") ?></option>
                                        <option value="6"><?= $this->lang->line("calculo_actividad") ?></option>
                                    </select>
                                    <small class="form-control-feedback"> <?= $this->lang->line('selecione_tipo') ?></small> 
                                </div>
                            </div>


                        </div>
                        <div class="row" id="ponderadoa" style="display: none;">
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('reporte_por') ?></label>
                                    <select class="form-control" name="tipo_repa" id="tipo_repa" required>
                                        <option value="0"><?= $this->lang->line('seleccione') ?></option>
                                        <option value="1"><?= $this->lang->line('usuarios') ?></option>
                                        <option value="2"><?= $this->lang->line('departamentos') ?></option>
                                    </select>
                                    
                                    <small class="form-control-feedback"> <?= $this->lang->line('seleccione_metodo') ?></small> 
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <?php 
                                $cond='id_company='.$user['empresa']->id_company.' and id_status=1';
                                
                                crear_select($this->lang->line('departamento'),'tbl_departamentos',$cond,'id_depto','departamento',0,0,0,'',0, $this->lang->line('todos') );

                                ?>
                            </div>
                        </div>
                        <div class="row" id="ponderadom" style="display: none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('mes') ?></label>
                                    <input type="month" id="mes" name="mes" class="form-control" max="<?= $fecha->format('Y-m'); ?>" required>
                                    <small class="form-control-feedback"> <?= $this->lang->line('mes_evaluar') ?> </small> 
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('reporte_por') ?></label>
                                    <select class="form-control" name="tipo_rep" id="tipo_rep" required>
                                        <option value="0"><?= $this->lang->line('seleccione') ?></option>
                                        <option value="1"><?= $this->lang->line('usuarios') ?></option>
                                        <option value="2"><?= $this->lang->line('departamentos') ?></option>
                                    </select>
                                    
                                    <small class="form-control-feedback"> <?= $this->lang->line('seleccione_metodo') ?></small> 
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <?php 
                                $cond='id_company='.$user['empresa']->id_company.' and id_status=1';
                                
                                crear_select($this->lang->line('departamento'),'tbl_departamentos',$cond,'id_depto','departamento',0,0,0,'',0, $this->lang->line('todos') );

                                ?>
                            </div>
                        </div>

                        <div class="row" id="rango">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('reporte_de') ?></label>

                                    <input type="date" name="finicio" class="form-control" max="<?= $fecha->format('Y-m-d'); ?>" required>
                                    <small class="form-control-feedback"> <?= $this->lang->line('reporte_fecha_inicio') ?> </small> 
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('reporte_a') ?></label>

                                    <input type="date" id="ffin" name="ffin" class="form-control form-control" value="<?= $fecha->format('Y-m-d'); ?>" max="<?= $fecha->format('Y-m-d'); ?>" required>
                                    <small class="form-control-feedback"> <?= $this->lang->line('reporte_fecha_fin') ?> </small> 
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row" id="rango_mes" style="display: none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('reporte_de') ?></label>

                                    <input type="month" name="finicio" class="form-control" max="<?= $fecha->format('Y-m'); ?>" required>
                                    <small class="form-control-feedback"> <?= $this->lang->line('reporte_fecha_inicio') ?></small> 
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('reporte_a') ?></label>
                                    <input type="month" id="ffin" name="ffin" class="form-control form-control" value="<?= $fecha->format('Y-m'); ?>" max="<?= $fecha->format('Y-m'); ?>" required>
                                    <small class="form-control-feedback"> <?= $this->lang->line('reporte_fecha_fin') ?> </small> 
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row" id="periodo_tipo" style="display: none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('periodo') ?></label>
                                    <select class="form-control" name="tipo_periodo" id="tipo_periodo" required>
                                        <option value="0"><?= $this->lang->line('seleccione') ?></option>
                                        <option value="2"><?= $this->lang->line('semanal') ?></option>
                                        <option value="1"><?= $this->lang->line('mensual') ?></option>
                                    </select>
                                    
                                    <small class="form-control-feedback"> <?= $this->lang->line('seleccione_periodo') ?></small> 
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= $this->lang->line('reporte_por') ?></label>
                                    <select class="form-control" name="tipo_rep_a" id="tipo_rep_a" required>
                                        <option value="0"><?= $this->lang->line('seleccione') ?></option>
                                        <option value="1"><?= $this->lang->line('usuarios') ?></option>
                                        <option value="2"><?= $this->lang->line('departamentos') ?></option>
                                    </select>
                                    
                                    <small class="form-control-feedback"> <?= $this->lang->line('seleccione_metodo') ?></small> 
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row" id="puestos">
                            <div class="col-md-6">
                                <?php 
                                $cond='id_company='.$user['empresa']->id_company.' and id_status=1';
                                
                                crear_select($this->lang->line('centro'),'tbl_centros',$cond,'id_centro','centro',0,0,0,'id_centropue',0, $this->lang->line('todos') );

                                ?>
                            </div>

                            <div class="col-md-6">
                                <?php 
                                $cond='id_company='.$user['empresa']->id_company.' and id_status=1';
                                
                                crear_select($this->lang->line('departamento'),'tbl_departamentos',$cond,'id_depto','departamento',0,0,0,'id_deptopue',0, $this->lang->line('todos') );

                                ?>
                            </div>
                                            
                                            <!--/span-->
                        </div>

                        <div class="row" id="puestos2" style="display: none;">
                            <div class="col-md-6">
                                <?php 
                                $cond='id_company='.$user['empresa']->id_company.' and id_status=1';
                                
                                crear_select($this->lang->line('centro_observado'),'tbl_centros',$cond,'id_centro','centro',0,0,0,'id_centro2',0, $this->lang->line('todos') );

                                ?>
                            </div>

                            <div class="col-md-6">
                                <?php 
                                $cond='id_company='.$user['empresa']->id_company.' and id_status=1';
                                
                                crear_select($this->lang->line('departamento_observado'),'tbl_departamentos',$cond,'id_depto','departamento',0,0,0,'id_depto2',0, $this->lang->line('todos') );

                                ?>
                            </div>
                                            
                                            <!--/span-->
                        </div>
                        
                        <div class="row" id="forms" style="display: none;">
                            <div class="col-md-6">
                                <?php 
                                $cond='id_company ='.$user['empresa']->id_company.' and observacion=1 or (id_company=20 and id_estatus=1)';
                                crear_select($this->lang->line('formulario'),'tbl_encuesta_master',$cond,'id_encuesta','titulo',0,0,0,'',0, $this->lang->line('todos') );
                                ?>
                            </div>
                        </div>

                        <div class="row" id="forms_all" style="display: none;">
                            <div class="col-md-6">
                                <?php 
                                $cond='id_company ='.$user['empresa']->id_company.' and (id_estatus=1  or id_estatus=8) and id_cat_encuesta=1 or (id_company=20 and id_estatus=1)';
                                crear_select($this->lang->line('formulario'),'tbl_encuesta_master',$cond,'id_encuesta','titulo',0,0,0,'',0, $this->lang->line('todos') );
                                ?>
                            </div>
                        </div>



                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="button" id="crear_tabla" class="btn btn-success btn"><?= $this->lang->line('generar_tabla') ?></button>
                                                        <button type="button" id="crear_grafica" class="btn btn-success btn"><?= $this->lang->line('generar_grafica') ?></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        
                    </div>
                </div>

<div class="row">
	<div class="col-lg-12" id="contenedor">

<?php 
    // crear_tabla_crud('Observaciones','vw_master_respuestas','id_respuesta',$condicion,'tbl_departamentos',$campos_ver,$defaults,'',$acciones,'1','1',$campos_ver_tabla);
?>
	</div>
</div>

</div>
 <script src="<?= base_url() ?>public/js/dev/reportes.js"></script>
 <script src="<?= base_url() ?>public/js/dev/combos-reporte.js"></script>



