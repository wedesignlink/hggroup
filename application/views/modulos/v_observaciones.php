<?php 
    $participacion=$_SESSION['user']['config']->Participacion;
?>
<div class="col-4">
    <div class="form-group" >
        <label for="nparticipacion"><?= $this->lang->line('numero_participacion') ?></label>
        <input type="number" onblur="CambiarConfig()" name="nparticipacion" id="valor_config" class="form-control" placeholder="Participación por departamento" value="<?= $participacion ?>" >
        <span class="help-block text-muted"><small>*<?= $this->lang->line('requerido_participacion') ?></small></span>
        <input type="hidden" id="campo_config" value="Participacion">
    </div>
</div>

<div class="row">
    <div class="col-12 m-t-30">
        <!-- <h4 class="m-b-0">Card styles</h4>
        <p class="text-muted m-t-0 font-12">Cards include various options for customizing their backgrounds, borders, and color.</p> -->
        <button id="agregar" rel="tbl_encuesta_master" type="button" class="accion btn waves-effect waves-light btn-rounded btn-primary"><?= $this->lang->line('agregar') ?></button>
        <br><br>
    </div>
    <!-- <div class="col-md-6">
        <div class="card card-outline-inverse">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Card Title</h4></div>
                <div class="card-body">
                    <h3 class="card-title">Special title treatment</h3>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Editar Evaluación</a>
            </div>
        </div>
    </div> -->
    <?php 
    $user = $_SESSION['user'];
    $campos_ver = '1,2,12';
    $tabla = 'vw_encuestas_master';
    $tabla_edit = 'tbl_encuesta_master';
    $campo_id = 'id_encuesta';
    $condicion = 'id_company ='.$user['company'].' and id_cat_encuesta=1 or (id_company=20 and id_cat_encuesta=3 and `id_estatus`=1)';
    $condicion2 = 'id_company = '.$user['company'];
    $defaults = array(
        'id_company'=>$user['company'],
        'id_user'=>$user['id_user'],
        'id_estatus'=>4,
        'id_cat_encuesta'=>1,
    );
    $tipos = array(
        'observacion'=>'checkbox,Observación,1',
        'descripcion'=>'textarea'
    );

    crear_cards_control($tabla,$tabla_edit,$campo_id,$condicion,$campos_ver,$defaults,$tipos,$condicion2); 

    ?>
        
</div>

<?php 

// print_r($user);
$titulo = 'Agregar Evaluación';
$campos_ver = '1,2,11';

$tipos = array(
        'observacion'=>'checkbox,Observación,1',
        'descripcion'=>'textarea'
    );

crear_modal_agregar($titulo,$tabla_edit,$campos_ver,$defaults,$tipos);
  
?>