
<?php 
    
    

    if(isset($_GET['id_seccion'])){
        $id_seccion = $_GET['id_seccion'];
    }else{
        $id_seccion = "0";
    }
    if(isset($_GET['id_encuesta'])){
        $id_encuesta = $_GET['id_encuesta'];
    }else{
        $id_encuesta = "0";
    }
    // ------ CONFIGURACION DEL CRUD -------
    $condicion ='id_encuesta='.$id_encuesta;
    // $id = 'id_puesto';

    // ________FORMULARIO__________
                $campos_ver='1'; //Los numeros de campos de la tabla que deseo ver en el form
                $campos_ver_tabla='0,1'; //Los numeros de campos de la tabla que deseo ver en el form
                $defaults = array(
                    'id_encuesta'=> $id_encuesta,
                    'id_status'=> 4
                ); // Array con los campos por default del form

                // ------ Datos para Generador de select funcion, crear_select() -------
                // $cond='id_role =3';
                // $combo = "select,Roles,cat_roles,".$cond.",id_role,rol_nombre,0";

                // ------ Tipos de campos especiales en el form de agregar -------
                // $tipos = array(
                //     'id_role'=> $combo,
                //     'email'=> 'email,Email',
                //     'passwd'=> 'password,Contraseña'
                // );
                // ------ Acciones del CRUD -------
                if($master->id_estatus==4 || $master->id_cat_encuesta==3){
                     $acciones = "agregar,editar,eliminar";
                }else{
                     $acciones = "";
                }
               
                // // ------ Condicion para mostrar datos de la tabla -------
                // $condicion ='id_company='.$user['empresa']->id_company;
?>
<div class="page-wrapper">
    <div class="container-fluid">
        <br><br>
        <?php if($master->id_cat_encuesta==3 && $user['rol']==6){ ?>
            <a class="btn btn-success" href="<?= base_url() ?>index.php/Formularios/"><?= $this->lang->line('regresar') ?></a>
        <?php }else{ ?>
            <a class="btn btn-success" href="<?= base_url() ?>index.php/Modulos/?nombre=Evaluaciones&id=10&tipo="><?= $this->lang->line('regresar') ?></a>
        <?php } ?>
        <br><br>
        <div class="row">
            <div class="col-lg-6">
<?php 

    crear_tabla_crud($this->lang->line('secciones'),'cat_secciones_encuesta','id_seccion',$condicion,'cat_secciones_encuesta',$campos_ver,$defaults,'',$acciones,'','',$campos_ver_tabla,"", "id_seccion");
?>
    </div></div>

        	<div class="col-lg-6">
        <?php 
            $condicion ='id_encuesta='.$id_encuesta.' AND id_seccion='.$id_seccion;
            $id_tipo = (int)$master->id_cat_encuesta;
            
            // // ________FORMULARIO__________
            crear_tree_preguntas($this->lang->line('preguntas'),'tbl_preguntas_encuestas',$condicion,'id_pregunta',$id_encuesta,$id_tipo);
             
        ?>
        	</div>
        </div>
    </div>
</div>

