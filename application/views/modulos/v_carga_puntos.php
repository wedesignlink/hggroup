<div class="row">
	<div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h3>Archivo de Asignación de Puntos:</h3>
                <br />
                <input type="file" name="file_read" id="file_read" data-max-file-size="1M" accept=".xls, .xlsx"  onchange="sendFile(this)">
                <br />

                <div class="row">
                    <div class="col-md-12 col-sm-12 p-20">
                        <div class="table-responsive">
                            <table id="tabla_asignador" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead id="tblHead"></thead>
                                <tbody id="tblBody"></tbody>
                            </table>

                            <br />

                            <button class="btn btn-primary" style="display:none;" id="sendData">Subir Información</button>
                            <input type="hidden" id="data" value="" />
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
        

        <!--<div class="mt-3">
            <button type="submit" class="btn btn-primary">Cargar</button>
        </div>-->
    </div>
</div>
<?php $this->load->view('controles/v_mensaje_flash'); ?>

<script>
    var name_file = "No se ha seleccionado un archivo";
    var archivoExcel = "";
    var data = [];
    var accion = -1;

    function sendFile(input) {
        //console.log(input.id);
        let peticion = new XMLHttpRequest();
        let formData = new FormData();
        debugger;
        let archivo = document.getElementById(input.id);
        archivoExcel = archivo.files[0];
        formData.append("file_read", archivo.files[0]);
        name_file = archivo.files[0].name;

        input.value = "";

        peticion.onreadystatechange = processResponse;
        peticion.open("POST", "../Controles/readFile", true);
        peticion.send(formData);

        document.getElementById(input.id).value = "";
    }

    function processResponse() {
        try {
            if (this.readyState == 4 && this.status == 200) {
                //Repuesta correcta
                //console.log(this.responseText)
                let response = JSON.parse(this.responseText);
                if (response) {
                    let header = response.header;
                    let values = response.values;
                    createHeaders(header);
                    createBody(values);

                    $('#sendData').show();
                } else {
                    alert("No es posible procesar el archivo");
                }
            }
        } catch (exception) {
            console.log(exception);
            alert("No es posible procesar el archivo");
        }
    }

    function createHeaders(headers){
        try {
            $('#tblHead').empty();
            for(key in headers){
                let data = headers[key];
                var html = "<tr>";
                for(keyVal in data){
                    html += "   <td>" + data[keyVal] + "</td>";
                }
                html += "</tr>";

                $('#tblHead').html(html);
            }
        } catch (error) {
            throw {
                message: error.message
            }
        }
    }

    function createBody(items){
        try {
            $('#tblBody').empty();
            var dataSave = [];
            for(x in items){
                let data = items[x];
                dataSave.push(data);
                var html = "<tr>";
                for(i in data){
                    html += "    <td>" +  data[i] + "</td>";
                }
                html += "</tr>";

                $('#tblBody').append(html);
            }

            $('#data').val(JSON.stringify(dataSave));
        } catch (error) {
            throw {
                message: error.message
            }
        }
    }

    $(document).on('click', '#sendData', function(){
        try {
            let info = JSON.parse($('#data').val());
            var dataSend = [];
            info.forEach(line => {
                let obj = Object.values(line);
                dataSend.push(obj);
                return true;
            });

            if(dataSend.length > 0){
                $.ajax({
                    type: 'POST',
                    data: { dataInsert: dataSend },
                    url: '<?= base_url() ?>index.php/Controles/bulkInsert',
                    success: function(response){
                        response = JSON.parse(response);
                        if(response.isSuccess){
                            let longitud = response.registros.length;

                            if(longitud > 0){
                                swal({
                                    title: "Ok!",
                                    text: longitud + " Registros Procesados Correctamente",
                                    type: "success",
                                    timer: 4000,
                                    buttons: false,
                                });

                                $('#tblHead').empty();
                                $('#tblBody').empty();
                                $('#data').val('');
                                $('#sendData').hide();
                            }else{
                                swal({
                                    title: "Error!",
                                    text: "Error no se cargaron registros",
                                    type: "warning",
                                    timer: 3000,
                                    buttons: false,
                                });
                            }
                            
                        }else{
                            swal({
                                title: "Error!",
                                text: response.message,
                                type: "warning",
                                timer: 3000,
                            });
                        }
                    }
                });
            }else{
                swal({
                    title: "Error!",
                    text: 'Archivo sin Registros',
                    type: "warning",
                    timer: 3000,
                });
            }
            
        } catch (error) {
            swal({
                title: "Error!",
                text: error.message,
                type: "warning",
                timer: 3000,
            });
        }
    });
</script>