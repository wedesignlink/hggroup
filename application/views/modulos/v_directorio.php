<?php 
    // ------ CONFIGURACION DEL CRUD -------

    // ________FORMULARIO__________
    $campos_ver='1,2,3,4'; //Los numeros de campos de la tabla que deseo ver en el form
    $defaults = array(
        'id_company'=> $user['empresa']->id_company,
        'id_status'=>1
    ); // Array con los campos por default del form

    // ------ Datos para Generador de select funcion, crear_select() -------
    //$titulo='',$tabla='',$condicion='',$campo_id='',$campo_nombre='',$default=''
    $cond='';
    $combo = "select,Tipo,cat_tipo_telefono,".$cond.",id_tipo_tel,nombre,0";

    // ------ Tipos de campos especiales en el form de agregar -------
    $tipos = array(
        'id_tipo_tel'=> $combo,
        'numero'=> 'phone,Phone',
    );
    // ------ Acciones del CRUD -------
    $acciones = "agregar,editar,eliminar";
    // ------ Condicion para mostrar datos de la tabla -------
    $condicion ='id_company='.$user['empresa']->id_company;


    crear_tabla_crud($modulo->nombre,'vw_directorio_tipo','id',$condicion,'tbl_directorio',$campos_ver,$defaults,$tipos,$acciones);
?>