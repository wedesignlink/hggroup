

<div class="page-wrapper">
    <div class="container-fluid">
    	<!-- <pre>
    		<?php //print_r($admins) ?>
			<?php //print_r($master) ?>
			<?php //print_r($respuestas) ?>
			<?php //print_r($bitacora) ?>
		</pre>  -->
        <br><br>
        <a class="btn btn-success" href="<?= base_url() ?>index.php/Modulos/?nombre=Bitacora&id=12&tipo="><?= $this->lang->line('regresar') ?></a>
        <br><br>
        <div class="row">
            <div class="col-lg-6">
            	<div class="card">
                        <div class="card-body">
                            <div class="d-flex no-block align-items-center">
                                <h4 class="card-title"><?= $master->titulo ?></h4>
                                <div class="ml-auto">
                                    
                                    <select name="estatus" id="estatus">
                                    		<option value="7"><?= $this->lang->line('cerrado') ?></option>
                                    		<option value="1"><?= $this->lang->line('activo') ?></option>
                                    	<option value="0"><?= $this->lang->line('seleccione') ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-flex align-items-center flex-row m-t-30">
                                <div class="p-2 display-5 text-info"><i class="mdi mdi-account-box"></i> <!-- <span>73<sup>°</sup> --></span></div>
                                <div class="p-2">
                                    <h3 class="m-b-0"><?= $master->departamento ?></h3><small><?= $master->rol ?></small></div>
                            </div>
                             <h3 class="m-b-0 text-center"><?= $this->lang->line('respuestas') ?> <small><?= $master->fecha_created ?></small></h3>
                            <table class="table no-border">
                                <tbody>
                                	<?php 
                                	$i=0;
                                	foreach ($respuestas as $key ) { 
                                		?>
                                    <tr>
                                        <td><?= $respuestas[$i]->label ?></td>
                                        <td class="font-medium"><?= $this->lang->line(clean($respuestas[$i]->respuesta))!=""? $this->lang->line(clean($respuestas[$i]->respuesta)):$respuestas[$i]->respuesta ?></td>
                                    </tr>
                                    <?php $i++; } ?>
                                </tbody>
                            </table>
                            <hr>
                            <ul class="list-unstyled row text-center city-weather-days" style="display: none;">
                                <li class="col"><i class="wi wi-day-sunny"></i><span>09:30</span>
                                    <h3>70<sup>°</sup></h3></li>
                                <li class="col"><i class="wi wi-day-cloudy"></i><span>11:30</span>
                                    <h3>72<sup>°</sup></h3></li>
                                <li class="col"><i class="wi wi-day-hail"></i><span>13:30</span>
                                    <h3>75<sup>°</sup></h3></li>
                                <li class="col"><i class="wi wi-day-sprinkle"></i><span>15:30</span>
                                    <h3>76<sup>°</sup></h3></li>
                            </ul>
                        </div>
                    </div>
    		</div>
			<div class="col-lg-6">
				<div class="card">
                            <div class="card-body">
                            	<div class="d-flex no-block align-items-center">
                                	<h4 class="card-title"><?= $this->lang->line('observaciones_bitacora') ?></h4>
									<div class="ml-auto">
                                    <?= $this->lang->line('asignar') ?>: <br><br>
										<input type="hidden" name="id_respuesta" id="id_respuesta" value="<?= $master->id_respuesta ?>">
	                                    <select name="asignar" id="asignar">
	                                    	<option value="0"><?= $this->lang->line('seleccione') ?></option>
	                                    	<?php $f=0; foreach ($admins as $key ) { ?>
	                                    	<option value="<?= $admins[$f]->id_user ?>"><?= $admins[$f]->nombres ?> <?= $admins[$f]->apellidos ?></option>
	                                    	<?php $f++; } ?>
	                                    </select>
	                                </div>
                            	</div>
                            </div>

                            <!-- ============================================================== -->
                            <!-- Comment widgets -->
                            <!-- ============================================================== -->
                            <div class="comment-widgets">
                                
                                <?php $g=0; foreach ($bitacora as $key ) { ?>
                                <!-- Comment Row -->
                                <div class="d-flex flex-row comment-row ">
                                    <div class="p-2">
                                    	<span class="round">
                                    	<?php if($bitacora[$g]->imagen==""){ ?>
                                    		<img src="<?= base_url() ?>public/assets/images/users/default-user.jpg" alt="user" width="50">
                                    	<?php }else{ ?>
                                    		<img src="<?= base_url() ?>public/uploads/img/<?= $bitacora[$g]->imagen ?>" alt="user" width="50">
                                    	<?php } ?>
                                    	</span>
                                	</div>
                                    <div class="comment-text active w-100">
                                        <h5><?= $bitacora[$g]->nombre ?> <?= $bitacora[$g]->apellido ?></h5>
                                        <p class="m-b-5"><?= $bitacora[$g]->comentario ?></p>
                                        <div class="comment-footer "> <span class="text-muted pull-right"><?= $bitacora[$g]->fecha ?></span> <!-- <span class="label label-light-success">Approved</span> --> <!-- <span class="action-icons active">
                                                    <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)"><i class="icon-close"></i></a>
                                                    <a href="javascript:void(0)"><i class="ti-heart text-danger"></i></a>    
                                                </span> --> </div>
                                    </div>
                                </div>
                                <?php $g++; } ?>

                            </div>
                            <div class="card-body b-t">
                                <div class="row">
                                	<input type="hidden" name="id_user" id="id_user" value="<?= $user['id_user'] ?>">
                                    <div class="col-8">
                                        <textarea id="comentario" placeholder="<?= $this->lang->line('escriba_observacion') ?>" class="form-control b-0"></textarea>
                                    </div>
                                    <div class="col-4 text-right">
                                        <button type="button" class="btn btn-info btn-circle btn-lg enviar_observacion"><i class="mdi mdi-near-me"></i> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
        	</div>
        	<!-- <div class="col-lg-4">
        	</div> -->
        </div>
    </div>
</div>
<script>
	var id_asignar= '<?= $master->id_user_assigned; ?>';
	$('#asignar').val(id_asignar);
	var id_status = '<?= $master->id_status; ?>';
	$('#estatus').val(id_status);
</script>