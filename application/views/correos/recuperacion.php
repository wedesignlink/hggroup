<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>RECUPERACIÓN</title>
  </head>
  <body>

    <img src="hgroupbbq/public/images/theme/icon/logo_negro.png" style="padding-top: 30px; width: 100px; height: 100px; margin-left: 620px;">

    <div style="width: 600px; height: 500px; margin: 0 auto; text-align: center; font-family: Arial, Helvetica, sans-serif; background-color: #2c353f;">
        <div style="width: 600px; height: 50px; color: white; padding-top: 40px;">
            <h2>¿Olvidaste tu contraseña?</h2>
            <p style="margin-bottom: 30px; text-align: center; font-family: 'Open Sans', sans-serif; color: white;">Restablece tu contraseña a través del siguiente enlace:</p>
            <a href="url" class="btn btn-primary btn-lg btn-outline-light bg-warning rounded mx-auto h-100 bg-info d-flex justify-content-center align-items-center" style="font-size: 12px; font-weight: 500; font-family: Montserrat, sans-serif; background-color: darkorange; padding: 15px 25px; color: white; text-decoration: none;">RECUPERAR MI CONTRASEÑA</a>
            <p class="font-weight-light align-content-center" style="text-align: left; text-align: justify; margin-left: 30px; margin-right: 30px; margin-top: 30px;"><small style="font-family: 'Open Sans', sans-serif; color: white;">Si no has solicitado el cambio de contraseña, puedes comunicarte con nosotros a través de: </small></p>
            <a href="url" style="color: darkorange;"><small>contacto</small></a>
        </div>

        <div style="margin-top: 270px;">

            <hr>
            <p class="text-medium font-weight-600 text-dark" style="text-align: left; padding-top: 10px; font-size: 16px; font-weight: 500; font-family: Montserrat, sans-serif; margin-left: 30px; color: darkgoldenrod;">H GROUP</p>
            <p class="font-weight-light align-content-center" style="text-align: left; text-align: justify; margin-left: 30px; margin-right: 30px;"><small style="font-family: 'Open Sans', sans-serif; color: white;">Somos un grupo especialista en el diseño de espacios prácticos, decorativos y funcionales para que pases los mejores momentos, da vida a tu patio con nuestras soluciones, diseña ahora tu espacio ideal.</small></p>
            <p class="lead">

        </div>

    </div>

  </body>
</html>