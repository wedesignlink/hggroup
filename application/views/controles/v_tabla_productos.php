<!-- <pre> -->
<?php
// print_r($tipos);
// print_r($row_names);
if ($acciones != "") {
    $acciones = explode(",", $acciones);

    $e = array_search('agregar', $acciones);
    if ($e !== FALSE) {
        $agregar = 1;
        unset($acciones[$e]);
    } else {
        $agregar = "";
    }
} else {
    $agregar = "";
}


?>
<!-- </pre> -->


<?php $tit = str_replace(" ", "", $titulo) ?>

<input type="hidden" id="campo" value="<?= $campo_id_tabla ?>">
<!-- <div class="row">
    <div class="col-lg-12"> -->
<div class="card">
    <!-- <div class="card-header">
                
            </div> -->
    <div class="card-body">
        <h4 class="card-title"><?= $titulo ?> </h4>
        <div class="row">

            <div class="col-md-12 col-sm-12 p-20">
                <?php if ($agregar == 1) { ?>
                    <button type="button" class="btn btn-success btn-rounded pull-right accion" id="agregar"><i class="fa fa-plus"></i> <?= $this->lang->line('agregar'); ?></button>
                <?php } ?>
                <div class="table-responsive ">
                    <table id="tabla_<?= strtolower($tit) ?>" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <?php foreach ($row_names as $key => $value) { ?>
                                    <th><?= $this->lang->line($value) != '' ? $this->lang->line($value) : $value; ?></th>
                                <?php } ?>
                                <?php if ($acciones != "") { ?>
                                    <th></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <?php foreach ($row_names as $key => $value) { ?>
                                    <th><?= $this->lang->line($value) != '' ? $this->lang->line($value) : $value; ?></th>
                                <?php } ?>
                                <?php if ($acciones != "") { ?>
                                    <th></th>
                                <?php } ?>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($data_table as $k) { ?>
                                <tr <?php if ($k->id_status == 8) {
                                        echo "class='table-danger'";
                                    } ?>>
                                    <?php foreach ($row_names as $key => $value) { ?>
                                        <?php //$k->$value ?>
                                        <?php if ($value == "url_images") { ?>
                                            <td class="<?= $value ?>"><?php $images = explode(',', $k->$value); ?>
                                                <img class="card-img-top img-fluid" src="<?php
                                                                                            $url = base_url() . "public/uploads/productos/" . $images[0];
                                                                                            echo $images != "" ? $url : "https://commercial.bunn.com/img/image-not-available.png";
                                                                                            ?>" alt="Card image cap" data-toggle="modal" data-target="#modal_">
                                                <br><br>                                           
                                                <button class="btn btn-sm btn-primary btn-block"  data-target="#modalUpload_<?= $k->id_producto ?>" data-toggle="modal">Subir imagen</button>
                                                
                                                <?php crear_modal_upload_producto('tbl_productos','id_producto',$k->id_producto,'url_images','producto') ?>
                                            </td>
                                        <?php } else { ?>
                                            <td class="<?= $value ?>"><span class="dato"><?= $k->$value; ?></span></td>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if ($acciones != "") { ?>
                                        <td style="text-align: center;">

                                            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-h"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" x-placement="top-start">
                                                <!-- <a id="asignar" class="dropdown-item accion" href="#">Asignar módulo</a> -->
                                                <?php foreach ($acciones as $key => $value) { ?>
                                                    <a id="<?= $value ?>" class="dropdown-item accion" href="#" rel="<?= $tabla ?>">
                                                        <?php if ($k->id_status == 8 && $value == "desactivar") {
                                                            echo $this->lang->line('activar');
                                                        } else { ?>
                                                            <?= $this->lang->line($value) ?>
                                                    </a>
                                            <?php }
                                                    } ?>
                                            </div>
                                            <!-- <button id="borrar" type="button" class="btn btn-danger accion"><i class="fa fa-times"></i></button> -->

                                        </td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>

                            <!-- <tr>
                                    <td>Tiger Nixon</td>
                                </tr> -->

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--     </div>
</div> -->
    <!-- </div> -->



    <script>
        var table = "<?= $tabla_edit ?>";
        var buttons = "<?= $buttons ?>";
        var paginar = "<?= $paginar ?>";

        $('#tabla_<?= strtolower($tit) ?>').DataTable({
            <?php if ($buttons == 1) { ?>
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
            <?php } ?>
            columnDefs: [{
                    targets: [<?= $campos_ver_tabla; ?>, -1],
                    visible: true
                },
                {
                    targets: '_all',
                    visible: false
                },

            ],
            paging: false,
            <?php if ($paginar == 0) { ?>
                paging: false,
                ordering: false,
                info: false,
                searching: false
            <?php } ?>

        });
    </script>