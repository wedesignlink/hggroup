
<!-- ------------ combo automático --------------------- -->
<div class="campo_concat">
	<div class="form-group">
	    <label for="select"><?= $titulo ?></label>
	    <select class="form-control" name="<?= $campo_id ?>" id="<?= $campo_id ?>" <?php if($requerido==1){ echo "required"; } ?> required>
	        <option value="<?= $default ?>"><?= $this->lang->line('seleccione'); ?></option>
	    </select>
	</div>
</div>
<!-- ------------ FIN combo automático --------------------- -->