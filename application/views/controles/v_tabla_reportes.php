<!-- <pre> -->
    <?php 
// print_r($data_table);
$botones="";
    if($botones!=""){
        $btn=array();
        foreach ($botones as $key => $value) {
             $btn[$key] = explode(",",$value);
        }
    }
    
// print_r($btn);
// echo $translate;
?>
<!-- </pre> -->
 

 <?php $tit = str_replace(" ", "", $titulo) ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <!-- <div class="card-header">
                
            </div> -->
           <div class="card-body">
            <h4 class="card-title"><?= $titulo ?> </h4>
            <div class="row">

                <div class="col-md-12 col-sm-12 p-20">

                    <!-- <button type="button" class="btn btn-success btn-rounded pull-right accion" id="agregar" ><i class="fa fa-plus"></i> Agregar</button> -->
                    <div class="table-responsive ">
                        <table id="tabla_<?= strtolower($tit) ?>" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                <?php foreach ($row_names as $key => $value) { ?>
                                    <?php if($translate==1 && $this->lang->line($value)!="" ){ ?>
                                        <th><?= $this->lang->line($value) ?></th>
                                    <?php }else{ ?>
                                        <th><?= $value ?></th>
                                    <?php } ?>
                                <?php } ?>
                                <?php if($botones!="") { ?>
                                    <th style="width: 150px !important"></th>
                                <?php } ?>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($data_table as $k ) { ?>
                                <tr <?php if(isset($k->id_status)){ if($k->id_status==1){ echo "class='table-success'"; }} ?> >
                                <?php foreach ($row_names as $key => $value) { ?>
                                    <td onclick=""><?= $this->lang->line(clean($k->$value))!=""? $this->lang->line(clean($k->$value)):$k->$value ?></td>
                                <?php } ?>
                                    <?php if($botones!="") { ?>
                                    <td>
                                    <?php foreach ($btn as $kb => $vb) { ?>
                                    <?php //if( $k->$campo  $condicion ) { ?>
                                        <button type="button" onclick="var id =table.row( this.closest('tr') ).id();<?= $vb[1] ?>" class="btn btn-<?= $vb[2] ?> btn-circle"><i class="fa <?= $vb[0] ?>"></i> 
                                        </button>
                                    <?php }// } ?>
                                    </td>
                                
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                                
                                
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



<script>

    var table = $('#tabla_<?= strtolower($tit) ?>').DataTable({
        dom: 'Bfrtip',
        rowId: 0,
        paging: true,
        buttons: [
        {
                extend: 'copy',
                title: '<?= $titulo ?>',
                exportOptions: {
                    columns: ':visible'
                }
            }, {
                extend: 'csv',
                title: '<?= $titulo ?>',
                exportOptions: {
                    columns: ':visible'
                }
            }, {
                extend: 'excel',
                title: '<?= $titulo ?>',
                exportOptions: {
                    columns: ':visible'
                }
            }, {
                extend: 'pdf',
                title: '<?= $titulo ?>',
                exportOptions: {
                    columns: ':visible'
                }
            }, {
                extend: 'print',
                title: '<?= $titulo ?>',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        <?php if($mostrar!=""){ ?>
        columnDefs: [
        { targets: [<?= $mostrar ?>], visible: true},
        { targets: '_all', visible: false },
    ]
    <?php } ?>
    });

    // $('#tabla_<?= strtolower($tit) ?>').on( 'click', 'tr', function () {
    //     var id = table.row( this ).id();

     
    //     alert( 'Clicked row id '+id );
    // } );




</script>