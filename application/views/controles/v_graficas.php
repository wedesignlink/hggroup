<style>
	svg.ct-chart-bar, svg.ct-chart-line{
		overflow: visible;
	}
	.ct-label.ct-label.ct-horizontal.ct-end {
	  position: relative;
	  justify-content: flex-end;
	  text-align: right;
	  transform-origin: 100% 0;
	  transform: translate(-100%) rotate(-45deg);
	  white-space:nowrap;
	}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card" style="height: 640px">
            <!-- <div class="card-header">
                
            </div> -->
           	<div class="card-body">
			<div class="download-state ct-bar-chart1" style="height:400px"><div style="clear: both;"></div></div>

		</div>
	</div>
</div>
<script>
	var nombres = [<?= rtrim($nombres,',') ?>];
	var valores = [<?= rtrim($valores,',') ?>];

	new Chartist.Bar('.ct-bar-chart1', {
	  labels: nombres,
	  height:400,
	  series: [
	    valores,
	    // [2, 1, 3.5, 7, 3],
	    // [1, 3, 4, 5, 6]
	  ]
	}, {
	  fullWidth: true,
	  
	  plugins: [
	    Chartist.plugins.tooltip()
	  ]
	});
</script>