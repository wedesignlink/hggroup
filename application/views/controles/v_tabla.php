<!-- <pre> -->
    <?php 
// print_r($data_table);

    if($botones!=""){
        $btn=array();
        foreach ($botones as $key => $value) {
             $btn[$key] = explode(",",$value);
        }
    }
    
// print_r($btn);
?>
<!-- </pre> -->
 

 <?php $tit = str_replace(" ", "", $titulo) ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <!-- <div class="card-header">
                
            </div> -->
           <div class="card-body">
            <h4 class="card-title"><?= $titulo ?> </h4>
            <div class="row">

                <div class="col-md-12 col-sm-12 p-20">

                    <!-- <button type="button" class="btn btn-success btn-rounded pull-right accion" id="agregar" ><i class="fa fa-plus"></i> Agregar</button> -->
                    <div class="table-responsive ">
                        <table id="tabla_<?= strtolower($tit) ?>" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                <?php foreach ($row_names as $key => $value) { ?>
                                    <th><?= $value ?></th>
                                <?php } ?>
                                <?php if($botones!="") { ?>
                                    <th style="width: 150px !important"></th>
                                <?php } ?>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($data_table as $k ) { ?>
                                <tr <?php if(isset($k->id_status)){ if($k->id_status==1){ echo "class='table-success'"; }} ?> >
                                <?php foreach ($row_names as $key => $value) { ?>
                                    <td class="<?= $value ?>" onclick=""><?= $this->lang->line(strtolower(clean($k->$value)))!=null? $this->lang->line(strtolower(clean($k->$value))):$k->$value ?></td>
                                <?php } ?>
                                    <?php if($botones!="") { ?>
                                    <td>
                                    <?php foreach ($btn as $kb => $vb) { ?>
                                    <?php //if( $k->$campo  $condicion ) { ?>
                                        <button type="button" onclick="var id =table.row( this.closest('tr') ).id();<?= $vb[1] ?>" class="btn btn-<?= $vb[2] ?> btn-circle"><i class="fa <?= $vb[0] ?>"></i> 
                                        </button>
                                    <?php }// } ?>
                                    </td>
                                
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                                
                                
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



<script>

    var table = $('#tabla_<?= strtolower($tit) ?>').DataTable({
        dom: 'Bfrtip',
        rowId: 0,
        buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 3, "desc" ]],
        columnDefs: [
        <?= $formato_campos; ?>
        { targets: [<?= $mostrar ?>], visible: true},
        { targets: '_all', visible: false },
    ]
    });

    // $('#tabla_<?= strtolower($tit) ?>').on( 'click', 'tr', function () {
    //     var id = table.row( this ).id();

     
    //     alert( 'Clicked row id '+id );
    // } );

<?php if($head==0){ ?>
    $("#tabla_<?= strtolower($tit) ?> thead").hide()
<?php } ?>


</script>