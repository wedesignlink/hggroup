<?php 
// ----extraemos los campos visibles----
$visibles = explode(",", $campos_ver);


$camposVisibles = array();
foreach ($row_names_edit as $key => $value) {
    foreach ($visibles as $k => $v) {
        if($key==$v){
            $camposVisibles[]=$value;
        }
    }
}

$titulo = str_replace(' ', '_', $titulo);

?>

<script>
    var table<?= $titulo ?> = '<?= $tabla ?>';
</script>
 <script>
     $(document).on('change','#form_editar_<?= $titulo ?> .concat',function(){
        var id = $(this).val();
        // id_puesto
        var data = 'id_combo='+id;
        
        $.ajax({
            url: base_url+'index.php/Controles/cargarCombo',
            data: data,
            method: 'POST',
            beforeSend: function(){
                $('#form_editar_<?= $titulo ?> #id_puesto:visible').html('<option>Cargando...</option>');
                $('#form_editar_<?= $titulo ?> #id_puesto:visible').prop('disabled',true);
            },
            success: function(res){
                $('#form_editar_<?= $titulo ?> #id_puesto:visible').html('');
                $('#form_editar_<?= $titulo ?> #id_puesto:visible').prop('disabled',false);
                $('#form_editar_<?= $titulo ?> #campo_concat:visible').html(res);                
            }
        });
    });


</script>

        <!-- <div class="card"> -->
        <div class="col-lg-12">
            <!-- <div class="card-header">
                <h4 class="card-title">Agregar o editar</h4>
            </div> -->
            <div class="card-body">
                <form id="form_editar_<?= $titulo ?>" class="form-horizontal form-material">
                    <input type="hidden" class="form-control" name="campo" value="<?= $campo_id_tabla ?>" >


                <?php foreach ($row_names as $key =>$value) { 
                    if($value ==$campo_id_tabla){ ?>
                        <input type="hidden" class="form-control" name="<?= $value ?>" id="<?= $value ?>" value="<?= $data_table[0]->$value ?>" >
                <?php }} ?>

                <?php foreach ($camposVisibles as $key =>$value) { ?>

                <?php 

                    if($tipos!=''){
                        $tipo = array_key_exists($value, $tipos);
                    }else{
                       $tipo = ""; 
                    }
                    

                    if($tipo!=''){

                    $datacampo = explode(",", $tipos[$value]);

                    // print_r($datacampo);
                       
                       switch ($datacampo[0]) {
                            case 'email':
                               echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="email" class="form-control" name="'.$value.'" id="'.$value.'" value="'.$data_table[0]->$value.'"  required>
                                </div>';
                            break;
                            case 'password':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="password" class="form-control" name="'.$value.'" id="'.$value.'" value="'.$data_table[0]->$value.'"  required>
                                </div>';
                            break;
                            case 'phone':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="tel" class="form-control" name="'.$value.'" id="'.$value.'" value="'.$data_table[0]->$value.'"  required>
                                </div>';
                            break;
                            case 'file':
                                echo '<div class="form-group">
                                    <label>'. ucfirst($value).'</label>
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                    <input type="hidden">
                                    <input type="hidden"><input type="file" name="'.$value.'"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                </div>';
                            break;
                            case 'select':
                                $dataDel = array_shift($datacampo);
                                
                                // $parametros = "'".implode("','", $datacampo)."'";
                                crear_select($datacampo[0],$datacampo[1],$datacampo[2],$datacampo[3], $datacampo[4],$datacampo[5],$value,$datacampo[7]);
                                // crear_select($titulo='',$tabla='',$condicion='',$campo_id='',$campo_nombre='',$default='',$selected='',$concat='',$titulo_campo='')
                                echo "<script>$('#".$datacampo[3]."').val(".$data_table[0]->$value.").trigger('change');</script>";
                            break;
                            case 'select2':
                                $dataDel = array_shift($datacampo);
                                // print_r($datacampo);
                                // $parametros = "'".implode("','", $datacampo)."'";
                                crear_select_vacio($datacampo[0],$datacampo[1],$datacampo[2],$datacampo[3], $datacampo[4]);
                                // echo "<script>$('#".$datacampo[3]."').append('<option value='".$data_table[0]->$value."'><option>');</script>";

                                 echo "<script>  var selector = '#".$datacampo[3]."'; var valorSel = ".$data_table[0]->$value."; </script>";    
                            break;
                       }
                ?>
                    
                <?php }else{ ?>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label"><?=  $this->lang->line($value)!= ''?ucfirst(str_replace('_', ' ',$this->lang->line($value))): ucfirst(str_replace('_', ' ', $value)); ?>:</label>
                        <input type="text" onkeyup="var start = this.selectionStart;var end = this.selectionEnd;this.value = this.value.toUpperCase();this.setSelectionRange(start, end);" class="form-control" name="<?= $value ?>" id="<?= $value ?>" value="<?= $data_table[0]->$value ?>"  required>
                    </div>
                <?php }} ?>

                <!-- ------------ Agregamos los campos por defecto ------------ -->

                <?php if($defaults!=''){ foreach ($defaults as $key => $value) { ?>       
                        <input type="hidden" class="form-control" name = "<?= $key ?>" value="<?= $value ?>" >
                <?php }} ?>


                    <!-- <div class="form-group">
                        <label for="message-text" class="control-label">Message:</label>
                        <textarea class="form-control" id="message-text"></textarea>
                    </div> -->
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="button" onclick="Editar<?= $titulo ?>()" class="btn btn-success waves-effect waves-light"><?= $this->lang->line('actualizar') ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

           <!--  <div class="card-footer">
                
            </div> -->
       <!--  </div> -->
   
    <script>
        // alert(valorSel);
$(document).ajaxComplete(function(){ 
    if(valorSel!=0 || valorSel!=""){
        $(selector).val(valorSel);
    }else{
        $(selector).val(0); 
    } 
});
        
        function Editar<?= $titulo ?>(){

            var datos = $('#form_editar_<?= $titulo ?>').serializeArray();
            // var email = $('email').val()!=undefined?$('email').val():"";
            var campo=datos[0]['value'];
            var id=datos[1]['value'];
            datos.splice(0, 2);
            var data = ArrayToSerial(datos);
            var update = data;
            if(lang =="spanish"){
                var msjn = "No pudimos realizar los cambios";
                var msjs = "Listo!, ya se realizo el ajuste";
            }else{
                var msjn = "An error occurred while processing your request.";
                var msjs = "Changes Saved Successfully";
            }
            
            var controlador = 'UPD_User'

            UpdateElement(table<?= $titulo ?>,campo,id,update,msjn,msjs,controlador,'recargarSesion');
        }
    </script>
