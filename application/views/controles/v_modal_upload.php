<?php 
// ----extraemos los campos visibles----


?>
<link rel="stylesheet" href="<?= base_url() ?>public/assets/plugins/dropify/dist/css/dropify.min.css">
<div id="modalUpload" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><?= $this->lang->line("subir_archivo") ?></h4>
            </div>
            <div class="modal-body">
                <form id="form_upload" action="<?= base_url() ?>index.php/Controles/Upload_img" method="POST" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" name="campo_id" value="<?= $campo_id ?>" >
                    <input type="hidden" class="form-control" name="tipo" value="<?= $tipo ?>" >
                    <input type="hidden" class="form-control" name="valor_id" value="<?= $valor_id ?>" >
                    <input type="hidden" class="form-control" name="tabla" value="<?= $tabla ?>" >
                    <input type="hidden" class="form-control" name="campo_upload" value="<?= $campo_upload ?>" >
                    <div class="form-group">
                        <!-- <label for="input-file-now">Elija el archivo a subir</label> -->
                        <input type="file" name="imagen_subir" data-max-width="801" data-max-height="801" id="input-file-now" class="dropify" data-max-file-size="1M" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect pull-left" data-dismiss="modal"><?= $this->lang->line("borrar_img"); ?></button>
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal"><?= $this->lang->line("cerrar"); ?></button>
                <button type="button" id="btn_form" onclick="$('#form_upload').submit()" class="btn btn-primary waves-effect waves-light"><?= $this->lang->line("subir"); ?></button>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url() ?>public/assets/plugins/dropify/dist/js/dropify.min.js"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
    })
</script>