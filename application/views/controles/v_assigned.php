<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?= $titulo ?> </h4>
                <div class="row">
                    <div class="col-md-12 col-sm-12 p-20">
                        <br />
                        <div class="table-responsive ">
                            <table id="tabla_asignador" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                    <?php foreach ($row_names as $key => $value) { ?>
                                        <th><?= $value ?></th>
                                    <?php } ?>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    <?php $idLine = ''; ?>
                                    <?php foreach ($row_names as $key => $value) { ?>
                                        <th><?= $value ?></th>
                                    <?php } ?>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($data_table as $k ) { ?>
                                    <tr>
                                    <?php foreach ($row_names as $key => $value) { ?>
                                        <?php if($value === $keyWhere) { $idLine = $k->$value; } ?>
                                        <td class="<?= $value ?>"><span class="dato"><?= $k->$value ?></span></td>
                                    <?php } ?>
                                        <td style="text-align: center;">
                                            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-ellipsis-h"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" x-placement="top-start" >
                                                <?php 
                                                    foreach($actions as $key => $value){
                                                ?>
                                                    <a <?= $value->isModal ? 'onclick="openModal(this);" data-action="' . $value->action . '"' : 'onclick="' . $value->action . '"'; ?> class="dropdown-item accion" href="#" data-id="<?= $idLine; ?>"><?= $value->labelBtn; ?></a>
                                                <?php 
                                                    }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_estatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Pedido</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="<?= base_url() ?>index.php/Controles/updateBasic">
        <div class="modal-body">
            <input type="hidden" name="keyWhere" value="<?= $keyWhere; ?>" >
            <input type="hidden" name="table" value="<?= $tableUpdate;  ?>" >
            <input type="hidden" name="idModal" id="idModal" value="" >
            <input type="hidden" name="isModal" id="idModal" value="1" >
            <input type="hidden" name="sendEmail" value="1" />
            <select name="estatus_orden" class="form-control">
                <?php foreach($options as $key => $value){ ?>
                    <option value="<?= $value->value; ?>"> <?= $value->text; ?> </option>
                <?php } ?>
            </select>
            <br /><br />
            <input type="text" class="form-control" name="paqueteria" placeholder="Numero de Guia" />
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar cambios</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
    function openModal(element){
        try{
            let action = $(element).attr('data-action');
            let idAssigned = $(element).attr('data-id');

            $('#idModal').val(idAssigned);
            $('#' + action).modal('show');
        }catch(error){
            alertify.alert('Entrevistando MX', 'Error al abrir el modal');
        }
    }
</script>