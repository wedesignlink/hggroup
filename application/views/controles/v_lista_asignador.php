<!-- <pre>
    <?php 
print_r($data_table);
print_r($row_names);
?>
</pre>
 -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
 
           <div class="card-body">
            <h4 class="card-title"><?= $titulo ?> </h4>
            <div class="row">
                <div class="col-md-3 col-sm-4 p-20" style="display: none;">
                    <h4 class="card-title">Listado </h4>
                    <div class="list-group">
                        <a href="javascript:void(0)" id="todos" class="list-group-item" style="color: black">Todos</a>
                        <?php foreach ($listado as $key) { ?>
                            <a href="javascript:void(0)" id="id_<?= $key[$campo_id_list] ?>" class="list-group-item" style="color: black"><?= ucfirst(strtolower($key[$campo_nombre_list])) ?></a>
                        <?php } ?>
                    </div>  
                </div>
                <div class="col-md-12 col-sm-12 p-20">

                    <button type="button" class="btn btn-success btn-rounded pull-right accion" id="agregar"><i class="fa fa-plus" ></i> Agregar</button>
                    <div class="table-responsive ">
                        <table id="tabla_asignador" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                <?php foreach ($row_names as $key => $value) { ?>
                                    <th><?= $value ?></th>
                                <?php } ?>
                                    <th></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <?php foreach ($row_names as $key => $value) { ?>
                                    <th><?= $value ?></th>
                                <?php } ?>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($data_table as $k ) { ?>
                                <tr <?php if($k->id_status==8){ echo "class='table-danger'"; } ?> >
                                <?php foreach ($row_names as $key => $value) { ?>
                                    <td class="<?= $value ?>"><span class="dato"><?= $k->$value ?></span></td>
                                <?php } ?>
                                    <td style="text-align: center;">

                                            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-ellipsis-h"></i>
                                            </button>
                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" x-placement="top-start" >
                                                    <!-- <a id="asignar" class="dropdown-item accion" href="#">Asignar módulo</a> -->
                                                    <a id="editar" class="dropdown-item accion" href="#">
                                                    Editar
                                                    </a>
                                                    <a id="admin" class="dropdown-item accion" href="#">
                                                    Convertir en administrador
                                                    </a>
                                                    <a id="desactivar" class="dropdown-item accion" href="#">
                                                    Activar/Desactivar
                                                    </a>
                                                    <a id="eliminar" class="dropdown-item accion" href="#">
                                                    Eliminar
                                                    </a>
                                                </div>
                                            <!-- <button id="borrar" type="button" class="btn btn-danger accion"><i class="fa fa-times"></i></button> -->
                                       
                                    </td>
                                </tr>
                                <?php } ?>

                                <!-- <tr>
                                    <td>Tiger Nixon</td>
                                </tr> -->
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



<script>

    var table = "<?= $tabla_edit ?>";

    $('#tabla_asignador').DataTable({
        // dom: 'Bfrtip',
        // buttons: [
        // 'copy', 'csv', 'excel', 'pdf', 'print'
        // ],
        columnDefs: [
        { targets: [0,1,2,3,4,-1], visible: true},
        { targets: '_all', visible: false }
    ]
    });

</script>