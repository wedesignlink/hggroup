<!-- <pre> -->
<?php 

// print_r($defaults);
// print_r($tipos);
// print_r($row_names_edit);
if(isset($_GET['id_seccion'])){
        $id_seccion = $_GET['id_seccion'];
    }else{
        $id_seccion = "0";
    }
// ----extraemos los campos visibles----
$visibles = explode(",", $campos_ver);
// print_r($visibles);

$camposVisibles = array();
foreach ($row_names_edit as $key => $value) {
    foreach ($visibles as $k => $v) {
        if($key==$v){
            $camposVisibles[]=$value;
        }
    }
}
 // print_r($camposVisibles);
 $tit = str_replace(" ", "", $titulo);
?>
<!-- </pre> -->

<script>
    var tableTree = "<?= $tabla ?>";
</script>
<style>
    .select2-container {
    width: 100% !important;
}
input.select2-search__field {
    width: 100% !important;
}
</style>

<div id="modalAddtree" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><?= $this->lang->line('agregar'); ?></h4>
            </div>
            <div class="modal-body">
                <form id="form_add_encuesta">
                <?php foreach ($camposVisibles as $key =>$value) { ?>

                <?php 
                    if($tipos != ""){
                        $tipo = array_key_exists($value, $tipos);
                    }else{
                        $tipo='';
                    }

                    if($tipo!=''){

                    $datacampo = explode(",", $tipos[$value]);

                    // print_r($datacampo);
                    $tit = $this->lang->line($value)!= ''?ucfirst($this->lang->line($value)):ucfirst($value); 
                       
                       switch ($datacampo[0]) {
                            case 'email':
                               echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="email" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'respuesta':
                               echo '<div class="form-group respuestas">
                                    <label for="recipient-name" class="control-label">'.$tit.':</label>
                                    <input type="text" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                                echo '<div class="form-group respuesta_check" style="display:none;">';
                                echo'<label for="recipient-name" class="control-label">'. $tit.':</label><br>
                                    <select class="form-control" name="'.$value.'" id="'.$value.'" multiple="multiple" required ></select>';
                                echo '</div>';
                            break;
                            case 'password':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="password" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'number':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="number" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'phone':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="tel" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'select':
                                $dataDel = array_shift($datacampo);
                                // print_r($datacampo);
                                // $parametros = "'".implode("','", $datacampo)."'";
                                crear_select($datacampo[0],$datacampo[1],$datacampo[2],$datacampo[3], $datacampo[4],$datacampo[5],$datacampo[6],$datacampo[7],$datacampo[8],$datacampo[9]);
                                // crear_select($titulo='',$tabla='',$condicion='',$campo_id='',$campo_nombre='',$default='',$selected='',$concat='',$titulo_campo='',$requerido='',$default_text='')
                                
                            break;
                            case 'select_trans':
                                $dataDel = array_shift($datacampo);
                                // print_r($datacampo);
                                // $parametros = "'".implode("','", $datacampo)."'";
                                crear_select_trans($datacampo[0],$datacampo[1],$datacampo[2],$datacampo[3], $datacampo[4],$datacampo[5],$datacampo[6],$datacampo[7],$datacampo[8],$datacampo[9]);
                                // crear_select($titulo='',$tabla='',$condicion='',$campo_id='',$campo_nombre='',$default='',$selected='',$concat='',$titulo_campo='',$requerido='',$default_text='')
                                
                            break;
                            case 'no_required':
                            echo '<div class="form-group dataoptions" style="display:none;">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label><br>
                                    <select type="text" class="form-control" name="'.$value.'" id="'.$value.'" multiple="multiple" required ></select>
                                </div>';
                            break;
                            case 'checkbox':
                            if($datacampo[2]==1){
                                    $visible='';
                            }else if($datacampo[2]==0){
                                $visible = 'display:none;';
                            }
                            echo '<div class="checkbox" style="'.$visible.' ">
                                        <input type="checkbox" name="'.$value.'" id="'.$value.'" value="1">
                                        <label class="checkbox" for="'.$value.'">'. ucfirst($datacampo[1]).'</label>
                                    </div><br>';
                            break;
                       }
                ?>
                    
                <?php }else{ ?>
                    <div class="form-group demo">
                        
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label"><?=  $this->lang->line($value)!= ''?ucfirst($this->lang->line($value)):ucfirst($value); ?>:</label>
                        <input type="text"  class="form-control" name="<?= $value ?>" id="<?= $value ?>"  required>
                    </div>
                <?php }} ?>

                <!-- ------------ Agregamos los campos por defecto ------------ -->
                <?php foreach ($defaults as $key => $value) { ?>       
                        <input type="hidden" class="form-control" name = "<?= $key ?>" value="<?= $value ?>" >
                <?php } ?>
                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="button" id="btn_form" onclick="AgregarEncuesta()" class="btn btn-danger waves-effect waves-light">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script>
    if(lang=='spanish'||lang==''){
        var opciones = 'Agregue opciones';
    }else{
        var opciones = 'Add options';
    }
$("#data_options,#respuesta_b").select2({
    tags: true,
    tokenSeparators: [','],
    placeholder: opciones,
    multiple: true,
});

$(document).on('change','#data_options',function(){
    var datos = $(this).val();
    // console.log(datos);

    $('.respuesta_check #respuesta').empty().trigger("change");

    datos.forEach(function(element,index) {
        // console.log(element);
        var data = {
            id: element,
            text: element
        };

        // Set the value, creating a new option if necessary
        if (!$('.respuesta_check #respuesta').find("option[value='" + data.id + "']").length) 
        {
            // Create a DOM Option and pre-select by default
            var newOption = new Option(data.text, data.id, false, false);
            // Append it to the select
            $('.respuesta_check #respuesta').append(newOption).trigger('change');
        }
    });     
});

$(".respuesta_check #respuesta").select2({
    // tags: true,
    tokenSeparators: [','],
    placeholder: opciones,
    multiple: true,
});


</script>