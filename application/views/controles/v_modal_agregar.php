<!-- <pre> -->
<?php 

// print_r($defaults);
// print_r($tipos);
// print_r($row_names_edit);

// ----extraemos los campos visibles----
$visibles = explode(",", $campos_ver);
// print_r($visibles);

$camposVisibles = array();
foreach ($row_names_edit as $key => $value) {
    foreach ($visibles as $k => $v) {
        if($key==$v){
            $camposVisibles[]=$value;
        }
    }
}
 // print_r($camposVisibles);
 $tit = str_replace(" ", "", $titulo);

 if(!isset($funcion)){
    $funcion="";
 }
?>
<!-- </pre> -->

<script>
    var tabla = "<?= $tabla ?>";
</script>

<div id="modalAdd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><?= $this->lang->line('agregar'); ?></h4>
            </div>
            <div class="modal-body">
                <form id="form_add">
                <?php foreach ($camposVisibles as $key =>$value) { ?>

                <?php 
                    if($tipos != ""){
                        $tipo = array_key_exists($value, $tipos);
                    }else{
                        $tipo='';
                    }

                    if($tipo!=''){

                    $datacampo = explode(",", $tipos[$value]);

                    // print_r($datacampo);
                    $titu = $this->lang->line($value)!=''?ucfirst($this->lang->line($value)):ucfirst($value);
                       switch ($datacampo[0]) {
                            case 'number-no-required':
                               echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="number" class="form-control" name="'.$value.'" id="'.$value.'"  >
                                </div>';
                            break;
                            case 'email':
                               echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="email" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'password':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="password" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'date':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="date" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'number':
                                echo '<div class="form-group">';
                                echo '<label for="recipient-name" class="control-label">'. $this->lang->line($value)!= ''?ucfirst($this->lang->line($value)):ucfirst($value).':</label>';
                                echo '<input type="number" class="form-control" onblur="javascritpt: if($(this).val()>10){$(this).val(\'10\');}" name="'.$value.'" id="'.$value.'" ';
                                if(isset($datacampo[1])){
                                    echo 'max="'.$datacampo[1].'"';
                                }
                                if(isset($datacampo[2])){
                                    echo 'min="'.$datacampo[2].'"';
                                }
                                echo 'required>';
                                echo '</div>';
                                
                            break;
                            case 'phone':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="tel" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'textarea':
                                $titu = $this->lang->line($value)!=''?ucfirst($this->lang->line($value)):ucfirst($value);
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'.$titu.':</label>
                                    <textarea  class="form-control" name="'.$value.'" id="'.$value.'"  required></textarea>
                                </div>';
                            break;
                            case 'link':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'.$titu.':</label>
                                    <input type="tel" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'select':
                                $dataDel = array_shift($datacampo);
                                // print_r($datacampo);
                                // $parametros = "'".implode("','", $datacampo)."'";
                                crear_select($datacampo[0],$datacampo[1],$datacampo[2],$datacampo[3], $datacampo[4],$datacampo[5],$datacampo[6],$datacampo[7]);
                                
                            break;
                            case 'select2':
                                $dataDel = array_shift($datacampo);

                                // print_r($datacampo);
                                // $parametros = "'".implode("','", $datacampo)."'";
                                crear_select_vacio($datacampo[0],$datacampo[1],$datacampo[2],$datacampo[3], $datacampo[4],$datacampo[5],$datacampo[6],$datacampo[7]);
                                
                            break;
                            case 'checkbox':
                            echo '<div class="switch">
                            <label>OFF
                                        <input type="checkbox" name="'.$value.'" id="'.$value.'" value="1" ><span class="lever"></span>
                                        '.ucfirst($datacampo[1]).'</label>
                                </div><br>';

                            break;
                       }
                ?>
                    
                <?php }else{ ?>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label"><?=  $this->lang->line($value)!= ''?ucfirst($this->lang->line($value)):ucfirst($value); ?>:</label>
                        <input type="text" class="form-control" name="<?= $value ?>" id="<?= $value ?>"  required>
                        <!-- <input type="text" onkeyup="var start = this.selectionStart;var end = this.selectionEnd;this.value = this.value.toUpperCase();this.setSelectionRange(start, end);" class="form-control" name="<?= $value ?>" id="<?= $value ?>"  required> -->
                    </div>
                <?php }} ?>

                <!-- ------------ Agregamos los campos por defecto ------------ -->
                <?php foreach ($defaults as $key => $value) { ?>       
                        <input type="hidden" class="form-control" name = "<?= $key ?>" value="<?= $value ?>" >
                <?php } ?>
                    <input type="hidden" id="campo" name="campo" value="<?= $campo_id_tabla ?>">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="button" id="btn_form" onclick="var funcion = '<?= $funcion ?>'; Agregar(funcion);" class="btn btn-danger waves-effect waves-light">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script>
     $(document).on('change','#form_add .concat',function(){
        var id = $(this).val();
        // id_puesto
        var data = 'id_combo='+id;
        
        $.ajax({
            url: base_url+'index.php/Controles/cargarCombo',
            data: data,
            method: 'POST',
            beforeSend: function(){
                $('#form_add #id_puesto:visible').html('<option>Cargando...</option>');
                $('#form_add #id_puesto:visible').prop('disabled',true);
            },
            success: function(res){
                $('#form_add #id_puesto:visible').html('');
                $('#form_add #id_puesto:visible').prop('disabled',false);
                // $('#form_add .campo_concat:visible').html(res);  
                $('#form_add #id_puesto:visible').closest('.campo_concat').html(res);              
            }
        });
    });

    $(document).on('change','#form_add .concatDepto',function(){
        var id = $(this).val();
        // id_puesto
        var data = 'id_combo='+id;
        
        $.ajax({
            url: base_url+'index.php/Controles/cargarComboDepto',
            data: data,
            method: 'POST',
            beforeSend: function(){
                $('#form_add #id_depto:visible').html('<option>Cargando...</option>');
                $('#form_add #id_depto:visible').prop('disabled',true);
            },
            success: function(res){
                console.log(res);
                $('#form_add #id_depto:visible').html('');
                $('#form_add #id_depto:visible').prop('disabled',false);
                $('#form_add #id_depto:visible').closest('.campo_concat').html(res);               
            }
        });
    });
</script>