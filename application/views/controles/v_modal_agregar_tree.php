<!-- <pre> -->
<?php 

// print_r($defaults);
// print_r($tipos);
// print_r($row_names_edit);
if(isset($_GET['id_depto'])){
        $id_depto = $_GET['id_depto'];
    }else{
        $id_depto = "0";
    }
// ----extraemos los campos visibles----
$visibles = explode(",", $campos_ver);
// print_r($visibles);

$camposVisibles = array();
foreach ($row_names_edit as $key => $value) {
    foreach ($visibles as $k => $v) {
        if($key==$v){
            $camposVisibles[]=$value;
        }
    }
}
 // print_r($camposVisibles);
 $tit = str_replace(" ", "", $titulo);
?>
<!-- </pre> -->

<script>
    var tableTree = "<?= $tabla ?>";
</script>

<div id="modalAddtree" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><?= $this->lang->line('agregar'); ?></h4>
            </div>
            <div class="modal-body">
                <form id="form_add_tree">
                <?php foreach ($camposVisibles as $key =>$value) { ?>

                <?php 
                    if($tipos != ""){
                        $tipo = array_key_exists($value, $tipos);
                    }else{
                        $tipo='';
                    }

                    if($tipo!=''){

                    $datacampo = explode(",", $tipos[$value]);

                    // print_r($datacampo);
                       
                       switch ($datacampo[0]) {
                            case 'email':
                               echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="email" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'password':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="password" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'phone':
                                echo '<div class="form-group">
                                    <label for="recipient-name" class="control-label">'. ucfirst($value).':</label>
                                    <input type="tel" class="form-control" name="'.$value.'" id="'.$value.'"  required>
                                </div>';
                            break;
                            case 'select':
                                $dataDel = array_shift($datacampo);
                                // print_r($datacampo);
                                // $parametros = "'".implode("','", $datacampo)."'";
                                crear_select($datacampo[0],$datacampo[1],$datacampo[2],$datacampo[3], $datacampo[4]);
                                
                            break;
                       }
                ?>
                    
                <?php }else{ ?>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label"><?=  $this->lang->line($value)!= ''?ucfirst($this->lang->line($value)):ucfirst($value); ?>:</label>
                        <input type="text" onkeyup="var start = this.selectionStart;var end = this.selectionEnd;this.value = this.value.toUpperCase();this.setSelectionRange(start, end);" class="form-control" name="<?= $value ?>" id="<?= $value ?>"  required>
                    </div>
                <?php }} ?>

                <!-- ------------ Agregamos los campos por defecto ------------ -->
                <?php foreach ($defaults as $key => $value) { ?>       
                        <input type="hidden" class="form-control" name = "<?= $key ?>" value="<?= $value ?>" >
                <?php } ?>
                    <input type="hidden" class="form-control" name = "id_depto" id="combo_id_depto" value="<?= $id_depto ?>">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="button" id="btn_form" onclick="AgregarTree()" class="btn btn-danger waves-effect waves-light">Save changes</button>
            </div>
        </div>
    </div>
</div>

