<!-- <pre>
<?php  //print_r($rel_status) ?>     
</pre>   -->
 



<?php 


foreach ($data_table as $k ) { 

    $estatus = $k->id_estatus;

    switch ($estatus) {
        case '1':
            if($k->id_cat_encuesta==1 || $k->id_cat_encuesta==3){
                $color = "primary";
                $detalle= 'Detalle';
            }else{
                $detalle= 'Detalle_e';
                $color = "success"; 
            }
            $texto='Ver Detalle'; 
            $activar='Desactivar';
            $nstatus='Activa';
            break;
        case '8':
            if($k->id_cat_encuesta==1){
                $detalle= 'Detalle';
                $color = "inverse";
                $texto='Ver Detalle';
                $activar='Activar';
                $nstatus='Inactiva';
            }if($k->id_cat_encuesta==3){
                $detalle= 'Detalle';
                $color = "secondary";
                $texto='Editar detalle';
                $activar='Activar';
                $nstatus='Pendiente';
            }else if($k->id_cat_encuesta==2){
                $detalle= 'Detalle_e';
                $color = "inverse";
                $texto='Ver Detalle';
                $activar='Activar';
                $nstatus='Inactiva';
            }
            // $color = "inverse";
            // $texto='Ver Detalle';
            // $activar='Activar';
            // $nstatus='Inactiva';
            break;
        case '4':
            if($k->id_cat_encuesta==1 || $k->id_cat_encuesta==3){
                $detalle= 'Detalle';
            }else if($k->id_cat_encuesta==2){
                $detalle= 'Detalle_e';
            }
            $color = "secondary";
            $texto='Editar detalle';
            $activar='Activar';
            $nstatus='Pendiente';
            break;    
        
        default:
            # code...
            break;
    }

    if($k->id_cat_encuesta==3 && ($user['rol']==1 || $user['rol']==6)){

        // $estatusS = $k->status_sistema;
        foreach ($rel_status as $rel) {
            if($rel->id_encuesta==$k->id_encuesta){
                $estatusS = $rel->id_estatus;
                $id_encuestaS = $rel->id_encuesta;
            }
        }



        
        


        switch ($estatusS){
            case '1':
                if($k->id_cat_encuesta==1 || $k->id_cat_encuesta==3 ){
                    $color = "primary";
                    $detalle= 'Detalle';
                }else if($k->id_cat_encuesta==2){
                    $detalle= 'Detalle_e';
                    $color = "success"; 
                }
                $texto='Ver Detalle'; 
                $activar='Desactivar';
                $nstatus='Activa';
                break;
            case '8':
                if($k->id_cat_encuesta==1){
                    $detalle= 'Detalle';
                    $color = "inverse";
                    $texto='Ver Detalle';
                    $activar='Activar';
                    $nstatus='Inactiva';
                }if($k->id_cat_encuesta==3 ){
                    $detalle= 'Detalle';
                    $color = "secondary";
                    $texto='Editar detalle';
                    $activar='Activar';
                    $nstatus='Pendiente';
                }else if($k->id_cat_encuesta==2){
                    $detalle= 'Detalle_e';
                    $color = "inverse";
                    $texto='Ver Detalle';
                    $activar='Activar';
                    $nstatus='Inactiva';
                }
                // $color = "inverse";
                // $texto='Ver Detalle';
                // $activar='Activar';
                // $nstatus='Inactiva';
                break;
                case '':
                    if($k->id_cat_encuesta==1){
                        $detalle= 'Detalle';
                        $color = "inverse";
                        $texto='Ver Detalle';
                        $activar='Activar';
                        $nstatus='Inactiva';
                    }if($k->id_cat_encuesta==3){
                        $detalle= 'Detalle';
                        $color = "secondary";
                        $texto='Editar detalle';
                        $activar='Activar';
                        $nstatus='Pendiente';
                    }else if($k->id_cat_encuesta==2){
                        $detalle= 'Detalle_e';
                        $color = "inverse";
                        $texto='Ver Detalle';
                        $activar='Activar';
                        $nstatus='Inactiva';
                    }
                    // $color = "inverse";
                    // $texto='Ver Detalle';
                    // $activar='Activar';
                    // $nstatus='Inactiva';
                    break;
        }
    }
    
?>

<style>
    .card-outline-secondary .card-header{
        background-color: #99abb4 !important;
    }

    .activo {
        margin-right: 40px;
        position: absolute;
        right: 20px;
        top: 13px;
        font-weight: normal;
        color: white;
        font-size: 1em;
    }
    
</style>

<?php if(($k->id_cat_encuesta ==3 && $k->id_estatus!=4 ) || $k->id_cat_encuesta ==1 || $k->id_cat_encuesta ==2){ ?>
<div class="col-md-6">

    
        <div class="card card-outline-<?= $color; ?>">
            <div class="card-header">   
            
                <h4 class="m-b-0 text-white" style="max-width: 70%;"><?= $k->titulo ?></h4><small class="pull-right activo" style="margin-right: 40px;"><?= $this->lang->line(strtolower($nstatus)); ?></small></div>
                <div class="card-body">
                <?php if($k->id_cat_encuesta ==3){ ?>
                    <span class="badge badge-pill badge-info pull-right"><?= $this->lang->line('sistema'); ?></span>
                    <?php } ?>
                    <?php if($k->observacion ==1){ ?>
                    <span class="badge badge-pill badge-info pull-right"><?= $this->lang->line('observacion'); ?></span>
                    <?php } ?>
                    <h5 class="card-title"><?= $k->fecha ?></h5>
                    <input type="hidden" name="status" id="status" value="<?= $k->id_estatus ?>">
                <?php if($k->observacion !=""){ ?>
                    <input type="hidden" name="observacion_val" id="observacion_val" value="<?= $k->observacion ?>">
                <?php } ?>
                <?php if($k->obligatorio !=""){ ?>
                    <input type="hidden" name="obligatorio_val" id="obligatorio_val" value="<?= $k->obligatorio ?>">
                <?php } ?>
                <?php if($k->calificacion !=""){ ?>
                    <input type="hidden" name="calificacion_val" id="calificacion_val" value="<?= $k->calificacion ?>">
                <?php } ?>
                <?php if($k->link_elearning !=""){ ?>
                    <input type="hidden" name="link" id="link" value="<?= $k->link_elearning ?>">
                    <iframe style="width: 100%;min-height: 300px;" src="<?= $k->link_elearning ?>" frameborder="0"></iframe>
                <?php } ?>
                    <p class="card-text"><?= ucfirst(strtolower($k->descripcion)) ?></p>
                <?php if($k->id_cat_encuesta !=3 ){ ?>
                    <a href="#<?= $k->id_encuesta ?>" rel='<?= $tabla ?>' class="btn btn-<?= $color ?> editar"><i class="fa fa-edit"></i></a>
                <?php } ?>
                <?php if($k->id_cat_encuesta ==3 && $user['rol']==6 ){ ?>
                    <a href="#<?= $k->id_encuesta ?>" rel='<?= $tabla ?>' class="btn btn-<?= $color ?> editar"><i class="fa fa-edit"></i></a>
                <?php } ?>
                
                <?php if(($user['rol']==1||$user['rol']==6) && $k->id_cat_encuesta !=3){ ?>
                    <a href="<?= base_url() ?>index.php/Modulos/<?= $detalle ?>/?id_encuesta=<?= $k->id_encuesta ?>"  class="btn btn-<?= $color ?>" ><?= $this->lang->line(strtolower(clean($texto))); ?></a>
                <?php } ?>

                <?php if($user['rol']==6 && $k->id_cat_encuesta ==3){ ?>
                    <a href="<?= base_url() ?>index.php/Modulos/<?= $detalle ?>/?id_encuesta=<?= $k->id_encuesta ?>"  class="btn btn-<?= $color ?>" ><?= $this->lang->line(strtolower(clean($texto))); ?></a>
                <?php } ?>

                    
                <?php if($k->preguntas > 0 && ($user['rol']==1 || $user['rol']==6) && $k->id_cat_encuesta !=3){ ?>    
                    <a href="#<?= $k->id_encuesta ?>" rel="<?= $tabla ?>" alt="<?= $estatus!=""? $estatus:8 ?>" class="btn btn-<?= $color ?>  btn-rounded pull-right desactivar" data="<?= $estatus ?>" >
                        <?= $this->lang->line(strtolower($activar)) ?>
                    </a>
                <?php } ?> 

                <?php if($k->id_cat_encuesta == 3 && ($user['rol']==1 || $user['rol']==6)){ ?>    
                    <a href="#<?= $k->id_encuesta ?>" rel="rel_encuesta_company" alt="<?= $estatusS!=""?$estatusS:8 ?>" class="btn btn-<?= $color ?> btn-rounded pull-right desactivarS"  >
                        <?= $this->lang->line(strtolower($activar)) ?>
                    </a>
                <?php } ?> 
                 
            </div>
        </div>
        
    </div>
    <?php } ?>
    <?php if($k->id_cat_encuesta==2){ $t_encuesta = $k->id_cat_encuesta; } ?>
<?php } ?>




