<?php 
// $folio 
// $finicio 
// $ffin 
// $plan
$subtotal = $monto;
$iva = $monto*.16;
$montoIva = $subtotal+$iva;
 ?>
<html>
<head>
  <meta charset='UTF-8'>
  <title>Document</title>
</head>
<body>
  <br />
  <div style='height:auto; border: none 2px lightgray; font-family:sans-serif;padding: 40px;' ><br />
    <img src="<?= base_url() ?>public/images/logo.png" >
    <br><br>
    <?= $this->lang->line('hola') ?> <?= $nombre ?>,
    <br>
    <h2><?= $this->lang->line('hola') ?> <?= $this->lang->line('por_registrarse') ?></h2>
    <br>
    <span style="color: #000;font-weight: lighter; font-size: 1.1em;"><?= $this->lang->line('recibo') ?><?= $folio ?>. <br><?= $this->lang->line('recibo_activar') ?></span>
    <br><br><br><br>
    <hr>
    <br><br>
    <span style="color: #000;font-weight: lighter;"><?= $this->lang->line('info_recibo') ?>:</span>
    <br><br>
    <?= $this->lang->line('folio') ?>: <?= $folio ?> <br>
    <?= $this->lang->line('fecha_inicio') ?>: <?= $finicio ?> <br>
    <?= $this->lang->line('fecha_vencimiento') ?>: <?= $ffin ?> <br>
    <?= $this->lang->line('servicios') ?>: <?= $plan ?> <br>
    <?= $this->lang->line('subtotal') ?>: $<?= number_format($subtotal,2) ?>  MXN <br>
    <?= $this->lang->line('iva') ?>: $<?= number_format($iva,2) ?>  MXN <br>
    <?= $this->lang->line('total') ?>: $<?= number_format($montoIva,2) ?>  MXN <br><br><br>
    <span style="color: #000;font-weight: lighter;"><?= $this->lang->line('ingresa_email') ?></span>
    <p class="text-center hidden-print">
            <a style="display: inline-block;background-color: #e15516; padding: 20px 40px; color:white; text-decoration: none; border-radius: 60px; box-shadow: 1px 1px 10px #ccc;" href="<?= base_url() ?>">Login</a></a>
        </p>
    <br><br>
    <br>
    <hr>
    <br><br><br><strong>Entrevistando MX</strong><br> 
    <h3>Tu opinión vale.</h3>
  </div>
</body>
</html>

