<link href="<?= base_url() ?>public/assets/plugins/nestable/nestable.css" rel="stylesheet" type="text/css" />

<!-- <pre> -->
<?php 
// print_r($data_table); 
if(isset($_GET['id_depto'])){
        $id_depto = $_GET['id_depto'];
    }else{
        $id_depto = "0";
    }

if(!empty($data_table)){
    $tree = crear_array_tree($data_table,$campo_id);
}
 
?>
<!-- </pre> -->

<style>
    a.deleteTree.pull-right {
        display: block;
        color: red;
        position: absolute;
        right: 17px;
        top: 9px;
    }
</style>
<!-- <div class="row"> -->
   
    <!-- <div class="col-lg-8 col-md-12"> -->
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?= $titulo ?></h4>
                <div class="form-group combo_select">
                    <!-- <select name="" id="" class="form-control">
                        <option value="0">Seleccione un puesto</option>
                        <option value=""></option>
                    </select> -->
                    <?php // ------ Datos para Generador de select funcion, crear_select() -------
                    $cond=' id_company='.$user['empresa']->id_company;
                    $combo = "select,Roles,cat_roles,".$cond.",id_role,rol_nombre,0"; 

                    crear_select($this->lang->line('departamentos'),'tbl_departamentos',$cond,'id_depto','departamento',0,$id_depto);
                ?>
                </div>
                <div class="dd myadmin-dd" id="nestable-menu">
                    
                    <ol class="dd-list">
                    <?php if(!empty($data_table)){ ?>
                    <?php foreach ($tree as $key) {  ?>
                        <li class="dd-item" data-id="<?= $key['id'] ?>">

                            <div class="dd-handle"><?= $key['nombre'] ?></div>
                            <a href="#" class="deleteTree pull-right" id="<?= $key['id'] ?>">x</a>
                        <?php if(isset($key['children'])){ ?>
                            <ol class="dd-list">
                            <?php foreach ($key['children'] as $k) { ?>
                                <li class="dd-item" data-id="<?= $k['id'] ?>">

                                    <div class="dd-handle"><?= $k['nombre'] ?></div>
                                    <a href="#" class="deleteTree pull-right" id="<?= $k['id'] ?>">x</a>
                                    <?php if(isset($k['children'])){ ?>
                                        <ol class="dd-list">
                                            <?php foreach ($k['children'] as $j) { ?>
                                                <li class="dd-item" data-id="<?= $j['id'] ?>">

                                                    <div class="dd-handle"><?= $j['nombre'] ?></div>
                                                    <a href="#" class="deleteTree pull-right" id="<?= $j['id'] ?>">x</a>
                                                    <?php if(isset($j['children'])){ ?>
                                                        <ol class="dd-list">
                                                           <?php foreach ($j['children'] as $l) { ?> 
                                                            <li class="dd-item" data-id="<?= $l['id'] ?>">

                                                                <div class="dd-handle"><?= $l['nombre'] ?></div>
                                                                <a href="#" class="deleteTree pull-right" id="<?= $l['id'] ?>">x</a>
                                                                <?php if(isset($l['children'])){ ?>
                                                                <ol class="dd-list">
                                                                    <?php foreach ($l['children'] as $m) { ?> 
                                                                    <li class="dd-item" data-id="<?= $m['id'] ?>">

                                                                        <div class="dd-handle"><?= $m['nombre'] ?></div>
                                                                        <a href="#" class="deleteTree pull-right" id="<?= $m['id'] ?>">x</a>
                                                                    </li>
                                                                    <?php } ?>
                                                                </ol>
                                                                <?php } ?> 

                                                            </li>
                                                            <?php } ?>
                                                            
                                                        </ol>
                                                    <?php } ?>   
                                                </li>
                                            <?php } ?>
                                        </ol>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                            </ol>
                        <?php } ?>
                        </li>
                    <?php }  ?>
                    <?php }  ?>
                    </ol>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success btn-block accion"  data-toggle="modal" data-target="#modalAddtree"> <?= $this->lang->line('agregar') ?></button>
            </div>
        </div>
    <!-- </div> -->
    
<!-- </div> -->

<?php 
    $campos_ver='2';
    $defaults = array(
        'id_company'=> $user['empresa']->id_company,
        'id_padre'=>0,
        'nivel'=>0
    ); // Array con los campos por default del form
    crear_modal_agregar_tree('Agregar',$tabla,$campos_ver,$defaults,'');
?>

<script src="<?= base_url() ?>public/assets/plugins/nestable/jquery.nestable.js"></script>
<script>
    var table = '<?= $tabla ?>';
    var campo_id = '<?= $campo_id ?>';
    $('#nestable-menu').nestable();
    $('.dd').on('change', function() {
        var datos = $('.dd').nestable('serialize');
        var tabla = '<?= $tabla ?>';
        var campo_id = '<?= $campo_id ?>';
        $.ajax({
           type: "POST",
           data: {tabla: tabla, campo: campo_id, data:datos},
           url: base_url+"index.php/Controles/UPD_Tree",
           success: function(res){
             // $('.answer').html(msg);
             console.log(res);
           }
        });
    });
    $(document).on('change','.combo_select #id_depto',function(){
        var id = $(this).val();
        // $('#combo_id_depto:hidden').val(id);
        // alert(id);
        var ruta = window.location.href;

        var res = ruta.replace("&id_depto=", "");

        var newStr = res.substr(0, res.length-1);

         // alert(res);
         window.location.href=newStr+'&id_depto='+id;

    });

    // var id = $('#id_depto').val();
    // $('#combo_id_depto:hidden').val(id);

</script>