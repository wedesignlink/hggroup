
<!-- ------------ combo automático --------------------- -->
<?php 
	if($titulo_campo !=''){
		$titulo_campo = $titulo_campo;
	}else{
	 	$titulo_campo= $campo_id;
	}
?>
<div class="form-group">
    <label for="select"><?= $this->lang->line(strtolower($titulo)) ?></label>
    <select class="form-control <?= $size ?> <?= $concat ?>" name="<?= $titulo_campo; ?>" id="<?= $titulo_campo ?>" <?php if($requerido==1){ echo "required"; } ?> >
    <?php if($default_text==""){ ?>
        <option value="<?= $default ?>"><?= $this->lang->line('seleccione'); ?></option>
    <?php }else { ?>
        <option value="<?= $default ?>"><?= $default_text; ?></option>
    <?php } ?>
        <?php foreach ($select as $key ) { ?>
            <option value="<?= $key->$campo_id ?>" <?php if($selected==$key->$campo_id){ echo "selected"; } ?>  > <?= $key->$campo_nombre ?></option>
        <?php } ?>
    </select>
</div>
<!-- ------------ FIN combo automático --------------------- -->