<!-- <pre>
<?php //print_r($rel_status); ?>     
</pre>    -->




<?php 

// if(($user['rol']==2 || $user['rol']==3 )){


$calificado = array();

$search="";
foreach ($data_table as $k ) { 
    if($k->id_cat_encuesta==2){ 
    foreach ($calificacion as $key ) {

        if($k->id_encuesta == $key->id_encuesta ){
            
            if(!isset($calificado[$k->id_encuesta])){
                // echo "entro=".$k->id_encuesta;
                $calificado[$k->id_encuesta] = $key->promedio;
            }
        }
    }
    }
}
// }

// $estatusS = count($rel_status)>0?$rel_status[0]->id_estatus:8;


//  print_r($data_table);
foreach ($data_table as $k ) { 

    $estatus = $k->id_estatus;

    switch ($estatus) {
        case '1':
            if($k->id_cat_encuesta==1){
                $color = "primary";
                $detalle= 'Detalle';
            }elseif ($k->id_cat_encuesta==3){
                $detalle= 'Detalle';
                $color = "danger"; 
            }else{
                $detalle= 'Detalle_e';
                $color = "success"; 
            }
            $texto='Ver Detalle'; 
            $activar='Desactivar';
            $nstatus='Activa';
            break;
        case '8':
            if($k->id_cat_encuesta==1){
                $detalle= 'Detalle';
            }else{
                $detalle= 'Detalle_e';
            }
            $color = "inverse";
            $texto='Ver Detalle';
            $activar='Activar';
            $nstatus='Inactiva';
            break;
        case '4':
            if($k->id_cat_encuesta==1){
                $detalle= 'Detalle';
            }else{
                $detalle= 'Detalle_e';
            }
            $color = "secondary";
            $texto='Editar detalle';
            $activar='Activar';
            $nstatus='Pendiente';
            break;    
        
        default:
            # code...
            break;
    }

    foreach ($rel_status as $rel) {
        if($rel->id_encuesta==$k->id_encuesta){
            $estatusS = $rel->id_estatus;
            $id_encuestaS = $rel->id_encuesta;
        }
    }
    // echo "encuesta=".$k->id_encuesta."<br>";
    // echo "status rel = ".$estatusS."<br>";
    // echo "encuesta rel = ".$id_encuestaS."<br>";
?>

<style>
    .card-outline-secondary .card-header{
        background-color: #99abb4 !important;
    }
    
</style>

<?php if($k->id_cat_encuesta!=3 ){ ?>


<div class="col-md-6">

        <div class="card card-outline-<?= $color; ?>">
            <div class="card-header">
                <?php
                // print_r($key->promedio);
                $calificacionK = number_format($k->calificacion*.1,2);

                if($k->id_cat_encuesta==2){ 
                foreach ($calificacion as $key) {
                    
                if($k->id_cat_encuesta==2 && $key->id_encuesta == $k->id_encuesta && $key->promedio > $calificacionK){ ?>
                    <div class="ribbon ribbon-bookmark ribbon-vertical-r ribbon-primary"><i class="fa fa-star"></i></div>
                
                <?php }}} ?>

                <h4 class="m-b-0 text-white"><?= $k->titulo ?> <small class="pull-right" style="margin-right: 40px;"><?= $this->lang->line(strtolower($nstatus)); ?></small></h4></div>
                <div class="card-body">

                    <h5 class="card-title"><?= $k->fecha ?></h5>
                    <?php if($k->id_cat_encuesta==3){ ?>
                    <span class="badge badge-pill badge-info pull-right"><?= $this->lang->line('sistema'); ?></span>
                    <?php } ?>
                    <?php if($k->observacion ==1){ ?>
                    <span class="badge badge-pill badge-info pull-right"><?= $this->lang->line('observacion'); ?></span>
                    <?php } ?>
                <?php if($k->obligatorio !=""){ ?>
                    <input type="hidden" name="obligatorio_val" id="obligatorio_val" value="<?= $k->obligatorio ?>">
                <?php } ?>
                <?php if($k->calificacion !=""){ ?>
                    <input type="hidden" name="calificacion_val" id="calificacion_val" value="<?= $k->calificacion ?>">
                <?php } ?>
                <?php if($k->link_elearning !=""){ ?>
                    <input type="hidden" name="link" id="link" value="<?= $k->link_elearning ?>">
                    <iframe style="width: 100%;min-height: 300px;" src="<?= $k->link_elearning ?>" frameborder="0"></iframe>
                <?php } ?>
                    <p class="card-text"><?= ucfirst(strtolower($k->descripcion)) ?></p>
                <?php if($estatus==4){ ?>
                    <a href="#<?= $k->id_encuesta ?>" rel='<?= $tabla ?>' class="btn btn-<?= $color ?> editar"><i class="fa fa-edit"></i></a>
                <?php } ?>
                <?php if( $k->id_cat_encuesta==1 || $k->id_cat_encuesta==3){ ?>
                    <a href="<?= base_url() ?>index.php/Observacion/Responder/<?= $k->id_encuesta ?>/<?= $k->titulo ?>"  class="btn btn-success" ><?= $this->lang->line('responder') ?></a>
                <?php } ?>
                <?php 
                if($k->id_cat_encuesta==2 ){ 
                foreach ($calificacion as $key ) {
                if($k->id_cat_encuesta==2 && $key->id_encuesta == $k->id_encuesta && $key->promedio >= $calificacionK){ 

                    $calif = $key->promedio*100;
                    ?>
                <!-- <div class="progress m-t-20">
                                    <div class="progress-bar bg-success" style="width: 90%; height:15px;" role="progressbar">90%</div>
                                </div> -->
                    <div style="text-align: center;">
                        <div class="chart easy-pie-chart-4" data-percent="<?= $calif ?>" data-scale-color="#ffb400">
                            <span style="position: absolute;top: 42%;left: 46%;"><?= $calif ?></span>
                        </div> 
                        <?php if($key->promedio >= $calificacionK){ ?>
                            <p class="text-success"><i class="fa fa-check"></i> <?= $this->lang->line("curso_aprobado"); ?> </p> 
                        <?php } ?>
                    </div>

                    
                    <script>
                        $(function() {
                            $('.chart').easyPieChart({
                                easing: 'easeOutBounce',
                                barColor : '#13dafe',
                                lineWidth: 3,
                                scaleColor: 'false',
                                onStep: function(from, to, percent) {
                                    $(this.el).find('span').text(Math.round(percent));
                                }
                            });
                        });
                    </script>

                <?php }}} ?> 

                <?php

                    $buscar = array_key_exists($k->id_encuesta, $calificado);

                    // echo $buscar;
                    if($k->id_cat_encuesta==2){ 
                        // echo "entro";
                        // echo "encuesta=".$k->id_encuesta;
                        // echo "calificado=".$calificado[$k->id_encuesta];
                        if(!isset($calificado[$k->id_encuesta])){ ?>
                        <a href="<?= base_url() ?>index.php/Curso/Responder/<?= $k->id_encuesta ?>/<?= $k->titulo ?>"  class="btn btn-success btn-block" ><?= $this->lang->line('responder') ?></a>
                        <?php }else{
                        if($calificado[$k->id_encuesta] < $calificacionK || $calificado[$k->id_encuesta]=="" || $buscar==false ){ ?>
                    <a href="<?= base_url() ?>index.php/Curso/Responder/<?= $k->id_encuesta ?>/<?= $k->titulo ?>"  class="btn btn-success btn-block" ><?= $this->lang->line('responder') ?></a>
                <?php   }} }  ?>
                    
                 
            </div>
        </div>
    </div>
    <?php }else if($k->id_cat_encuesta==3 && $estatusS==1 && $id_encuestaS==$k->id_encuesta){ ?>
        <div class="col-md-6">

        <div class="card card-outline-<?= $color; ?>">
            <div class="card-header">
                <?php
                if($k->id_cat_encuesta==2){ 
                foreach ($calificacion as $key) {

                if($k->id_cat_encuesta==2 && $key->id_encuesta == $k->id_encuesta && $key->promedio >$calificacionK ){ ?>
                    <div class="ribbon ribbon-bookmark ribbon-vertical-r ribbon-primary"><i class="fa fa-star"></i></div>
                
                <?php }}} ?>

                <h4 class="m-b-0 text-white"><?= $k->titulo ?> <small class="pull-right" style="margin-right: 40px;"><?= $this->lang->line(strtolower($nstatus)); ?></small></h4></div>
                <div class="card-body">

                    <h5 class="card-title"><?= $k->fecha ?></h5>
                    <?php if($k->id_cat_encuesta==3){ ?>
                    <span class="badge badge-pill badge-info pull-right"><?= $this->lang->line('sistema'); ?></span>
                    <?php } ?>
                    <?php if($k->observacion ==1){ ?>
                    <span class="badge badge-pill badge-info pull-right"><?= $this->lang->line('observacion'); ?></span>
                    <?php } ?>
                <?php if($k->obligatorio !=""){ ?>
                    <input type="hidden" name="obligatorio_val" id="obligatorio_val" value="<?= $k->obligatorio ?>">
                <?php } ?>
                <?php if($k->calificacion !=""){ ?>
                    <input type="hidden" name="calificacion_val" id="calificacion_val" value="<?= $k->calificacion ?>">
                <?php } ?>
                <?php if($k->link_elearning !=""){ ?>
                    <input type="hidden" name="link" id="link" value="<?= $k->link_elearning ?>">
                    <iframe style="width: 100%;min-height: 300px;" src="<?= $k->link_elearning ?>" frameborder="0"></iframe>
                <?php } ?>
                    <p class="card-text"><?= ucfirst(strtolower($k->descripcion)) ?></p>
                <?php if($estatus==4){ ?>
                    <a href="#<?= $k->id_encuesta ?>" rel='<?= $tabla ?>' class="btn btn-<?= $color ?> editar"><i class="fa fa-edit"></i></a>
                <?php } ?>
                <?php if( $k->id_cat_encuesta==1 || $k->id_cat_encuesta==3){ ?>
                    <a href="<?= base_url() ?>index.php/Observacion/Responder/<?= $k->id_encuesta ?>/<?= $k->titulo ?>"  class="btn btn-success" ><?= $this->lang->line('responder') ?></a>
                <?php } ?>
                <?php 
                if($k->id_cat_encuesta==2 ){ 
                foreach ($calificacion as $key ) {
                if($k->id_cat_encuesta==2 && $key->id_encuesta == $k->id_encuesta && $key->promedio >=$calificacionK){ 

                    $calif = $key->promedio*100;
                    ?>
                <!-- <div class="progress m-t-20">
                                    <div class="progress-bar bg-success" style="width: 90%; height:15px;" role="progressbar">90%</div>
                                </div> -->
                    <div style="text-align: center;">
                        <div class="chart easy-pie-chart-4" data-percent="<?= $calif ?>" data-scale-color="#ffb400">
                            <span style="position: absolute;top: 42%;left: 46%;"><?= $calif ?></span>
                        </div> 
                        <?php if($key->promedio >= $calificacionK){ ?>
                            <p class="text-success"><i class="fa fa-check"></i> <?= $this->lang->line("curso_aprobado"); ?> </p> 
                        <?php } ?>
                    </div>

                    
                    <script>
                        $(function() {
                            $('.chart').easyPieChart({
                                easing: 'easeOutBounce',
                                barColor : '#13dafe',
                                lineWidth: 3,
                                scaleColor: 'false',
                                onStep: function(from, to, percent) {
                                    $(this.el).find('span').text(Math.round(percent));
                                }
                            });
                        });
                    </script>

                <?php }}} ?> 

                <?php

                    $buscar = array_key_exists($k->id_encuesta, $calificado);

                    // echo $buscar;
                    if($k->id_cat_encuesta==2){ 
                        // echo "entro";
                        // echo "encuesta=".$k->id_encuesta;
                        // echo "calificado=".$calificado[$k->id_encuesta];
                        if(!isset($calificado[$k->id_encuesta])){ ?>
                        <a href="<?= base_url() ?>index.php/Curso/Responder/<?= $k->id_encuesta ?>/<?= $k->titulo ?>"  class="btn btn-success btn-block" ><?= $this->lang->line('responder') ?></a>
                        <?php }else{
                        if($calificado[$k->id_encuesta] < $calificacionK || $calificado[$k->id_encuesta]=="" || $buscar==false ){ ?>
                    <a href="<?= base_url() ?>index.php/Curso/Responder/<?= $k->id_encuesta ?>/<?= $k->titulo ?>"  class="btn btn-success btn-block" ><?= $this->lang->line('responder') ?></a>
                <?php   }} }  ?>
                    
                 
            </div>
        </div>
    </div>
    <?php } ?>
<?php } ?>

