<?php if($this->session->flashdata('mensaje')!=""){ ?>
         <script>
            swal({
                title: "Ok!",
                text: "<?= $this->session->flashdata('mensaje') ?>",
                type: "success"
            });
        </script>
        <?php } ?>
        <?php if($this->session->flashdata('mensajeError')!=""){ ?>
         <script>
            swal({
                title: "Error!",
                text: "<?= $this->session->flashdata('mensajeError') ?>",
                type: "warning"
            });
        </script>
<?php } ?>