<link href="<?= base_url() ?>public/assets/plugins/nestable/nestable.css" rel="stylesheet" type="text/css" />

<!-- <pre> -->
<?php 
// print_r($master); 

if(isset($_GET['id_encuesta'])){
        $id_encuesta = $_GET['id_encuesta'];
    }else{
        $id_encuesta = "0";
    }

if(isset($_GET['id_seccion'])){
        $id_seccion = $_GET['id_seccion'];
    }else{
        $id_seccion = "0";
    }

// if(!empty($data_table)){
//     $tree = crear_array_tree($data_table,$campo_id);
// }
 
?>
<!-- </pre> -->

<style>
    a.deleteTree.pull-right {
        display: block;
        color: red;
        position: absolute;
        right: 20px;
        top: 9px;
    }
    a.editTree.pull-right {
        display: block;
        color: red;
        position: absolute;
        right: 40px;
        top: 10px;
    }
</style>
<!-- <div class="row"> -->
   
    <!-- <div class="col-lg-8 col-md-12"> -->
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?= $titulo ?></h4>
                <div class="form-group combo_select">
                    <!-- <select name="" id="" class="form-control">
                        <option value="0">Seleccione un puesto</option>
                        <option value=""></option>
                    </select> -->
                    <?php // ------ Datos para Generador de select funcion, crear_select() -------
                    $cond='id_encuesta='.$id_encuesta;
                    $combo = "select,Roles,cat_roles,".$cond.",id_role,rol_nombre,0"; 

                    crear_select($this->lang->line('secciones'),'cat_secciones_encuesta',$cond,'id_seccion','nombre',0,$id_seccion);
                ?>
                </div>
                <div class="dd myadmin-dd" id="nestable-menu">
                    
                    <ol class="dd-list">
                    <?php if(!empty($data_table)){ ?>
                    <?php foreach ($data_table as $key) {  ?>
                        <li class="dd-item" data-id="<?= $key->id_pregunta; ?>">

                            <div class="dd-handle"><?= $key->label ?></div>
                            <!-- <input type="text" placeholder="<?= $key->placeholder ?>" disabled=""> -->
                        <?php if($master->id_estatus==4 || $master->id_cat_encuesta==3){ ?>
                            <!-- <a href="#" class="editTree pull-right" id="<?= $key->id_pregunta; ?>"><i class="fa fa-edit"></i>
                            </a> -->
                            <a href="#" class="deleteTree pull-right" id="<?= $key->id_pregunta; ?>">x</a>
                        <?php } ?>
                        </li>
                    <?php }  ?>
                    <?php }  ?>
                    </ol>
                </div>
            </div>
            
            
        <?php if($id_seccion!=0 && ($master->id_estatus==4  || $master->id_cat_encuesta==3)){ ?>
            <div class="card-footer">
                <button class="btn btn-success btn-block accion"  data-toggle="modal" data-target="#modalAddtree"> <?= $this->lang->line('agregar') ?></button>
            </div>
        <?php } ?>
        </div>
    <!-- </div> -->
    
<!-- </div> -->

<?php 
    $campos_ver='2,4,5,6,7,8,11';
    $defaults = array(
        'id_encuesta'=> $id_encuesta,
        'id_seccion'=>$id_seccion
    ); // Array con los campos por default del form

    $condicion="id_tipo_pregunta = 1 or id_tipo_pregunta = 2 or id_tipo_pregunta = 3 or id_tipo_pregunta = 4 or id_tipo_pregunta = 6 or id_tipo_pregunta = 7 ";

    $combo = 'select_trans,'.$this->lang->line('tipo_campo').',cat_tipo_pregunta,'.$condicion.',id_tipo_pregunta,tipo_elemento_web, 0, 0, 0,id_tipo,1';
    // crear_select($titulo='',$tabla='',$condicion='',$campo_id='',$campo_nombre='',$default='',$selected='',$concat='',$titulo_campo='',$requerido='',$default_text='')

    $tipos = array(
        'id_tipo'=> $combo,
        'data_options'=>'no_required',
        'valida'=>'checkbox,'.$this->lang->line('obligatorio').',1',
        'estadistico'=>'checkbox,'.$this->lang->line('estadistico').',0'
    );
    crear_modal_agregar_encuesta($this->lang->line('agregar'),$tabla,$campos_ver,$defaults,$tipos);
?>

<script src="<?= base_url() ?>public/assets/plugins/nestable/jquery.nestable.js"></script>
<script>
    var table = '<?= $tabla ?>';
    var campo_id = '<?= $campo_id ?>';
    $('#nestable-menu').nestable({
        maxDepth: 1
    });

    $('.dd').on('change', function() {
        var datos = $('.dd').nestable('serialize');
        var tabla = '<?= $tabla ?>';
        var campo_id = '<?= $campo_id ?>';

        console.log(datos);
        $.ajax({
           type: "POST",
           data: {tabla: tabla, campo: campo_id, data:datos},
           url: base_url+"index.php/Controles/UPD_Tree",
           success: function(res){
             // $('.answer').html(msg);
             console.log(res);
           }
        });
    });

    $(document).on('change','.combo_select #id_seccion',function(){
        var id = $(this).val();
        // $('#combo_id_depto:hidden').val(id);
        // alert(id);
        var ruta = base_url+'index.php/Modulos/Detalle/';
        var id_seccion ='<?= $id_seccion ?>';

        if(id_seccion!='0'){
            var newStr = ruta+'?id_encuesta='+'<?= $id_encuesta ?>';
        }else{
            var newStr='?id_encuesta='+'<?= $id_encuesta ?>';
        }

        // alert(res);
        window.location.href=newStr+'&id_seccion='+id;

    });

    // var id = $('#id_depto').val();
    // $('#combo_id_depto:hidden').val(id);

</script>