<?php $this->lang->load('store_detail',$this->session->userdata('site_lang'));?>
<style>
    .carousel-item-next,
    .carousel-item-prev,
    .carousel-item.active {
        display: block;
    }

    .xzoom-gallery {
        margin-top: 10px
    }

    .xzoom {
        margin-top: 40px
    }
    .input-group-append {
    margin-left: -1px;
    display: flex;
    }
</style>
<section class="bg-white cover-background padding-110px-tb feature-style3 xs-padding-60px-tb tz-builder-bg-image" data-img-size="(W)1920px X (H)750px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?= base_url() ?>public/images/theme/uploads/bbq.jpg');">
    <div class="container" style="background-color: black;"></div>
</section>
<section class="bg-white builder-bg padding-40px-tb feature-style3 xs-padding-60px-tb" id="feature-section32">
    <div class="container">
        <div class="tracing">
            <a href="<?= base_url(); ?>index.php/Store/catalog"><b>Store</b> </a> > Detail
        </div>
        <div class="row four-column">
            <div class="col-md-6">
                
                <script type="text/javascript" src="https://unpkg.com/xzoom/dist/xzoom.min.js"></script>
                <link rel="stylesheet" type="text/css" href="https://unpkg.com/xzoom/dist/xzoom.css" media="all" />
                
                <?php if ($images == null):  ?>
                    <div class="xzoom-container" style="width: 90%;">
                        <img class="xzoom" id="xzoom-default" style="width: 90%;" src="<?=  base_url('public/uploads/productos/default.png' ) ?>" xoriginal="<?=  base_url('public/uploads/productos/default.png' ) ?>" />
                        <div class="xzoom-thumbs">
                            <a href="<?=  base_url('public/uploads/productos/default.png' ) ?>">
                                <img class="xzoom-gallery" width="80" src="<?=  base_url('public/uploads/productos/default.png' ) ?>" xpreview="<?=  base_url('public/uploads/productos/default.png' ) ?>">
                            </a>
                        </div>
                    </div>
                <?php else:?>
                    <div class="xzoom-container" style="width: 90%;">
                        <img class="xzoom" id="xzoom-default" style="width: 90%;" src="<?=  base_url('public/uploads/productos/'. $images[0]['imagen'] ) ?>" xoriginal="<?= base_url('public/uploads/productos/'. $images[0]['imagen'] ) ?>" />
                        <div class="xzoom-thumbs">
                            <?php foreach ($images as $image) { ?>
                                <a href="<?= base_url('public/uploads/productos/'. $image['imagen'] ) ?>">
                                    <img class="xzoom-gallery" width="80" src="<?= base_url('public/uploads/productos/'. $image['imagen'] ) ?>" xpreview="<?= base_url('public/uploads/productos/'. $image['imagen'] ) ?>">
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                <?php endif ?>
            
            </div>

            <div class="col-md-6">
                <div class="card card-body printableArea">
                    <h4><?= $product["categoria"] ?></h4>
                    <h3><b><?= $product["nombre"] ?></b></h3>
                    <br>

                    <h2>$ <?= number_format( $product["precio"], 2 ) ?></h2>
                    <hr>
                    <div class="row">

                        <?php if(count($colors)>"0") { ?>
                        <!-- Colors -->
                        <div class="col-md-12" style="margin-bottom: 20px;">
                            <h5>Colors:</h5>
                            <div id="colors-carousel" class="carousel slide" data-interval="false" wrap="false" style="width:500px; height:40px;margin:auto;">
                                <div class="carousel-inner" role="listbox">
                                    <input id="select-color" type="hidden" value="<?= $colors[0]["color"]?>">
                                    
                                    <?php $index=0; for($i=0;$i<count($colors);$i=$i+6) { ?>
                                        <div class="item <?php if($index==0) {  ?> active <?php $index++;} ?>">    
                                            <div class="row">
                                                <?php for($j=0;$j<6;$j++) { ?>
                                                    <div class="col-md-2 text-center" style=" <?php if($j==0) { ?>margin-left:75px; <?php } else { ?> margin-left:-30px; <?php  } ?>" >
                                                        <?php if (($i+$j)<count($colors)) { ?><img alt="" id="option-color" value="<?=$colors[$i+$j]["color"]?>" src="<?= base_url()?>public/uploads/productos/colores/<?= ($colors[$i+$j]["imagen"]==NULL) ? "No_image.png" : $colors[$i+$j]["imagen"] ?>" style="height:40px;widht:100%; border:1px solid gray;"> <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                
                                    <!-- Controls -->
                                    <div style="background-color:white">
                                        <a class="left carousel-control" id="prevColors" href="#colors-carousel" role="button" data-slide="prev" style="margin-left:30px;width:40px; border-radius: 100px 100px 100px 100px; background-color: rgba(200,175,50,1); opacity:1;background-image:none;">
                                            <span class="icon-prev" aria-hidden="true" ></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" id="nextColors" href="#colors-carousel" role="button" data-slide="next" style="margin-right:30px;width:40px; border-radius: 100px 100px 100px 100px; background-color: rgba(200,175,50,1); opacity:1;background-image:none;">
                                            <span class="icon-next" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Colors -->
                        <?php } ?>
                        <!-- Textures -->
                        <?php if(count($textures)>"0") { ?>
                        <div class="col-md-12" style="margin-bottom: 20px;">
                            <h5>Textures:</h5>
                            <div id="textures-carousel" class="carousel slide" data-interval="false" style="width:500px; height:40px;margin:auto;">
                                <div class="carousel-inner" role="listbox">
                                    <input id="select-texture" type="hidden" value="<?= $textures[0]["textura"]?>">
                                    
                                    <?php $index=0; for($i=0;$i<count($textures);$i=$i+6) { ?>
                                        <div class="item <?php if($index==0) {  ?> active <?php $index++;} ?> carouselItem">    
                                            <div class="row">
                                                <?php for($j=0;$j<6;$j++) { ?>
                                                    <div class="col-md-2 text-center" style=" <?php if($j==0) { ?>margin-left:75px; <?php } else { ?> margin-left:-30px; <?php  } ?>" >
                                                        <?php if (($i+$j)<count($textures)) { ?><img alt="" id="option-texture" value="<?=$textures[$i+$j]["textura"]?>" src="<?= base_url()?>public/uploads/productos/texturas/<?= ($textures[$i+$j]["imagen"]==NULL) ? "No_image.png" : $textures[$i+$j]["imagen"] ?>" style="height:40px;widht:100%; border:1px solid gray;"> <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                
                                    <!-- Controls -->
                                    <div style="background-color:white">
                                        <a class="left carousel-control" href="#textures-carousel" id="prevTextures" role="button" data-slide="prev" style="margin-left:30px;width:40px; border-radius: 100px 100px 100px 100px; background-color: rgba(200,175,50,1); opacity:1;background-image:none;">
                                            <span class="icon-prev" aria-hidden="true" ></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#textures-carousel" id="nextTextures" role="button" data-slide="next" style="margin-right:30px;width:40px; border-radius: 100px 100px 100px 100px; background-color: rgba(200,175,50,1); opacity:1;background-image:none;">
                                            <span class="icon-next" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Textures -->
                        <?php } ?>
                        <div class="col-md-12">
                            <h5><?= $this->lang->line('dimension');?></h5>
                            <p> 
                                <span <?php echo ($product["anchura"])?"":"style='display:none'" ?> >
                                    <b><?= $this->lang->line('peso');?></b>: <?= $product["anchura"]?>"<br>
                                </span>
                                <span <?php echo ($product["altura"])?"":"style='display:none'" ?> >
                                    <b><?= $this->lang->line('altura');?></b>: <?= $product["altura"]?>"<br>
                                </span>
                                <span <?php echo ($product["largo"])?"":"style='display:none'" ?> >
                                    <b><?= $this->lang->line('longitud');?></b>: <?= $product["largo"]?>"<br>
                                </span>
                                <span <?php echo ($product["anchura"])?"":"style='display:none'" ?> >
                                    <b><?= $this->lang->line('ancho');?></b>: <?= $product["profundidad"]?>"<br>                                
                                </span>
                                <span <?php echo ($product["peso"])?"":"style='display:none'" ?> >
                                    <b><?= $this->lang->line('peso');?></b>: <?= $product["peso"]?>"<br>                                
                                </span>
                                <span <?php echo ($product["diametro_interior"])?"":"style='display:none'" ?> >
                                    <b><?= $this->lang->line('internalDiam');?></b>: <?= $product["diametro_interior"]?>"<br>                                
                                </span>
                                <span <?php echo ($product["diametro_exterior"])?"":"style='display:none'" ?> >
                                    <b><?= $this->lang->line('externalDiam');?></b>: <?= $product["diametro_exterior"]?>"<br>                                
                                </span>
                                <span <?php echo ($product["altura"]&&$product["anchura"])?"":"style='display:none'" ?> >
                                    <b><?= $this->lang->line('area_cook');?></b>: <?= $product["altura"] * $product["anchura"] ?> Sq. In.<br>                                
                                </span>
                            </p>
                        </div>

                        <div class="col-md-12">
                            <h5><?= $this->lang->line('caracteristica');?></h5>
                            <p><?= $product["descripcion"] ?></p>
                            
                            <h5><?= $this->lang->line('sku');?> <?= $product["sku"] ?></h5>
                            <h5><?= $this->lang->line('categoria');?> <?= $product["categoria"] ?></h5>
                            <br>

                            <br>
                            <div class="col-lg-12 ">
                                <div class="input-group add_product_group">
                                    <input type="number" min=1 class="form-control" id="input_qty" style="width: 40%; height: 45px;" value="1">
                                    <div class="input-group-append">
                                        <a class="btn btn-info btn-lg add_product"  href="<?= $product['id_producto'] ?>">
                                            <?= $this->lang->line('agregar_cart');?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <br>
                            <div class="sharethis-inline-share-buttons"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    (function($) {
        $(document).ready(function() {
            $('.xzoom, .xzoom-gallery').xzoom({
                zoomWidth: 400,
                title: true,
                tint: '#333',
                Xoffset: 15
            });
            $('.xzoom2, .xzoom-gallery2').xzoom({
                position: '#xzoom2-id',
                tint: '#ffa200'
            });
            $('.xzoom3, .xzoom-gallery3').xzoom({
                position: 'lens',
                lensShape: 'circle',
                sourceClass: 'xzoom-hidden'
            });
            $('.xzoom4, .xzoom-gallery4').xzoom({
                tint: '#006699',
                Xoffset: 15
            });
            $('.xzoom5, .xzoom-gallery5').xzoom({
                tint: '#006699',
                Xoffset: 15
            });

            //Integration with hammer.js
            var isTouchSupported = 'ontouchstart' in window;

            if (isTouchSupported) {
                //If touch device
                $('.xzoom, .xzoom2, .xzoom3, .xzoom4, .xzoom5').each(function() {
                    var xzoom = $(this).data('xzoom');
                    xzoom.eventunbind();
                });

                $('.xzoom, .xzoom2, .xzoom3').each(function() {
                    var xzoom = $(this).data('xzoom');
                    $(this).hammer().on("tap", function(event) {
                        event.pageX = event.gesture.center.pageX;
                        event.pageY = event.gesture.center.pageY;
                        var s = 1,
                            ls;

                        xzoom.eventmove = function(element) {
                            element.hammer().on('drag', function(event) {
                                event.pageX = event.gesture.center.pageX;
                                event.pageY = event.gesture.center.pageY;
                                xzoom.movezoom(event);
                                event.gesture.preventDefault();
                            });
                        }

                        xzoom.eventleave = function(element) {
                            element.hammer().on('tap', function(event) {
                                xzoom.closezoom();
                            });
                        }
                        xzoom.openzoom(event);
                    });
                });

                $('.xzoom4').each(function() {
                    var xzoom = $(this).data('xzoom');
                    $(this).hammer().on("tap", function(event) {
                        event.pageX = event.gesture.center.pageX;
                        event.pageY = event.gesture.center.pageY;
                        var s = 1,
                            ls;

                        xzoom.eventmove = function(element) {
                            element.hammer().on('drag', function(event) {
                                event.pageX = event.gesture.center.pageX;
                                event.pageY = event.gesture.center.pageY;
                                xzoom.movezoom(event);
                                event.gesture.preventDefault();
                            });
                        }

                        var counter = 0;
                        xzoom.eventclick = function(element) {
                            element.hammer().on('tap', function() {
                                counter++;
                                if (counter == 1) setTimeout(openfancy, 300);
                                event.gesture.preventDefault();
                            });
                        }

                        function openfancy() {
                            if (counter == 2) {
                                xzoom.closezoom();
                                $.fancybox.open(xzoom.gallery().cgallery);
                            } else {
                                xzoom.closezoom();
                            }
                            counter = 0;
                        }
                        xzoom.openzoom(event);
                    });
                });

                $('.xzoom5').each(function() {
                    var xzoom = $(this).data('xzoom');
                    $(this).hammer().on("tap", function(event) {
                        event.pageX = event.gesture.center.pageX;
                        event.pageY = event.gesture.center.pageY;
                        var s = 1,
                            ls;

                        xzoom.eventmove = function(element) {
                            element.hammer().on('drag', function(event) {
                                event.pageX = event.gesture.center.pageX;
                                event.pageY = event.gesture.center.pageY;
                                xzoom.movezoom(event);
                                event.gesture.preventDefault();
                            });
                        }

                        var counter = 0;
                        xzoom.eventclick = function(element) {
                            element.hammer().on('tap', function() {
                                counter++;
                                if (counter == 1) setTimeout(openmagnific, 300);
                                event.gesture.preventDefault();
                            });
                        }

                        function openmagnific() {
                            if (counter == 2) {
                                xzoom.closezoom();
                                var gallery = xzoom.gallery().cgallery;
                                var i, images = new Array();
                                for (i in gallery) {
                                    images[i] = {
                                        src: gallery[i]
                                    };
                                }
                                $.magnificPopup.open({
                                    items: images,
                                    type: 'image',
                                    gallery: {
                                        enabled: true
                                    }
                                });
                            } else {
                                xzoom.closezoom();
                            }
                            counter = 0;
                        }
                        xzoom.openzoom(event);
                    });
                });

            } else {
                //If not touch device

                //Integration with fancybox plugin
                $('#xzoom-fancy').bind('click', function(event) {
                    var xzoom = $(this).data('xzoom');
                    xzoom.closezoom();
                    $.fancybox.open(xzoom.gallery().cgallery, {
                        padding: 0,
                        helpers: {
                            overlay: {
                                locked: false
                            }
                        }
                    });
                    event.preventDefault();
                });

                //Integration with magnific popup plugin
                $('#xzoom-magnific').bind('click', function(event) {
                    var xzoom = $(this).data('xzoom');
                    xzoom.closezoom();
                    var gallery = xzoom.gallery().cgallery;
                    var i, images = new Array();
                    for (i in gallery) {
                        images[i] = {
                            src: gallery[i]
                        };
                    }
                    $.magnificPopup.open({
                        items: images,
                        type: 'image',
                        gallery: {
                            enabled: true
                        }
                    });
                    event.preventDefault();
                });
            }
        });
    })(jQuery);
</script>