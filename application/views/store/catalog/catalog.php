<?php $this->lang->load('store_catalog',$this->session->userdata('site_lang'));?>
<section class="bg-white cover-background padding-110px-tb feature-style3 xs-padding-60px-tb tz-builder-bg-image" data-img-size="(W)1920px X (H)750px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?= base_url() ?>public/images/theme/uploads/bbq.jpg');">
    <div class="container" style="background-color: black;"></div>
</section>
<section class="bg-white builder-bg padding-40px-tb feature-style3 xs-padding-60px-tb" id="feature-section32">
    <div class="container">

        <div class="col-lg-3">
            <input type="hidden" value="0" id="categoryInput">
            <h4>Type</h4>
            <br>
            <ul>
                <li>
                    <a href="#" id="0" class="category" style="font-size:16px">All</a>
                </li>
                <?php foreach ($categories as $category) { ?>
                    <li>
                        <a href="#"
                            id="<?= $category["id_categoria"] ?>"
                            class="category"
                            style="font-size:16px">
                            <?= $category["nombre"] ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>  

        <div class="col-lg-9">

            <div class="col-lg-12">
                <!-- input search -->
                <div class="col-lg-4 pull-left">
                    <label><?= $this->lang->line('buscar');?></label>
                    <input id="input_search" type="text" placeholder="Name, category, price..." style="border-radius:  6px; ">
                </div>
                <!-- order select -->
                <div class="form-group col-lg-4 pull-right">
                    <label><?= $this->lang->line('ordenar');?></label>
                    <select class="form-control order_catalog_select">
                        <option value="max_min" selected><?= $this->lang->line('mayormenor');?></option>
                        <option value="min_max"><?= $this->lang->line('menormayor');?></option>
                        <option value="a_z">A-Z</option>
                        <option value="z_a">Z-A</option>
                    </select>
                </div>
            </div>

            <!-- loader -->
            <div class="col-lg-12">
                <div id="loader" class="loader-ring" style="height: 70px; display: none"></div>
            </div>
            
            <div id="prod_page_container" style="min-height: 700px;">
            
                <!-- feature box -->
                <div style="min-height: 515px;" class="col-lg-12">
                    <input type="hidden" id="input_qty" value="1">
                    <?php foreach ( $products as $product ) { ?>
                        <div class="col-md-4 col-sm-6 col-xs-12 text-center sm-margin-nine-bottom xs-margin-fifteen-bottom">
                            <div class="margin-ten-bottom">
                                <a href="<?= base_url('index.php/Store/detail/' . $product["id_producto"] ) ?>">
                                    <img alt="" src="<?= base_url() ?>public/uploads/productos/<?= $product["url_image"] ?>" data-img-size="(W)800px X (H)800px" style="border-radius: 0px; border-color: rgb(78, 78, 78); border-style: none; border-width: 1px !important;">
                                </a>
                            </div>
                            <h3 class="text-medium line-height-18 alt-font display-block tz-text">
                                <a href="<?= base_url('index.php/Store/detail/' . $product["id_producto"] ) ?>" class="text-dark-gray">
                                    <?= $product["nombre"] ?>
                                </a>
                            </h3>
                            <div class="text-medium-gray text-medium margin-three-bottom tz-text">
                                <?= $product["categoria"] ?>
                            </div>
                            <div class="text-large alt-font text-deep-red tz-text margin-ten-bottom">
                                $<?= number_format( $product["precio"], 2 ) ?>
                            </div>
                            <a class="add_product btn btn-medium propClone btn-circle bg-fast-blue text-white" href="<?= $product['id_producto'] ?>" style="color: rgb(255, 255, 255); border-color: rgba(0, 0, 0, 0); font-size: 12px; font-weight: 500; font-family: Montserrat, sans-serif; text-transform: none; border-radius: 30px; background-color: rgb(191, 144, 0) !important;">
                                <span class="tz-text"><?= $this->lang->line('buy');?></span>
                                <i class="fa fa-angle-right icon-extra-small tz-icon-color"></i>
                            </a>
                        </div>                    
                    <?php }  ?>
                </div>
                <!-- end feature box -->

                <!-- pagination box -->
                <div class="col-lg-12 pagination-centered">
                    <input type="hidden" id="actual_page" value="0">
                    <input type="hidden" id="total_pages" value="<?=$pages?>">
                    <nav <?php echo ($pages > 1)?"":"style='display:none'" ?>>
                        <ul class="pagination">
                        
                            <?php for($i=0;$i<$pages;$i++) { ?>
                                <li><a  value="<?= $i ?>" class="page-number" href="#">
                                    <?= $i+1?>
                                </a></li>
                            <?php }  ?>
                            <li>
                                <a value="next" class="page-button" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- end pagination box -->
        
            </div>


        </div>

    </div>

</section>