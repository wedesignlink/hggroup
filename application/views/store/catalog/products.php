<!-- feature box -->
<?php $this->lang->load('store_catalog',$this->session->userdata('site_lang'));?>
<div class="col-lg-12" style="min-height: 515px;">
    <input type="hidden" id="input_qty" value="1">
    <?php foreach ( $products as $product ) { ?>
        <div class="col-md-4 col-sm-6 col-xs-12 text-center sm-margin-nine-bottom xs-margin-fifteen-bottom">
            <div class="margin-ten-bottom">
                <a href="<?= base_url('index.php/Store/detail/' . $product["id_producto"] ) ?>">
                    <img alt="" src="<?= base_url() ?>public/uploads/productos/<?= $product["url_image"] ?>" data-img-size="(W)800px X (H)800px" style="border-radius: 0px; border-color: rgb(78, 78, 78); border-style: none; border-width: 1px !important;">
                </a>
            </div>
            <h3 class="text-medium line-height-18 alt-font display-block tz-text">
                <a href="<?= base_url('index.php/Store/detail/' . $product["id_producto"] ) ?>" class="text-dark-gray">
                    <?= $product["nombre"] ?>
                </a>
            </h3>
            <div class="text-medium-gray text-medium margin-three-bottom tz-text">
                <?= $product["categoria"] ?>
            </div>
            <div class="text-large alt-font text-deep-red tz-text margin-ten-bottom">
                $<?= number_format( $product["precio"], 2 ) ?>
            </div>
            <a class="add_product btn btn-medium propClone btn-circle bg-fast-blue text-white" href="<?= $product['id_producto'] ?>" style="color: rgb(255, 255, 255); border-color: rgba(0, 0, 0, 0); font-size: 12px; font-weight: 500; font-family: Montserrat, sans-serif; text-transform: none; border-radius: 30px; background-color: rgb(191, 144, 0) !important;">
                <span class="tz-text"><?= $this->lang->line('buy');?></span>
                <i class="fa fa-angle-right icon-extra-small tz-icon-color"></i>
            </a>
        </div>                    
    <?php }  ?>
</div>
<!-- end feature box -->

<!-- pagination box -->
<div class="col-lg-12 pagination-centered">

    <input type="hidden" id="actual_page" value="<?=$actual_page?>">
    <input type="hidden" id="total_pages" value="<?=$pages?>">
    <nav <?php echo ($pages > 1)?"":"style='display:none'" ?> >
        <ul class="pagination">
       
            <li <?php echo ($actual_page != 0)?"":"style='display:none'" ?>>
                <a value="prev" class="page-button" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <?php for($i=0;$i<$pages;$i++) { ?>
                <li><a  value="<?= $i ?>" class="page-number" href="#">
                    <?= $i+1?>
                </a></li>
            <?php }  ?>
            <li <?php echo ($actual_page < $pages-1)?"":"style='display:none'" ?>>
                <a value="next" class="page-button" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
    
</div>
<!-- end pagination box -->
