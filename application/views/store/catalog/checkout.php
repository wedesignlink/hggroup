<?php $this->lang->load('store_checkout',$this->session->userdata('site_lang'));?>
<section class="bg-white cover-background padding-110px-tb feature-style3 xs-padding-60px-tb tz-builder-bg-image" data-img-size="(W)1920px X (H)750px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?= base_url() ?>public/images/theme/uploads/bbq.jpg');">
    <div class="container" style="background-color: black;"></div>
</section>
<section class="bg-white builder-bg padding-40px-tb feature-style3 xs-padding-60px-tb" id="feature-section32">
    <div class="container">
        <div class="row four-column">
            <!-- Form -->
            <div class="col-md-6">
                <div class="card card-body printableArea">
                    <h6><small>Information > Payment</small> </h6>
                    <hr class="solid white">
                    <form action="/action_page.php">
                        <div class="form-group">
                            <h5>Contact information</h5>
                            <br>
                            <input type="email" placeholder="Email" class="form-control" id="email">
                            <div class="checkbox-inline">
                                <input type="checkbox" value=""><h6><small>Keep me up to date on news and exclusive offers</small></h6>
                            </div>
                            <hr class="solid white">
                            <h5>Billing address</h5>
                            <br>
                            <div class="row four-column">
                                <div class="col-md-7">
                                    <input type="text" placeholder="First Name (optional)" class="form-control" id="firstName">
                                </div>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Last Name" class="form-control" id="lastName">
                                </div>
                            </div>
                            <input type="text" placeholder="Company (optional)" class="form-control" id="company">
                            <input type="text" placeholder="Address" class="form-control" id="address">
                            <hr class="solid white">                         
                        </div>
                    </form>
                </div>
            </div>
            <!-- End Form -->

            <!-- List Products -->
            <div class="col-md-6 productsList">
                <div class="card card-body printableArea productsMargin">
                    <!-- div Products -->
                    <div id="Products" class="products">
                        <div class="row four-column">
                            <div class="col-md-3">
                                <img class="productImage" alt="" src="<?= base_url() ?>public/uploads/productos/test.png">
                            </div>
                            <div class="col-md-6">
                                <p class="productInfo"><b>Nombre del Producto</b><br>
                                Hola<p>
                            </div>
                            <div class="col-md-3 pull-right price">
                               <h6>$Precio</h6>
                            </div>
                        </div>
                    </div>
                    <!-- end div Products -->
                    <hr class="solid gray">
                    <!-- div Pricing -->
                    <div id="pricing">
                        <div class="row four-column">
                            <div class="col-md-9">
                                <h6><small>Subtotal</small></h6>
                            </div>
                            <div class="col-md-3 pricePosition">
                               <h6>$<?= number_format($cart_session["total_price"] , 2 ) ?></h6>
                            </div>
                        </div>
                        <div class="row four-column">
                            <div class="col-md-9">
                                <h6><small>Taxes (estimated)</small></h6>
                            </div>
                            <div class="col-md-3 pricePosition">
                               <h6>$<?= number_format($cart_session["total_price"] , 2 ) ?></h6>
                            </div>
                        </div>
                    </div>
                    <!-- end div Pricing -->
                    <hr class="solid gray">
                    <!-- div Total -->
                    <div class="row four-column">
                        <div class="col-md-2">
                            <h6>Total</h6>
                        </div>
                        <div class="col-md-10 pricePosition"> 
                            <h6><small>USD</small>
                            <b>$<?= number_format($cart_session["total_price"] , 2 ) ?><b></h6>
                        </div>
                    </div>
                    <!-- end div Total -->
                    <hr class="solid clear">
                    <!-- div Button -->
                    <div class="row four-column text-center">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-defualt btn-lg btn-block paymentButton"><h4>PAGAR CON PAYPAL</h4></button>
                        </div>
                    </div>
                    <!-- div Button-->
                </div>
            </div>
            <!-- End List Products -->
        </div>
    </div>
</section>