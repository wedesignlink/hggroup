<!-- Refresh cart table -->
<?php $this->lang->load('store_catalog_carrito',$this->session->userdata('site_lang'));?>

<table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th><?= $this->lang->line('description');?></th>
                                            <th class="text-right"><?= $this->lang->line('color');?></th>
                                            <th class="text-right"><?= $this->lang->line('texture');?></th>
                                            <th class="text-right"><?= $this->lang->line('quantity');?></th>
                                            <th class="text-right"><?= $this->lang->line('unitario');?></th>
                                            <th class="text-right"><?= $this->lang->line('total');?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                if ( count( $cart_session["products"] ) > 0 ) {

                    $index = 0;
                    foreach ( $cart_session["products"] as $key => $value ) {?>
                                            <tr>
                                                <td class="text-center"><?=++$index?></td>
                                                <td><?=$value["product"]["nombre"]?></td>
                                                <td class="text-right"><?=$value["color"]?></td>
                                                <td class="text-right"><?=$value["texture"]?></td>
                                                <td class="text-right inputCart"><input type="number" min="1" id="product_<?=$value["product"]["id_producto"]?>" class="product_quantity" value=<?=$value["quantity"]?> ></td>
                                                <td class="text-right">$ <?=number_format( $value["product"]["precio"], 2 )?></td>
                                                <td class="text-right">$ <?=number_format( $value["product"]["precio"] * $value["quantity"], 2 )?> </td>
                                                <td class="text-right"><button type="button" name="remove" id="remove_<?=$value["product"]["id_producto"]?>" class="btn btn-warning remove_inventory">Remove</button></td>
                                            </tr>
                                        <?php }} else {

    ?> <tr>
                                            <td colspan = "8"> Your cart is empty </td>
                                        </tr> <?php }
?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>


                        <!-- <div class="col-md-12"> -->
                            <div class="pull-right m-t-30 text-right">
                                <p>$ <?=number_format( $cart_session["total_price"], 2 )?></p>

                                <div align="right">
                        <button type="button" id="clear_cart" class="btn btn-warning"><?= $this->lang->line('clear');?></button>
                                </div>

                                <hr>
                                <h3><b>Total :</b>$ <?=number_format( $cart_session["total_price"], 2 )?></h3>
                            </div>

                            <div class="clearfix"></div>
                            <hr>
                            <div class="text-right">
                                <button onclick="window.location.href='../Checkout'" class="btn btn-success btn-lg" type="submit"> <?= $this->lang->line('proceed');?> </button>
                                <!-- <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button> -->
                            </div>
                        </div>
                    </div>
                </div>
