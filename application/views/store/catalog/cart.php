<?php
    $this->lang->load('store_catalog_carrito',$this->session->userdata('site_lang'));
    $cart_session = $this->session->userdata("cart");
    if (!$cart_session) {
        $cart_session=1;
    }

?>
<section class="bg-white cover-background padding-110px-tb feature-style3 xs-padding-60px-tb tz-builder-bg-image" data-img-size="(W)1920px X (H)750px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?=base_url()?>public/images/theme/uploads/bbq.jpg');">
    <div class="container" style="background-color: black;"></div>
</section>
<style>
  input[type="number"]{
padding:4%;
border-right: 0 px solid black;
width: 55px;
}
</style>

<link href="<?=base_url()?>public/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet">


<section class="bg-white builder-bg padding-40px-tb feature-style3 xs-padding-60px-tb" id="feature-section32">
    <div class="container">
        <div class="row four-column">
            <div class="col-md-12">
                <div class="card card-body printableArea">
                    <h3><b><?= $this->lang->line('carrito');?></b></h3>
                      <!-- loader -->
                    <div class="col-lg-12">
                    <div id="loader" class="loader-ring" style="height: 70px; display: none"></div>
                </div>
                    <hr>
                    <div class="row" >
                        <div class=" col-md-12 " id="cart_container" >
                            <div class="table-responsive m-t-40" style="clear: both;">
                                <table class="table table-hover" >
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th><?= $this->lang->line('description');?></th>
                                            <th class="text-right"><?= $this->lang->line('color');?></th>
                                            <th class="text-right"><?= $this->lang->line('texture');?></th>
                                            <th class="text-right"><?= $this->lang->line('quantity');?></th>
                                            <th class="text-right"><?= $this->lang->line('unitario');?></th>
                                            <th class="text-right"><?= $this->lang->line('total');?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

if ( count( $cart_session["products"] ) > 0 ) {

    $index = 0;
    foreach ( $cart_session["products"] as $key => $value ) {?>
                                            <tr>
                                                <td class="text-center"><?=++$index?></td>
                                                <td><?=$value["product"]["nombre"]?></td>
                                                <td class="text-right"><?=$value["color"]?></td>
                                                <td class="text-right"><?=$value["texture"]?></td>
                                                <td class="text-right inputCart"><input type="number" min="1" id="product_<?=$value["product"]["id_producto"]?>" class="product_quantity" value=<?=$value["quantity"]?> ></td>


                                                <td class="text-right">$ <?=number_format( $value["product"]["precio"], 2 )?></td>
                                                <td class="text-right">$ <?=number_format( $value["product"]["precio"] * $value["quantity"], 2 )?> </td>
                                                <td class="text-right"><button type="button" name="remove" id="remove_<?=$value["product"]["id_producto"]?>" class="btn btn-warning remove_inventory">Remove</button></td>
                                            </tr>
                                        <?php }} else {

    ?> <tr>
                                            <td colspan = "8"> Your cart is empty </td>
                                        </tr> <?php }
?>
                                    </tbody>
                                </table>



                        <!-- <div class="col-md-12"> -->
                            <div class="pull-right m-t-30 text-right">
                                <p>$ <?=number_format( $cart_session["total_price"], 2 )?></p>

                                <div align="right">
                        <button type="button" id="clear_cart" class="btn btn-warning"><?= $this->lang->line('clear');?></button>
                                </div>

                                <hr>
                                <h3><b>Total :</b>$ <?=number_format( $cart_session["total_price"], 2 )?></h3>
                            </div>

                            <div class="clearfix"></div>
                            <hr>
                            <div class="text-right">
                            <button onclick="window.location.href='../Checkout'" 
                                class="btn btn-success btn-lg <?php if ( count( $cart_session["products"] ) <= 0 ) { ?> disabled <?php } ?>" type="submit"><?= $this->lang->line('proceed');?></button>
                                <!-- <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button> -->
                            </div>

                        </div>
                    </div>
                </div>

        </div>
    </div>
</section>

<!-- JQuery script // gets the ID value -->
<script>
/* Updates quantity per item on cart */
$(document).on('change', '.product_quantity', function(e){
    "use strict";

    e.preventDefault();
    var id_product = $(this).attr('id');
    id_product = id_product.split('_');
    id_product = id_product[1];
    var quantity = $('#product_' + id_product).val();

    var sumIndex = 0;
    $( ".product_quantity" ).each(function( index ) {
        sumIndex += parseInt( $( this ).val());
    });
    $('#tot_quantity').text(sumIndex);

    if (quantity < 1) {
        quantity = 1;
        swal("Article cannot be less than 1", "Remove article instead")
    }

    // ajax script
    $.ajax({
            url: base_url + 'index.php/Store/updateCart',
            data: {id_product: id_product, quantity: quantity},
            method: 'POST',
            beforeSend: function () {
                $('#cart_container').html();
            },
            success: function (data) {
                $('#cart_container').html(data);
            },
            complete: function () {
            }
        });
});




/* Remove items on cart */
$( document).on( 'click', '.remove_inventory', function(e) {
        e.preventDefault();
        var removeItem = $(this).attr('id');
        removeItem = removeItem.split('_');
        removeItem = removeItem[1];


        $.ajax({
            url: base_url + 'index.php/Store/deleteCart',
            data: "id_product="+removeItem,
            method: 'POST',
            beforeSend: function () {
                $('#loader').fadeIn('slow'); // Spinner
                $('#cart_container').html();
            },
            success: function (data) {
                $('#cart_container').html(data);
                $('#loader').fadeOut('slow', function(){
                    if(data){
                        $('#cart_container').html(data);
                    }

                    var sumIndex = 0;
                    $( ".product_quantity" ).each(function( index ) {
                    sumIndex += parseInt( $( this ).val());
                    });
                    $('#tot_quantity').text(sumIndex);
                });
            },
            complete: function () {

            }
        });

    });



/* Clear cart */
$( document ).on ('click', '#clear_cart', function(e) {
    e.preventDefault();
    var clearCart = $(this).attr('id');

    swal({
                title: "Clear cart?",
                text: "This will remove all your items",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes, clear cart',
                denyButtonText: 'No',
    }, function () {
        if (this.showCancelButton) {
            $.ajax({
                    url: base_url + 'index.php/Store/clearCart',
                    data: "",
                    method: 'POST',
                    beforeSend: function () {
                        $('#loader').fadeIn('slow');
                        $('#cart_container').html();
                    },
                    success: function (data) {
                        $('#cart_container').html(data);
                        $('#loader').fadeOut('slow', function(){
                            if(data){
                                $('#cart_container').html(data);
                            }
                        });
                        $("#tot_quantity").html('0');
                    },
                    complete: function () {

                    }
                });
        }
    })
});


</script>
<script src="<?=base_url()?>public/assets/plugins/sweetalert/sweetalert.min.js"></script>

