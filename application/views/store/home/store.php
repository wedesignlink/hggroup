
<?php 
//cargar el diccionario de idioma
$this->lang->load('store_home_store',$this->session->userdata('site_lang'));?>
                <section class="no-padding cover-background tz-builder-bg-image" data-img-size="(W)1920px X (H)750px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?= base_url() ?>public/images/theme/uploads/bbq.jpg');" id="ui-id-11">
                    <div class="container one-fourth-screen position-relative">
                        <div class="slider-typography text-center">
                            <div class="slider-text-middle-main">
                                <div class="slider-text-middle">
                                    <!-- slider text -->
                                    <div class="col-md-12 header-banner">
                                        <a href="https://www.youtube.com/watch?v=_xG68lkrMDU" class="tz-edit title-extra-large-6 text-white margin-five-bottom display-inline-block banner-icon popup-youtube">
                                            
                                        </a>
                                        <div class="text-white font-weight-600 title-extra-large-3 alt-font margin-two-bottom banner-title xs-margin-seven-bottom">
                                            <span class="tz-text"><?= strtoupper($this->lang->line('titulo_bienvenida'));?></span>
                                        </div>
                                        <div class="text-white title-medium sm-title-medium margin-six-bottom banner-text width-60 md-width-90 center-col">
                                            <span class="tz-text"><?= strtoupper($this->lang->line('subtitulo_bienvenida'));?></span>
                                        </div>
                                        <div class="display-inline-block margin-one">
                                            <a class="font-weight-600 btn-large btn line-height-20 bg-white text-black no-letter-spacing" href="#"><span class="tz-text"><?= strtoupper($this->lang->line('design_bbq'));?></span><i class="fa fa-caret-right tz-icon-color"></i></a>
                                        </div>
                                        <div class="display-inline-block margin-one">
                                            
                                        </div>
                                    </div>
                                    <!-- end slider text -->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>                
            </div><section class="padding-60px-tb bg-sky-blue-dark builder-bg border-none" id="callto-action2" style="padding: 60px 0px; border-color: rgb(112, 112, 112); background-color: rgb(255, 153, 0) !important;">
                <div class="container">
                    <div class="row equalize">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center" style="">
                            <div class="display-inline-block sm-display-block vertical-align-middle margin-five-right sm-no-margin-right sm-margin-ten-bottom tz-text alt-font text-white title-medium sm-title-medium"><?= ($this->lang->line('design_style'));?></div>
                            <a class="btn-large btn text-white highlight-button-white-border btn-circle" href="#"><span class="tz-text"><?= strtoupper($this->lang->line('design_bbq'));?></span><i class="fa fa-long-arrow-right icon-extra-small tz-icon-color"></i></a>
                        </div>
                    </div>
                </div>
            </section><section class="padding-110px-tb xs-padding-60px-tb bg-white builder-bg border-none" id="title-section1">
                <div class="container">
                    <div class="row">
                        <!-- section title -->
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2 class="section-title-large sm-section-title-medium text-dark-gray font-weight-600 alt-font margin-three-bottom xs-margin-fifteen-bottom tz-text" style="color: rgb(40, 40, 40); background-color: rgba(0, 0, 0, 0); font-weight: 600; font-family: Montserrat, sans-serif; text-transform: none; border-radius: 0px; font-size: 50px !important;" id="ui-id-32">H Group BBQ'S &amp; Patio</h2>
                            <div class="text-medium width-60 margin-lr-auto md-width-70 sm-width-100 tz-text" style="color: rgb(112, 112, 112); background-color: rgba(0, 0, 0, 0); font-weight: 400; font-family: 'Open Sans', sans-serif; text-transform: none; border-radius: 0px; font-size: 18px !important;" id="ui-id-9"><?= ($this->lang->line('descriptionBBQ'));?></div>
                        </div>
                        <!-- end section title -->
                    </div>                    
                </div>
            </section><section class="bg-deep-green builder-bg border-none hero-style25" id="hero-section26" style="padding: 0px; border-color: rgb(112, 112, 112); background-color: rgb(255, 153, 0) !important;">
                <div class="container-fluid">
                    <div class="row">
                        <!-- image -->
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding xs-no-padding-15 bg-gray tz-builder-bg-image one-sixth-screen sm-one-third-screen cover-background " data-img-size="(W)1000px X (H)1000px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?= base_url() ?>public/images/theme/uploads/grill.jpg') !important;" id="ui-id-6"></div>
                        <!-- end image -->
                        <!-- slider text -->
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 one-sixth-screen sm-one-third-screen xs-height-auto display-table text-center xs-no-padding">
                            <div class="display-table-cell-vertical-middle padding-twenty-five sm-padding-seven xs-padding-twenty no-padding-tb">
                                <h2 class="alt-font text-white letter-spacing-minus-2 font-weight-600 title-big md-title-extra-large-3 xs-title-extra-large-4 margin-five-bottom tz-text">Grill Masters</h2>
                                <div class="title-large tz-text margin-ten-bottom text-white font-weight-300 width-50 md-width-100 xs-width-80 center-col xs-title-extra-large" style="color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0); font-weight: 300; font-family: 'Open Sans', sans-serif; text-transform: none; border-radius: 0px; font-size: 24px !important;" id="ui-id-7">&nbsp;<?= ($this->lang->line('parrillerosprofesionales'));?></div>
                                <a class="btn btn-large bg-dark-gray text-white btn-circle propClone" href="#" id="ui-id-30" style="color: rgb(255, 255, 255); border-color: rgba(0, 0, 0, 0); font-size: 14px; font-weight: 500; font-family: Montserrat, sans-serif; text-transform: none; border-radius: 30px; background-color: rgb(12, 52, 61) !important;"><span class="tz-text"><?= strtoupper($this->lang->line('design_bbq'));?></span></a>
                                <div class="position-absolute position-bottom position-left position-right margin-ten-bottom xs-position-inherit xs-margin-ten xs-no-margin-bottom xs-no-margin-lr">
                                <!--
                                <ul class="display-inline no-padding">
                                    <li class="display-inline padding-four"><a href="#"><i class="fa fa-facebook text-white title-small tz-icon-color" data-selector=".tz-icon-color"></i></a></li>
                                    <li class="display-inline padding-four"><a href="#"><i class="fa fa-google text-white title-small tz-icon-color" data-selector=".tz-icon-color" style=""></i></a></li>
                                    <li class="display-inline padding-four"><a href="#"><i class="fa fa-dribbble text-white title-small tz-icon-color" data-selector=".tz-icon-color"></i></a></li>
                                    <li class="display-inline padding-four"><a href="#"><i class="fa fa-pinterest-p text-white title-small tz-icon-color" data-selector=".tz-icon-color" style=""></i></a></li>
                                    <li class="display-inline padding-four"><a href="#"></a></li>
                                </ul>
                                -->
                            </div>
                            </div>
                        </div>
                        <!-- end slider text -->
                    </div>
                </div>
            </section><section class="bg-white builder-bg padding-110px-tb feature-style3 xs-padding-60px-tb" id="feature-section32">
                <div class="container">
                    <div class="row four-column">

                    <?php foreach($products as $p) {?>
                        <!-- feature box -->
                        <div class="col-md-3 col-sm-6 col-xs-12 text-center sm-margin-nine-bottom xs-margin-fifteen-bottom">
                            <div class="margin-ten-bottom text-center">
                                <a href="<?= base_url('index.php/Store/detail/'.$p["id_producto"] ) ?>">
                                    <img src="<?= base_url() ?>public/uploads/productos/<?= $p["url_image"] ?>" data-img-size="(W)800px X (H)800px" id="ui-id-21" style="border-radius: 0px; border-color: rgb(78, 78, 78); border-style: none; border-width: 1px !important;">
                                </a>
                            </div>
                            <h3 class="text-medium line-height-18 alt-font display-block text-center">
                                <?= $p["nombre"] ?>
                            </h3>
                            <div class="text-medium-gray text-medium margin-three-bottom text-center">
                                <?= $p["categoria"] ?>
                            </div>
                            <div class="text-large alt-font text-deep-red text-center margin-ten-bottom">
                                $<?= $p["precio"] ?>
                            </div>
                            <div class="text-center">
                                <a class="btn btn-medium propClone btn-circle bg-fast-blue text-white" href="<?= base_url('index.php/Store/detail/'.$p["id_producto"] ) ?>" id="ui-id-17" style="color: rgb(255, 255, 255); border-color: rgba(0, 0, 0, 0); font-size: 12px; font-weight: 500; font-family: Montserrat, sans-serif; text-transform: none; border-radius: 30px; background-color: rgb(191, 144, 0)!important;">
                                    <span class="text-center">Go to store</span><i class="fa fa-angle-right icon-extra-small tz-icon-color"></i>
                                </a>
                            </div>
                        </div>
                        <!-- end feature box -->
                    <?php }?>
                        
                    </div>
                </div>
            </section><section class="padding-60px-tb bg-fast-blue builder-bg border-none" id="callto-action9" style="padding: 60px 0px; border-color: rgb(112, 112, 112); background-color: rgb(255, 153, 0) !important;">
                <div class="container">
                    <div class="row equalize sm-equalize-auto">
                        <!-- section title -->
                        <div class="col-md-6 col-sm-12 col-xs-12 sm-text-center display-table sm-margin-ten-bottom xs-margin-fifteen-bottom" style="">
                            <div class="offer-box-left display-table-cell-vertical-middle">
                                <div class="display-table-cell-vertical-middle sm-display-inline-block">
                                <span class="title-medium xs-title-extra-large text-white display-block font-weight-300 tz-text"><?= ($this->lang->line('start'));?></span>
                                <span class="title-extra-large xs-title-extra-large text-white font-weight-400 display-block tz-text"><?= ($this->lang->line('design_grill_text'));?>&nbsp;</span>
                            </div>
                                </div>
                        </div>
                        <!-- end section title -->
                        <div class="col-md-6 col-sm-12 col-xs-12 sm-text-center text-right display-table" style="">
                            <div class="btn-dual display-table-cell-vertical-middle">
                                <a class="btn btn-large propClone bg-white text-fast-blue btn-circle xs-margin-ten-bottom" href="#"><span class="tz-text" id="ui-id-28" style="background-color: rgba(0, 0, 0, 0); font-size: 14px; font-weight: 500; font-family: Montserrat, sans-serif; text-transform: none; border-radius: 0px; color: rgb(191, 144, 0) !important;"><?= ($this->lang->line('design_grill'));?></span><i class="fa icon-extra-small tz-icon-color fa-shopping-cart" id="ui-id-29" style="font-size: 14px; background-color: rgba(0, 0, 0, 0); color: rgb(191, 144, 0) !important;"></i></a>
                               
                                </div>
                        </div>
                    </div>
                </div>
            </section><section class="bg-white builder-bg border-none" id="contact-section10">
                <div class="container-fluid">
                    <div class="row">
                        <!-- map -->
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center location-map no-padding maps">
                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3080.31582476464!2d-119.78389948501847!3d39.46219327948757!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80993ffdca9ec4e7%3A0xbc3f25af96472307!2s7100%20S%20Virginia%20St%2C%20Reno%2C%20NV%2089511%2C%20EE.%20UU.!5e0!3m2!1ses-419!2smx!4v1616641620693!5m2!1ses-419!2smx" width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>  
                        </div>
                        <!-- end map -->
                    </div>
                </div>
            </section><section class="padding-110px-tb bg-dark-blue builder-bg contact-form-style1 xs-padding-60px-tb border-none" id="contact-section15">
                <div class="container">
                    <div class="row">
                        <!-- section title -->
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2 class="section-title-large sm-section-title-medium xs-section-title-large text-white font-weight-600 alt-font margin-three-bottom xs-margin-fifteen-bottom tz-text"><?= ($this->lang->line('contact'));?></h2>
                            <div class="text-medium text-blue-gray width-60 margin-lr-auto md-width-70 sm-width-100 tz-text margin-thirteen-bottom xs-margin-nineteen-bottom"><?= ($this->lang->line('contact_text'));?></div>
                        </div>
                        <!-- end section title -->
                        <!-- contact details -->
                        <div class="col-md-6 col-sm-6 col-xs-12 xs-margin-twenty-three-bottom">
                            <div class="builder-bg contact-details xs-text-center">                                
                                <div class="details-box float-left width-100">
                                    <div class="details-icon col-md-2 xs-width-100 xs-display-block no-padding xs-margin-three-bottom">
                                        <i class="fa icon-large text-sky-blue tz-icon-color fa-map-marker" id="ui-id-15" style="font-size: 35px; background-color: rgba(0, 0, 0, 0); color: rgb(191, 144, 0) !important;"></i>
                                    </div>
                                    <div class="details-text xs-width-100 col-md-10 no-padding">
                                        <div class="text-medium font-weight-600 text-white display-block tz-text">H GROUP</div>
                                        <div class="text-medium text-blue-gray tz-text">7100, south Virginia st<br>Reno nevada, 89511.</div>
                                    </div>
                                </div>
                                <div class="details-box float-left width-100">
                                    <div class="details-icon col-md-2 xs-width-100 xs-display-block no-padding xs-margin-three-bottom">
                                        <i class="fa icon-medium text-sky-blue tz-icon-color fa-envelope" id="ui-id-14" style="font-size: 30px; background-color: rgba(0, 0, 0, 0); color: rgb(191, 144, 0) !important;"></i>
                                    </div>
                                    <div class="details-text xs-width-100 col-md-10 no-padding">
                                        <div class="text-medium font-weight-600 text-white display-block tz-text"><?= ($this->lang->line('sales'));?></div>
                                        <a class="text-medium tz-text text-blue-gray" href="mailto:no-reply@domain.com">no-reply@domain.com</a>
                                    </div>
                                </div>                                
                                <div class="details-box float-left width-100">
                                    <div class="details-icon col-md-2 xs-width-100 xs-display-block no-padding xs-margin-three-bottom">
                                        <i class="fa icon-large text-sky-blue tz-icon-color fa-phone" id="ui-id-16" style="font-size: 35px; background-color: rgba(0, 0, 0, 0); color: rgb(191, 144, 0) !important;"></i>
                                    </div>
                                    <div class="details-text xs-width-100 col-md-10 no-padding">
                                        <div class="text-medium font-weight-600 text-white display-block tz-text"><?= ($this->lang->line('call'));?></div>
                                        <div class="text-medium text-blue-gray tz-text">+44 (0) 123 456 7890</div>
                                    </div>
                                </div>                               
                            </div>
                        </div>
                        <!-- end contact details -->
                        <!-- contact form -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <form id="form_contact">
                                <input type="text" name="name" data-email="required" id="name" placeholder="<?= ($this->lang->line('txt_name'));?>" class="medium-input alt-font">                                
                                <input type="text" name="email" data-email="required" id="email" placeholder="<?= ($this->lang->line('txt_email'));?>" class="medium-input alt-font">                                
                                <textarea name="comment" id="comment" placeholder="<?= ($this->lang->line('txt_msj'));?>" class="medium-input alt-font"></textarea>                                
                                <button class=" btn-medium btn bg-sky-blue text-white tz-text" type="button" id="contact-button" style="color: rgb(255, 255, 255); font-size: 12px; font-weight: 500; font-family: Montserrat, sans-serif; text-transform: none; border-radius: 4px; background-color: rgb(191, 144, 0) !important;"><?= ($this->lang->line('btn_msj'));?></button>                                
                            </form>
                        </div>
                        <!-- end contact form -->
                    </div>
                </div>
            </section>

                
