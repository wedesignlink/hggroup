<?php $this->lang->load('store_checkout',$this->session->userdata('site_lang'));?>
<section class="bg-white cover-background padding-110px-tb feature-style3 xs-padding-60px-tb tz-builder-bg-image" data-img-size="(W)1920px X (H)750px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?= base_url() ?>public/images/theme/uploads/bbq.jpg');">
    <div class="container" style="background-color: black;"></div>
</section>
<section class="bg-white builder-bg padding-40px-tb feature-style3 xs-padding-60px-tb" id="feature-section32">
    
    <div class="container">
        <div class="row four-column">
            
        <div class="tracing">
            <a href="<?= base_url(); ?>index.php/Store/cart"><b>Cart</b> </a> > Payment
        </div>
        
        <!-- Forms container -->
        <div class="col-md-6 userInfo" id="forms_container">
        
            <?php if ( !$this->session->userdata('logged_in') ){?>

                <!-- Login or Register Selection  -->
                <div class="col-md-12 col-xs-12" id="login_register_option" >
                    <div class="card card-body printableArea">
                        <hr class="solid white">
                        <div class="selection">
                            <h3 style="color: black;"><?= $this->lang->line('hello');?><br><?= $this->lang->line('ingresa');?></h3>
                            <br>
                            <button class="select_form btn" id="login" ><?= $this->lang->line('i_have');?></button>
                            <br>
                            <button class="select_form btn" id="register" ><?= $this->lang->line('no_have');?></button>
                        </div>
                    </div>
                </div>
                <!-- End Login or Register Selection -->
                    
                <!-- Register Form -->
                <div class="col-md-12 col-xs-12" style="display:none" id="register_form">
                    <div class="card card-body printableArea">
                        <hr class="solid white">
                        <form id="form-to-regiter">
                            <div class="form-group">
                                <h5><?= $this->lang->line('inf_contact');?></h5>
                                <br>
                                <div class="row four-column">
                                    <div class="col-md-6 col-xs-6">
                                        <input type="text" placeholder="Name" class="form-control" name="name">
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <input type="text" placeholder="Last Name" class="form-control" name="last_name">
                                    </div>
                                </div>
                                <div class="row four-column">
                                    <div class="col-md-6 col-xs-6">
                                        <input type="email" placeholder="Email" class="form-control" name="email">
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <input type="tel" placeholder="Phone" class="form-control" name="phone"> 
                                    </div>
                                </div>

                                <div class="row four-column">
                                    <div class="col-md-6 col-xs-6">
                                        <input type="password" placeholder="Password" class="form-control" name="password">
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <input type="password" placeholder="Confirm Password" class="form-control" name="c_password">
                                    </div>
                                </div>
                                <br>
                                <button class="btn" id="register-button"><?= $this->lang->line('continua');?></button>
                                <button class="btn cancel-button" ><?= $this->lang->line('cancel');?></button>
                                <br>                  
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Register End Form -->

                <!-- Login Form -->
                <div class="col-md-12 col-xs-12" style="display:none" id="login_form">
                    <div class="card card-body printableArea">
                        <hr class="solid white">
                        <form>
                            <div class="form-group">
                                <h5><?= $this->lang->line('back');?></h5>
                                <br>
                                <div class="row four-column">
                                    <div class="col-md-9 col-xs-9">
                                        <input type="email" placeholder="Email" class="form-control" id="email-login">
                                    </div>
                                </div>
                                <div class="row four-column">
                                    <div class="col-md-9 col-xs-9">
                                        <input type="password" placeholder="Password" class="form-control" id="password-login">
                                    </div>
                                </div>
                                <br>
                                <button class="btn" id="login-button" ><?= $this->lang->line('continua');?></button>
                                <button class="btn cancel-button" ><?= $this->lang->line('cancel');?></button>                        
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Login End Form -->
                <?php }else{?>

                <!-- Logged Data -->
                <div class="col-md-12 col-xs-12" >
                    <div class="card card-body printableArea">
                        <hr class="solid white">
                        <div class="form-group">
                            <h5><?= $this->lang->line('inf_account');?></h5>
                            <br>
                            <div class="row four-column">
                                <div class="col-md-6 col-xs-6">
                                    <input type="text" class="form-control" value="<?= $this->session->userdata('client_session')['nombre'] ?>" disabled>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <input type="text" class="form-control" value="<?= $this->session->userdata('client_session')['apellido'] ?>" disabled>
                                </div>
                            </div>
                            <div class="row four-column">
                                <div class="col-md-6 col-xs-6">
                                    <input type="email" class="form-control" value="<?= $this->session->userdata('client_session')['email'] ?>" disabled>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <input type="tel" class="form-control" value="<?= $this->session->userdata('client_session')['telefono'] ?>" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Logged Data -->
                <?php if($direccion == null){ ?>
                <div class="col-md-12 col-xs-12">
                    <h5><?= $this->lang->line('direcc');?></h5>
                    <br>
                    <input type="hidden" id="status_direction" value="0">
                    <form id="form_direction">
                        <div class="row">
                            <div class="col-md-7 col-xs-7">
                            <input type="hidden" class="form-control" name="id_cliente" value="<?= $this->session->userdata('client_session')['id_cliente'] ?>"   required>
                                <input type="text" class="form-control" name="titulo" placeholder="titulo"   required>
                            </div>
                            <div class="col-md-5 col-xs-5">
                                <input type="text" class="form-control" name="calle" placeholder="calle" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-4">
                            <input type="text" class="form-control" name="numero" placeholder="numero" required>
                            </div>
                            <div class="col-md-3 col-xs-3">
                                <input type="text" class="form-control" name="int" placeholder="int">  
                            </div>
                            <div class="col-md-5 col-xs-5">
                            <input type="text" class="form-control" name="ciudad" placeholder="ciudad" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-xs-5">
                            <select name="estado">
                                <?php foreach ($us_states as $state) { ?>
                                    <option value="<?=$state?>" ><?= $state ?></option>
                                <?php } ?>      
                            </select>
                            </div>
                            <div class="col-md-4 col-xs-4">
                            <input type="text" class="form-control" name="cp" placeholder="cp" required>
                            </div>
                        </div>
                        <div class="row" style="text-align: center;">
                            <button  class="btn btn-block" id="direction-button" style="background-color: #fac80a;"><?= $this->lang->line('agregar');?></button>&nbsp;&nbsp;
                        </div>
                    </form>                
                </div>


                <?php }else{ ?>
                <div class="col-md-12 col-xs-12">
                    <h5><?= $this->lang->line('direcc');?></h5>
                    <br>
                    <input type="hidden" id="status_direction" value="1">
                    <form id="form_direction">
                        <div class="row">
                            <div class="col-md-7 col-xs-7">
                                <input type="hidden" class="form-control" name="id_cliente" value="<?php echo $direccion[0]["id_cliente"] ?>"  required>
                                <input type="text" class="form-control" name="titulo" value="<?php echo $direccion[0]["titulo"] ?>"  required>
                            </div>
                            <div class="col-md-5 col-xs-5">
                                <input type="text" class="form-control" name="calle" value="<?php echo $direccion[0]["calle"]  ?>" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-4">
                            <input type="text" class="form-control" name="numero" value="<?php echo $direccion[0]["numero"]  ?>"  required>
                            </div>
                            <div class="col-md-3 col-xs-3">
                                <input type="text" class="form-control" name="int" value="<?php echo $direccion[0]["int"]  ?>" required>  
                            </div>
                            <div class="col-md-5 col-xs-5">
                            <input type="text" class="form-control" name="ciudad" value="<?php echo $direccion[0]["ciudad"]  ?>"  required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-xs-5">
                            <select name="estado">
                                <?php foreach ($us_states as $state) { ?>
                                    <option value="<?=$state?>" <?= ($state == $direccion[0]["estado"])? 'selected' : ''  ?> ><?= $state ?></option>
                                <?php } ?>       
                            </select>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control" name="cp" value="<?php echo $direccion[0]["cp"]  ?>" required>
                            </div>
                        </div>
                        <div class="row" style="text-align: center;">
                            <button  class="btn btn-block btn-success" id="direction-button"><?= $this->lang->line('editar');?></button>
                        </div>
                    </form>
                </div>

                <?php }?>
                <?php }?> 

                <!-- loader -->
                <div class="col-lg-12">
                    <div id="loader" class="loader-ring" style="height: 70px; display: none"></div>
                </div>
                <!-- end loader -->
                    
            </div>
            <!-- Forms container -->

            <!-- List Products -->
            <div class="col-md-6 productsList">
                <div class="card card-body printableArea productsMargin">
                    <!-- div Products -->
                    <div id="Products" class="products">
                        <!-- Show cart products-->
                        <?php foreach ($cart_session["products"] as $key => $value) { ?>
                            <div class="row productDisplay">
                                <div class="col-md-3 col-xs-3">
                                    <a href="#">
                                        <span class="countIcon"><?= $value["quantity"] ?></span>    
                                        <img class="productImage" alt="" src="<?= base_url() ?>public/uploads/productos/<?= $value["product"]["url_image"] ?>">
                                    </a>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <p class="productInfo">
                                        <b><?= $value["product"]["nombre"] ?></b>
                                        <br>
                                        $<?= number_format($value["product"]["precio"], 2 ) ?>
                                    <p>
                                </div>
                                <div class="col-md-3 col-xs-3 price">
                                    <h6>$<?= number_format($value["product"]["precio"] * $value["quantity"], 2) ?></h6>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- End show cart products -->
                    </div>
                    <!-- end div Products -->
                    <hr class="solid gray">
                    <!-- div Pricing -->
                    <div id="pricing">
                        <div class="row four-column">
                            <div class="col-md-9 col-xs-9">
                                <h6><small><?= $this->lang->line('subtotal');?></small></h6>
                            </div>
                            <div class="col-md-3 col-xs-3 pricePosition">
                               <h6>
                                    $<?= number_format($cart_session["total_price"], 2 ) ?>
                               </h6>
                            </div>
                        </div>
                        <div class="row four-column">
                            <div class="col-md-9 col-xs-9">
                                <h6><small><?= $this->lang->line('taxes');?></small></h6>
                            </div>
                            <div class="col-md-3 col-xs-3 pricePosition">
                               <h6>
                                    $<?= number_format($taxes, 2 ) ?>
                               </h6>
                            </div>
                        </div>
                    </div>
                    <!-- end div Pricing -->
                    <hr class="solid gray">
                    <!-- div Total -->
                    <div class="row four-column">
                        <div class="col-md-2 col-xs-2">
                            <h6><?= $this->lang->line('total');?></h6>
                        </div>
                        <div class="col-md-10 col-xs-10 pricePosition"> 
                            <h6><small>USD</small>
                            <b>$<?= number_format($cart_session["total_price"]+$taxes, 2 ) ?><b></h6>
                        </div>
                    </div>
                    <!-- end div Total -->
                    <hr class="solid clear">
                    <!-- Paypal Button -->
                   
                    <div class="row four-column text-center" id="paypal-payment" style="<?= ($direccion)?"":"display:none" ?>">
                        <div id="paypal-button-container" class="col-md-12 col-xs-12"></div>
                    </div>
                    <!-- Paypal Button-->
                </div>
            </div>
            <!-- End List Products -->
        </div>
    </div>
</div>
</section>

<!-- paypal -->
<script src="https://www.paypal.com/sdk/js?client-id=test&currency=USD"></script>
<script type="text/javascript" src="<?= base_url() ?>public/js/paypal.js"></script>
<script>
    var order = createOrderObject( <?php echo json_encode($this->session->userdata('cart'));?> );
    var value = <?php echo $this->session->userdata('cart')["total_price"] * 1.08375 ?> ;
    value = value.toFixed(2);

    paypal.Buttons({
        createOrder: function(data, actions) {
            $('#overlayToCheckout').show();
            return actions.order.create({
                purchase_units:[{
                    amount: {
                        currency_code: "USD",
                        value: value.toString(),
                    },
                }]
            });
        },
        onApprove: function(data, actions) {
            return actions.order.capture().then(function(details) {
                swal({
                    title: 'Transaction completed by ' + details.payer.name.given_name,
                    type: "success",
                    confirmButtonText: "<?= $this->lang->line('continue_shop');?>",
                }, function() {
                    $.ajax({
                        url: base_url + 'index.php/Checkout/view_success_and_error',
                        method: 'POST',
                        complete: function(){
                            $('#overlayToCheckout').hide();
                            window.location.replace(base_url + "index.php/Store/catalog");
                        }                        
                    });
                });
            });
        },
        onCancel: function(data) {
            $('#overlayToCheckout').hide();
        },
        onError: function (err) {
            $('#overlayToCheckout').hide();
            swal("Error in transaction");
        }
    }).render('#paypal-button-container');
</script>
<!-- end paypal -->