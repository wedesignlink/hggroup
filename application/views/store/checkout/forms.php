<?php if ( !$this->session->userdata('logged_in') ){?>
<!-- Login or Register Selection  -->
<div class="col-md-12 col-xs-12" id="login_register_option" >
    <div class="card card-body printableArea">
        <hr class="solid white">
        <div class="selection">
            <h3 style="color: black;">¡Hola! Para comprar,<br>ingresa a tu cuenta</h3>
            <br>
            <button class="select_form btn" id="login" >I have an account</button>
            <br>
            <button class="select_form btn" id="register" >I'm new</button>
        </div>
    </div>
</div>
<!-- End Login or Register Selection -->
    
<!-- Register Form -->
<div class="col-md-12 col-xs-12" style="display:none" id="register_form">
    <div class="card card-body printableArea">
        <hr class="solid white">
        <form id="form-to-regiter">
            <div class="form-group">
                <h5>Contact information</h5>
                <br>
                <div class="row four-column">
                    <div class="col-md-6 col-xs-6">
                        <input type="text" placeholder="Name" class="form-control" name="name">
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <input type="text" placeholder="Last Name" class="form-control" name="last_name">
                    </div>
                </div>
                <div class="row four-column">
                    <div class="col-md-6 col-xs-6">
                        <input type="email" placeholder="Email" class="form-control" name="email">
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <input type="tel" placeholder="Phone" class="form-control" name="phone"> 
                    </div>
                </div>
                <div class="row four-column">
                    <div class="col-md-6 col-xs-6">
                        <input type="password" placeholder="Password" class="form-control" name="password">
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <input type="password" placeholder="Confirm Password" class="form-control" name="c_password">
                    </div>
                </div>
                <br>
                <button class="btn" id="register-button">Continue</button>
                <button class="btn cancel-button" >Cancel</button>
                <br>                  
            </div>
        </form>
    </div>
</div>
<!-- Register End Form -->

<!-- Login Form -->
<div class="col-md-12 col-xs-12" style="display:none" id="login_form">
    <div class="card card-body printableArea">
        <hr class="solid white">
        <form>
            <div class="form-group">
                <h5>Welcome back</h5>
                <br>
                <div class="row four-column">
                    <div class="col-md-9 col-xs-9">
                        <input type="email" placeholder="Email" class="form-control" id="email-login">
                    </div>
                </div>
                <div class="row four-column">
                    <div class="col-md-9 col-xs-9">
                        <input type="password" placeholder="Password" class="form-control" id="password-login">
                    </div>
                </div>
                <br>
                <button class="btn" id="login-button" >Continue</button>
                <button class="btn cancel-button" >Cancel</button>                        
            </div>
        </form>
    </div>
</div>
<!-- Login End Form -->
<?php }else{?>

<!-- Logged Data -->
<div class="col-md-12 col-xs-12" >
    <div class="card card-body printableArea">
        <hr class="solid white">
        <div class="form-group">
            <h5>Account Information</h5>
            <br>
            <div class="row four-column">
                <div class="col-md-6 col-xs-6">
                    <input type="text" class="form-control" value="<?= $this->session->userdata('client_session')['nombre'] ?>" disabled>
                </div>
                <div class="col-md-6 col-xs-6">
                    <input type="text" class="form-control" value="<?= $this->session->userdata('client_session')['apellido'] ?>" disabled>
                </div>
            </div>
            <div class="row four-column">
                <div class="col-md-6 col-xs-6">
                    <input type="email" class="form-control" value="<?= $this->session->userdata('client_session')['email'] ?>" disabled>
                </div>
                <div class="col-md-6 col-xs-6">
                    <input type="tel" class="form-control" value="<?= $this->session->userdata('client_session')['telefono'] ?>" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Logged Data -->

    <?php if($direccion == null){ ?>
    <div class="col-md-12 col-xs-12">
        <hr class="solid white">
        <h5> Dirección</h5>
        <br>
        <input type="hidden" id="status_direction" value="0">
        <form id="form_direction">
            <div class="row">
                <div class="col-md-7 col-xs-7">
                <input type="hidden" class="form-control" name="id_cliente" value="<?= $this->session->userdata('client_session')['id_cliente'] ?>"   required>
                    <input type="text" class="form-control" name="titulo" placeholder="titulo"   required>
                </div>
                <div class="col-md-5 col-xs-5">
                    <input type="text" class="form-control" name="calle" placeholder="calle" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-xs-4">
                <input type="text" class="form-control" name="numero" placeholder="numero" required>
                </div>
                <div class="col-md-3 col-xs-3">
                    <input type="text" class="form-control" name="int" placeholder="int">  
                </div>
                <div class="col-md-5 col-xs-5">
                <input type="text" class="form-control" name="ciudad" placeholder="ciudad" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-xs-5">
                <select name="estado">
                    <?php foreach ($us_states as $state) { ?>
                        <option value="<?=$state?>"><?= $state ?></option>
                    <?php } ?>      
                </select>
                </div>
                <div class="col-md-4 col-xs-4">
                <input type="text" class="form-control" name="cp" placeholder="cp" required>
                </div>
            </div>
            <div class="row" style="text-align: center;">
                <button  class="btn btn-block" id="direction-button" style="background-color: #fac80a;">Agregar</button>&nbsp;&nbsp;
            </div>
        </form>                
    </div>

    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <h5> Dirección</h5>
        <br>
        <input type="hidden" id="status_direction" value="1">
        <form id="form_direction">
            <div class="row">
                <div class="col-md-7 col-xs-7">
                    <input type="hidden" class="form-control" name="id_cliente" value="<?php echo $direccion[0]["id_cliente"] ?>"  required>
                    <input type="text" class="form-control" name="titulo" value="<?php echo $direccion[0]["titulo"] ?>"  required>
                </div>
                <div class="col-md-5 col-xs-5">
                    <input type="text" class="form-control" name="calle" value="<?php echo $direccion[0]["calle"]  ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-xs-4">
                <input type="text" class="form-control" name="numero" value="<?php echo $direccion[0]["numero"]  ?>"  required>
                </div>
                <div class="col-md-3 col-xs-3">
                    <input type="text" class="form-control" name="int" value="<?php echo $direccion[0]["int"]  ?>">  
                </div>
                <div class="col-md-5 col-xs-5">
                <input type="text" class="form-control" name="ciudad" value="<?php echo $direccion[0]["ciudad"]  ?>"  required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-xs-5">
                <select name="estado">
                    <?php foreach ($us_states as $state) { ?>
                        <option value="<?=$state?>" <?= ($state == $direccion[0]["estado"])? 'selected' : ''  ?> ><?= $state ?></option>
                    <?php } ?>      
                </select>
                </div>
                <div class="col-md-4 col-xs-4">
                <input type="text" class="form-control" name="cp" value="<?php echo $direccion[0]["cp"]  ?>" required>
                </div>
            </div>
            <div class="row" style="text-align: center;">
                <button  class="btn btn-block btn-success" id="direction-button">Editar</button>
            </div>
        </form>
    </div>
    <?php } ?>
<?php } ?> 

<!-- loader -->
<div class="col-lg-12">
    <div id="loader" class="loader-ring" style="height: 70px; display: none">
    </div>
</div>
<!-- end loader -->