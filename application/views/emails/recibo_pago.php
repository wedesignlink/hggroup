<?php
// $folio
// $finicio
// $ffin
// $plan
// $subtotal = $monto;
// $iva = $monto*.16;
// $montoIva = $subtotal+$iva;
?>
<html>
<head>
  <meta charset='UTF-8'>
  <title>Document</title>
</head>
<body>
  <br />
  <div style='height:auto; border: none 2px lightgray; font-family:sans-serif;padding: 40px;' ><br />
    <img src="http://hgroupcustombbqspatio.com/demo/public/images/theme/uploads/logo_blanco.png" style="border-radius: 0px; border-color: rgb(78, 78, 78); border-style: none; width: 150px; max-height: none; border-width: 1px !important;" >
    <br><br><br>
    <h3> Hello <?=$nombre?>, </h3>
    <br>
    <h3>Gracias por tu compra! te hacemos llegar el resumen de tu compra</h3>
    <br>
    <span style="color: #000;font-weight: lighter; font-size: 2.1em;">H GROUP BBQ</span>
    <br><br><br><br>

    <span style="color: #000;font-weight: lighter; font-size: 32px;">Información de tu recibo</span>
    <br><br>
    <strong style="font-size: 20px; color: #000;">Folio: <?=$folio?></strong>  <br><br>
    <strong style="font-size: 20px; color: #000;">Fecha: <?=$fecha_pago?> </strong><br><br>



    <strong style="font-size: 20px; color: #000;">Productos </strong> <br><br><br>
    <table class="table table-hover" width="100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Description</th>
                                            <th class="text-center">Color</th>
                                            <th class="text-center">Texture</th>
                                            <th class="text-center">Total</th>
                                        </tr>

                                    </thead>
                                    <tbody>

                                  <?php
$index = 0;
foreach ( $cart_session["products"] as $key => $value ) {?>
                                    <tr>
                                      <td ><?=++$index?></td>
                                      <td class="text-center"> <?=$value["product"]["nombre"]?>
                                      </td>
                                      <td class="text-center"> <?=$value["color"]?></td>
                                      <td class="text-center"> <?=$value["texture"]?></td>
                                      <td class="text-center">$ <?=number_format( $value["product"]["precio"] * $value["quantity"], 2 )?> </td>
                                    </tr>
                                   <?php }?>
                                    </tbody>
    </table>
    <br><br>
    <br>
    <strong style="font-size: 20px; color: #000;">Subtotal: $<?=number_format( $montoTotal, 2 )?>  USD</strong><br><br>
    <strong style="font-size: 20px; color: #000;">Taxes: $<?=number_format( $montoImpuestos, 2 )?>  USD</strong><br><br>
    <strong style="font-size: 20px; color: #000;">Total: $<?=number_format( $total, 2 )?>  USD</strong><br><br>

    <br>
     <br><br><br>
    <p class="text-center hidden-print">
            <a style="display: inline-block;background-color: #e15516; padding: 20px 40px; color:white; text-decoration: none; border-radius: 60px; box-shadow: 1px 1px 10px #ccc;"> Gracias por su Compra</a>
        </p>
    <br>
    <hr>
    <br><br><br><strong>H GROUP BBQ AND PATIO</strong><br>

  </div>
</body>
</html>

