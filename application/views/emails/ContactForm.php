<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
​
    <title>INFORMACIÓN DEL CONTACTO</title>
  </head>
  <body>
​
    <div style="width: 600px; margin: 0 auto; text-align: center; font-family: Arial, Helvetica, sans-serif;">
        <div style="width: 600px; height: 50px; background-color: #2c353f; color: white;">
            <h2 style="margin-top: 30px; padding-top: 10px;">Bienvenido a H Group BBQ & Patio</h2>
        </div>
        <div class="container text-center p-4">

            <img src="hgroupbbq/public/images/theme/icon/logo_blanco.png" style="padding-top: 20px;">
​
        </div>
​
        <p style="margin-bottom: 30px; text-align: center; font-family: 'Open Sans', sans-serif;"> La persona con el Nombre de :  <strong><?=$nombre?></strong>
        <br> Con el Correo <?=$email?>.&nbsp;&nbsp;&nbsp; </p>
        <br><br><br><br><br><br>
        <p style="margin-bottom: 30px; text-align: center; font-family: 'Open Sans', sans-serif;">Requiere de nuestro apoyo con el siguiente Mensaje:&nbsp;&nbsp; <?=$comentario?>
        <br><br><br>

        <a href="url" class="btn btn-primary btn-lg btn-outline-light bg-warning rounded mx-auto h-100 bg-info d-flex justify-content-center align-items-center" style="font-size: 12px; font-weight: 500; font-family: Montserrat, sans-serif; background-color: darkorange; padding: 15px 25px; color: white; text-decoration: none;"> </a>
​
        <div style="margin-top: 50px;">
​
            <hr>
            <p class="text-medium font-weight-600 text-dark" style="text-align: left; padding-top: 10px; font-size: 16px; font-weight: 500; font-family: Montserrat, sans-serif">H GROUP</p>
            <p class="font-weight-light align-content-center" style="text-align: left; text-align: justify;"><small style="font-family: 'Open Sans', sans-serif;">Somos un grupo especialista en el diseño de espacios prácticos, decorativos y funcionales para que pases los mejores momentos, da vida a tu patio con nuestras soluciones, diseña ahora tu espacio ideal.</small></p>
            <p class="lead">
​
        </div>

    </div>

  </body>
</html>