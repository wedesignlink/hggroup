<section class="bg-white cover-background padding-110px-tb feature-style3 xs-padding-60px-tb tz-builder-bg-image" data-img-size="(W)1920px X (H)750px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?= base_url() ?>public/images/theme/uploads/bbq.jpg');">
    <div class="container" style="background-color: black;"></div>
</section>
<section class="bg-white builder-bg padding-40px-tb feature-style3 xs-padding-60px-tb" id="feature-section32">
<input type="hidden" id="actualStep" value="Start">

    <!-- Step List -->
    <div class="row text-center hidden" id="stepButtons">
        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 ">
            <button type="button" class="btn directionButton" value="prev">
                <h1><</h1>
                <h6>Previous</h6>
            </button>
        </div>
        <div class="col-md-8 col-lg-8 col-sm-8 col-xs-8">
            <button type="button" class="btn activeButton" id="One">
                <p class="numberStep">1</p> 
                <p class="titleStep">Kitchen Layout</p>
            </button>
            <button type="button" class="btn stepButton" id="Two">
                <p class="numberStep">2</p> 
                <p class="titleStep">Island Sizes</p>
            </button>
            <button type="button" class="btn stepButton" id="Three">
                <p class="numberStep">3</p> 
                <p class="titleStep">Appliance Layout</p>
            </button>
            <button type="button" class="btn stepButton" id="Four">
                <p class="numberStep">4</p> 
                <p class="titleStep">Appliances</p>
            </button>
            <button type="button" class="btn stepButton" id="Five">
                <p class="numberStep">5</p> 
                <p class="titleStep">3D View</p>
            </button>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
            <button type="button" class="btn directionButton" value="next">
                <h1>></h1>
                <h6>Next</h6>
            </button>
        </div>
    </div>
    <!-- End Step List -->
    
    <div class="row">
        <div class="col-lg-12 overlaySpin text-center">
            <div class="loader-ring-white" id="blackLoader"></div>
        </div>
    </div>
    
    <div id="projectView">
        <!-- Grill Project -->
        <div class="grillProject">
            <h2>Start your project</h2>
            <br>
            <label>Create a new plan:</label>
            <br>
            <input placeholder="Enter project name" id="projectName"></input>
            <br>
            <button type="button" class="btn" id="start">START DESIGNING</button>
        </div>
        <!-- End Grill Project -->
    </div>

</section>

