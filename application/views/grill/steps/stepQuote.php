<h4 class="currentStep">Get Your Custom Outdoor Kitchen Design Quote:</h4>
<div class="row quoteForm">

    <div class="col-md-12 col-xs-12">
        <form id="formQuote">
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <label>First Name</label>
                    <input type="text" placeholder="First Name" class="form-control" name="name" required>
                </div>
                <div class="col-md-6 col-xs-6">
                    <label>Last Name</label>
                    <input type="text" placeholder="Last Name" class="form-control" name="last_name" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <label>Email (quote sent here)</label>
                    <input type="email" placeholder="Email Address" class="form-control" name="email" required>
                </div>
                <div class="col-md-6 col-xs-6">
                    <label>Phone</label>
                    <input type="text" class="form-control" name="number" placeholder="111-222-3333" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <label>Zip Code</label>
                    <input type="text" class="form-control" name="zip" placeholder="11111" required>
                </div>
                <div class="col-md-6 col-xs-6">
                    <label>I...</label>
                    <select name="reason">
                            <option value="Know the product I want to purchase">Know the product I want to purchase</option>
                            <option value="Would like to speak with someone about an RTA design">Would like to speak with someone about an RTA design</option>
                            <option value="Am actively researching for a project">Am actively researching for a project</option>
                            <option value="Am just browsing">Am just browsing</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <label>What is the timeline for you outdoor kitchen project?</label>
                    <select name="timeline">
                            <option value="Inmediate">Inmediate</option>
                            <option value="1">1 Month</option>
                            <option value="3">3 Months</option>
                            <option value="6">6 Months</option>
                    </select>
                </div>
                <div class="col-md-6 col-xs-6">
                    <label>Island Grill/Burner Fuel Source</label>
                    <select name="fuelSource">
                            <option value="NG">Natural Gas (NG)</option>
                            <option value="LP">Liquid Propane (LP)</option>
                            <option value="NA">Not Applicable</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <label>Delaer (if applicable)</label>
                    <input type="text" class="form-control" placeholder="Company Name" name="dealer">  
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <label>Is there anything else you would like to tell us about your design project?</label>
                    <textarea class="form-control" name="comments" placeholder="Additional Project Comments:"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <button  class="btn" id="quoteButton">Submit</button>
                </div>
            </div>
        </form>
    </div>

</div>