<div class="row">
    <div class="col-md-2 text-center">
        <span style="color:black;"><b>Current Island</b></span>   
        <br> 
        <a href="">
            <img class="layoutImg" alt="" src="<?= base_url() ?>public/uploads/productos/BH-02.png">
        </a>
    </div>
    <div class="col-md-8">
        <h4 class="currentStep">Choose your Appliances (Part 1):<h4>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="card card-body printableArea applianceView">
            <!-- Appliance Slider Large -->
            <div id="applianceSliderLarge" class="carousel slide" data-interval="false">
                <!-- Content -->
                <div class="carousel-inner" role="listbox">
                    <!-- Slide 1 -->
                    <div class="item active">
                    <img src="<?= base_url() ?>public/uploads/productos/308148.png">
                    </div>
                    <!-- Slide 2 -->
                    <div class="item">
                    <img src="<?= base_url() ?>public/uploads/productos/308155.png">
                    </div>
                    <!-- Slide 3 -->
                    <div class="item">
                    <img src="<?= base_url() ?>public/uploads/productos/308156.png">
                    </div>
                </div>
                <!-- End Content -->
                <!-- Control Buttons Large -->
                <a class="left carousel-control" href="#applianceSliderLarge" id="prevButtonLarge" role="button" data-slide="prev">
                    <span aria-hidden="true"><img src="<?= base_url() ?>public/uploads/grill/arrow.png"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#applianceSliderLarge" id="nextButtonLarge" role="button" data-slide="next">
                    <span aria-hidden="true"><img src="<?= base_url() ?>public/uploads/grill/arrow.png"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!-- End Control Buttons Large -->
            </div>
            <!-- End Appliance Slider Large -->

            <!-- Appliance Slider -->
            <div id="applianceSlider" class="carousel slide" data-interval="false">
                <!-- Content -->
                <div class="carousel-inner" role="listbox">
                    <!-- Slide 1 -->
                    <div class="item active">
                    <img src="<?= base_url() ?>public/uploads/productos/308555.png">
                    </div>
                    <!-- Slide 2 -->
                    <div class="item">
                    <img src="<?= base_url() ?>public/uploads/productos/308556.png">
                    </div>
                    <!-- Slide 3 -->
                    <div class="item">
                    <img src="<?= base_url() ?>public/uploads/productos/308567.png">
                    </div>
                </div>
                <!-- End Content -->
                <!-- Control Buttons -->
                <a class="left carousel-control" href="#applianceSlider" id="prevButton" role="button" data-slide="prev">
                    <span aria-hidden="true"><img src="<?= base_url() ?>public/uploads/grill/arrow.png"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#applianceSlider" id="nextButton" role="button" data-slide="next">
                    <span aria-hidden="true"><img src="<?= base_url() ?>public/uploads/grill/arrow.png"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!-- End Control Buttons -->
            </div>
            <!-- End Appliance Slider -->
        </div>
    </div>

    <div class="col-md-5">
        <div class="card card-body printableArea">

            <div class="applianceItem">
                <h6><b>Grill:</b></h6>
                <span>Click arrow to select appliance</span>
            </div>
            <div class="applianceItem">
                <h6><b>Grill:</b></h6>
                <span>Click arrow to select appliance</span>
            </div>
        </div>
    </div>
</div>