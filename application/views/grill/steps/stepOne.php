<h4 class="currentStep">Select your Kitchen Layout:</h4>
<div class="row text-center justify-content-center layoutList">
    <div class="col-md-4 text-center layoutItem">
        <a href="">
            <img class ="layoutImg" alt="" src="<?= base_url() ?>public/uploads/productos/BH-02.png">
        </a>
        <br>
        <span class="typeLayout">Straight</span>
        <br>
        <span class="typeLayoutExtra">(Right-Aligned)</span>
    </div>
    <div class="col-md-4 text-center">
        <a href="">
            <img class ="layoutImgSelected" alt="" src="<?= base_url() ?>public/uploads/productos/BH-02.png">
        </a>
        <br>
        <span class="typeLayout">Straight</span>
        <br>
        <span class="typeLayoutExtra">(Right-Aligned)</span>
    </div>
    <div class="col-md-4 text-center">
        <a href="">
            <img class ="layoutImg" alt="" src="<?= base_url() ?>public/uploads/productos/BH-02.png">
        </a>
        <br>
        <span class="typeLayout">Straight</span>
        <br>
        <span class="typeLayoutExtra">(Right-Aligned)</span>
    </div>
</div>        

