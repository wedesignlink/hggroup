
<div class="row">
            <div class="col-md-2 text-center">
                <span style="color:black;"><b>Current Island</b></span>   
                <br> 
                <a href="">
                    <img class="layoutImg" alt="" src="<?= base_url() ?>public/uploads/productos/BH-02.png">
                </a>
            </div>
            <div class="col-md-8">
                <h4 class="currentStep">Select your Grill Island Size:</h4>
            </div>
        </div>

        <div class="row text-center justify-content-center islandList">
            <div class="col-md-3 text-center islandItem">
                <a href="">
                    <img class ="islandImg" alt="" src="<?= base_url() ?>public/uploads/productos/BH-02.png">
                </a>
                <br>
                <span class="typeIsland">4ft Island</span>
                <br>
                <span class="typeIslandBase">(Base is 40")</span>
            </div>
            <div class="col-md-3 text-center islandItemSelected">
                <a href="">
                    <img class ="islandImg" alt="" src="<?= base_url() ?>public/uploads/productos/BH-02.png">
                </a>
                <br>
                <span class="typeIsland">5ft Island</span>
                <br>
                <span class="typeIslandBase">(Base is 60")</span>
            </div>
            <div class="col-md-3 text-center islandItem">
                <a href="">
                    <img class ="islandImg" alt="" src="<?= base_url() ?>public/uploads/productos/BH-02.png">
                </a>
                <br>
                <span class="typeIsland">6ft Island</span>
                <br>
                <span class="typeIslandBase">(Base is 72")</span>
            </div>
            <div class="col-md-3 text-center islandItem">
                <a href="">
                    <img class ="islandImg" alt="" src="<?= base_url() ?>public/uploads/productos/BH-02.png">
                </a>
                <br>
                <span class="typeIsland">8ft Island</span>
                <br>
                <span class="typeIslandBase">(Base is 90")</span>
            </div>
        </div>  
        
        <div class="note text-center">
            <h5><b>Note:</b></h5>
            <p>The connecting island is an <br>8ft Bar Island</p>
        </div>