<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
        <!-- title -->
        <title>H Group Custom BBQ'S & Patio</title>
        <meta name="description" content="H Group BBQ'S & Patio diseña tu patio ideal" />
        <meta name="keywords" content="barbecue, patio, custom" />
        <meta name="author" content="Devux">
        <!-- favicon -->
        <link rel="shortcut icon" href="<?=base_url()?>public/images/theme/icon/logo_negro.png">
        <!-- animation -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/animate.css" />
        <!-- bootstrap -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/bootstrap.min.css" />
        <!-- font-awesome icon -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/font-awesome.min.css" />
        <!-- themify-icons -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/themify-icons.css" />
        <!-- owl carousel -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/owl.transitions.css" />
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/owl.carousel.css" />
        <!-- magnific popup -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/magnific-popup.css" />
        <!-- base -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/base.css" />
        <!-- elements -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/elements.css" />
        <!-- responsive -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/theme/responsive.css" />
        <!-- checkout -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/checkout.css" />
        <!-- grill -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/grill.css" />
        <!-- spinners -->
        <link rel="stylesheet" href="<?=base_url()?>public/css/spinners.css" />
        <!-- sweetaler -->
        <link rel="stylesheet" href="<?=base_url()?>public/assets/plugins/sweetalert/sweetalert.css" />
        <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>public/css/theme/ie.css" />
        <![endif]-->
        <!--[if IE]>
            <script src="<?=base_url()?>public/js/theme/html5shiv.min.js"></script>
        <![endif]-->
        <?php $this->lang->load('store_home_header',$this->session->userdata('site_lang'));?>

    <!-- jQuery -->
    <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js'></script>
        <!-- Popper JS -->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js'></script>
        <!-- Bootstrap JS -->
        <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'></script>
    
<style>
@media (min-width: 576px) {
  .modal-dialog {
    max-width: 400px;
  }
  .modal-dialog .modal-content {
    padding: 1rem;
  }
}
.modal-header .close {
  margin-top: -1.5rem;
}

.form-title {
  margin: -2rem 0rem 2rem;
}

.btn-round {
  font-weight: 500px;
  max-width: 100%;
  width: 100%;
  height: 100%;
  padding: 7px;
  background: #FF9900;
  border-color: #FF9900;
  border-radius: 35px;
}

.delimiter {
  padding: 10rem;
}

.social-buttons .btn {
  margin: 0 0.5rem 1rem;
}

.signup-section {
  padding: 0.3rem 0rem;
}


</style>
</head>

        <body>


        <div id="page" class="page"><div class="header-style8">
                <header class="header-style8" id="header-section16">
                    <!-- nav -->
                    <nav class="navbar tz-header-bg no-margin alt-font navigation-menu dark-header" style="max-height: none;">
                        <!-- logo -->
                        <div class="pull-left" style="
    height: 200px !important;
">
                            <a href="#home" class="inner-link" style="height: 200px !important;"><img alt="" src="<?=base_url()?>public/images/theme/uploads/logo_blanco.png" data-img-size="(W)163px X (H)40px" id="ui-id-7" style="border-radius: 0px; border-color: rgb(78, 78, 78); border-style: none; width: 200px; max-height: none; border-width: 1px !important;"></a>
                        </div>
                        <!-- end logo -->
                        <div class="pull-right">
                            <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse pull-right">
                                <ul class="nav navbar-nav">
                                    <li class="propClone"><a class="inner-link" href="<?= base_url() ?>index.php/Store/catalog" style="color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0); border-color: rgb(255, 255, 255) rgb(255, 255, 255) rgba(0, 0, 0, 0); font-size: 12px; font-family: Montserrat, sans-serif; text-transform: none; font-weight: 700 !important;" id="ui-id-33"><?= strtoupper($this->lang->line('productos'));?></a></li>
                                    <li class="propClone"><a class="inner-link" href="#" style="color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0); border-color: rgb(255, 255, 255) rgb(255, 255, 255) rgba(0, 0, 0, 0); font-size: 12px; font-family: Montserrat, sans-serif; text-transform: none; font-weight: 700 !important;" id="ui-id-34"><?= strtoupper($this->lang->line('diseña'));?></a></li>
                                    <li class="propClone"><a class="inner-link" href="#" style="color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0); border-color: rgb(255, 255, 255) rgb(255, 255, 255) rgba(0, 0, 0, 0); font-size: 12px; font-family: Montserrat, sans-serif; text-transform: none; font-weight: 700 !important;" id="ui-id-35"><?= strtoupper($this->lang->line('contacto'));?></a></li>
                                    <?php if ($this->session->userdata('logged_in') == 1){?>
                                    <li><a id="logged_link" style="color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0); border-color: rgb(255, 255, 255) rgb(255, 255, 255) rgba(0, 0, 0, 0); font-size: 12px; font-family: Montserrat, sans-serif; text-transform: none; font-weight: 700 !important;" href="<?= base_url()?>index.php/Dashboard/logout"><?= strtoupper($this->lang->line('cerrar sesion'));?></a></li>     
                                    <?php }else{?>
                                    <li><a id="logged_link" class="open_signup-modal" style="color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0); border-color: rgb(255, 255, 255) rgb(255, 255, 255) rgba(0, 0, 0, 0); font-size: 12px; font-family: Montserrat, sans-serif; text-transform: none; font-weight: 700 !important;" href="#"><?= strtoupper($this->lang->line('iniciar sesion'));?></a></li>
                                    <?php }?>
                                    <li><a href="<?= base_url() ?>index.php/LangSwitch/switchLanguage/<?= $this->lang->line('idioma');?>" class="btn btn-default" style="color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0); border-color: rgb(255, 255, 255) rgb(255, 255, 255) rgba(0, 0, 0, 0); font-size: 12px; font-family: Montserrat, sans-serif; text-transform: none; font-weight: 700 !important;"><?= $this->lang->line('idioma');?></a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <!-- end nav -->
                </header>

                <!-- Login Modal -->
                <div class="modal fade bd-example-modal-lg" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" style= "text-align: center">Login</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                        <form>
                            <div class="form-group">
                            <input type="email" class="form-control" id="loginEmail" placeholder="Your email address...">

                            </div>
                            <div class="form-group">
                            <input type="password" class="form-control" id="loginPass" placeholder="Your password...">
                            </div>
                            <button type="button" id= "loginButton" class="btn btn-info btn-block btn-round loginButton">Login</button>
                        </form>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <div class="signup-section">Not a member yet? <a href="#a" id="signup" class="text-info signup"> Sign Up</a>.</div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Signup Modal -->
                <div class="modal fade bd-example-modal-lg" id="signup_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" style= "text-align: center">Register</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                        <form id="signup_form">
                            <div class="form-group">
                            <input type="text" class="form-control" id="register_first-name" placeholder="First name" name="name">
                            <input type="email" class="form-control" id="register_lastname" placeholder="Last name" name="last_name">
                            <input type="email" class="form-control" id="register_telephone" placeholder="Telephone" name="phone">

                            </div>
                            <div class="form-group">
                            <input type="email" class="form-control" id="register_email" placeholder="Email address" name="email">
                            <input type="password" class="form-control" id="register_pass" placeholder="Password" name="password">
                            <input type="password" class="form-control" id="register_pass-confirm" placeholder="Confirm Password" name="c_password">
                            </div>
                            <button type="button" id= "register_button" class="btn btn-info btn-block btn-round register_button">Register</button>
                        </form>
                        </div>

                        </div>
                    </div>
                </div>



<script>


$(document).on('click', '.open_signup-modal', function(e){
    e.preventDefault();
    $('#login_modal').modal('show');

});

$(document).on('click', '.signup', function(e){
    $('#login_modal').modal('hide');
    $('#signup_modal').modal('show');

});

/* Login button on modal */
$(document).ready(function (e) {

    // Login by click
    $('#loginButton').click(function(){
        let email = $('#loginEmail').val();
        let password = $('#loginPass').val();

    if ( !IsEmail(email) ) {
        swal({
            title:'Enter a valid email :(',
            timer: 2000
        });
    }
    else if (password.length < 7) {
        swal({
            title:'The password is to short :(',
            timer: 2000
        });
    }
    else {
        $.ajax({
            url: base_url + 'index.php/Checkout/login',
            data: { email:email, password:password },
            method: 'POST',
            success: function (res) {
                    if (res) {
                        swal({
                            title: "Welcome Back :)",
                            timer: 3000,
                        });
                        $('#logged_link').attr('href',base_url+'index.php/Dashboard/logout');
                        $('#logged_link').text("LOGOUT");
                        $('#logged_link').removeClass('open_signup-modal');
                        $('#login_modal').modal('hide');
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    }
                    else {
                        swal({
                            title: "The email or password are not correct :(",
                            timer: 2000,
                        });
                    }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    })
    // Login pressing enter
    $('#loginPass, #loginEmail').keypress(function(e){
        if(e.which == 13) {
            $('#loginButton').click();
        }
    })
});


/* Sign-up on modal */
$(document).on('click', '#register_button', function (e) {
    e.preventDefault();

    let data = $('#signup_form').serializeArray();

    var dataObj = {};

    // convert array to object
    for (var i = 0; i < data.length; i++)
        dataObj[data[i].name] = data[i].value;

    let errors = verifyRegisterData( dataObj, data );

    // valid form
    if ( !errors.length ){
        $.ajax({
            url: base_url + 'index.php/Checkout/registerThenLogin',
            data: {
                nombre   :dataObj.name,
                apellido :dataObj.last_name,
                email    :dataObj.email,
                password :dataObj.password,
                telefono :dataObj.phone
            },
            method: 'POST',
            success: function (res) {
                    if (res) {
                        swal({
                            title: "Thanks for registering!",
                            text: "Check your email for verification",
                            timer: 4000
                        });
                        $('#logged_link').attr('href',base_url+'index.php/Dashboard/logout');
                        $('#logged_link').removeClass('open_signup-modal');
                        $('#logged_link').text("LOGOUT");
                        $('#signup_modal').modal('hide');
                        setTimeout(() => {
                            location.reload();
                        }, 2000);
                    } else {
                        swal({
                            title: "The email is already registered :(",
                            timer: 2000
                        });
                    }
            },
        });
    }
    else {
        swal({
            title: errors[0],
            timer: 2000
        });
    }

});


function verifyRegisterData( data, keys ) {
    let mistakes = [];

    // change color of border
    for (let i = 0; i < keys.length; i++) {
        if ( !keys[i].value )
            $(`[name="${keys[i].name}"]`).css("border","solid rgba(139,0,0,0.7) 1px");
        else
            $(`[name="${keys[i].name}"]`).css("border","1px solid #ccc");
    }

    // name
    if ( !data.name || !data.last_name || !data.email
         || !data.email || !data.phone || !data.password
         || !data.c_password)
    {
        mistakes.push( "Please complete all fields" );
        return mistakes;
    }

    if ( !IsEmail( data.email ) ) {
        mistakes.push( "Not valid email" );
        $(`[name="email"]`).css("border","solid rgba(139,0,0,0.7) 1px");
    }

    if ( String(data.phone).length != 10 ) {
        mistakes.push( "Not valid phone number" );
        $(`[name="phone"]`).css("border","solid rgba(139,0,0,0.7) 1px");
    }

    if ( data.password.length < 7) {
        mistakes.push( "The password is to short" );
        $(`[name="password"]`).css("border","solid rgba(139,0,0,0.7) 1px");
        $(`[name="c_password"]`).css("border","solid rgba(139,0,0,0.7) 1px");
    }

    else if ( data.password !== data.c_password ) {
        mistakes.push( "Passwords do not match" );
        $(`[name="password"]`).css("border","solid rgba(139,0,0,0.7) 1px");
        $(`[name="c_password"]`).css("border","solid rgba(139,0,0,0.7) 1px");
    }

    return mistakes;
}

function IsEmail( email ) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) return false;
    else return true;
}

</script>
