<footer id="footer-section11" class="padding-30px-tb bg-dark-gray builder-bg">
    <div class="container">
        <div class="row equalize">
            <!-- caption -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 xs-text-center xs-margin-four-bottom display-table" style="">
                <div class="display-table-cell-vertical-middle">
                    <a href="#home" class="inner-link"><img src="<?= base_url() ?>public/images/theme/uploads/logo_blanco.png" data-img-size="(W)163px X (H)40px" alt="" id="ui-id-31" style="border-radius: 0px; border-color: rgb(78, 78, 78); border-style: none; border-width: 1px !important; width:150px;"></a>
                </div>
            </div>
            <!-- end caption -->
            <!-- caption -->
            <div class="col-md-6 col-sm-6 col-xs-12 text-right xs-text-center display-table" style="">
                <div class="display-table-cell-vertical-middle">
                    <span class="text-light-gray tz-text">© 2021 HG is proudly powered by <a class="text-light-gray" href="http://www.devux.com.mx.com/">Devux.</a></span>
                </div>
            </div>
            <!-- end caption -->
        </div>
    </div>
</footer>
</div><!-- /#page -->

<script>
    var base_url = "<?= base_url() ?>";
</script>
<!-- javascript libraries -->


<script type="text/javascript" src="<?= base_url() ?>public/js/theme/jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/jquery.appear.js"></script>
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/smooth-scroll.js"></script>
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/bootstrap.min.js"></script>
<!-- wow animation -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/wow.min.js"></script>
<!-- owl carousel -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/owl.carousel.min.js"></script>
<!-- images loaded -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/imagesloaded.pkgd.min.js"></script>
<!-- isotope -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/jquery.isotope.min.js"></script>
<!-- magnific popup -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/jquery.magnific-popup.min.js"></script>
<!-- navigation -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/jquery.nav.js"></script>
<!-- equalize -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/equalize.min.js"></script>
<!-- fit videos -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/jquery.fitvids.js"></script>
<!-- number counter -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/jquery.countTo.js"></script>
<!-- time counter  -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/counter.js"></script>
<!-- twitter Fetcher  -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/twitterFetcher_min.js"></script>
<!-- main -->
<script type="text/javascript" src="<?= base_url() ?>public/js/theme/main.js"></script>
<!-- sweetalert -->
<script type="text/javascript" src="<?= base_url() ?>public/assets/plugins/sweetalert/sweetalert.min.js"></script>
<!-- checkout -->
<script type="text/javascript" src="<?= base_url() ?>public/js/checkout.js"></script>
<!-- checkout -->
<script type="text/javascript" src="<?= base_url() ?>public/js/grill.js"></script>
</body>

</html>