<style>
body {
    background-color: gray;
}
</style>
<section>
    <div>
        <input type="number" placeholder="ID" id="id_test">
        <button id="test_function">Touch me!</button>
    </div>

    <div id="res"></div>
</section>

<script>
    var base_url = "<?= base_url() ?>";
</script>

<script type="text/javascript" src="<?= base_url() ?>public/js/theme/jquery.min.js"></script>

<script>
    $(document).on("click","#test_function", function () {
        let id = $("#id_test").val();

        $.ajax({
            url: base_url + 'index.php/API/postDesign',
            data:
            { 
                model :
                {
                    id_client: 3,
                    name:"Este es un nuevo proyecto3",
                    id_plan: 1,
                    id_fuel: 1,
                    id_shape: 1,
                    id_bar: 1,
                    id_finish: 1,
                    commentary: "This is a commentary3",
                    islands: [
                        {id_dist:3,id_products:[14]},
                        {id_dist:4,id_products:[31]},
                    ]
                } 
            },
            method: 'POST',
            success: function (res) {
                console.log ( JSON.parse(res) );
            }
        });
    });
</script>