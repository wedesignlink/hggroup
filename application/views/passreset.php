<section class="bg-white cover-background padding-110px-tb feature-style3 xs-padding-60px-tb tz-builder-bg-image" data-img-size="(W)1920px X (H)750px" style="padding-top: 0px; padding-bottom: 0px; background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 0.01)), url('<?= base_url() ?>public/images/theme/uploads/bbq.jpg');">
    <div class="container" style="background-color: black;"></div>
</section>

<section class="bg-white builder-bg padding-40px-tb feature-style3 xs-padding-60px-tb" id="feature-section32" style="min-height: 50vh;">
    <div class="container">

		<div class="row">
			<div class="col-md-8 float-none" style="margin: 0 auto;">
					<?php if($access){ ?>
						<h2 class="text-center">Resetea tu contraseña</h2> <br>
						
						<form method="POST" action="<?= base_url() ?>index.php/Dashboard/Reseting/?token=<?=$token?>">
							<div class="form-group">
								<input placeholder="Ingresa tu nueva contraseña" type="password" name="nuevapass" minlength="8">
							</div>

							<div class="form-group">
								<input placeholder="Confirma tu contraseña" type="password" name="nuevapassc">
							</div>

							<div class="form-group">
							</div>
							
							<div class="form-group">
								<?php if($this->session->flashdata('noiguales')){ ?>
									<div class="alert alert-danger text-center" role="alert">
										<?=$this->session->flashdata('noiguales')?>
									</div>
								<?php }?>
							</div>


							<input class="btn btn-info btn-block" type="submit">

							
						</form>
					<?php }else{ ?>
						<h2 class="text-center">El token ha expirado</h2>
					<?php }?>
			
			</div>
		</div>
	</div>
</section>

<!-- 
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resetear Contraseña</title>
</head>
<body style="margin: 0; background-color: #504840;display: inline-block;">
	<div style="padding-top:2em; position: absolute; width: 50%; height: 25em; left: 23em; clear: both;">
		<form style="display: inline-block; position: absolute; left:45%; bottom: 30%;" method="POST" action="<?= base_url() ?>index.php/Dashboard/Reseting/?token=<?=$token?>">
			<img style="display: block; position: absolute; height: 130px; width: 130px; left: 65px; bottom: 100px;" src="<?= base_url() ?>public/images/theme/uploads/muestra_dorado.png">
			<p style=" position: absolute; left: -80px; margin:5px 0px 2em 0px;bottom: 10%;" ><input placeholder="Ingresa tu nueva contraseña" style=" width: 30em;height:20px; text-align: center;" type="password" name="nuevapass" minlength="8"></p>

			<p style="position: absolute; left: -80px; margin:5px 0px 2em 0px"><input placeholder="Confirma tu contraseña" style="width: 30em; height: 20px; text-align: center;" type="password" name="nuevapassc"></p>

			<input style=" position: absolute; left: 30px; width: 200px; height: 30px; border: none; border-radius: 10px; top: 6em; background-color: darkorange" type="submit">
		</form>
		<div style="font-size: 20px; position: absolute; left: 46%; bottom: -10%; display: inline-block; padding-top: 10px">
	        <?php if($this->session->flashdata('noiguales')){ ?>
    	        <p style="color: #C20404;"><?=$this->session->flashdata('noiguales')?></p>
    	    <?php }?>
        </div>
	</div>
	<footer style="width: 100%;height: 110px; position: absolute; border-top: ridge #392F24 5px; top: 80%; right: 0em ; display: inline-block">
	    <span style="font-size: 20px; color: #807E7C" >© 2021 HG is proudly powered by <a style="color: #807E7C; font-size: 20px; text-decoration: none;" class="text-light-gray" href="http://www.devux.com.mx.com/">Devux.</a></span>
    </footer>
</body>
</html> -->