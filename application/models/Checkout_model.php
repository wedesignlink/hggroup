<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database(); 
    
    }

     /**
     * Get direction from client
     *
     * @param string $id_client
     * @return void
     */
     function CNS_DirectionClientData( $id_client)
     {
         $this->db->select("*");
         $this->db->from("tbl_direcciones");
         $this->db->where("id_cliente", $id_client);

         $query = $this->db->get();
         return $query->result_array();
     }

     /**
     * Get images from product
     *
     * @param string $data
     * @return void
     */
    public function receipt_registered ($data)
    {
        $this->db->insert('tbl_recibos', $data);
    }
    

    

    /**
     * Get images from product
     *
     * @param string $id_producto
     * @return void
     */
    function CNS_ImagesProduct( $id_producto ){
        $this->db->select("imagen");
        $this->db->from("rel_imagenes_producto");
        $this->db->where("id_producto", $id_producto);
        
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Get adresses from client
     *
     * @param string $id_client
     * @return void
     */
   
    public function Direccion_insert($data, $bandera=0 )
    {
        if( $bandera == 0){ 
            $this->db->insert('tbl_direcciones', $data);
        } else {
            $this->db->where('id_cliente', $data["id_cliente"] );
            $this->db->update('tbl_direcciones', $data);
        }
    }

    public function CNS_ClientAdresses( $id_cliente ){
        $this->db->select("nombre, apellido, empresa, direccion, codigo_postal");
        $this->db->from("tbl_direcciones");
        $this->db->where("id_cliente", $id_cliente);
        
        $query = $this->db->get();
        return $query->result_array();
    }

}