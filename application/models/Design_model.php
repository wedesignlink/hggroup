<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Design_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database(); 
    }

    public function getShapes()
    {
        $this->db->select("*");
        $this->db->from("tbl_formas");

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOneShape( $id_shape )
    {
        $this->db->select("*");
        $this->db->from("tbl_formas");
        $this->db->where("id",$id_shape);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getIslandsFromShape( $id_shape )
    {
        $this->db->select("*");
        $this->db->from("tbl_islas as i");
        $this->db->join("rel_formas_islas as r_fs", "r_fs.id_isla = i.id", "left");
        $this->db->where("r_fs.id_forma", $id_shape);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOneIsland( $id_island )
    {
        $this->db->select("*");
        $this->db->from("tbl_islas");
        $this->db->where("id", $id_island);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSizes()
    {
        $this->db->select("*");
        $this->db->from("tbl_tamaños");

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOneSize( $id_size )
    {
        $this->db->select("*");
        $this->db->from("tbl_tamaños");
        $this->db->where("id",$id_size);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAccessories()
    {
        $this->db->select("*");
        $this->db->from("tbl_accesorios");

        $query = $this->db->get();
        return $query->result_array(); 
    }

    public function getOneAccessory( $id_accesorie )
    {
        $this->db->select("*");
        $this->db->from("tbl_accesorios");
        $this->db->where("id", $id_accesorie);

        $query = $this->db->get();
        return $query->result_array(); 
    }

    public function getAccessoriesFromDist( $id_dist )
    {
        $this->db->select("id_accesorio, id_posicion");
        $this->db->from("rel_distribuciones_accesorios");
        $this->db->where("id_distribucion", $id_dist);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDistributions( $id_island, $id_size )
    {
        $this->db->select("*");
        $this->db->from("tbl_distribuciones");
        $this->db->where("id_isla", $id_island);
        $this->db->where("id_tamaño", $id_size);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOneDistribution( $id_dist )
    {
        $this->db->select("*");
        $this->db->from("tbl_distribuciones");
        $this->db->where("id", $id_dist);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPositions()
    {
        $this->db->select("*");
        $this->db->from("tbl_posiciones");

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTextures()
    {
        $this->db->select("*");
        $this->db->from("tbl_acabados");

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOneTexture( $id_texture )
    {
        $this->db->select("*");
        $this->db->from("tbl_acabados");
        $this->db->where("id", $id_texture);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPlans()
    {
        $this->db->select("*");
        $this->db->from("tbl_planes");

        $query = $this->db->get();
        return $query->result_array();
    } 
    public function getOnePlan( $id_plan )
    {
        $this->db->select("*");
        $this->db->from("tbl_planes");
        $this->db->where("id", $id_plan);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFuels()
    {
        $this->db->select("*");
        $this->db->from("tbl_combustibles");

        $query = $this->db->get();
        return $query->result_array();
    } 
    public function getOneFuel( $id_fuel )
    {
        $this->db->select("*");
        $this->db->from("tbl_combustibles");
        $this->db->where("id", $id_fuel);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getBars()
    {
        $this->db->select("*");
        $this->db->from("tbl_barras");

        $query = $this->db->get();
        return $query->result_array();
    } 
    public function getOneBar( $id_bar )
    {
        $this->db->select("*");
        $this->db->from("tbl_barras");
        $this->db->where("id", $id_bar);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getGrills()
    {
        $this->db->select("p.id_producto, p.nombre, p.altura, p.anchura");
        $this->db->from("tbl_productos as p");
        $this->db->where("id_categoria", 1);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPropsFromAccessory( $id_accessory )
    {
        $this->db->select("p.id_producto, p.nombre, p.altura, p.anchura");
        $this->db->from("tbl_productos as p");
        $this->db->join("tbl_accesorios as a", "a.pulgadas >= p.anchura", "left");
        $this->db->where("a.id", $id_accessory);
        $this->db->where("id_categoria", 9);
        $this->db->where("id_status", 1);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProductsModel( $id_products )
    {
        $this->db->select("p.id_producto, p.modelo3d");
        $this->db->from("tbl_productos as p");
        $this->db->where_in("p.id_producto", $id_products);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function postDesign( $model )
    {
        try {
            // design
            $design = Array(
                "id_cliente" => $model["id_client"],
                "nombre" => $model["name"],
                "id_forma" => $model["id_shape"],
                "id_acabado" => $model["id_finish"],
                "id_barra" => $model["id_bar"],
                "id_plan" => $model["id_plan"],
                "id_combustible" => $model["id_fuel"],
                "comentario" => $model["commentary"]
            );
            // insert design
            $this->db->insert('tbl_diseños', $design);
            $id_design = $this->db->insert_id();

            // selections
            $id_distributions = Array();
            foreach( $model["islands"] as $is ) {
                array_push($id_distributions, Array( 
                    "id_distribucion" => $is["id_dist"],
                    "id_diseño" => $id_design
                ));
            }

            // insert selections
            $this->db->insert_batch('tbl_selecciones', $id_distributions);
            // difference between id's and island's
            $id_selection = intval( $this->db->insert_id() );

            // relation selection and products
            $id_sel_prod = Array();
            foreach( $model["islands"] as $is ) {
                foreach( $is["id_products"] as $p ) {
                    array_push($id_sel_prod, Array( 
                        "id_seleccion" => $id_selection ,
                        "id_producto" => $p,
                        "id_posicion" => null
                    ));
                }
                $id_selection++;
            }

            // insert relations 
            $this->db->insert_batch('rel_selecciones_productos', $id_sel_prod);
            return ["Ok insert"];
        }
        catch (Exception $e) {
            return $e;
        }
    }
}
// post design model
    /* require data model
        model :{
            id_cliente: number,
            name: string,
            id_shape: number,
            id_finish: number,
            id_bar: number,
            id_plan: number,
            id_fuel: number,
            commentary: string,
            islands: [
                {id_dist:number, id_products:[number,...]}
            ]
        }
    */
?>
