<?php
class Register_user extends CI_Model {

    public $title;
    public $content;
    public $date;

    public function get_last_ten_entries()
    {
        $query = $this->db->get('entries', 10);
        return $query->result();
    }

    public function insert_user( $data )
    {
        $this->nombre    = $data["nombre"];
        $this->apellido  = $data["apellido"];
        $this->email     = $data["email"];
        $this->telefono  = $data["telefono"];
        $this->password  = $data["password"];
        $this->id_status = 0;

        $sql = "select id_cliente from tbl_clientes where email=?";
        $query=$this->db->query($sql, $data['email']);
        $row = $query->row();
        if (!isset($row))
        {
            $this->db->insert('tbl_clientes', $data);
            return true;
        }
        else{
            return false;
        }
    }

    public function update_entry()
    {
        $this->title    = $_POST['title'];
        $this->content  = $_POST['content'];
        $this->date     = time();

        $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

}
?>