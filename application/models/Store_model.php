<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Store_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();

    }

    /**
     * Get products filter by category
     * @param string $order Field and order direction for order
     * @param int $id_categoria category id for filter
     * @return array result query
     */
    function CNS_ProductsByCategory( $order = "NOMBRE_ASC", $id_categoria = null ) {
        $this->db->select( "p.id_producto, p.nombre, p.precio, url_image, c.nombre as categoria" );
        $this->db->from( "tbl_productos as p" );
        $this->db->join( "cat_categoria as c", "p.id_categoria = c.id_categoria", 'left' );
        $this->db->where( "p.id_status", 1 );

        if ( $id_categoria ) {
            $this->db->where( "p.id_categoria", $id_categoria );
        }

        switch ( $order ) {
        case "NOMBRE_ASC":
            $this->db->order_by( "p.nombre", "ASC" );
            break;
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Get all active categories
     *
     * @return array result query
     */
    function CNS_Categories() {
        $this->db->select( "id_categoria, nombre" );
        $this->db->from( "cat_categoria" );
        $this->db->where( "id_status", 1 );

        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Get producto by ID
     *
     * @param int $id_producto
     * @return object result result array
     */
    function CNS_Producto( $id_producto ) {
        $this->db->select( "p.id_producto, p.nombre, p.precio, url_image, c.nombre as categoria" );
        $this->db->from( "tbl_productos as p" );
        $this->db->join( "cat_categoria as c", "p.id_categoria = c.id_categoria", 'left' );
        $this->db->where( "p.id_producto", $id_producto );

        $query = $this->db->get();
        return $query->row_array();
    }

    /**
     * Get producto by ID
     *
     * @param int $id_producto
     * @return object result result array
     */
    function CNS_ProductoFull( $id_producto ) {
        $this->db->select( "p.*, url_image, c.nombre as categoria" );
        $this->db->from( "tbl_productos as p" );
        $this->db->join( "cat_categoria as c", "p.id_categoria = c.id_categoria", 'left' );
        $this->db->where( "p.id_producto", $id_producto );

        $query = $this->db->get();
        return $query->row_array();
    }

    /**
     * Get images from product
     *
     * @param string $id_producto
     * @return void
     */
    function CNS_ImagesProduct( $id_producto ) {
        $this->db->select( "imagen" );
        $this->db->from( "rel_imagenes_producto" );
        $this->db->where( "id_producto", $id_producto );

        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Get images from product
     *
     * @param string $id_producto
     * @return void
     */
    function CNS_ColorsProduct( $id_producto ) {
        $this->db->select( "color,imagen" );
        $this->db->from( "det_productos_color" );
        $this->db->where( "id_producto", $id_producto );

        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Get images from product
     *
     * @param string $id_producto
     * @return void
     */
    function CNS_TexturesProduct( $id_producto ) {
        $this->db->select( "textura,imagen" );
        $this->db->from( "det_productos_textura" );
        $this->db->where( "id_producto", $id_producto );

        $query = $this->db->get();
        return $query->result_array();
    }

    function CNS_ProductsByManyFilteres( $id_category = null, $order = "NOMBRE_ASC", $search = null, $page = 0 ) {
        $this->db->select( "p.id_producto, p.nombre, p.precio, url_image, c.nombre as categoria" );
        $this->db->from( "tbl_productos as p" );
        $this->db->join( "cat_categoria as c", "p.id_categoria = c.id_categoria", 'left' );
        $this->db->where( "p.id_status", 1 );

        // category filter
        // -1 is an identifier that you want all the products without counting the category
        if ( $id_category && $page != -1 ) {
            $this->db->where( "p.id_categoria", $id_category );
        }

        // search filter
        if ( $search ) {
            $this->db->like( 'p.nombre', $search );
            $this->db->or_like( 'p.precio', $search );
            $this->db->or_like( 'c.nombre', $search );
        }

        // order filter
        switch ( $order ) {
        case "max_min":
            $this->db->order_by( "p.precio", "DESC" );
            break;
        case "min_max":
            $this->db->order_by( "p.precio", "ASC" );
            break;
        case "a_z":
            $this->db->order_by( "p.nombre", "ASC" );
            break;
        case "z_a":
            $this->db->order_by( "p.nombre", "DESC" );
            break;
        default:
            $this->db->order_by( "p.precio", "DESC" );
            break;
        }

        // pagination filter
        // -1 is an identifier that you want all the products without counting the pagination
        if ( $page != -1 ) {
            $page = $page * 9;
            $this->db->limit( 9 );
            $this->db->offset( $page );
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function CNS_Access( $datos ) {
        $this->db->select( '*' );
        $this->db->from( 'tbl_clientes' );
        $this->db->where( 'email', $datos['email'] );
        $this->db->where( 'password', md5( $datos['pass'] ) );

        $query = $this->db->get();
        if ( $query->conn_id->affected_rows == 1 ) {
            return $query->result_array();
        } else {
            return NULL;
        }
    }

    /**
     * Get all featured products
     *
     * @return array result query
     */
    function CNS_FeatureProducts(){
        $this->db->select("p.id_producto, p.nombre, p.precio, url_image, c.nombre as categoria");
        $this->db->from("tbl_productos as p");
        $this->db->join("cat_categoria as c", "p.id_categoria = c.id_categoria", 'left');
        $this->db->where("p.id_status", 1);
        $this->db->where("p.destacado", 1);
        
        $query = $this->db->get();
        return $query->result_array();
    }

    public function PassRecovery($datos){
        $this->db->select('*');
        $this->db->from('tbl_clientes');
        $this->db->where('email', $datos['email']);

        $query=$this->db->get();
        if($query->conn_id->affected_rows==1){
            return $query->result_array();
        }
        else{
            return NULL;
        }
    }


    public function Insert_token($data){
        $this->db->insert('password_resets',$data);
    }


    public function get_token($token){
        $this->db->select('token');
        $this->db->select('email');
        $this->db->from('password_resets');
        $this->db->where('token',$token);

        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result_array();
        }
        else{
            return NULL;
        }  
    }


    public function PassReset($password,$access){
        $this->db->set('password',$password);
        $this->db->where('email',$access[0]['email']);
        $this->db->update('tbl_clientes');
    }

    public function Deletokens($data){
        $this->db->where('email',$data);
        $this->db->delete('password_resets');  
        return $this->db->affected_rows();
    } 
}
/* End of file ModelName.php */
