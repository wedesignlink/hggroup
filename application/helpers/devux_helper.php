<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function crearCookie($id_user)
{
    $ci = &get_instance();
    $ci->load->model('Controles_model');
    $confirmacion = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

    $cookie = array(
        'name'   => 'recordar',
        'value'  => $confirmacion,
        'expire' => '1209600',  // Two weeks
        'domain' => 'localhost',
        'path'   => '/'
    );

    set_cookie($cookie);

    $datos = array(
        'confirm' => $confirmacion
    );
    $update = $ci->Controles_model->UPD_User('tbl_user', 'id_user', $datos, $id_user);
}

function callWSRestApi($url, $params, $method){
    try {
        $ci = &get_instance();
        $ci->curl->create($url);
        $ci->curl->http_login(USER_API, PASS_API);

        switch($method){
            case 'POST':
                $ci->curl->post($params);
            break;

            case 'GET':
                $ci->curl->get();
            break;

            case 'PUT':
                $ci->curl->put($params);
            break;
        }
        $result = json_decode($ci->curl->execute());

        return $result;
    } catch (Exception $e) {
        throw new Exception("Error al ejecutar rest api " . $e->getMessage());
    }
}

function rand_sha1($length) {
    $max = ceil($length / 40);
    $random = '';
    for ($i = 0; $i < $max; $i ++) {
      $random .= sha1(microtime(true).mt_rand(10000,90000));
    }
    return substr($random, 0, $length);
}

function Recargar_usuario(){

    $ci = &get_instance();
    $usuario = $ci->session->userdata('user');
    $validate = $ci->Login_model->loginNeptuno($usuario['email']);
    
    if($validate){
        $rol = $ci->Login_model->CNS_ROL_ID($validate->id_role);
        $empresa = $ci->Ordenes_model->CNS_Company_ID($validate->id_company);
        $modulos = $ci->Login_model->CNS_Modulos_Company($validate->id_company);
        $config_manager = $ci->Dashboard_model->CNS_config_manager();
        $consultaPuntos = $ci->Login_model->CNS_Puntos_User($validate->id_user);

        $user = array(
            'id_user' => $validate->id_user,
            'nombres' => $validate->nombres,
            'apellidos' => $validate->apellidos,
            'email' => $validate->email,
            'sexo' => $validate->sexo,
            'telefono' => $validate->telefono,
            'calle' => $validate->calle,
            'numero_ext' => $validate->numero_ext,
            'numero_int' => $validate->numero_int,
            'colonia' => $validate->colonia,
            'cp' => $validate->cp,
            // 'id_depto' => $validate->id_depto,
            'rol' => $validate->id_role,
            'rol_nombre' => $rol->rol_nombre,
            'company' => $validate->id_company,
            'photo' => $validate->photo,
            'confirm' =>  $validate->confirm,
            'empresa' => $empresa,
            'modulos' => $modulos,
            'amai' => $validate->amai,
            'aviso_privacidad' => $validate->avisoPrivacidad,
            'codigoPartner' => $validate->codigoPartner,
            'consumoCheck' => $validate->consumoCheck,
            'amai_check' => $validate->amai_check,
            'avisoPrivacidadCheck' => $validate->avisoPrivacidadCheck, 
            'config_manager' => $config_manager,
            'puntos' => $consultaPuntos[0]['puntos'],
            'level' => $validate->levelUser
        );

        $ci->session->set_userdata('user', $user);

        return true;
    } else {
        return false;
    }
    
    
}

?>