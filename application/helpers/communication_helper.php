<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function sendEmailUser($id, $asunto, $view){
    $ci = &get_instance();
    $ci->load->model('Controles_model');
    $user = $ci->Controles_model->CNS_user_ID($id);
    // Configuración del mail
    $correo = $user->email;
    $dato['id']= $id;

    $mensaje = $ci->load->view($view,$dato,true);
    $enviado = sendEmail($asunto, $mensaje,$correo);

    return $enviado;
}

function sendEmailPedido($id, $asunto, $view){
    $ci = &get_instance();
    $ci->load->model('Controles_model');
    $order = $ci->Controles_model->CNS_pedido($id);
    // Configuración del mail
    $correo = $order->email;
    $dato['order']= $id;

    $mensaje = $ci->load->view($view,$dato,true);
    $enviado = sendEmail($asunto, $mensaje,$correo);

    return $enviado;
}

?>