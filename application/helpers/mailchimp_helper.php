<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


    function createCampaing($list_id, $subject){
        try {
            $ci = &get_instance();
            $ci->load->library('mailchimp');
            $reply_to   = 'diego.huerta.b22@gmail.com';
            $from_name  = 'Entrevistando México';
            $campaign_id = '';
            //$subject = "Unete a Entrevistando México";
        
            $body = array(
                'recipients'    => array('list_id' => $list_id),
                'type'          => 'regular',
                'settings'      => array('subject_line' => $subject,
                                        'reply_to'      => $reply_to,
                                        'from_name'     => $from_name
                                        )
            );
        
            $create_campaign = $ci->mailchimp->call('POST', 'campaigns', $body );

            if ( $create_campaign ) {
                if ( ! empty( $create_campaign['id'] ) && isset( $create_campaign['status'] ) && 'save' == $create_campaign['status'] ) {
                    // The campaign id: 
                    $campaign_id = $create_campaign['id'];
                }
            }
        
            return $campaign_id ? $campaign_id : false;
        } catch (Exception $e) {
            throw new Exception('Error when create campaing');
        }
    }

    function updateCampaingWithTempleate($campaign_id, $template_content){
        try {
            $ci = &get_instance();
            $ci->load->library('mailchimp');
            $set_content = "";
            $set_campaign_content = $ci->mailchimp->call("PUT","campaigns/$campaign_id/content", $template_content );
        
            if ( $set_campaign_content ) {
                if ( ! empty( $set_campaign_content['html'] ) ) {
                    $set_content = true;
                }
            }
            return $set_content ? true : false;
        } catch (Exception $e) {
            throw new Exception('Error al actualizar la campaña ' . $e->getMessage());
        }
    }

    function sendCampaing($campaign_id){
        try {
            $ci = &get_instance();
            $ci->load->library('mailchimp');
            $send_campaign = $ci->mailchimp->call('POST', "campaigns/$campaign_id/actions/send");

            if ( empty( $send_campaign ) ) {
                $response = (object)[
                    "isSuccess" => true
                ];
            } elseif( isset( $send_campaign['detail'] ) ) {
                $error_detail = $send_campaign['detail'];
                $response = (object)[
                    "isSuccess" => false,
                    "error" => $error_detail
                ];
            }
        
            return $response;
        } catch (Exception $e) {
            throw new Exception('Error when send campain' . $e->getMessage());
        }
    }

    function getListMembers(){
        try {
            $ci = &get_instance();
            $ci->load->library('mailchimp');
            $listId = "caa0d14b4c";

            $members 	    = $ci->mailchimp->call('GET', "lists/$listId/members?offset=0&count=1000");
            $hashMembers    = [];
            foreach($members["members"] as $key){
                if($key['status'] == "subscribed"){
                    array_push($hashMembers, [
                        "email_address" => $key["email_address"],
                        "status" => "unsubscribed"
                    ]);
                }
            }

            return $hashMembers;
        } catch (Exception $e) {
            throw new Exception('Error al consultar lista de miembros ' . $e->getMessage());
        }
        //$this->subscribeUnsubscribe($listId, $hashMembers);
    }

    function subscribeUnsubscribe($list_id, $members){
        try {
            $ci = &get_instance();
            $ci->load->library('mailchimp');

            $subscription 	    = $ci->mailchimp->call('POST', "lists/$list_id", ["members" => $members, "update_existing" => true]);

            return true;
        } catch (Exception $e) {
            throw new Exception('subscripe unsubscribe error ' . $e->getMessage());
        }
    }

    function getExistsMembers($membersFile, $membersMailChimp){
        try{
            $foundMembers = [];
            foreach($membersFile as $key){
                $emailFile = $key['email_address'];
                foreach($membersMailChimp as $row){
                    if($emailFile == $row['email_address']){
                        array_push($foundMembers, $row);
                        break;
                    }
                }
            }

            return $foundMembers;
        }catch(Exception $e){
            throw new Exception('Error al buscar members existentes');
        }
    }

    function getNonFoundMembers($membersFile, $membersFound){
        try {
            $nonFoundMembers = [];
            foreach ($membersFile as $key) {
                $emailFile = $key['email_address'];
                $position = array_search($emailFile, array_column($membersFound, 'email_address'));
                if(!$position){
                    array_push($nonFoundMembers, $key);
                }
            }

            return $nonFoundMembers;
        } catch (Exception $e) {
            throw new Exception('Error non found');
        }
    }

    function createCampaingMarketing($request){
        try {
            $ci = &get_instance();
            $db = $ci->load->database();
            $ci->load->library('mailchimp');
            $listId = "caa0d14b4c";
            $members = $request['members'];
            $project = $request['proyecto'];
            $subject = $request["subject"];

            if(is_null($listId)){
                throw new Exception("Error Processing Request Create List", 1);
            }

            $currentMembers = getListMembers();

            if(count($currentMembers) > 0){
                $unsubscribedList = subscribeUnsubscribe($listId, $currentMembers);

                $result_array = getExistsMembers($members, $currentMembers);
            }else{
                $result_array = $members;
            }
            //change for limit of account
            
            if(count($result_array) > 0){
                for($i = 0; $i < count($result_array); $i++){
                    $result_array[$i]['status'] = "subscribed";
                }

                $subscribeMembers = subscribeUnsubscribe($listId, $result_array);
                if(count($currentMembers) > 0){
                    $members = getNonFoundMembers($members, $result_array);
                    unset($members[0]);
                }
                
            }

            $membersList = [];
            foreach ($members as $key) {
                array_push($membersList, [
                    "email_address" => $key["email_address"],
                    "merge_fields" => [
                        "FNAME" => $key["FNAME"],
                        "LNAME" => $key["LNAME"]
                    ],
                    "status" => "subscribed"
                ]);
            }
            
            $batchListMembers 	= $ci->mailchimp->call('POST', 'lists/'.$listId, [ 'list_id' => $listId, "members" => $membersList]);
            
            $campaign_id = createCampaing($listId, $subject);

            if(!$campaign_id){
                throw new Exception('Error al crear la campaña');
            }

            if(is_null($project)){
                $template_content = array(
                    'template' => array(
                            'id' => 405586,
                        )
                    );
            }else{
                $projectInfo['project'] = $ci->db->query("select * from vw_proyecto_empresa where id_proyecto = " . $project)->row();
                
                $viewProject = $ci->load->view('templatesMC/invitacion_encuesta', $projectInfo, true);

                $template_content = array(
                    'template' => array(
                            'id' => 410070,
                            'sections'  => array(
                                'body_content' => $viewProject//'<p>This is a test content for send campaign</p>'
                            )   
                        )
                    );
                
            }

            $createTempleate = updateCampaingWithTempleate($campaign_id, $template_content);

            if(!$createTempleate){
                throw new Exception('Error al crear/cargar plantilla');
            }

            $sendCampaing = sendCampaing($campaign_id);
            
            return json_encode($sendCampaing);
        } catch (Exception $e) {
            throw new Exception('Error createCampaingMarketing ' . $e->getMessage());
        }
    }
?>