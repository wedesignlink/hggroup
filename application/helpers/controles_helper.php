<?php if ( !defined( 'BASEPATH' ) ) {
    exit( 'No direct script access allowed' );
}

// ------------------FUNCIONES DE CONTROLES -------------------

// function crear_asignador( $titulo = '', $listado = '', $tabla = '', $campo_nombre_list = '', $campo_id_list, $condicion = '', $tabla_edit = '', $campo_id_tabla ) {
//     $ci = &get_instance();
//     $ci->load->database();

//     $sql = "select * from " . $tabla;
//     if ( $condicion != "" ) {
//         $sql .= " where " . $condicion;
//     }
//     $query = $ci->db->query( $sql );
//     $row = $query->result();

//     $campos = $ci->db->list_fields( $tabla );

//     $data['tabla_edit'] = $tabla_edit;
//     $data['listado'] = $listado;
//     $data['data_table'] = $row;
//     $data['row_names'] = $campos;
//     $data['campo_nombre_list'] = $campo_nombre_list;
//     $data['campo_id_list'] = $campo_id_list;
//     $data['campo_id_tabla'] = $campo_id_tabla;
//     $data['titulo'] = $titulo;
//     $asignador_usuarios = $ci->load->view( 'controles/v_lista_asignador', $data );
//     return $asignador_usuarios;
// }

function crear_tabla( $titulo = '', $tabla = '', $condicion = '', $mostrar = "", $botones = "", $head = '', $formato_campos = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
    }
    $query = $ci->db->query( $sql );
    $row = $query->result();
    $campos = $ci->db->list_fields( $tabla );

    // $data['tabla_edit'] = $tabla_edit;
    $data['data_table'] = $row;
    $data['row_names'] = $campos;
    $data['titulo'] = $titulo;
    $data['mostrar'] = $mostrar;
    $data['botones'] = $botones;
    $data['head'] = $head;
    $data['formato_campos'] = $formato_campos;
    $tabla = $ci->load->view( 'controles/v_tabla', $data );
    return $tabla;
}

function crear_tree( $titulo = '', $tabla = '', $condicion = '', $campo_id = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
        $sql .= " order by orden";
    }
    $query = $ci->db->query( $sql );
    $row = $query->result();
    $campos = $ci->db->list_fields( $tabla );

    // $data['tabla_edit'] = $tabla_edit;
    $data['data_table'] = $row;
    $data['row_names'] = $campos;
    $data['titulo'] = $titulo;
    $data['campo_id'] = $campo_id;
    $data['tabla'] = $tabla;
    $data['condicion'] = $condicion;

    $tabla = $ci->load->view( 'controles/v_tree', $data );
    return $tabla;
}
function crear_tree_preguntas( $titulo = '', $tabla = '', $condicion = '', $campo_id = '', $id_encuesta = '', $id_tipo = "" ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
        $sql .= " order by orden ASC, id_pregunta ASC";
    }
    $query = $ci->db->query( $sql );
    $row = $query->result();
    $campos = $ci->db->list_fields( $tabla );

    // $data['tabla_edit'] = $tabla_edit;
    $data['data_table'] = $row;
    $data['row_names'] = $campos;
    $data['titulo'] = $titulo;
    $data['campo_id'] = $campo_id;
    $data['tabla'] = $tabla;
    $data['condicion'] = $condicion;
    $data['id_encuesta'] = $id_encuesta;

    if ( $id_tipo == 1 || $id_tipo == 3 ) {
        $tabla = $ci->load->view( 'controles/v_tree_preguntas', $data );
    } else {
        $tabla = $ci->load->view( 'controles/v_tree_elearning', $data );
    }

    return $tabla;
}

function crear_array_tree( $data_table = '', $campo_id = '' ) {

    $cuenta = count( $data_table );
    // Armamos la estructura del arbol
    for ( $i = 0; $i < $cuenta; $i++ ) {
        if ( $data_table[$i]->nivel == 0 ) {
            $tree[$i]['id'] = $data_table[$i]->$campo_id;
            $tree[$i]['nombre'] = $data_table[$i]->nombre;
            $tree[$i]['nivel'] = $data_table[$i]->nivel;
            $tree[$i]['id_padre'] = $data_table[$i]->id_padre;

            for ( $j = 0; $j < $cuenta; $j++ ) {
                if ( $data_table[$j]->nivel == 1 && $data_table[$j]->id_padre == $data_table[$i]->$campo_id ) {
                    $tree[$i]['children'][$j]['id'] = $data_table[$j]->$campo_id;
                    $tree[$i]['children'][$j]['nombre'] = $data_table[$j]->nombre;
                    $tree[$i]['children'][$j]['nivel'] = $data_table[$j]->nivel;
                    $tree[$i]['children'][$j]['id_padre'] = $data_table[$j]->id_padre;

                    for ( $k = 0; $k < $cuenta; $k++ ) {
                        if ( $data_table[$k]->nivel == 2 && $data_table[$k]->id_padre == $data_table[$j]->$campo_id ) {
                            $tree[$i]['children'][$j]['children'][$k]['id'] = $data_table[$k]->$campo_id;
                            $tree[$i]['children'][$j]['children'][$k]['nombre'] = $data_table[$k]->nombre;
                            $tree[$i]['children'][$j]['children'][$k]['nivel'] = $data_table[$k]->nivel;
                            $tree[$i]['children'][$j]['children'][$k]['id_padre'] = $data_table[$k]->id_padre;

                            for ( $l = 0; $l < $cuenta; $l++ ) {
                                if ( $data_table[$l]->nivel == 3 && $data_table[$l]->id_padre == $data_table[$k]->$campo_id ) {
                                    $tree[$i]['children'][$j]['children'][$k]['children'][$l]['id'] = $data_table[$l]->$campo_id;
                                    $tree[$i]['children'][$j]['children'][$k]['children'][$l]['nombre'] = $data_table[$l]->nombre;
                                    $tree[$i]['children'][$j]['children'][$k]['children'][$l]['nivel'] = $data_table[$l]->nivel;
                                    $tree[$i]['children'][$j]['children'][$k]['children'][$l]['id_padre'] = $data_table[$l]->id_padre;

                                    for ( $m = 0; $m < $cuenta; $m++ ) {
                                        if ( $data_table[$m]->nivel == 4 && $data_table[$m]->id_padre == $data_table[$l]->$campo_id ) {
                                            $tree[$i]['children'][$j]['children'][$k]['children'][$l]['children'][$m]['id'] = $data_table[$m]->$campo_id;
                                            $tree[$i]['children'][$j]['children'][$k]['children'][$l]['children'][$m]['nombre'] = $data_table[$m]->nombre;
                                            $tree[$i]['children'][$j]['children'][$k]['children'][$l]['children'][$m]['nivel'] = $data_table[$m]->nivel;
                                            $tree[$i]['children'][$j]['children'][$k]['children'][$l]['children'][$m]['id_padre'] = $data_table[$m]->id_padre;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    $cuenta_t = count( $tree );
    // ksort($tree);
    $tree = array_values( $tree );

    return $tree;
}

// function crear_tabla_crud( $titulo = '', $tabla = '', $campo_id_tabla, $condicion = '', $tabla_edit = '', $campos_ver = "", $defaults = '', $tipos = '', $acciones = '', $buttons = '', $paginar = '', $campos_ver_tabla = "", $funcion = "", $order = "" ) {
//     $ci = &get_instance();
//     $ci->load->database();

//     $sql = "select * from " . $tabla;
//     if ( $condicion != "" ) {
//         $sql .= " where " . $condicion;
//     }
//     if ( $order != "" ) {
//         $sql .= " order by " . $order;
//     }
//     $query = $ci->db->query( $sql );
//     $row = $query->result();
//     $campos = $ci->db->list_fields( $tabla );

//     $camposEdit = $ci->db->list_fields( $tabla_edit );

//     $data['tabla_edit'] = $tabla_edit;
//     $data['tabla'] = $tabla_edit;
//     $data['data_table'] = $row;
//     $data['row_names'] = $campos;
//     $data['row_names_edit'] = $camposEdit;
//     $data['campo_id_tabla'] = $campo_id_tabla;
//     $data['titulo'] = $titulo;
//     $data['campos_ver'] = $campos_ver;
//     $data['campos_ver_tabla'] = $campos_ver_tabla;
//     $data['defaults'] = $defaults;
//     $data['tipos'] = $tipos;
//     $data['condicion'] = $condicion;
//     $data['acciones'] = $acciones;
//     $data['buttons'] = $buttons;
//     $data['paginar'] = $paginar;
//     $data['funcion'] = $funcion;
//     $tabla = $ci->load->view( 'controles/v_tabla_crud', $data );
//     $tabla = $ci->load->view( 'controles/v_modal_editar', $data );
//     $tabla = $ci->load->view( 'controles/v_modal_agregar', $data );
//     return $tabla;
// }

// function crear_tabla_productos( $titulo = '', $tabla = '', $campo_id_tabla, $condicion = '', $tabla_edit = '', $campos_ver = "", $defaults = '', $tipos = '', $acciones = '', $buttons = '', $paginar = '', $campos_ver_tabla = "", $funcion = "", $order = "" ) {
//     $ci = &get_instance();
//     $ci->load->database();

//     $sql = "select * from " . $tabla;
//     if ( $condicion != "" ) {
//         $sql .= " where " . $condicion;
//     }
//     if ( $order != "" ) {
//         $sql .= " order by " . $order;
//     }
//     $query = $ci->db->query( $sql );
//     $row = $query->result();
//     $campos = $ci->db->list_fields( $tabla );

//     $camposEdit = $ci->db->list_fields( $tabla_edit );

//     $data['tabla_edit'] = $tabla_edit;
//     $data['tabla'] = $tabla_edit;
//     $data['data_table'] = $row;
//     $data['row_names'] = $campos;
//     $data['row_names_edit'] = $camposEdit;
//     $data['campo_id_tabla'] = $campo_id_tabla;
//     $data['titulo'] = $titulo;
//     $data['campos_ver'] = $campos_ver;
//     $data['campos_ver_tabla'] = $campos_ver_tabla;
//     $data['defaults'] = $defaults;
//     $data['tipos'] = $tipos;
//     $data['condicion'] = $condicion;
//     $data['acciones'] = $acciones;
//     $data['buttons'] = $buttons;
//     $data['paginar'] = $paginar;
//     $data['funcion'] = $funcion;
//     $tabla = $ci->load->view( 'controles/v_tabla_productos', $data );
//     $tabla = $ci->load->view( 'controles/v_modal_editar', $data );
//     $tabla = $ci->load->view( 'controles/v_modal_agregar', $data );
//     return $tabla;
// }

function crear_modal_agregar( $titulo = '', $tabla = '', $campos_ver = "", $defaults = '', $tipos = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;

    $camposEdit = $ci->db->list_fields( $tabla );

    $data['titulo'] = $titulo;
    $data['tabla'] = $tabla;
    $data['tabla_edit'] = $tabla;
    $data['row_names_edit'] = $camposEdit;
    $data['campos_ver'] = $campos_ver;
    $data['defaults'] = $defaults;
    $data['tipos'] = $tipos;
    $modal = $ci->load->view( 'controles/v_modal_agregar', $data );
    return $modal;
}

function crear_cards( $tabla = '', $tabla_edit = '', $campo_id = '', $condicion = '', $campos_ver = '', $defaults = '', $tipos = '', $condicion2 = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
    }

    $sql2 = "select * from rel_encuesta_company";
    if ( $condicion2 != "" ) {
        $sql2 .= " where " . $condicion2;
    }

    $query = $ci->db->query( $sql );
    $query2 = $ci->db->query( $sql2 );
    $rowStatus = $query2->result();
    $row = $query->result();
    $campos = $ci->db->list_fields( $tabla );

    $data['tabla'] = $tabla_edit;
    $data['tabla_edit'] = $tabla_edit;
    $data['rel_status'] = $rowStatus;
    $data['data_table'] = $row;
    $data['row_names'] = $campos;
    $data['row_names_edit'] = $campos;
    $data['campo_id'] = $campo_id;
    $data['campo_id_tabla'] = $campo_id;
    $data['campos_ver'] = $campos_ver;
    $data['defaults'] = $defaults;
    $data['tipos'] = $tipos;

    $cards = $ci->load->view( 'controles/v_cards', $data );
    $cards = $ci->load->view( 'controles/v_modal_editar', $data );
    return $cards;
}

function crear_cards_control( $tabla = '', $tabla_edit = '', $campo_id = '', $condicion = '', $campos_ver = '', $defaults = '', $tipos = '', $condicion2 = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
    }
    $sql2 = "select * from rel_encuesta_company";
    if ( $condicion2 != "" ) {
        $sql2 .= " where " . $condicion2;
    }

    $query = $ci->db->query( $sql );
    $query2 = $ci->db->query( $sql2 );
    $row = $query->result();
    $rowStatus = $query2->result();
    $campos = $ci->db->list_fields( $tabla );

    $data['tabla'] = $tabla_edit;
    $data['tabla_edit'] = $tabla_edit;
    $data['data_table'] = $row;
    $data['rel_status'] = $rowStatus;
    $data['row_names'] = $campos;
    $data['row_names_edit'] = $campos;
    $data['campo_id'] = $campo_id;
    $data['campo_id_tabla'] = $campo_id;
    $data['campos_ver'] = $campos_ver;
    $data['defaults'] = $defaults;
    $data['tipos'] = $tipos;

    $cards = $ci->load->view( 'controles/v_cards_control', $data );
    $cards = $ci->load->view( 'controles/v_modal_editar', $data );
    return $cards;
}

function crear_cards_control_manager( $tabla = '', $tabla_edit = '', $campo_id = '', $condicion = '', $campos_ver = '', $defaults = '', $tipos = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
    }

    $query = $ci->db->query( $sql );
    $row = $query->result();
    $campos = $ci->db->list_fields( $tabla );

    $data['tabla'] = $tabla_edit;
    $data['tabla_edit'] = $tabla_edit;
    $data['data_table'] = $row;
    $data['row_names'] = $campos;
    $data['row_names_edit'] = $campos;
    $data['campo_id'] = $campo_id;
    $data['campo_id_tabla'] = $campo_id;
    $data['campos_ver'] = $campos_ver;
    $data['defaults'] = $defaults;
    $data['tipos'] = $tipos;

    $cards = $ci->load->view( 'controles/v_cards_control_manager', $data );
    $cards = $ci->load->view( 'controles/v_modal_editar', $data );
    return $cards;
}

function crear_modal_agregar_tree( $titulo = '', $tabla = '', $campos_ver = "", $defaults = '', $tipos = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;

    $camposEdit = $ci->db->list_fields( $tabla );

    $data['titulo'] = $titulo;
    $data['tabla'] = $tabla;
    $data['row_names_edit'] = $camposEdit;
    $data['campos_ver'] = $campos_ver;
    $data['defaults'] = $defaults;
    $data['tipos'] = $tipos;
    $modal = $ci->load->view( 'controles/v_modal_agregar_tree', $data );
    return $modal;
}

function crear_modal_agregar_encuesta( $titulo = '', $tabla = '', $campos_ver = "", $defaults = '', $tipos = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;

    $camposEdit = $ci->db->list_fields( $tabla );

    $data['titulo'] = $titulo;
    $data['tabla'] = $tabla;
    $data['row_names_edit'] = $camposEdit;
    $data['campos_ver'] = $campos_ver;
    $data['defaults'] = $defaults;
    $data['tipos'] = $tipos;
    $modal = $ci->load->view( 'controles/v_modal_agregar_encuesta', $data );
    return $modal;
}

function crear_modal_upload( $tabla = '', $campo_id = '', $valor_id = '', $campo_upload = '', $tipo = '' ) {
    $ci = &get_instance();
    // $data['titulo'] = $titulo;
    $data['tabla'] = $tabla;
    $data['tipo'] = $tipo;
    $data['campo_id'] = $campo_id;
    $data['valor_id'] = $valor_id;
    $data['campo_upload'] = $campo_upload;
    $modal = $ci->load->view( 'controles/v_modal_upload', $data );
    return $modal;
}
function crear_modal_upload_producto( $tabla = '', $campo_id = '', $valor_id = '', $campo_upload = '', $tipo = '' ) {
    $ci = &get_instance();
    // $data['titulo'] = $titulo;
    $data['tabla'] = $tabla;
    $data['tipo'] = $tipo;
    $data['campo_id'] = $campo_id;
    $data['valor_id'] = $valor_id;
    $data['campo_upload'] = $campo_upload;
    $modal = $ci->load->view( 'controles/v_modal_upload_producto', $data );
    return $modal;
}

function crear_form_editar( $titulo = '', $tabla = '', $campo_id_tabla = '', $condicion = '', $campos_ver = "", $defaults = '', $tipos = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
    }
    $query = $ci->db->query( $sql );
    $row = $query->result();
    $campos = $ci->db->list_fields( $tabla );

    $camposEdit = $ci->db->list_fields( $tabla );

    $data['titulo'] = $titulo;
    $data['tabla'] = $tabla;
    $data['data_table'] = $row;
    $data['row_names'] = $campos;
    $data['row_names_edit'] = $camposEdit;
    $data['campo_id_tabla'] = $campo_id_tabla;
    $data['campos_ver'] = $campos_ver;
    $data['defaults'] = $defaults;
    $data['tipos'] = $tipos;
    $form = $ci->load->view( 'controles/v_form_editar', $data );
    return $form;
}
function crear_form_agregar( $titulo = '', $tabla = '', $campo_id_tabla = '', $campos_ver = "", $defaults = '', $tipos = '' ) {
    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;

    $query = $ci->db->query( $sql );
    $row = $query->result();
    $campos = $ci->db->list_fields( $tabla );

    $camposEdit = $ci->db->list_fields( $tabla );

    $data['titulo'] = $titulo;
    $data['tabla'] = $tabla;
    $data['data_table'] = $row;
    $data['row_names'] = $campos;
    $data['row_names_edit'] = $camposEdit;
    $data['campo_id_tabla'] = $campo_id_tabla;
    $data['campos_ver'] = $campos_ver;
    $data['defaults'] = $defaults;
    $data['tipos'] = $tipos;
    $form = $ci->load->view( 'controles/v_form_agregar', $data );
    return $form;
}

function crear_select( $titulo = '', $tabla = '', $condicion = '', $campo_id = '', $campo_nombre = '', $default = '', $selected = '', $concat = '', $titulo_campo = '', $requerido = '', $default_text = '', $size = '' ) {

    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
    }
    $query = $ci->db->query( $sql );
    $row = $query->result();

    $data['titulo'] = $titulo;
    $data['campo_id'] = $campo_id;
    $data['campo_nombre'] = $campo_nombre;
    $data['default'] = $default;
    $data['select'] = $row;
    $data['selected'] = $selected;
    $data['size'] = $size;
    $data['concat'] = $concat;
    $data['titulo_campo'] = $titulo_campo;
    $data['requerido'] = $requerido;
    $data['default_text'] = $default_text;
    $select = $ci->load->view( 'controles/v_select', $data );
    return $select;
}

function crear_select_trans( $titulo = '', $tabla = '', $condicion = '', $campo_id = '', $campo_nombre = '', $default = '', $selected = '', $concat = '', $titulo_campo = '', $requerido = '', $default_text = '', $size = '' ) {

    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
    }
    $query = $ci->db->query( $sql );
    $row = $query->result();

    $data['titulo'] = $titulo;
    $data['campo_id'] = $campo_id;
    $data['campo_nombre'] = $campo_nombre;
    $data['default'] = $default;
    $data['select'] = $row;
    $data['selected'] = $selected;
    $data['size'] = $size;
    $data['concat'] = $concat;
    $data['titulo_campo'] = $titulo_campo;
    $data['requerido'] = $requerido;
    $data['default_text'] = $default_text;
    $select = $ci->load->view( 'controles/v_select_trans', $data );
    return $select;
}

function crear_select_vacio( $titulo = '', $tabla = '', $condicion = '', $campo_id = '', $campo_nombre = '', $default = '', $selected = '' ) {

    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
    }
    $query = $ci->db->query( $sql );
    $row = $query->result();

    $data['titulo'] = $titulo;
    $data['campo_id'] = $campo_id;
    $data['campo_nombre'] = $campo_nombre;
    $data['default'] = $default;
    $data['select'] = $row;
    $data['selected'] = $selected;
    $select = $ci->load->view( 'controles/v_select_vacio', $data );
    return $select;
}

// ------------------URL LOG -------------------

function urlLog() {
    $ci = &get_instance();
    $ci->load->helper( 'url' );
    $currentURL = current_url();
    $sistema = base_url();
    $ip = $_SERVER['SERVER_ADDR'];
    $mensaje = 'URL:' . $currentURL . '<br>';
    $mensaje .= 'BASE_URL:' . $sistema . '<br>';
    $mensaje .= 'IP:' . $ip . '<br>';
    sendEmail( 'ACCESS INFO', $mensaje, 'wedesignlink@gmail.com' );
}

// ------------------FUNCIONES DE ENVIO -------------------

function sendEmail( $asunto = '', $mensaje = '', $correo = '' ) {
    $ci = &get_instance();
    $ci->load->library( 'email' );

    // $config['smtp_crypto']='ssl';
    // $config['protocol']    = 'smtp';
    // $config['smtp_host']    = 'mail.devux.com.mx';
    // $config['smtp_port']    = '465';
    // $config['smtp_timeout'] = '7';
    // $config['smtp_user']    = 'contacto@devux.com.mx';
    // $config['smtp_pass']    = 'D3vux2018*';
    // $config['charset']    = 'utf-8';
    // $config['newline']    = "\r\n";
    // $config['mailtype'] = 'html'; // or html
    // $config['validation'] = TRUE; // bool whether to validate email or not

    $config = [
        'protocol'  => 'smtp',
        //'smtp_host' => 'smtp.googlemail.com',
        'smtp_host' => 'mail.hgroupcustombbqspatio.com',
        'smtp_port' => 465,
        'smtp_user' => 'webmaster@hgroupcustombbqspatio.com',
        'smtp_pass' => 'z+HZS)S]C3Fj',
        'mailtype'  => 'html',
        'charset'   => 'utf-8',
        'wordwrap'  => TRUE,
    ];
    $config['smtp_crypto'] = 'ssl';
    $config['newline'] = "\r\n";

    $ci->email->initialize( $config );

    // $correo = "wedesignlink@gmail.com";

    //Correo del cliente
    $ci->email->from( 'webmaster@hgroupcustombbqspatio.com', 'H Group BBQ and Patio' );
    $ci->email->to( $correo );

    $ci->email->subject( $asunto );
    $ci->email->message( $mensaje );

    $enviado = false;

    if ( $ci->email->send( FALSE ) ) {
        $enviado = true;
    }

    echo $ci->email->print_debugger();
    return $enviado;
}

function mandaNotificacion( $id_user = '', $dataarray = '' ) {
    #   Variables de la notificación
    $titulo = $dataarray["titulo"];
    $mensaje = $dataarray["mensaje"];
    $nameRequest = $dataarray["who"];

    #   Varibles de solicitud para la curl
    $path_to_fcm = "https://fcm.googleapis.com/fcm/send";
    $server_key = "AAAAE63Wd1c:APA91bEVyZGlLRWGMDu2yOBlW3WftQAGicTk8T97UEBe8Ovs3EflaM09Q-rdSpvcuqgI3W126Jo9mt4zZCPeZrNf5ra0VIT88ndzqY2m26E9JWWk8KcHj5OxAw13HX3IRzTuZlSX1aWD";

    $ci = &get_instance();
    $ci->load->database();

    #   Se obtiene el token del usuario al que se le enviará la notificación
    $sql = "SELECT * FROM vw_det_colaborador WHERE id_user = " . $id_user . "";
    $query = $ci->db->query( $sql );
    $row = $query->result();
    $usuario = $row;
    $TokenUsuario = $usuario[0]->token;

    if ( !$TokenUsuario ) {
        return false;
        exit();
    }

    #   Encabezados del request
    $headers = [
        "Authorization:key=" . $server_key,
        "Content-Type:application/json",
    ];

    #   Cuerpo del request
    $fields = [
        "to"           => $TokenUsuario,
        "notification" => [
            "title" => $titulo,
            "body"  => $mensaje . $nameRequest,
            "sound" => "default",
        ],
        "data"         => [
            "message"      => "message body",
            "click_action" => "PUSH_INTENT",
        ],
        "priority"     => "high",
    ];

    $payload = json_encode( $fields );

    #   Se hace la solicitud CURL
    $ch = curl_init( $path_to_fcm );
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
    $result = curl_exec( $ch );
    curl_close( $ch );

    #   Se codifica el JSON para obtener ol número de exitos
    $jsond = json_decode( $result );

    if ( $jsond->success > 0 ) {
        return true;
    }
}

// ------------------FUNCIONES LOG -------------------

function activity_log( $actividad = '', $interno = '', $id = '' ) {
    $ci = &get_instance();
    $ci->load->library( 'session' );
    $ci->load->helper( 'url' );
    $ci->load->model( 'Controles_model' );

    $currentURL = current_url();
    $usuario = $ci->session->userdata( 'user' );

    $dataInsert = [
        'actividad'     => $actividad,
        'id_user'       => $usuario['id_user'],
        'id_company'    => $usuario['empresa']->id_company,
        'interno'       => $interno,
        'id_referencia' => $id,
        'url'           => $currentURL,
    ];

    $log = $ci->Controles_model->INS_activity_log( $dataInsert );
    // urlLog();
    // return $log;
}

// ------------------FUNCIONES VERIFY -------------------
function check_value( $tabla, $campo = '', $valor = '' ) {
    $ci = &get_instance();
    $ci->load->library( 'session' );
    $ci->load->model( 'Controles_model' );
    // $this->output->enable_profiler(TRUE);
    $valida = $ci->Controles_model->SRC_check( $tabla, $campo, $valor );

    if ( $valida > 0 ) {
        return false;
    } else {
        return true;
    }
}

function hashPass( $password = '' ) {
    $SALT = "d3vuxxx3v3r!!";
    // define ("SALT","d3vuxxx3v3r!!");
    return hash( 'sha512', $SALT . $password );
}

function verify( $password = '', $hash = '' ) {
    return ( $hash == hashPass( $password ) );
}

// ------------------LoginEmail ------------------------------

function LoginEmail( $email = "" ) {
    $ci = &get_instance();
    $ci->load->helper( 'url' );
    $ci->load->library( 'session' );
    $ci->load->model( 'Login_model' );
    $ci->load->model( 'Ordenes_model' );
    $ci->load->model( 'Dashboard_model' );

    $validate = $ci->Login_model->loginNeptuno( $email );
    $rol = $ci->Login_model->CNS_ROL_ID( $validate->id_role );
    $empresa = $ci->Ordenes_model->CNS_Company_ID( $validate->id_company );
    $modulos = $ci->Login_model->CNS_Modulos_Company( $validate->id_company );
    $config_manager = $ci->Dashboard_model->CNS_config_manager();

    $user = [
        'id_user'        => $validate->id_user,
        'nombres'        => $validate->nombres,
        'apellidos'      => $validate->apellidos,
        'email'          => $validate->email,
        'id_depto'       => $validate->id_depto,
        'rol'            => $validate->id_role,
        'rol_nombre'     => $rol->rol_nombre,
        'company'        => $validate->id_company,
        'photo'          => $validate->photo,
        'confirm'        => $validate->confirm,
        'empresa'        => $empresa,
        'modulos'        => $modulos,
        'quickstart'     => $validate->quickstart,
        'config_manager' => $config_manager,
    ];
    $ci->session->set_flashdata( "quickstart", $validate->quickstart );
    $ci->session->set_userdata( 'user', $user );
    redirect( 'Dashboard/Control' );
}
function LoginEmailPanelista( $email = "" ) {
    $ci = &get_instance();
    $ci->load->helper( 'url' );
    $ci->load->library( 'session' );
    $ci->load->model( 'Login_model' );
    $ci->load->model( 'Ordenes_model' );
    $ci->load->model( 'Dashboard_model' );

    $validate = $ci->Login_model->loginNeptuno( $email );
    $rol = $ci->Login_model->CNS_ROL_ID( $validate->id_role );
    $empresa = $ci->Ordenes_model->CNS_Company_ID( $validate->id_company );
    $modulos = $ci->Login_model->CNS_Modulos_Company( $validate->id_company );
    $config_manager = $ci->Dashboard_model->CNS_config_manager();
    $consultaPuntos = $ci->Login_model->CNS_Puntos_User( $validate->id_user );

    $user = [
        'id_user'              => $validate->id_user,
        'nombres'              => $validate->nombres,
        'apellidos'            => $validate->apellidos,
        'email'                => $validate->email,
        'sexo'                 => $validate->sexo,
        'telefono'             => $validate->telefono,
        'calle'                => $validate->calle,
        'numero_ext'           => $validate->numero_ext,
        'numero_int'           => $validate->numero_int,
        'colonia'              => $validate->colonia,
        'cp'                   => $validate->cp,
        // 'id_depto' => $validate->id_depto,
        'rol'                  => $validate->id_role,
        'rol_nombre'           => $rol->rol_nombre,
        'company'              => $validate->id_company,
        'photo'                => $validate->photo,
        'confirm'              => $validate->confirm,
        'empresa'              => $empresa,
        'modulos'              => $modulos,
        'amai'                 => $validate->amai,
        'aviso_privacidad'     => $validate->avisoPrivacidad,
        'codigoPartner'        => $validate->codigoPartner,
        'consumoCheck'         => $validate->consumoCheck,
        'amai_check'           => $validate->amai_check,
        'avisoPrivacidadCheck' => $validate->avisoPrivacidadCheck,
        // 'quickstart' => $validate->quickstart,
        'config_manager'       => $config_manager,
        'puntos'               => $consultaPuntos[0]['puntos'],
        'level'                => $validate->levelUser,
        // 'config' => $config
    ];
    // $ci->session->set_flashdata("quickstart", $validate->quickstart);
    $ci->session->set_userdata( 'user', $user );
    redirect( "Panelistas/inicio" );
}
// function LoginEmail_cookie($email=""){
//     $ci=& get_instance();
//     $ci->load->helper('url');
//     $ci->load->library('session');
//     $ci->load->model('Login_model');
//     $ci->load->model('Ordenes_model');

//     $validate = $ci->Login_model->loginNeptuno($email);
//     $rol = $ci->Login_model->CNS_ROL_ID($validate->id_role);
//     $empresa = $ci->Ordenes_model->CNS_Company_ID($validate->id_company);
//     $modulos = $ci->Login_model->CNS_Modulos_Company($validate->id_company);

//         $user = array(
//             'id_user' => $validate->id_user,
//             'nombres' => $validate->nombres,
//             'apellidos' => $validate->apellidos,
//             'email' => $validate->email,
//             'rol' => $validate->id_role,
//             'rol_nombre' => $rol->rol_nombre,
//             'company' => $validate->id_company,
//             'photo' => $validate->photo,
//             'confirm' =>  $validate->confirm,
//             'empresa' => $empresa,
//             'modulos' => $modulos
//         );

//     $ci->session->set_userdata('user',$user);
//     // redirect(base_url()'index.php/Dashboard/Control');
// }

// ------------------FUNCIONES GENERALES ------------------------------

function clean( $string ) {
    $string = str_replace( ' ', '_', $string ); // Replaces all spaces with hyphens.
    return preg_replace( '/[^A-Za-z0-9\-]/', '', $string ); // Removes special chars.
}
// Export data in CSV format
function exportCSVTable( $titulo = '', $tabla = '', $condicion = '' ) {

    $ci = &get_instance();
    $ci->load->database();

    $sql = "select * from " . $tabla;
    if ( $condicion != "" ) {
        $sql .= " where " . $condicion;
    }
    $query = $ci->db->query( $sql );
    $row = $query->result();
    $campos = $ci->db->list_fields( $tabla );

    // file name
    $filename = $titulo . '_' . date( 'Ymd' ) . '.csv';
    header( "Content-Description: File Transfer" );
    header( "Content-Disposition: attachment; filename=$filename" );
    header( "Content-Type: application/csv; " );

    // get data
    // $usersData = $this->Main_model->getUserDetails();

    // file creation
    $file = fopen( 'php://output', 'w' );

    // $header = array("Username","Name","Gender","Email");
    $header = $campos;
    fputcsv( $file, $header );
    foreach ( $row as $key => $line ) {
        fputcsv( $file, $line );
    }
    fclose( $file );
    exit;
}

// Export data in CSV format
// function exportCSV( $titulo = '', $data, $campos ) {
//     // file name
//     $filename = $titulo . '_' . date( 'Ymd' ) . '.csv';
//     header( "Content-Description: File Transfer" );
//     header( "Content-Disposition: attachment; filename=$filename" );
//     header( "Content-Type: application/csv; " );

//     // get data
//     // $usersData = $this->Main_model->getUserDetails();

//     // file creation
//     $file = fopen( 'php://output', 'w' );

//     // $header = array("Username","Name","Gender","Email");
//     $header = $campos;
//     fputcsv( $file, $header );
//     foreach ( $data as $key => $line ) {
//         fputcsv( $file, $line );
//     }
//     fclose( $file );
//     exit;
// }

function view_assigned( $configArray ) {
    try {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "select ";
        $sql .= !array_key_exists( 'fields', $configArray ) ? '*' : implode( ',', $configArray['fields'] );
        if ( !$configArray['table'] ) {
            throw new Exception( 'Error no tiene ninguna tabla para buscar en sql' );
        }

        $sql .= " from " . $configArray['table'];
        $sql .= !array_key_exists( 'condition', $configArray ) ? "" : " where " . $configArray['condition'];
        $query = $ci->db->query( $sql );
        $row = $query->result();

        if ( $configArray['options'] ) {
            if ( is_string( $configArray['options'] ) ) {
                try {
                    $configArray['options'] = $ci->db->query( $configArray['options'] )->result();
                } catch ( Exception $e ) {
                    throw new Exception( 'Error al querer obtener las opciones de la base de datos' );
                }
            }
        }

        $configArray['data_table'] = $row;
        $configArray['row_names'] = is_null( $configArray['fields'] ) ? $ci->db->list_fields( $tabla ) : $configArray['fields'];
        return $ci->load->view( 'controles/v_assigned', $configArray );
    } catch ( Exception $e ) {
        throw new Exception( "Error al querer realizar la pantalla view assigned " . $e->getMessage() );
    }
}
